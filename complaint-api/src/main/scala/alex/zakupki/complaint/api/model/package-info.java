@XmlSchema(
        namespace = "http://zakupki.gov.ru/oos/export/1",
        xmlns = {
                @XmlNs(prefix = "oos", namespaceURI = "http://zakupki.gov.ru/oos/types/1")
        },
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlAccessorType(XmlAccessType.FIELD)
package alex.zakupki.complaint.api.model;
import javax.xml.bind.annotation.*;