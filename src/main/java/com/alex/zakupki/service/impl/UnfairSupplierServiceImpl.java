package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.UnfairSupplierService;
import com.alex.zakupki.domain.UnfairSupplier;
import com.alex.zakupki.repository.UnfairSupplierRepository;
import com.alex.zakupki.repository.search.UnfairSupplierSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UnfairSupplier.
 */
@Service
@Transactional
public class UnfairSupplierServiceImpl implements UnfairSupplierService{

    private final Logger log = LoggerFactory.getLogger(UnfairSupplierServiceImpl.class);

    private final UnfairSupplierRepository unfairSupplierRepository;

    private final UnfairSupplierSearchRepository unfairSupplierSearchRepository;

    public UnfairSupplierServiceImpl(UnfairSupplierRepository unfairSupplierRepository, UnfairSupplierSearchRepository unfairSupplierSearchRepository) {
        this.unfairSupplierRepository = unfairSupplierRepository;
        this.unfairSupplierSearchRepository = unfairSupplierSearchRepository;
    }

    /**
     * Save a unfairSupplier.
     *
     * @param unfairSupplier the entity to save
     * @return the persisted entity
     */
    @Override
    public UnfairSupplier save(UnfairSupplier unfairSupplier) {
        log.debug("Request to save UnfairSupplier : {}", unfairSupplier);
        UnfairSupplier result = unfairSupplierRepository.save(unfairSupplier);
        unfairSupplierSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the unfairSuppliers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UnfairSupplier> findAll(Pageable pageable) {
        log.debug("Request to get all UnfairSuppliers");
        return unfairSupplierRepository.findAll(pageable);
    }

    /**
     *  Get one unfairSupplier by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UnfairSupplier findOne(Long id) {
        log.debug("Request to get UnfairSupplier : {}", id);
        return unfairSupplierRepository.findOne(id);
    }

    /**
     *  Delete the  unfairSupplier by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UnfairSupplier : {}", id);
        unfairSupplierRepository.delete(id);
        unfairSupplierSearchRepository.delete(id);
    }

    /**
     * Search for the unfairSupplier corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UnfairSupplier> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UnfairSuppliers for query {}", query);
        Page<UnfairSupplier> result = unfairSupplierSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
