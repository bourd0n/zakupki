package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.ReturnedComplaint;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ReturnedComplaint entity.
 */
public interface ReturnedComplaintSearchRepository extends ElasticsearchRepository<ReturnedComplaint, Long> {
}
