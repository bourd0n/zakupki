package com.alex.zakupki.service;

import com.alex.zakupki.domain.TenderSuspension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing TenderSuspension.
 */
public interface TenderSuspensionService {

    /**
     * Save a tenderSuspension.
     *
     * @param tenderSuspension the entity to save
     * @return the persisted entity
     */
    TenderSuspension save(TenderSuspension tenderSuspension);

    /**
     *  Get all the tenderSuspensions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TenderSuspension> findAll(Pageable pageable);

    /**
     *  Get the "id" tenderSuspension.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TenderSuspension findOne(Long id);

    /**
     *  Delete the "id" tenderSuspension.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the tenderSuspension corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TenderSuspension> search(String query, Pageable pageable);
}
