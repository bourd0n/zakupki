import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlannedCheckResult } from './planned-check-result.model';
import { PlannedCheckResultService } from './planned-check-result.service';

@Injectable()
export class PlannedCheckResultPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private plannedCheckResultService: PlannedCheckResultService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.plannedCheckResultService.find(id).subscribe((plannedCheckResult) => {
                    if (plannedCheckResult.createdDate) {
                        plannedCheckResult.createdDate = {
                            year: plannedCheckResult.createdDate.getFullYear(),
                            month: plannedCheckResult.createdDate.getMonth() + 1,
                            day: plannedCheckResult.createdDate.getDate()
                        };
                    }
                    if (plannedCheckResult.actDate) {
                        plannedCheckResult.actDate = {
                            year: plannedCheckResult.actDate.getFullYear(),
                            month: plannedCheckResult.actDate.getMonth() + 1,
                            day: plannedCheckResult.actDate.getDate()
                        };
                    }
                    if (plannedCheckResult.actPrescriptionDate) {
                        plannedCheckResult.actPrescriptionDate = {
                            year: plannedCheckResult.actPrescriptionDate.getFullYear(),
                            month: plannedCheckResult.actPrescriptionDate.getMonth() + 1,
                            day: plannedCheckResult.actPrescriptionDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.plannedCheckResultModalRef(component, plannedCheckResult);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.plannedCheckResultModalRef(component, new PlannedCheckResult());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    plannedCheckResultModalRef(component: Component, plannedCheckResult: PlannedCheckResult): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.plannedCheckResult = plannedCheckResult;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
