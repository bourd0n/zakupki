import { BaseEntity } from './../../shared';

export class ReturnedComplaint implements BaseEntity {
    constructor(
        public id?: number,
        public returnInfoBase?: string,
        public complaint?: BaseEntity,
    ) {
    }
}
