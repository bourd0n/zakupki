package com.alex.zakupki.complaint.api.model.versions.v5_0

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.GeneralizedEntity
import com.google.common.base.MoreObjects
import com.alex.zakupki.complaint.api.model.general
import org.apache.commons.lang3.ObjectUtils

@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class CheckComplaintResult(@xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                                checkResultNumber: String,
                                @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:complaintNumber/text()")
                                complaintNumber: String,
                                @xmlPath("checkResult/oos:complaint/oos:complaintNumber/text()")
                                complaintNumberV61: String,
                                @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                                createDate: Date,
                                @xmlPath("checkResult/oos:commonInfo/oos:owner")
                                owner: Owner,
                                @xmlPath("checkResult/oos:commonInfo/oos:result/text()")
                                result: String,
                                @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:checkSubjects/oos:subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                checkSubject: Client,
                                @xmlPath("checkResult/oos:complaint/oos:checkSubjects/oos:subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                checkSubjectV61: Client) extends GeneralizedEntity[general.CheckComplaintResult] {
    private def this() = this(null, null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("result", result).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }

    override def mapToGeneral(): general.CheckComplaintResult = general.CheckComplaintResult(
        checkResultNumber,
        createDate,
        owner.mapToGeneral(),
        result,
        Option(ObjectUtils.firstNonNull(checkSubject, checkSubjectV61)).map(_.mapToGeneral()).orNull,
        ObjectUtils.firstNonNull(complaintNumber, complaintNumberV61)
    )
}
