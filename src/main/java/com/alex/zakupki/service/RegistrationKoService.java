package com.alex.zakupki.service;

import com.alex.zakupki.domain.RegistrationKo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing RegistrationKo.
 */
public interface RegistrationKoService {

    /**
     * Save a registrationKo.
     *
     * @param registrationKo the entity to save
     * @return the persisted entity
     */
    RegistrationKo save(RegistrationKo registrationKo);

    /**
     *  Get all the registrationKos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RegistrationKo> findAll(Pageable pageable);

    /**
     *  Get the "id" registrationKo.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RegistrationKo findOne(Long id);

    /**
     *  Delete the "id" registrationKo.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the registrationKo corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RegistrationKo> search(String query, Pageable pageable);
}
