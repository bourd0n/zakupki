package com.alex.zakupki.service.mapper.xml

import java.time.ZoneId
import java.util.concurrent.TimeUnit
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general.{Applicant => XmlApplicant, Client => XmlClient, Complaint => XmlComplaint, Purchase => XmlPurchase, RegistrationKO => XmlRegistrationKo}
import com.alex.zakupki.domain._
import com.alex.zakupki.repository.{ComplaintRepository, ReturnedComplaintRepository}
import com.google.common.cache.{Cache, CacheBuilder}
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component

@Component
class ComplaintXmlToDataMapper @Inject()(val complaintRepository: ComplaintRepository,
                                         val returnedComplaintRepository: ReturnedComplaintRepository,
                                         val applicantXmlToDataMapper: XmlToDataMapper[XmlApplicant, Applicant],
                                         val purchaseXmlToDataMapper: XmlToDataMapper[XmlPurchase, Purchase],
                                         val clientXmlToDataMapper: XmlToDataMapper[XmlClient, Client],
                                         val registrationKoXmlToDataMapper: XmlToDataMapper[XmlRegistrationKo, RegistrationKo])
    extends XmlToDataMapper[XmlComplaint, Complaint] with DateToLocalDateConversion {

    private val cache: Cache[String, Complaint] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, Complaint]]

    override def map(from: XmlComplaint): Option[Complaint] = Option(from).map {
        fromComplaint => {
            cache.get(fromComplaint.complaintNumber, () => {
                val repositoryComplaint = complaintRepository.findOneByComplaintNumber(fromComplaint.complaintNumber)

                //todo: rewrite to map
                if (repositoryComplaint.isPresent) {
                    val complaintFromRepository = repositoryComplaint.get()
                    if (fromComplaint.text != null) {
                        complaintFromRepository.text(fromComplaint.text)
                    }
                    if (fromComplaint.regDate != null) {
                        complaintFromRepository.regDate(fromComplaint.regDate)
                    }
                    if (fromComplaint.applicant != null) {
                        complaintFromRepository.applicant(applicantXmlToDataMapper.map(fromComplaint.applicant).orNull)
                    }
                    if (fromComplaint.purchase != null) {
                        complaintFromRepository.purchase(purchaseXmlToDataMapper.map(fromComplaint.purchase).orNull)
                    }
                    if (fromComplaint.indictedClient != null) {
                        complaintFromRepository.indictedClient(clientXmlToDataMapper.map(fromComplaint.indictedClient).orNull)
                    }
                    if (fromComplaint.registrationKO != null) {
                        complaintFromRepository.registrationKo(registrationKoXmlToDataMapper.map(fromComplaint.registrationKO).orNull)
                    }
                    val savedComplaint = complaintRepository.save(complaintFromRepository)

                    if (StringUtils.isNotEmpty(from.returnInfo)) {
                        val returnedComplaint = new ReturnedComplaint()
                            .returnInfoBase(from.returnInfo)
                            .complaint(savedComplaint)
                        returnedComplaintRepository.save(returnedComplaint)
                    }
                    savedComplaint
                } else {
                    val complaint = new Complaint()
                    complaint.setComplaintNumber(fromComplaint.complaintNumber)
                    complaint.setText(fromComplaint.text)
                    val localDate = fromComplaint.regDate.toInstant.atZone(ZoneId.systemDefault()).toLocalDate
                    complaint.setRegDate(localDate)

                    val applicant = applicantXmlToDataMapper.map(fromComplaint.applicant).orNull
                    complaint.setApplicant(applicant)

                    val purchase = purchaseXmlToDataMapper.map(fromComplaint.purchase).orNull
                    complaint.setPurchase(purchase)

                    val client = clientXmlToDataMapper.map(fromComplaint.indictedClient).orNull
                    complaint.setIndictedClient(client)

                    val registrationKo = registrationKoXmlToDataMapper.map(fromComplaint.registrationKO).orNull
                    complaint.setRegistrationKo(registrationKo)

                    val savedComplaint = complaintRepository.save(complaint)

                    if (StringUtils.isNotEmpty(from.returnInfo)) {
                        val returnedComplaint = new ReturnedComplaint()
                            .returnInfoBase(from.returnInfo)
                            .complaint(savedComplaint)
                        returnedComplaintRepository.save(returnedComplaint)
                    }
                    savedComplaint
                }
            })
        }
    }
}
