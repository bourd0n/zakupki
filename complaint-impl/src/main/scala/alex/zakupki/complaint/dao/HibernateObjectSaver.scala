package alex.zakupki.complaint.dao

import com.typesafe.scalalogging.LazyLogging
import org.hibernate.{SessionFactory, Session}
import org.hibernate.exception.ConstraintViolationException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class HibernateObjectSaver @Autowired()(sessionFactory: SessionFactory) extends ObjectSaver with LazyLogging {

  override def saveObject(unmarshalObject: AnyRef): Unit = {
    logger.debug("Save object {}", unmarshalObject)
    val session: Session = sessionFactory.openSession
    try {
      session.beginTransaction
      session.saveOrUpdate(unmarshalObject)
      session.getTransaction.commit()
    }
    catch {
      case e: ConstraintViolationException =>
        logger.warn("Constraint violation occurred for {}", unmarshalObject)
        session.getTransaction.rollback()
    }
    session.close()
  }

}
