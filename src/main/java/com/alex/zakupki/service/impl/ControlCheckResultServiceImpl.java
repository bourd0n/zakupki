package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.ControlCheckResultService;
import com.alex.zakupki.domain.ControlCheckResult;
import com.alex.zakupki.repository.ControlCheckResultRepository;
import com.alex.zakupki.repository.search.ControlCheckResultSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ControlCheckResult.
 */
@Service
@Transactional
public class ControlCheckResultServiceImpl implements ControlCheckResultService{

    private final Logger log = LoggerFactory.getLogger(ControlCheckResultServiceImpl.class);

    private final ControlCheckResultRepository controlCheckResultRepository;

    private final ControlCheckResultSearchRepository controlCheckResultSearchRepository;

    public ControlCheckResultServiceImpl(ControlCheckResultRepository controlCheckResultRepository, ControlCheckResultSearchRepository controlCheckResultSearchRepository) {
        this.controlCheckResultRepository = controlCheckResultRepository;
        this.controlCheckResultSearchRepository = controlCheckResultSearchRepository;
    }

    /**
     * Save a controlCheckResult.
     *
     * @param controlCheckResult the entity to save
     * @return the persisted entity
     */
    @Override
    public ControlCheckResult save(ControlCheckResult controlCheckResult) {
        log.debug("Request to save ControlCheckResult : {}", controlCheckResult);
        ControlCheckResult result = controlCheckResultRepository.save(controlCheckResult);
        controlCheckResultSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the controlCheckResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ControlCheckResult> findAll(Pageable pageable) {
        log.debug("Request to get all ControlCheckResults");
        return controlCheckResultRepository.findAll(pageable);
    }

    /**
     *  Get one controlCheckResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ControlCheckResult findOne(Long id) {
        log.debug("Request to get ControlCheckResult : {}", id);
        return controlCheckResultRepository.findOne(id);
    }

    /**
     *  Delete the  controlCheckResult by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ControlCheckResult : {}", id);
        controlCheckResultRepository.delete(id);
        controlCheckResultSearchRepository.delete(id);
    }

    /**
     * Search for the controlCheckResult corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ControlCheckResult> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ControlCheckResults for query {}", query);
        Page<ControlCheckResult> result = controlCheckResultSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
