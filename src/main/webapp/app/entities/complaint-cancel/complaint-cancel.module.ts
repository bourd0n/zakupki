import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    ComplaintCancelService,
    ComplaintCancelPopupService,
    ComplaintCancelComponent,
    ComplaintCancelDetailComponent,
    ComplaintCancelDialogComponent,
    ComplaintCancelPopupComponent,
    ComplaintCancelDeletePopupComponent,
    ComplaintCancelDeleteDialogComponent,
    complaintCancelRoute,
    complaintCancelPopupRoute,
} from './';

const ENTITY_STATES = [
    ...complaintCancelRoute,
    ...complaintCancelPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ComplaintCancelComponent,
        ComplaintCancelDetailComponent,
        ComplaintCancelDialogComponent,
        ComplaintCancelDeleteDialogComponent,
        ComplaintCancelPopupComponent,
        ComplaintCancelDeletePopupComponent,
    ],
    entryComponents: [
        ComplaintCancelComponent,
        ComplaintCancelDialogComponent,
        ComplaintCancelPopupComponent,
        ComplaintCancelDeleteDialogComponent,
        ComplaintCancelDeletePopupComponent,
    ],
    providers: [
        ComplaintCancelService,
        ComplaintCancelPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiComplaintCancelModule {}
