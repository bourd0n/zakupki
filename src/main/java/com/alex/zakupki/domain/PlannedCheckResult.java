package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A PlannedCheckResult.
 */
@Entity
@Table(name = "planned_check_result")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "plannedcheckresult")
public class PlannedCheckResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "check_result_number", nullable = false)
    private String checkResultNumber;

    @Column(name = "check_number")
    private String checkNumber;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "act_date")
    private LocalDate actDate;

    @Column(name = "act_prescription_date")
    private LocalDate actPrescriptionDate;

    @ManyToOne
    private Owner owner;

    @ManyToOne
    private Client checkSubject;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCheckResultNumber() {
        return checkResultNumber;
    }

    public PlannedCheckResult checkResultNumber(String checkResultNumber) {
        this.checkResultNumber = checkResultNumber;
        return this;
    }

    public void setCheckResultNumber(String checkResultNumber) {
        this.checkResultNumber = checkResultNumber;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public PlannedCheckResult checkNumber(String checkNumber) {
        this.checkNumber = checkNumber;
        return this;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public PlannedCheckResult createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getActDate() {
        return actDate;
    }

    public PlannedCheckResult actDate(LocalDate actDate) {
        this.actDate = actDate;
        return this;
    }

    public void setActDate(LocalDate actDate) {
        this.actDate = actDate;
    }

    public LocalDate getActPrescriptionDate() {
        return actPrescriptionDate;
    }

    public PlannedCheckResult actPrescriptionDate(LocalDate actPrescriptionDate) {
        this.actPrescriptionDate = actPrescriptionDate;
        return this;
    }

    public void setActPrescriptionDate(LocalDate actPrescriptionDate) {
        this.actPrescriptionDate = actPrescriptionDate;
    }

    public Owner getOwner() {
        return owner;
    }

    public PlannedCheckResult owner(Owner owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Client getCheckSubject() {
        return checkSubject;
    }

    public PlannedCheckResult checkSubject(Client client) {
        this.checkSubject = client;
        return this;
    }

    public void setCheckSubject(Client client) {
        this.checkSubject = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlannedCheckResult plannedCheckResult = (PlannedCheckResult) o;
        if (plannedCheckResult.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plannedCheckResult.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlannedCheckResult{" +
            "id=" + getId() +
            ", checkResultNumber='" + getCheckResultNumber() + "'" +
            ", checkNumber='" + getCheckNumber() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", actDate='" + getActDate() + "'" +
            ", actPrescriptionDate='" + getActPrescriptionDate() + "'" +
            "}";
    }
}
