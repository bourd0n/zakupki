import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UnplannedCheckResult } from './unplanned-check-result.model';
import { UnplannedCheckResultService } from './unplanned-check-result.service';

@Injectable()
export class UnplannedCheckResultPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private unplannedCheckResultService: UnplannedCheckResultService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unplannedCheckResultService.find(id).subscribe((unplannedCheckResult) => {
                    if (unplannedCheckResult.createdDate) {
                        unplannedCheckResult.createdDate = {
                            year: unplannedCheckResult.createdDate.getFullYear(),
                            month: unplannedCheckResult.createdDate.getMonth() + 1,
                            day: unplannedCheckResult.createdDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.unplannedCheckResultModalRef(component, unplannedCheckResult);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.unplannedCheckResultModalRef(component, new UnplannedCheckResult());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unplannedCheckResultModalRef(component: Component, unplannedCheckResult: UnplannedCheckResult): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unplannedCheckResult = unplannedCheckResult;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
