import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {SyncInfo} from './sync-info.model';
import {JhiDateUtils} from 'ng-jhipster';

@Injectable()
export class DataSyncService {

    constructor(private http: Http,
                private dateUtils: JhiDateUtils) {
    }

    getSyncInfos(): Observable<Response> {
        return this.http.get('sync-data/info').map((res: Response) => {
            return this.transformSyncInfos(res);
        });
    }

    performSync(syncInfo: SyncInfo): Observable<SyncInfo> {
        return this.http.post('/sync-data/sync', syncInfo).map((res: Response) => res.json());
    }

    private transformSyncInfos(res: any): any {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].syncDate = this.dateUtils
                .convertLocalDateFromServer(jsonResponse[i].syncDate);
        }
        res._body = jsonResponse;
        return res;
    }

}
