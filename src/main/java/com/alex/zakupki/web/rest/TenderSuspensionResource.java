package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.TenderSuspension;
import com.alex.zakupki.service.TenderSuspensionService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TenderSuspension.
 */
@RestController
@RequestMapping("/api")
public class TenderSuspensionResource {

    private final Logger log = LoggerFactory.getLogger(TenderSuspensionResource.class);

    private static final String ENTITY_NAME = "tenderSuspension";

    private final TenderSuspensionService tenderSuspensionService;

    public TenderSuspensionResource(TenderSuspensionService tenderSuspensionService) {
        this.tenderSuspensionService = tenderSuspensionService;
    }

    /**
     * POST  /tender-suspensions : Create a new tenderSuspension.
     *
     * @param tenderSuspension the tenderSuspension to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tenderSuspension, or with status 400 (Bad Request) if the tenderSuspension has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tender-suspensions")
    @Timed
    public ResponseEntity<TenderSuspension> createTenderSuspension(@RequestBody TenderSuspension tenderSuspension) throws URISyntaxException {
        log.debug("REST request to save TenderSuspension : {}", tenderSuspension);
        if (tenderSuspension.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new tenderSuspension cannot already have an ID")).body(null);
        }
        TenderSuspension result = tenderSuspensionService.save(tenderSuspension);
        return ResponseEntity.created(new URI("/api/tender-suspensions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tender-suspensions : Updates an existing tenderSuspension.
     *
     * @param tenderSuspension the tenderSuspension to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tenderSuspension,
     * or with status 400 (Bad Request) if the tenderSuspension is not valid,
     * or with status 500 (Internal Server Error) if the tenderSuspension couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tender-suspensions")
    @Timed
    public ResponseEntity<TenderSuspension> updateTenderSuspension(@RequestBody TenderSuspension tenderSuspension) throws URISyntaxException {
        log.debug("REST request to update TenderSuspension : {}", tenderSuspension);
        if (tenderSuspension.getId() == null) {
            return createTenderSuspension(tenderSuspension);
        }
        TenderSuspension result = tenderSuspensionService.save(tenderSuspension);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tenderSuspension.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tender-suspensions : get all the tenderSuspensions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tenderSuspensions in body
     */
    @GetMapping("/tender-suspensions")
    @Timed
    public ResponseEntity<List<TenderSuspension>> getAllTenderSuspensions(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of TenderSuspensions");
        Page<TenderSuspension> page = tenderSuspensionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tender-suspensions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tender-suspensions/:id : get the "id" tenderSuspension.
     *
     * @param id the id of the tenderSuspension to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tenderSuspension, or with status 404 (Not Found)
     */
    @GetMapping("/tender-suspensions/{id}")
    @Timed
    public ResponseEntity<TenderSuspension> getTenderSuspension(@PathVariable Long id) {
        log.debug("REST request to get TenderSuspension : {}", id);
        TenderSuspension tenderSuspension = tenderSuspensionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tenderSuspension));
    }

    /**
     * DELETE  /tender-suspensions/:id : delete the "id" tenderSuspension.
     *
     * @param id the id of the tenderSuspension to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tender-suspensions/{id}")
    @Timed
    public ResponseEntity<Void> deleteTenderSuspension(@PathVariable Long id) {
        log.debug("REST request to delete TenderSuspension : {}", id);
        tenderSuspensionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tender-suspensions?query=:query : search for the tenderSuspension corresponding
     * to the query.
     *
     * @param query the query of the tenderSuspension search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/tender-suspensions")
    @Timed
    public ResponseEntity<List<TenderSuspension>> searchTenderSuspensions(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of TenderSuspensions for query {}", query);
        Page<TenderSuspension> page = tenderSuspensionService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/tender-suspensions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
