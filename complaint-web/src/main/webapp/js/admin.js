function start_load(contextPath) {
    $.ajax({
        url: contextPath + "/loadComplaints",
        method: "POST"
    }).done(function (response) {
        var loadBtn = $("#load_btn");
        //loadBtn.addClass("loading");
        loadBtn.addClass("disabled");
        loadBtn.popup({
            popup: $("#loading_popup"),
            position : 'top center'
        });
        loadBtn.popup('show')
    });
}
