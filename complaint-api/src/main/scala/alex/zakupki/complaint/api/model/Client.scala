package alex.zakupki.complaint.api.model

import javax.persistence.{Entity, Table}
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType}

/**
 * customer, authority, commission94/44 others
 */
@Entity
@Table(name = "clients")
@XmlAccessorType(XmlAccessType.FIELD)
case class Client(@id
                  regNum: String,
                  @column(length = 4000)
                  fullName: String,
                  clientType: String) {
  private def this() = this(null, null, null)
}