package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.ApplicantService;
import com.alex.zakupki.domain.Applicant;
import com.alex.zakupki.repository.ApplicantRepository;
import com.alex.zakupki.repository.search.ApplicantSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Applicant.
 */
@Service
@Transactional
public class ApplicantServiceImpl implements ApplicantService{

    private final Logger log = LoggerFactory.getLogger(ApplicantServiceImpl.class);

    private final ApplicantRepository applicantRepository;

    private final ApplicantSearchRepository applicantSearchRepository;

    public ApplicantServiceImpl(ApplicantRepository applicantRepository, ApplicantSearchRepository applicantSearchRepository) {
        this.applicantRepository = applicantRepository;
        this.applicantSearchRepository = applicantSearchRepository;
    }

    /**
     * Save a applicant.
     *
     * @param applicant the entity to save
     * @return the persisted entity
     */
    @Override
    public Applicant save(Applicant applicant) {
        log.debug("Request to save Applicant : {}", applicant);
        Applicant result = applicantRepository.save(applicant);
        applicantSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the applicants.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Applicant> findAll(Pageable pageable) {
        log.debug("Request to get all Applicants");
        return applicantRepository.findAll(pageable);
    }

    /**
     *  Get one applicant by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Applicant findOne(Long id) {
        log.debug("Request to get Applicant : {}", id);
        return applicantRepository.findOne(id);
    }

    /**
     *  Delete the  applicant by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Applicant : {}", id);
        applicantRepository.delete(id);
        applicantSearchRepository.delete(id);
    }

    /**
     * Search for the applicant corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Applicant> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Applicants for query {}", query);
        Page<Applicant> result = applicantSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
