import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { Principal } from './principal.service';

/**
 * @whatItDoes Conditionally includes an HTML element if current user has admin authority
 *
 * @howToUse
 * ```
 *     <some-element *adminOnly>...</some-element>
 *
 *     <some-element *adminOnly>...</some-element>
 * ```
 */
@Directive({
    selector: '[adminOnly]'
})
export class AdminOnlyDirective {

    constructor(private principal: Principal, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {
    }

    @Input()
    set adminOnly(value: string) {
        this.updateView();
        // Get notified each time authentication state changes.
        this.principal.getAuthenticationState().subscribe(identity => this.updateView());
    }

    private updateView(): void {
        this.principal.hasAnyAuthority(['ROLE_ADMIN']).then(result => {
            if (result) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainerRef.clear();
            }
        });
    }
}
