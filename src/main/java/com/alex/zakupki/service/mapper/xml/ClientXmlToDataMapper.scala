package com.alex.zakupki.service.mapper.xml

import java.util.concurrent.TimeUnit
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general.{Client => XmlClient}
import com.alex.zakupki.domain.Client
import com.alex.zakupki.repository.ClientRepository
import com.google.common.cache.{Cache, CacheBuilder}
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Component

@Component
class ClientXmlToDataMapper @Inject()(val clientRepository: ClientRepository)
    extends XmlToDataMapper[XmlClient, Client] {

    private val cache: Cache[String, Client] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, Client]]

    override def map(from: XmlClient): Option[Client] = Option(from) map {
        xmlClient => {
            if (StringUtils.isEmpty(xmlClient.regNum)) {
                val client = new Client()
                    .fullName(xmlClient.fullName)
                    .clientType(xmlClient.clientType)
                clientRepository.save(client)
            } else {
                cache.get(xmlClient.regNum, () => {
                    clientRepository
                        .findOneByRegNum(xmlClient.regNum)
                        .orElseGet(() => {
                            val client = new Client()
                                .regNum(xmlClient.regNum)
                                .fullName(xmlClient.fullName)
                                .clientType(xmlClient.clientType)
                            clientRepository.save(client)
                        })
                })
            }
        }
    }
}
