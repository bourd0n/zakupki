import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RegistrationKo } from './registration-ko.model';
import { RegistrationKoPopupService } from './registration-ko-popup.service';
import { RegistrationKoService } from './registration-ko.service';

@Component({
    selector: 'jhi-registration-ko-dialog',
    templateUrl: './registration-ko-dialog.component.html'
})
export class RegistrationKoDialogComponent implements OnInit {

    registrationKo: RegistrationKo;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private registrationKoService: RegistrationKoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.registrationKo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.registrationKoService.update(this.registrationKo));
        } else {
            this.subscribeToSaveResponse(
                this.registrationKoService.create(this.registrationKo));
        }
    }

    private subscribeToSaveResponse(result: Observable<RegistrationKo>) {
        result.subscribe((res: RegistrationKo) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: RegistrationKo) {
        this.eventManager.broadcast({ name: 'registrationKoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-registration-ko-popup',
    template: ''
})
export class RegistrationKoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private registrationKoPopupService: RegistrationKoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.registrationKoPopupService
                    .open(RegistrationKoDialogComponent as Component, params['id']);
            } else {
                this.registrationKoPopupService
                    .open(RegistrationKoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
