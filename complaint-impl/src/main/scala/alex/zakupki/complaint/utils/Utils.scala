package alex.zakupki.complaint.utils

import java.io.Closeable

object Utils {

  def cleanly[A <: Closeable, B](resources: A*)(code: => B): B = {
    try {
      code
    } finally {
      for (res <- resources) {
        res.close()
      }
    }
  }
}
