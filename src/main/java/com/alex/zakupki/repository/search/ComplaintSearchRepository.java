package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.Complaint;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Complaint entity.
 */
public interface ComplaintSearchRepository extends ElasticsearchRepository<Complaint, Long> {
}
