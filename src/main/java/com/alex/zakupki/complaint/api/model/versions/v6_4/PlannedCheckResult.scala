package com.alex.zakupki.complaint.api.model.versions.v6_4

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import com.alex.zakupki.complaint.api.model.versions.common.DateAdapter
import com.google.common.base.MoreObjects
;

//@Entity
//@Table(name = "planned_check_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class PlannedCheckResult(checkResultNumber: String,
                              @xmlPath("ns2:eventResult/base/plannedCheck/checkNumber/text()")
                              checkNumber: String,
                              @xmlPath("ns2:eventResult/commonInfo/createDate/text()")
                              createDate: Date,
                              @xmlPath("ns2:eventResult/commonInfo/owner")
                              owner: Owner,
                              @xmlPath("ns2:eventResult/base/plannedCheck/act/actDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actDate: Date,
                             //todo: is specified?
//                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:actPrescription/oos:prescriptionDate/text()")
//                              @xmlJavaTypeAdapter(classOf[DateAdapter])
//                              actPrescriptionDate: Date,
                              @xmlPath("ns2:eventResult/base/plannedCheck/checkSubject/")
                              @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                              checkSubject: Client) extends GeneralizedEntity[general.PlannedCheckResult] {
    private def this() = this(null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }

    override def mapToGeneral(): general.PlannedCheckResult = general.PlannedCheckResult(
        checkResultNumber,
        checkNumber.trim,
        createDate,
        owner.mapToGeneral(),
        actDate,
        null,
        Option(checkSubject).map(_.mapToGeneral()).orNull
    )
}
