package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.PlannedCheckResult;
import com.alex.zakupki.service.PlannedCheckResultService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PlannedCheckResult.
 */
@RestController
@RequestMapping("/api")
public class PlannedCheckResultResource {

    private final Logger log = LoggerFactory.getLogger(PlannedCheckResultResource.class);

    private static final String ENTITY_NAME = "plannedCheckResult";

    private final PlannedCheckResultService plannedCheckResultService;

    public PlannedCheckResultResource(PlannedCheckResultService plannedCheckResultService) {
        this.plannedCheckResultService = plannedCheckResultService;
    }

    /**
     * POST  /planned-check-results : Create a new plannedCheckResult.
     *
     * @param plannedCheckResult the plannedCheckResult to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plannedCheckResult, or with status 400 (Bad Request) if the plannedCheckResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/planned-check-results")
    @Timed
    public ResponseEntity<PlannedCheckResult> createPlannedCheckResult(@Valid @RequestBody PlannedCheckResult plannedCheckResult) throws URISyntaxException {
        log.debug("REST request to save PlannedCheckResult : {}", plannedCheckResult);
        if (plannedCheckResult.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new plannedCheckResult cannot already have an ID")).body(null);
        }
        PlannedCheckResult result = plannedCheckResultService.save(plannedCheckResult);
        return ResponseEntity.created(new URI("/api/planned-check-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /planned-check-results : Updates an existing plannedCheckResult.
     *
     * @param plannedCheckResult the plannedCheckResult to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plannedCheckResult,
     * or with status 400 (Bad Request) if the plannedCheckResult is not valid,
     * or with status 500 (Internal Server Error) if the plannedCheckResult couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/planned-check-results")
    @Timed
    public ResponseEntity<PlannedCheckResult> updatePlannedCheckResult(@Valid @RequestBody PlannedCheckResult plannedCheckResult) throws URISyntaxException {
        log.debug("REST request to update PlannedCheckResult : {}", plannedCheckResult);
        if (plannedCheckResult.getId() == null) {
            return createPlannedCheckResult(plannedCheckResult);
        }
        PlannedCheckResult result = plannedCheckResultService.save(plannedCheckResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plannedCheckResult.getId().toString()))
            .body(result);
    }

    /**
     * GET  /planned-check-results : get all the plannedCheckResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of plannedCheckResults in body
     */
    @GetMapping("/planned-check-results")
    @Timed
    public ResponseEntity<List<PlannedCheckResult>> getAllPlannedCheckResults(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PlannedCheckResults");
        Page<PlannedCheckResult> page = plannedCheckResultService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/planned-check-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /planned-check-results/:id : get the "id" plannedCheckResult.
     *
     * @param id the id of the plannedCheckResult to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plannedCheckResult, or with status 404 (Not Found)
     */
    @GetMapping("/planned-check-results/{id}")
    @Timed
    public ResponseEntity<PlannedCheckResult> getPlannedCheckResult(@PathVariable Long id) {
        log.debug("REST request to get PlannedCheckResult : {}", id);
        PlannedCheckResult plannedCheckResult = plannedCheckResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plannedCheckResult));
    }

    /**
     * DELETE  /planned-check-results/:id : delete the "id" plannedCheckResult.
     *
     * @param id the id of the plannedCheckResult to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/planned-check-results/{id}")
    @Timed
    public ResponseEntity<Void> deletePlannedCheckResult(@PathVariable Long id) {
        log.debug("REST request to delete PlannedCheckResult : {}", id);
        plannedCheckResultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/planned-check-results?query=:query : search for the plannedCheckResult corresponding
     * to the query.
     *
     * @param query the query of the plannedCheckResult search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/planned-check-results")
    @Timed
    public ResponseEntity<List<PlannedCheckResult>> searchPlannedCheckResults(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PlannedCheckResults for query {}", query);
        Page<PlannedCheckResult> page = plannedCheckResultService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/planned-check-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
