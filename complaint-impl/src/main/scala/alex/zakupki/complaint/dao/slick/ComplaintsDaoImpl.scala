package alex.zakupki.complaint.dao.slick

import alex.zakupki.complaint.dao.ComplaintsDao
import com.typesafe.scalalogging.LazyLogging
import org.springframework.stereotype.Component
import slick.driver.PostgresDriver.api._

import scala.concurrent.Future

@Component
class ComplaintsDaoImpl extends ComplaintsDao with LazyLogging {

  import DB._

  def countAllComplaints(): Future[Int] = {
    logger.debug("countAllComplaints called")
    db.run(Tables.Complaints.length.result)
  }

}
