package com.alex.zakupki.repository;

import com.alex.zakupki.domain.RegistrationKo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the RegistrationKo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegistrationKoRepository extends JpaRepository<RegistrationKo, Long> {

    Optional<RegistrationKo> findOneByRegNum(String regNum);

}
