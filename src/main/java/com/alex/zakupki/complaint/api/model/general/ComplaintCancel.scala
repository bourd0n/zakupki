package com.alex.zakupki.complaint.api.model.general

import java.util.Date
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

case class ComplaintCancel(complaintNumber: String,
                           regDate: Date,
                           registrationKO: RegistrationKO,
                           text: String) {
  override def toString: String = {
    MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
  }
}
