package com.alex.zakupki.complaint.api.model.versions.v6_4

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, SchemaVersion, general}

object SchemaVersion6_4 extends SchemaVersion {

    override val map: Map[Class[_], Class[_ <: GeneralizedEntity[_]]] = Map(
        classOf[general.Complaint] -> classOf[Complaint],
        classOf[general.CheckComplaintResult] -> classOf[CheckComplaintResult],
        classOf[general.UnplannedCheckResult] -> classOf[UnplannedCheckResult],
        classOf[general.PlannedCheckResult] -> classOf[PlannedCheckResult]
    )

}
