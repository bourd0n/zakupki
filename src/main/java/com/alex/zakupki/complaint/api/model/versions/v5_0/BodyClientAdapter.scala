package com.alex.zakupki.complaint.api.model.versions.v5_0

import javax.xml.bind.annotation.adapters.XmlAdapter

import com.jcabi.xml.XMLDocument
import org.apache.commons.lang3.StringUtils
import org.slf4j.{Logger, LoggerFactory}
import org.w3c.dom.Element

sealed class BodyClientAdapter extends XmlAdapter[Body, Client] {
    private val log: Logger = LoggerFactory.getLogger(getClass)

    def unmarshal(v: Body): Client = {
        val element: Element = v.getBodyVal.asInstanceOf[Element]
        val clientType: String = element.getLocalName
        val xmlDoc = new XMLDocument(element)
        val middleNode = clientType match {
            case "commission94" | "commission44" =>
                "[local-name()='organization']//*"
            case _ => ""
        }
        var xpathExprRegNum = s"//*[local-name()='$clientType']//*$middleNode[local-name()='regNum']/text()"
        val xpathExprFullName = s"//*[local-name()='$clientType']//*$middleNode[local-name()='fullName']/text()"
        var regNum = evalAndSetResult(xmlDoc, xpathExprRegNum, clientType)
        regNum = if (StringUtils.isEmpty(regNum)) {
            xpathExprRegNum = s"//*[local-name()='$clientType']//*$middleNode[local-name()='INN']/text()"
            evalAndSetResult(xmlDoc, xpathExprRegNum, clientType)
        } else {
            regNum
        }
        val fullName = evalAndSetResult(xmlDoc, xpathExprFullName, clientType)
        if (StringUtils.isEmpty(regNum) && StringUtils.isEmpty(fullName)) {
            log.warn(s"RegNum and fullName is empty for client type $clientType. Xml: $xmlDoc")
            null
        } else {
            Client(regNum, fullName, clientType)
        }
    }


    def evalAndSetResult(xmlDoc: XMLDocument, xpathExpr: String, clientType: String): String = {
        xmlDoc.xpath(xpathExpr).toArray.headOption match {
            case Some(value) => value.asInstanceOf[String]
            case None => ""
        }
    }

  def marshal(v: Client): Body = {
    null
  }
}
