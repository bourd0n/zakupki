package alex.zakupki.complaint.api.model

import javax.persistence.{Entity, Table}
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType}

import com.google.common.base.MoreObjects

@Entity
@Table(name = "purchases")
@XmlAccessorType(XmlAccessType.FIELD)
case class Purchase(@id @xmlPath("oos:purchaseNumber/text()")
                    purchaseNumber: String,
                    determSupplierMethod: String,
                    electronicPlatform: String,
                    maxCost: Double) {
  private def this() = this(null, null, null, Double.NaN)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("purchaseNumber", purchaseNumber).add("determSupplierMethod", determSupplierMethod).toString
  }
}