package alex.zakupki.complaint.logic.ftp

import java.io._

import alex.zakupki.complaint.logic.config.ServerConfig
import alex.zakupki.complaint.temp.Consts._
import com.typesafe.scalalogging.LazyLogging
import org.apache.commons.net.ftp.{FTP, FTPClient, FTPFile, FTPReply}

object FTPLoader extends LazyLogging {

  val makeFiles = new File(ServerConfig.server_path + "/zakupki_tmp/unpacked/").mkdirs()

  def download(ftpType: FTPTypes.FTPType, limit: Int) = downloadFilesFrom(ftpType.ftpPath, limit)(_.getName.startsWith(ftpType.fileNameRegx))

  private def downloadFilesFrom(path: String, limit: Int)(filterFunc: FTPFile => Boolean) = {
    val ftp: FTPClient = new FTPClient
    try {
      var reply: Int = 0
      ftp.connect(FTP_URL_ZAKUPKI)
      ftp.login("free", "free")
      ftp.enterLocalPassiveMode()
      reply = ftp.getReplyCode
      if (!FTPReply.isPositiveCompletion(reply)) {
        ftp.disconnect()
        throw new RuntimeException("FTP server refused connection.")
      }
      ftp.setFileType(FTP.BINARY_FILE_TYPE)
      val files = ftp.listFiles(path).take(limit).filter(filterFunc)
      for (file: FTPFile <- files) {
        val name = file.getName
        val link: String = s"$path$name"
        val downloadFile: File = new File(ServerConfig.server_path + "/zakupki_tmp/" + name)
        //val absPath = downloadFile.getAbsolutePath
        //println(s"Current path: $absPath")
        val outputStream: OutputStream = new BufferedOutputStream(new FileOutputStream(downloadFile))
        ftp.retrieveFile(link, outputStream)
        println("File retrieved")
        outputStream.flush()
        outputStream.close()
      }
      ftp.logout
    } finally {
      if (ftp.isConnected) ftp.disconnect()
    }
  }

  def downloadInmemory(ftpType: FTPTypes.FTPType, limit: Int): Array[Array[Byte]] = downloadInmemoryFilesFrom(ftpType.ftpPath, limit)(_.getName.startsWith(ftpType.fileNameRegx))

  private def downloadInmemoryFilesFrom(path: String, limit: Int)(filterFunc: FTPFile => Boolean) = {
    val ftp: FTPClient = new FTPClient
    try {
      var reply: Int = 0
      ftp.connect(FTP_URL_ZAKUPKI)
      ftp.login("free", "free")
      ftp.enterLocalPassiveMode()
      reply = ftp.getReplyCode
      if (!FTPReply.isPositiveCompletion(reply)) {
        ftp.disconnect()
        throw new RuntimeException("FTP server refused connection.")
      }
      ftp.setFileType(FTP.BINARY_FILE_TYPE)
      ftp.listFiles(path).take(limit).filter(filterFunc).map(file => {
        val name = file.getName
        val link: String = s"$path$name"
        val outputStream: ByteArrayOutputStream = new ByteArrayOutputStream()
        ftp.retrieveFile(link, outputStream)
        logger.debug(s"File $link retrieved")
        outputStream.flush()
        outputStream.toByteArray
      })
    } finally {
      ftp.logout
      if (ftp.isConnected) ftp.disconnect()
    }
  }
}