import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('ReturnedComplaint e2e test', () => {

    let navBarPage: NavBarPage;
    let returnedComplaintDialogPage: ReturnedComplaintDialogPage;
    let returnedComplaintComponentsPage: ReturnedComplaintComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ReturnedComplaints', () => {
        navBarPage.goToEntity('returned-complaint');
        returnedComplaintComponentsPage = new ReturnedComplaintComponentsPage();
        expect(returnedComplaintComponentsPage.getTitle()).toMatch(/zakupkiApp.returnedComplaint.home.title/);

    });

    it('should load create ReturnedComplaint dialog', () => {
        returnedComplaintComponentsPage.clickOnCreateButton();
        returnedComplaintDialogPage = new ReturnedComplaintDialogPage();
        expect(returnedComplaintDialogPage.getModalTitle()).toMatch(/zakupkiApp.returnedComplaint.home.createOrEditLabel/);
        returnedComplaintDialogPage.close();
    });

    it('should create and save ReturnedComplaints', () => {
        returnedComplaintComponentsPage.clickOnCreateButton();
        returnedComplaintDialogPage.setReturnInfoBaseInput('returnInfoBase');
        expect(returnedComplaintDialogPage.getReturnInfoBaseInput()).toMatch('returnInfoBase');
        returnedComplaintDialogPage.complaintSelectLastOption();
        returnedComplaintDialogPage.save();
        expect(returnedComplaintDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ReturnedComplaintComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-returned-complaint div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ReturnedComplaintDialogPage {
    modalTitle = element(by.css('h4#myReturnedComplaintLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    returnInfoBaseInput = element(by.css('input#field_returnInfoBase'));
    complaintSelect = element(by.css('select#field_complaint'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setReturnInfoBaseInput = function (returnInfoBase) {
        this.returnInfoBaseInput.sendKeys(returnInfoBase);
    }

    getReturnInfoBaseInput = function () {
        return this.returnInfoBaseInput.getAttribute('value');
    }

    complaintSelectLastOption = function () {
        this.complaintSelect.all(by.tagName('option')).last().click();
    }

    complaintSelectOption = function (option) {
        this.complaintSelect.sendKeys(option);
    }

    getComplaintSelect = function () {
        return this.complaintSelect;
    }

    getComplaintSelectedOption = function () {
        return this.complaintSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
