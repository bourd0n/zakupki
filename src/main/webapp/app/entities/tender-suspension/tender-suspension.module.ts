import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    TenderSuspensionService,
    TenderSuspensionPopupService,
    TenderSuspensionComponent,
    TenderSuspensionDetailComponent,
    TenderSuspensionDialogComponent,
    TenderSuspensionPopupComponent,
    TenderSuspensionDeletePopupComponent,
    TenderSuspensionDeleteDialogComponent,
    tenderSuspensionRoute,
    tenderSuspensionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tenderSuspensionRoute,
    ...tenderSuspensionPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        TenderSuspensionComponent,
        TenderSuspensionDetailComponent,
        TenderSuspensionDialogComponent,
        TenderSuspensionDeleteDialogComponent,
        TenderSuspensionPopupComponent,
        TenderSuspensionDeletePopupComponent,
    ],
    entryComponents: [
        TenderSuspensionComponent,
        TenderSuspensionDialogComponent,
        TenderSuspensionPopupComponent,
        TenderSuspensionDeleteDialogComponent,
        TenderSuspensionDeletePopupComponent,
    ],
    providers: [
        TenderSuspensionService,
        TenderSuspensionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiTenderSuspensionModule {}
