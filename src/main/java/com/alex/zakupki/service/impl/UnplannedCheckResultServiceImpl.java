package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.UnplannedCheckResultService;
import com.alex.zakupki.domain.UnplannedCheckResult;
import com.alex.zakupki.repository.UnplannedCheckResultRepository;
import com.alex.zakupki.repository.search.UnplannedCheckResultSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing UnplannedCheckResult.
 */
@Service
@Transactional
public class UnplannedCheckResultServiceImpl implements UnplannedCheckResultService{

    private final Logger log = LoggerFactory.getLogger(UnplannedCheckResultServiceImpl.class);

    private final UnplannedCheckResultRepository unplannedCheckResultRepository;

    private final UnplannedCheckResultSearchRepository unplannedCheckResultSearchRepository;

    public UnplannedCheckResultServiceImpl(UnplannedCheckResultRepository unplannedCheckResultRepository, UnplannedCheckResultSearchRepository unplannedCheckResultSearchRepository) {
        this.unplannedCheckResultRepository = unplannedCheckResultRepository;
        this.unplannedCheckResultSearchRepository = unplannedCheckResultSearchRepository;
    }

    /**
     * Save a unplannedCheckResult.
     *
     * @param unplannedCheckResult the entity to save
     * @return the persisted entity
     */
    @Override
    public UnplannedCheckResult save(UnplannedCheckResult unplannedCheckResult) {
        log.debug("Request to save UnplannedCheckResult : {}", unplannedCheckResult);
        UnplannedCheckResult result = unplannedCheckResultRepository.save(unplannedCheckResult);
        unplannedCheckResultSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the unplannedCheckResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UnplannedCheckResult> findAll(Pageable pageable) {
        log.debug("Request to get all UnplannedCheckResults");
        return unplannedCheckResultRepository.findAll(pageable);
    }

    /**
     *  Get one unplannedCheckResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public UnplannedCheckResult findOne(Long id) {
        log.debug("Request to get UnplannedCheckResult : {}", id);
        return unplannedCheckResultRepository.findOne(id);
    }

    /**
     *  Delete the  unplannedCheckResult by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UnplannedCheckResult : {}", id);
        unplannedCheckResultRepository.delete(id);
        unplannedCheckResultSearchRepository.delete(id);
    }

    /**
     * Search for the unplannedCheckResult corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UnplannedCheckResult> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of UnplannedCheckResults for query {}", query);
        Page<UnplannedCheckResult> result = unplannedCheckResultSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
