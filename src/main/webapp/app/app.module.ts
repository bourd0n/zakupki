import './vendor.ts';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { ZakupkiSharedModule, UserRouteAccessService } from './shared';
import { ZakupkiHomeModule } from './home/home.module';
import { ZakupkiAdminModule } from './admin/admin.module';
import { ZakupkiAccountModule } from './account/account.module';
import { ZakupkiEntityModule } from './entities/entity.module';
import { AnalyticsModule  } from './analytics/analytics.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        ZakupkiSharedModule,
        ZakupkiHomeModule,
        ZakupkiAdminModule,
        ZakupkiAccountModule,
        ZakupkiEntityModule,
        AnalyticsModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class ZakupkiAppModule {}
