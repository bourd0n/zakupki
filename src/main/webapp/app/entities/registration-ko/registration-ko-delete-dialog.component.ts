import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RegistrationKo } from './registration-ko.model';
import { RegistrationKoPopupService } from './registration-ko-popup.service';
import { RegistrationKoService } from './registration-ko.service';

@Component({
    selector: 'jhi-registration-ko-delete-dialog',
    templateUrl: './registration-ko-delete-dialog.component.html'
})
export class RegistrationKoDeleteDialogComponent {

    registrationKo: RegistrationKo;

    constructor(
        private registrationKoService: RegistrationKoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.registrationKoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'registrationKoListModification',
                content: 'Deleted an registrationKo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-registration-ko-delete-popup',
    template: ''
})
export class RegistrationKoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private registrationKoPopupService: RegistrationKoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.registrationKoPopupService
                .open(RegistrationKoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
