import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { RegistrationKo } from './registration-ko.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RegistrationKoService {

    private resourceUrl = SERVER_API_URL + 'api/registration-kos';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/registration-kos';

    constructor(private http: Http) { }

    create(registrationKo: RegistrationKo): Observable<RegistrationKo> {
        const copy = this.convert(registrationKo);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(registrationKo: RegistrationKo): Observable<RegistrationKo> {
        const copy = this.convert(registrationKo);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<RegistrationKo> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RegistrationKo.
     */
    private convertItemFromServer(json: any): RegistrationKo {
        const entity: RegistrationKo = Object.assign(new RegistrationKo(), json);
        return entity;
    }

    /**
     * Convert a RegistrationKo to a JSON which can be sent to the server.
     */
    private convert(registrationKo: RegistrationKo): RegistrationKo {
        const copy: RegistrationKo = Object.assign({}, registrationKo);
        return copy;
    }
}
