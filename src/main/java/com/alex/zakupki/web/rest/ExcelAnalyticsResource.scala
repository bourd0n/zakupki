package com.alex.zakupki.web.rest

import java.io._
import java.net.URLConnection
import javax.inject.Inject
import javax.servlet.http.HttpServletResponse

import com.alex.zakupki.service.ExcelAnalyticsService
import org.slf4j.LoggerFactory
import org.springframework.util.FileCopyUtils
import org.springframework.web.bind.annotation.{GetMapping, PathVariable, RequestMapping, RestController}

@RestController
@RequestMapping(value = Array("/excel"))
class ExcelAnalyticsResource @Inject()(val excelAnalyticsService: ExcelAnalyticsService) {

    private val log = LoggerFactory.getLogger(classOf[ExcelAnalyticsResource])

    def downloadFile(response: HttpServletResponse, file: File): Unit = {
        var mimeType = URLConnection.guessContentTypeFromName(file.getName)
        if (mimeType == null) {
            log.info("mimetype is not detectable, will take default")
            mimeType = "application/octet-stream"
        }
        response.setContentType(mimeType)
        /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser
                    while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/ response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName + "\"")
        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
        //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        response.setContentLength(file.length.asInstanceOf[Int])
        val inputStream = new BufferedInputStream(new FileInputStream(file))
        //Copy bytes from source to destination(outputstream in this example), closes both streams.
        FileCopyUtils.copy(inputStream, response.getOutputStream)
    }

    @GetMapping(value = Array("/complaints/byFas/byYear/{year}"))
    @throws[IOException]
    def downloadCheckResultByFasByYear(response: HttpServletResponse, @PathVariable year: String): Unit = {
        val file = excelAnalyticsService.getCheckResultByFasByYear(year)
        downloadFile(response, file)
    }

    @GetMapping(value = Array("/complaints/results"))
    @throws[IOException]
    def downloadComplaintsStats(response: HttpServletResponse): Unit = {
        val file = excelAnalyticsService.getComplaintsResults()
        downloadFile(response, file)
    }

    @GetMapping(value = Array("/planned_unplanned_check/byFas/byYear/{year}"))
    @throws[IOException]
    def downloadUnplannedPlannedCheckByFasByYear(response: HttpServletResponse, @PathVariable year: String): Unit = {
        val file = excelAnalyticsService.getUnplannedPlannedCheckByFasByYear(year)
        downloadFile(response, file)
    }

    @GetMapping(value = Array("/applicants/complaint_stats"))
    @throws[IOException]
    def downloadApplicantStats(response: HttpServletResponse): Unit = {
        val file = excelAnalyticsService.getApplicantStats()
        downloadFile(response, file)
    }

    @GetMapping(value = Array("/applicants/complaint_stats/byYear/{year}"))
    @throws[IOException]
    def downloadApplicantStatsByYear(response: HttpServletResponse, @PathVariable year: String): Unit = {
        val file = excelAnalyticsService.getApplicantStatsByYear(year)
        downloadFile(response, file)
    }

    @GetMapping(value = Array("/clients/complaint_stats/byYear/{year}"))
    @throws[IOException]
    def downloadClientsStatsByYear(response: HttpServletResponse, @PathVariable year: String): Unit = {
        val file = excelAnalyticsService.getClientsStatsByYear(year)
        downloadFile(response, file)
    }
}
