package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.ComplaintCancelService;
import com.alex.zakupki.domain.ComplaintCancel;
import com.alex.zakupki.repository.ComplaintCancelRepository;
import com.alex.zakupki.repository.search.ComplaintCancelSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ComplaintCancel.
 */
@Service
@Transactional
public class ComplaintCancelServiceImpl implements ComplaintCancelService{

    private final Logger log = LoggerFactory.getLogger(ComplaintCancelServiceImpl.class);

    private final ComplaintCancelRepository complaintCancelRepository;

    private final ComplaintCancelSearchRepository complaintCancelSearchRepository;

    public ComplaintCancelServiceImpl(ComplaintCancelRepository complaintCancelRepository, ComplaintCancelSearchRepository complaintCancelSearchRepository) {
        this.complaintCancelRepository = complaintCancelRepository;
        this.complaintCancelSearchRepository = complaintCancelSearchRepository;
    }

    /**
     * Save a complaintCancel.
     *
     * @param complaintCancel the entity to save
     * @return the persisted entity
     */
    @Override
    public ComplaintCancel save(ComplaintCancel complaintCancel) {
        log.debug("Request to save ComplaintCancel : {}", complaintCancel);
        ComplaintCancel result = complaintCancelRepository.save(complaintCancel);
        complaintCancelSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the complaintCancels.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ComplaintCancel> findAll(Pageable pageable) {
        log.debug("Request to get all ComplaintCancels");
        return complaintCancelRepository.findAll(pageable);
    }

    /**
     *  Get one complaintCancel by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ComplaintCancel findOne(Long id) {
        log.debug("Request to get ComplaintCancel : {}", id);
        return complaintCancelRepository.findOne(id);
    }

    /**
     *  Delete the  complaintCancel by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ComplaintCancel : {}", id);
        complaintCancelRepository.delete(id);
        complaintCancelSearchRepository.delete(id);
    }

    /**
     * Search for the complaintCancel corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ComplaintCancel> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ComplaintCancels for query {}", query);
        Page<ComplaintCancel> result = complaintCancelSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
