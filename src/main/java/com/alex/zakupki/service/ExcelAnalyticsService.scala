package com.alex.zakupki.service

import java.io.File
import javax.inject.Inject

import com.alex.zakupki.repository.AnalyticsRepository
import com.alex.zakupki.service.dto.{ApplicantStats, ComplaintResult, FasComplaintsStats, FasPlannedUnplannedStats}
import org.springframework.stereotype.Service
import org.apache.poi.hssf.usermodel._
import java.io.FileOutputStream

import org.apache.poi.ss.usermodel._
import org.apache.poi.xssf.usermodel.XSSFWorkbook

@Service
class ExcelAnalyticsService @Inject()(val analysisRepository: AnalyticsRepository) {

    def getComplaintsResults(): File = {
        val checkResultByFasByYear: Seq[ComplaintResult] = analysisRepository.getComplaintsResults()

        val workbook = new XSSFWorkbook
        val sheet = workbook.createSheet

        val firstRow = sheet.createRow(0)

        firstRow.createCell(0).setCellValue("id")
        firstRow.createCell(1).setCellValue("complaint number")
        firstRow.createCell(2).setCellValue("reg date")
        firstRow.createCell(3).setCellValue("result")

        checkResultByFasByYear.foreach(complaintStats => {
            val lastRowNum = sheet.getLastRowNum
            val row = sheet.createRow(lastRowNum + 1)

            row.createCell(0).setCellValue(complaintStats.id)
            row.createCell(1).setCellValue(complaintStats.complaintNumber)
            row.createCell(2).setCellValue(if (complaintStats.regDate == null) null else complaintStats.regDate.toString)
            row.createCell(3).setCellValue(complaintStats.result)
            row
        })

        val fileName = s"complaints_results.xlsx"
        val fileOut = new FileOutputStream(fileName)
        workbook.write(fileOut)
        fileOut.close()

        new File(fileName)
    }


    def getCheckResultByFasByYear(year: String): File = {
        val checkResultByFasByYear: Seq[FasComplaintsStats] = analysisRepository.getCheckResultByFasByYear(year)

        val workbook = new HSSFWorkbook
        val sheet = workbook.createSheet

        val firstRow = sheet.createRow(0)

        firstRow.createCell(0).setCellValue("full name")
        firstRow.createCell(1).setCellValue("violations")
        firstRow.createCell(2).setCellValue("no violations")
        firstRow.createCell(3).setCellValue("partly valid")
        firstRow.createCell(4).setCellValue("not matched")
        firstRow.createCell(5).setCellValue("returned")
        firstRow.createCell(6).setCellValue("complaints sum")
        firstRow.createCell(7).setCellValue("empty")

        checkResultByFasByYear.foreach(complaintStats => {
            val lastRowNum = sheet.getLastRowNum
            val row = sheet.createRow(lastRowNum + 1)

            row.createCell(0).setCellValue(complaintStats.fasName)
            row.createCell(1).setCellValue(complaintStats.violations)
            row.createCell(2).setCellValue(complaintStats.noViolations)
            row.createCell(3).setCellValue(complaintStats.partlyValid)
            row.createCell(4).setCellValue(complaintStats.notMatched)
            row.createCell(5).setCellValue(complaintStats.returned)
            row.createCell(6).setCellValue(complaintStats.complaintSum)
            row.createCell(7).setCellValue(complaintStats.empty)
            row
        })

        val fileName = s"complaint_check_result_byFas_$year.xls"
        val fileOut = new FileOutputStream(fileName)
        workbook.write(fileOut)
        fileOut.close()

        new File(fileName)
    }

    def getUnplannedPlannedCheckByFasByYear(year: String): File = {
        val checkResultByFasByYear: Seq[FasPlannedUnplannedStats] = analysisRepository.getUnplannedPlannedCheckByFasByYear(year)

        val workbook = new HSSFWorkbook
        val sheet = workbook.createSheet

        val firstRow = sheet.createRow(0)

        firstRow.createCell(0).setCellValue("full name")
        firstRow.createCell(1).setCellValue("violations")
        firstRow.createCell(2).setCellValue("no violations")
        firstRow.createCell(3).setCellValue("unplannedCount")
        firstRow.createCell(4).setCellValue("plannedCount")

        checkResultByFasByYear.foreach(complaintStats => {
            val lastRowNum = sheet.getLastRowNum
            val row = sheet.createRow(lastRowNum + 1)

            row.createCell(0).setCellValue(complaintStats.fasName)
            row.createCell(1).setCellValue(complaintStats.violations)
            row.createCell(2).setCellValue(complaintStats.noViolations)
            row.createCell(3).setCellValue(complaintStats.unplannedCount)
            row.createCell(4).setCellValue(complaintStats.plannedCount)
            row
        })

        val fileName = s"unplanned_planned_check_byFas_$year.xls"
        val fileOut = new FileOutputStream(fileName)
        workbook.write(fileOut)
        fileOut.close()

        new File(fileName)
    }

    def getApplicantStats(): File = {
        val applicantStatsList: Seq[ApplicantStats] = analysisRepository.getApplicantStats()
        val fileName = s"applicant_stats.xls"
        formApplicantStats(fileName, applicantStatsList)
    }


    def getApplicantStatsByYear(year: String): File = {
        val applicantStatsList: Seq[ApplicantStats] = analysisRepository.getApplicantStatsByYear(year)
        val fileName = s"applicant_stats_$year.xls"
        formApplicantStats(fileName, applicantStatsList)
    }

    private def formApplicantStats(fileName: String, applicantStatsList: Seq[ApplicantStats]): File = {
        val workbook = new HSSFWorkbook
        val sheet = workbook.createSheet

        val firstRow = sheet.createRow(0)

        firstRow.createCell(0).setCellValue("organization name")
        firstRow.createCell(1).setCellValue("applicant type")
        firstRow.createCell(2).setCellValue("complaints count")
        firstRow.createCell(3).setCellValue("violations")
        firstRow.createCell(4).setCellValue("no violations")
        firstRow.createCell(5).setCellValue("partly valid")
        firstRow.createCell(6).setCellValue("not matched")
        firstRow.createCell(7).setCellValue("empty")


        applicantStatsList.foreach(applicantStats => {
            val lastRowNum = sheet.getLastRowNum
            val row = sheet.createRow(lastRowNum + 1)

            row.createCell(0).setCellValue(applicantStats.organizationName)
            row.createCell(1).setCellValue(applicantStats.applicantType)
            row.createCell(2).setCellValue(applicantStats.complaintsCount)
            row.createCell(3).setCellValue(applicantStats.violations)
            row.createCell(4).setCellValue(applicantStats.noViolations)
            row.createCell(5).setCellValue(applicantStats.partlyValid)
            row.createCell(6).setCellValue(applicantStats.notMatched)
            row.createCell(7).setCellValue(applicantStats.empty)
            row
        })

        val fileOut = new FileOutputStream(fileName)
        workbook.write(fileOut)
        fileOut.close()

        new File(fileName)
    }


    def getClientsStatsByYear(year: String): File = {
        val clientsStatsList = analysisRepository.getClientsStatsByYear(year)
        val fileName = s"client_stats_$year.xls"

        val workbook = new HSSFWorkbook
        val sheet = workbook.createSheet

        val firstRow = sheet.createRow(0)

        firstRow.createCell(0).setCellValue("full name")
        firstRow.createCell(1).setCellValue("client type")
        firstRow.createCell(2).setCellValue("complaints count")
        firstRow.createCell(3).setCellValue("violations")
        firstRow.createCell(4).setCellValue("no violations")
        firstRow.createCell(5).setCellValue("partly valid")
        firstRow.createCell(6).setCellValue("not matched")
        firstRow.createCell(7).setCellValue("empty")


        clientsStatsList.foreach(clientStats => {
            val lastRowNum = sheet.getLastRowNum
            val row = sheet.createRow(lastRowNum + 1)

            row.createCell(0).setCellValue(clientStats.fullName)
            row.createCell(1).setCellValue(clientStats.clientType)
            row.createCell(2).setCellValue(clientStats.complaintsCount)
            row.createCell(3).setCellValue(clientStats.violations)
            row.createCell(4).setCellValue(clientStats.noViolations)
            row.createCell(5).setCellValue(clientStats.partlyValid)
            row.createCell(6).setCellValue(clientStats.notMatched)
            row.createCell(7).setCellValue(clientStats.empty)
            row
        })

        val fileOut = new FileOutputStream(fileName)
        workbook.write(fileOut)
        fileOut.close()

        new File(fileName)
    }


}
