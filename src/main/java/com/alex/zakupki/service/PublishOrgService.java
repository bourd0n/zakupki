package com.alex.zakupki.service;

import com.alex.zakupki.domain.PublishOrg;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PublishOrg.
 */
public interface PublishOrgService {

    /**
     * Save a publishOrg.
     *
     * @param publishOrg the entity to save
     * @return the persisted entity
     */
    PublishOrg save(PublishOrg publishOrg);

    /**
     *  Get all the publishOrgs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PublishOrg> findAll(Pageable pageable);

    /**
     *  Get the "id" publishOrg.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PublishOrg findOne(Long id);

    /**
     *  Delete the "id" publishOrg.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the publishOrg corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PublishOrg> search(String query, Pageable pageable);
}
