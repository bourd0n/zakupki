import { BaseEntity } from './../../shared';

export class Owner implements BaseEntity {
    constructor(
        public id?: number,
        public regNum?: string,
        public fullName?: string,
    ) {
    }
}
