export class ClientComplaintsStats {
    constructor(public id?: number,
                public fullName?: string,
                public clientType?: string,
                public complaintsCount?: number,
                public noViolations?: number,
                public violations?: number,
                public partlyValid?: number,
                public notMatched?: number,
                public empty?: number) {
    }
}

export class ClientComplaintsStatsRequest {
    constructor(public fromDate?: string,
                public toDate?: string) {
    }
}

