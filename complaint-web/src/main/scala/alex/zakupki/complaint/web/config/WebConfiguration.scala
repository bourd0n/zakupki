package alex.zakupki.complaint.web.config

import alex.zakupki.complaint.app.config.AppSecurityConfig
import org.springframework.context.annotation.{Import, ComponentScan, Bean, Configuration}
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.{ResourceHandlerRegistry, WebMvcConfigurerAdapter, EnableWebMvc}
import org.springframework.web.servlet.view.velocity.{VelocityConfigurer, VelocityView, VelocityViewResolver}

@Configuration
@EnableWebMvc
//@ComponentScan(Array("alex.zakupki.complaint.web.config", "alex.zakupki.complaint.web.controller"))
//@Import(Array(classOf[AppSecurityConfig]))
class WebConfiguration extends WebMvcConfigurerAdapter {

  @Bean
  def viewResolver(): ViewResolver = {
    val velocityViewResolver = new VelocityViewResolver
    velocityViewResolver.setViewClass(classOf[VelocityView])
    velocityViewResolver.setPrefix("/WEB-INF/views/view/")
    velocityViewResolver.setSuffix(".vm")
    velocityViewResolver.setRequestContextAttribute("rc")
    velocityViewResolver
  }

  @Bean
  def velocityConfig(): VelocityConfigurer = {
    val velocityConfigurer = new VelocityConfigurer
    velocityConfigurer.setResourceLoaderPath("/")
    velocityConfigurer
  }

  override def addResourceHandlers(registry: ResourceHandlerRegistry): Unit = {
    registry.addResourceHandler("/semantic/**").addResourceLocations("/semantic/")
    registry.addResourceHandler("/js/**").addResourceLocations("/js/")
    registry.addResourceHandler("/images/**").addResourceLocations("/images/")
    registry.addResourceHandler("/css/**").addResourceLocations("/css/")
  }
}
