package com.alex.zakupki.complaint.logic.akka.messages

import java.nio.file.Path

case class ProcessDownloadedZip(zipFilePath: Path)
