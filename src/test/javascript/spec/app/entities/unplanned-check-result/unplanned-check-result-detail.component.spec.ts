/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { UnplannedCheckResultDetailComponent } from '../../../../../../main/webapp/app/entities/unplanned-check-result/unplanned-check-result-detail.component';
import { UnplannedCheckResultService } from '../../../../../../main/webapp/app/entities/unplanned-check-result/unplanned-check-result.service';
import { UnplannedCheckResult } from '../../../../../../main/webapp/app/entities/unplanned-check-result/unplanned-check-result.model';

describe('Component Tests', () => {

    describe('UnplannedCheckResult Management Detail Component', () => {
        let comp: UnplannedCheckResultDetailComponent;
        let fixture: ComponentFixture<UnplannedCheckResultDetailComponent>;
        let service: UnplannedCheckResultService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [UnplannedCheckResultDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    UnplannedCheckResultService,
                    JhiEventManager
                ]
            }).overrideTemplate(UnplannedCheckResultDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UnplannedCheckResultDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UnplannedCheckResultService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new UnplannedCheckResult(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.unplannedCheckResult).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
