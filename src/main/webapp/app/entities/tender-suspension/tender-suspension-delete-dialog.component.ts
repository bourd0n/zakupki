import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TenderSuspension } from './tender-suspension.model';
import { TenderSuspensionPopupService } from './tender-suspension-popup.service';
import { TenderSuspensionService } from './tender-suspension.service';

@Component({
    selector: 'jhi-tender-suspension-delete-dialog',
    templateUrl: './tender-suspension-delete-dialog.component.html'
})
export class TenderSuspensionDeleteDialogComponent {

    tenderSuspension: TenderSuspension;

    constructor(
        private tenderSuspensionService: TenderSuspensionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tenderSuspensionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tenderSuspensionListModification',
                content: 'Deleted an tenderSuspension'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tender-suspension-delete-popup',
    template: ''
})
export class TenderSuspensionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tenderSuspensionPopupService: TenderSuspensionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tenderSuspensionPopupService
                .open(TenderSuspensionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
