# README #

Work with data from zakupki gov

### How to run ###
* wildfly
* postgres
* mvn

### Installation ###
1. download and install nodejs https://nodejs.org/en/
2. Install gulp : `npm install --global gulp`
3. Do "Compile css"
4. `mvn package install`

### Compile css ###

1. `cd /zakupki/complaint-web/src/main/webapp/semantic`

2. `gulp build`
(gulp should be installed)

use `gulp watch` to compile changed files on the fly