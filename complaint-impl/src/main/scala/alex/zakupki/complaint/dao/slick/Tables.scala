package alex.zakupki.complaint.dao.slick

// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object Tables extends {
  val profile = slick.driver.PostgresDriver
} with Tables

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait Tables {
  val profile: slick.driver.JdbcProfile

  import profile.api._
  import slick.model.ForeignKeyAction

  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.

  import slick.jdbc.{GetResult => GR}

  /** DDL for all tables. Call .create to execute. */
  lazy val schema = Array(Applicants.schema, CheckComplaintResults.schema, Clients.schema, Complaints.schema, ComplaintsCancel.schema, ControlCheckResults.schema, Owners.schema, PlannedCheckResults.schema, PublishOrg.schema, Purchases.schema, RegistrationKo.schema, TenderSuspensions.schema, UnfairSuppliers.schema, UnplannedCheckResults.schema).reduceLeft(_ ++ _)


  /** Entity class storing rows of table Applicants
    * @param organizationname Database column organizationname SqlType(varchar), PrimaryKey, Length(255,true)
    * @param applicanttype Database column applicanttype SqlType(varchar), Length(255,true), Default(None) */
  case class ApplicantsRow(organizationname: String, applicanttype: Option[String] = None)

  /** GetResult implicit for fetching ApplicantsRow objects using plain SQL queries */
  implicit def GetResultApplicantsRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[ApplicantsRow] = GR {
    prs => import prs._
      ApplicantsRow.tupled((<<[String], <<?[String]))
  }

  /** Table description of table applicants. Objects of this class serve as prototypes for rows in queries. */
  class Applicants(_tableTag: Tag) extends Table[ApplicantsRow](_tableTag, "applicants") {
    def * = (organizationname, applicanttype) <>(ApplicantsRow.tupled, ApplicantsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(organizationname), applicanttype).shaped.<>({ r => import r._; _1.map(_ => ApplicantsRow.tupled((_1.get, _2))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column organizationname SqlType(varchar), PrimaryKey, Length(255,true) */
    val organizationname: Rep[String] = column[String]("organizationname", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column applicanttype SqlType(varchar), Length(255,true), Default(None) */
    val applicanttype: Rep[Option[String]] = column[Option[String]]("applicanttype", O.Length(255, varying = true), O.Default(None))
  }

  /** Collection-like TableQuery object for table Applicants */
  lazy val Applicants = new TableQuery(tag => new Applicants(tag))

  /** Entity class storing rows of table CheckComplaintResults
    * @param checkresultnumber Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param createdate Database column createdate SqlType(timestamp), Default(None)
    * @param result Database column result SqlType(varchar), Length(255,true), Default(None)
    * @param indictedId Database column indicted_id SqlType(varchar), Length(255,true), Default(None)
    * @param ownerId Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
  case class CheckComplaintResultsRow(checkresultnumber: String, createdate: Option[java.sql.Timestamp] = None, result: Option[String] = None, indictedId: Option[String] = None, ownerId: Option[String] = None)

  /** GetResult implicit for fetching CheckComplaintResultsRow objects using plain SQL queries */
  implicit def GetResultCheckComplaintResultsRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Option[String]]): GR[CheckComplaintResultsRow] = GR {
    prs => import prs._
      CheckComplaintResultsRow.tupled((<<[String], <<?[java.sql.Timestamp], <<?[String], <<?[String], <<?[String]))
  }

  /** Table description of table check_complaint_results. Objects of this class serve as prototypes for rows in queries. */
  class CheckComplaintResults(_tableTag: Tag) extends Table[CheckComplaintResultsRow](_tableTag, "check_complaint_results") {
    def * = (checkresultnumber, createdate, result, indictedId, ownerId) <>(CheckComplaintResultsRow.tupled, CheckComplaintResultsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(checkresultnumber), createdate, result, indictedId, ownerId).shaped.<>({ r => import r._; _1.map(_ => CheckComplaintResultsRow.tupled((_1.get, _2, _3, _4, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val checkresultnumber: Rep[String] = column[String]("checkresultnumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column createdate SqlType(timestamp), Default(None) */
    val createdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("createdate", O.Default(None))
    /** Database column result SqlType(varchar), Length(255,true), Default(None) */
    val result: Rep[Option[String]] = column[Option[String]]("result", O.Length(255, varying = true), O.Default(None))
    /** Database column indicted_id SqlType(varchar), Length(255,true), Default(None) */
    val indictedId: Rep[Option[String]] = column[Option[String]]("indicted_id", O.Length(255, varying = true), O.Default(None))
    /** Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
    val ownerId: Rep[Option[String]] = column[Option[String]]("owner_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing Clients (database name fk_3go46qky36ylbu9cy9qckmt8) */
    lazy val clientsFk = foreignKey("fk_3go46qky36ylbu9cy9qckmt8", indictedId, Clients)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Owners (database name fk_2f31doq164wreywabos71b9s1) */
    lazy val ownersFk = foreignKey("fk_2f31doq164wreywabos71b9s1", ownerId, Owners)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table CheckComplaintResults */
  lazy val CheckComplaintResults = new TableQuery(tag => new CheckComplaintResults(tag))

  /** Entity class storing rows of table Clients
    * @param regnum Database column regnum SqlType(varchar), PrimaryKey, Length(255,true)
    * @param clienttype Database column clienttype SqlType(varchar), Length(255,true), Default(None)
    * @param fullname Database column fullname SqlType(varchar), Length(4000,true), Default(None) */
  case class ClientsRow(regnum: String, clienttype: Option[String] = None, fullname: Option[String] = None)

  /** GetResult implicit for fetching ClientsRow objects using plain SQL queries */
  implicit def GetResultClientsRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[ClientsRow] = GR {
    prs => import prs._
      ClientsRow.tupled((<<[String], <<?[String], <<?[String]))
  }

  /** Table description of table clients. Objects of this class serve as prototypes for rows in queries. */
  class Clients(_tableTag: Tag) extends Table[ClientsRow](_tableTag, "clients") {
    def * = (regnum, clienttype, fullname) <>(ClientsRow.tupled, ClientsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(regnum), clienttype, fullname).shaped.<>({ r => import r._; _1.map(_ => ClientsRow.tupled((_1.get, _2, _3))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column regnum SqlType(varchar), PrimaryKey, Length(255,true) */
    val regnum: Rep[String] = column[String]("regnum", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column clienttype SqlType(varchar), Length(255,true), Default(None) */
    val clienttype: Rep[Option[String]] = column[Option[String]]("clienttype", O.Length(255, varying = true), O.Default(None))
    /** Database column fullname SqlType(varchar), Length(4000,true), Default(None) */
    val fullname: Rep[Option[String]] = column[Option[String]]("fullname", O.Length(4000, varying = true), O.Default(None))
  }

  /** Collection-like TableQuery object for table Clients */
  lazy val Clients = new TableQuery(tag => new Clients(tag))

  /** Entity class storing rows of table Complaints
    * @param complaintnumber Database column complaintnumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param regdate Database column regdate SqlType(timestamp), Default(None)
    * @param text Database column text SqlType(varchar), Length(4000,true), Default(None)
    * @param applicantId Database column applicant_id SqlType(varchar), Length(255,true), Default(None)
    * @param indictedClientId Database column indicted_client_id SqlType(varchar), Length(255,true), Default(None)
    * @param purchaseId Database column purchase_id SqlType(varchar), Length(255,true), Default(None)
    * @param registrationKoId Database column registration_ko_id SqlType(varchar), Length(255,true), Default(None) */
  case class ComplaintsRow(complaintnumber: String, regdate: Option[java.sql.Timestamp] = None, text: Option[String] = None, applicantId: Option[String] = None, indictedClientId: Option[String] = None, purchaseId: Option[String] = None, registrationKoId: Option[String] = None)

  /** GetResult implicit for fetching ComplaintsRow objects using plain SQL queries */
  implicit def GetResultComplaintsRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Option[String]]): GR[ComplaintsRow] = GR {
    prs => import prs._
      ComplaintsRow.tupled((<<[String], <<?[java.sql.Timestamp], <<?[String], <<?[String], <<?[String], <<?[String], <<?[String]))
  }

  /** Table description of table complaints. Objects of this class serve as prototypes for rows in queries. */
  class Complaints(_tableTag: Tag) extends Table[ComplaintsRow](_tableTag, "complaints") {
    def * = (complaintnumber, regdate, text, applicantId, indictedClientId, purchaseId, registrationKoId) <>(ComplaintsRow.tupled, ComplaintsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(complaintnumber), regdate, text, applicantId, indictedClientId, purchaseId, registrationKoId).shaped.<>({ r => import r._; _1.map(_ => ComplaintsRow.tupled((_1.get, _2, _3, _4, _5, _6, _7))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column complaintnumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val complaintnumber: Rep[String] = column[String]("complaintnumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column regdate SqlType(timestamp), Default(None) */
    val regdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("regdate", O.Default(None))
    /** Database column text SqlType(varchar), Length(4000,true), Default(None) */
    val text: Rep[Option[String]] = column[Option[String]]("text", O.Length(4000, varying = true), O.Default(None))
    /** Database column applicant_id SqlType(varchar), Length(255,true), Default(None) */
    val applicantId: Rep[Option[String]] = column[Option[String]]("applicant_id", O.Length(255, varying = true), O.Default(None))
    /** Database column indicted_client_id SqlType(varchar), Length(255,true), Default(None) */
    val indictedClientId: Rep[Option[String]] = column[Option[String]]("indicted_client_id", O.Length(255, varying = true), O.Default(None))
    /** Database column purchase_id SqlType(varchar), Length(255,true), Default(None) */
    val purchaseId: Rep[Option[String]] = column[Option[String]]("purchase_id", O.Length(255, varying = true), O.Default(None))
    /** Database column registration_ko_id SqlType(varchar), Length(255,true), Default(None) */
    val registrationKoId: Rep[Option[String]] = column[Option[String]]("registration_ko_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing Applicants (database name fk_hf8c3l5pdmcmtmgvat7nn6jlp) */
    lazy val applicantsFk = foreignKey("fk_hf8c3l5pdmcmtmgvat7nn6jlp", applicantId, Applicants)(r => Rep.Some(r.organizationname), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Clients (database name fk_ahk1d1hm972m4x7a7nwwpao1e) */
    lazy val clientsFk = foreignKey("fk_ahk1d1hm972m4x7a7nwwpao1e", indictedClientId, Clients)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Purchases (database name fk_2oai1b35ov3uxmpbm91w44v2l) */
    lazy val purchasesFk = foreignKey("fk_2oai1b35ov3uxmpbm91w44v2l", purchaseId, Purchases)(r => Rep.Some(r.purchasenumber), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing RegistrationKo (database name fk_2t0rgl1368cnx2j3dfo0c2a0g) */
    lazy val registrationKoFk = foreignKey("fk_2t0rgl1368cnx2j3dfo0c2a0g", registrationKoId, RegistrationKo)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table Complaints */
  lazy val Complaints = new TableQuery(tag => new Complaints(tag))

  /** Entity class storing rows of table ComplaintsCancel
    * @param complaintnumber Database column complaintnumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param regdate Database column regdate SqlType(timestamp), Default(None)
    * @param text Database column text SqlType(varchar), Length(4000,true), Default(None)
    * @param registrationKoId Database column registration_ko_id SqlType(varchar), Length(255,true), Default(None) */
  case class ComplaintsCancelRow(complaintnumber: String, regdate: Option[java.sql.Timestamp] = None, text: Option[String] = None, registrationKoId: Option[String] = None)

  /** GetResult implicit for fetching ComplaintsCancelRow objects using plain SQL queries */
  implicit def GetResultComplaintsCancelRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Option[String]]): GR[ComplaintsCancelRow] = GR {
    prs => import prs._
      ComplaintsCancelRow.tupled((<<[String], <<?[java.sql.Timestamp], <<?[String], <<?[String]))
  }

  /** Table description of table complaints_cancel. Objects of this class serve as prototypes for rows in queries. */
  class ComplaintsCancel(_tableTag: Tag) extends Table[ComplaintsCancelRow](_tableTag, "complaints_cancel") {
    def * = (complaintnumber, regdate, text, registrationKoId) <>(ComplaintsCancelRow.tupled, ComplaintsCancelRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(complaintnumber), regdate, text, registrationKoId).shaped.<>({ r => import r._; _1.map(_ => ComplaintsCancelRow.tupled((_1.get, _2, _3, _4))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column complaintnumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val complaintnumber: Rep[String] = column[String]("complaintnumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column regdate SqlType(timestamp), Default(None) */
    val regdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("regdate", O.Default(None))
    /** Database column text SqlType(varchar), Length(4000,true), Default(None) */
    val text: Rep[Option[String]] = column[Option[String]]("text", O.Length(4000, varying = true), O.Default(None))
    /** Database column registration_ko_id SqlType(varchar), Length(255,true), Default(None) */
    val registrationKoId: Rep[Option[String]] = column[Option[String]]("registration_ko_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing RegistrationKo (database name fk_p9d6by14qoiey1qd24qkfv2s2) */
    lazy val registrationKoFk = foreignKey("fk_p9d6by14qoiey1qd24qkfv2s2", registrationKoId, RegistrationKo)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table ComplaintsCancel */
  lazy val ComplaintsCancel = new TableQuery(tag => new ComplaintsCancel(tag))

  /** Entity class storing rows of table ControlCheckResults
    * @param checkresultnumber Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param createdate Database column createdate SqlType(timestamp), Default(None)
    * @param result Database column result SqlType(varchar), Length(255,true), Default(None)
    * @param subjectClientId Database column subject_client_id SqlType(varchar), Length(255,true), Default(None)
    * @param ownerId Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
  case class ControlCheckResultsRow(checkresultnumber: String, createdate: Option[java.sql.Timestamp] = None, result: Option[String] = None, subjectClientId: Option[String] = None, ownerId: Option[String] = None)

  /** GetResult implicit for fetching ControlCheckResultsRow objects using plain SQL queries */
  implicit def GetResultControlCheckResultsRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Option[String]]): GR[ControlCheckResultsRow] = GR {
    prs => import prs._
      ControlCheckResultsRow.tupled((<<[String], <<?[java.sql.Timestamp], <<?[String], <<?[String], <<?[String]))
  }

  /** Table description of table control_check_results. Objects of this class serve as prototypes for rows in queries. */
  class ControlCheckResults(_tableTag: Tag) extends Table[ControlCheckResultsRow](_tableTag, "control_check_results") {
    def * = (checkresultnumber, createdate, result, subjectClientId, ownerId) <>(ControlCheckResultsRow.tupled, ControlCheckResultsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(checkresultnumber), createdate, result, subjectClientId, ownerId).shaped.<>({ r => import r._; _1.map(_ => ControlCheckResultsRow.tupled((_1.get, _2, _3, _4, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val checkresultnumber: Rep[String] = column[String]("checkresultnumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column createdate SqlType(timestamp), Default(None) */
    val createdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("createdate", O.Default(None))
    /** Database column result SqlType(varchar), Length(255,true), Default(None) */
    val result: Rep[Option[String]] = column[Option[String]]("result", O.Length(255, varying = true), O.Default(None))
    /** Database column subject_client_id SqlType(varchar), Length(255,true), Default(None) */
    val subjectClientId: Rep[Option[String]] = column[Option[String]]("subject_client_id", O.Length(255, varying = true), O.Default(None))
    /** Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
    val ownerId: Rep[Option[String]] = column[Option[String]]("owner_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing Clients (database name fk_4hhfdb5o1tuc3i981pnp9ur3t) */
    lazy val clientsFk = foreignKey("fk_4hhfdb5o1tuc3i981pnp9ur3t", subjectClientId, Clients)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Owners (database name fk_bqwuj4aa49gc8pjs4w6i3lufo) */
    lazy val ownersFk = foreignKey("fk_bqwuj4aa49gc8pjs4w6i3lufo", ownerId, Owners)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table ControlCheckResults */
  lazy val ControlCheckResults = new TableQuery(tag => new ControlCheckResults(tag))

  /** Entity class storing rows of table Owners
    * @param regnum Database column regnum SqlType(varchar), PrimaryKey, Length(255,true)
    * @param fullname Database column fullname SqlType(varchar), Length(255,true), Default(None) */
  case class OwnersRow(regnum: String, fullname: Option[String] = None)

  /** GetResult implicit for fetching OwnersRow objects using plain SQL queries */
  implicit def GetResultOwnersRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[OwnersRow] = GR {
    prs => import prs._
      OwnersRow.tupled((<<[String], <<?[String]))
  }

  /** Table description of table owners. Objects of this class serve as prototypes for rows in queries. */
  class Owners(_tableTag: Tag) extends Table[OwnersRow](_tableTag, "owners") {
    def * = (regnum, fullname) <>(OwnersRow.tupled, OwnersRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(regnum), fullname).shaped.<>({ r => import r._; _1.map(_ => OwnersRow.tupled((_1.get, _2))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column regnum SqlType(varchar), PrimaryKey, Length(255,true) */
    val regnum: Rep[String] = column[String]("regnum", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column fullname SqlType(varchar), Length(255,true), Default(None) */
    val fullname: Rep[Option[String]] = column[Option[String]]("fullname", O.Length(255, varying = true), O.Default(None))
  }

  /** Collection-like TableQuery object for table Owners */
  lazy val Owners = new TableQuery(tag => new Owners(tag))

  /** Entity class storing rows of table PlannedCheckResults
    * @param checkresultnumber Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param actdate Database column actdate SqlType(timestamp), Default(None)
    * @param actprescriptiondate Database column actprescriptiondate SqlType(timestamp), Default(None)
    * @param createdate Database column createdate SqlType(timestamp), Default(None)
    * @param subjectCustomerId Database column subject_customer_id SqlType(varchar), Length(255,true), Default(None)
    * @param ownerId Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
  case class PlannedCheckResultsRow(checkresultnumber: String, actdate: Option[java.sql.Timestamp] = None, actprescriptiondate: Option[java.sql.Timestamp] = None, createdate: Option[java.sql.Timestamp] = None, subjectCustomerId: Option[String] = None, ownerId: Option[String] = None)

  /** GetResult implicit for fetching PlannedCheckResultsRow objects using plain SQL queries */
  implicit def GetResultPlannedCheckResultsRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Option[String]]): GR[PlannedCheckResultsRow] = GR {
    prs => import prs._
      PlannedCheckResultsRow.tupled((<<[String], <<?[java.sql.Timestamp], <<?[java.sql.Timestamp], <<?[java.sql.Timestamp], <<?[String], <<?[String]))
  }

  /** Table description of table planned_check_results. Objects of this class serve as prototypes for rows in queries. */
  class PlannedCheckResults(_tableTag: Tag) extends Table[PlannedCheckResultsRow](_tableTag, "planned_check_results") {
    def * = (checkresultnumber, actdate, actprescriptiondate, createdate, subjectCustomerId, ownerId) <>(PlannedCheckResultsRow.tupled, PlannedCheckResultsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(checkresultnumber), actdate, actprescriptiondate, createdate, subjectCustomerId, ownerId).shaped.<>({ r => import r._; _1.map(_ => PlannedCheckResultsRow.tupled((_1.get, _2, _3, _4, _5, _6))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val checkresultnumber: Rep[String] = column[String]("checkresultnumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column actdate SqlType(timestamp), Default(None) */
    val actdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("actdate", O.Default(None))
    /** Database column actprescriptiondate SqlType(timestamp), Default(None) */
    val actprescriptiondate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("actprescriptiondate", O.Default(None))
    /** Database column createdate SqlType(timestamp), Default(None) */
    val createdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("createdate", O.Default(None))
    /** Database column subject_customer_id SqlType(varchar), Length(255,true), Default(None) */
    val subjectCustomerId: Rep[Option[String]] = column[Option[String]]("subject_customer_id", O.Length(255, varying = true), O.Default(None))
    /** Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
    val ownerId: Rep[Option[String]] = column[Option[String]]("owner_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing Clients (database name fk_fnkjvnyk7kej4ly5d33dsj3qs) */
    lazy val clientsFk = foreignKey("fk_fnkjvnyk7kej4ly5d33dsj3qs", subjectCustomerId, Clients)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Owners (database name fk_jrhy8ot83j422q7ffdbkpx42h) */
    lazy val ownersFk = foreignKey("fk_jrhy8ot83j422q7ffdbkpx42h", ownerId, Owners)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table PlannedCheckResults */
  lazy val PlannedCheckResults = new TableQuery(tag => new PlannedCheckResults(tag))

  /** Entity class storing rows of table PublishOrg
    * @param regnum Database column regnum SqlType(varchar), PrimaryKey, Length(255,true)
    * @param fullname Database column fullname SqlType(varchar), Length(255,true), Default(None) */
  case class PublishOrgRow(regnum: String, fullname: Option[String] = None)

  /** GetResult implicit for fetching PublishOrgRow objects using plain SQL queries */
  implicit def GetResultPublishOrgRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[PublishOrgRow] = GR {
    prs => import prs._
      PublishOrgRow.tupled((<<[String], <<?[String]))
  }

  /** Table description of table publish_org. Objects of this class serve as prototypes for rows in queries. */
  class PublishOrg(_tableTag: Tag) extends Table[PublishOrgRow](_tableTag, "publish_org") {
    def * = (regnum, fullname) <>(PublishOrgRow.tupled, PublishOrgRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(regnum), fullname).shaped.<>({ r => import r._; _1.map(_ => PublishOrgRow.tupled((_1.get, _2))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column regnum SqlType(varchar), PrimaryKey, Length(255,true) */
    val regnum: Rep[String] = column[String]("regnum", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column fullname SqlType(varchar), Length(255,true), Default(None) */
    val fullname: Rep[Option[String]] = column[Option[String]]("fullname", O.Length(255, varying = true), O.Default(None))
  }

  /** Collection-like TableQuery object for table PublishOrg */
  lazy val PublishOrg = new TableQuery(tag => new PublishOrg(tag))

  /** Entity class storing rows of table Purchases
    * @param purchasenumber Database column purchasenumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param determsuppliermethod Database column determsuppliermethod SqlType(varchar), Length(255,true), Default(None)
    * @param electronicplatform Database column electronicplatform SqlType(varchar), Length(255,true), Default(None)
    * @param maxcost Database column maxcost SqlType(float8) */
  case class PurchasesRow(purchasenumber: String, determsuppliermethod: Option[String] = None, electronicplatform: Option[String] = None, maxcost: Double)

  /** GetResult implicit for fetching PurchasesRow objects using plain SQL queries */
  implicit def GetResultPurchasesRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Double]): GR[PurchasesRow] = GR {
    prs => import prs._
      PurchasesRow.tupled((<<[String], <<?[String], <<?[String], <<[Double]))
  }

  /** Table description of table purchases. Objects of this class serve as prototypes for rows in queries. */
  class Purchases(_tableTag: Tag) extends Table[PurchasesRow](_tableTag, "purchases") {
    def * = (purchasenumber, determsuppliermethod, electronicplatform, maxcost) <>(PurchasesRow.tupled, PurchasesRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(purchasenumber), determsuppliermethod, electronicplatform, Rep.Some(maxcost)).shaped.<>({ r => import r._; _1.map(_ => PurchasesRow.tupled((_1.get, _2, _3, _4.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column purchasenumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val purchasenumber: Rep[String] = column[String]("purchasenumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column determsuppliermethod SqlType(varchar), Length(255,true), Default(None) */
    val determsuppliermethod: Rep[Option[String]] = column[Option[String]]("determsuppliermethod", O.Length(255, varying = true), O.Default(None))
    /** Database column electronicplatform SqlType(varchar), Length(255,true), Default(None) */
    val electronicplatform: Rep[Option[String]] = column[Option[String]]("electronicplatform", O.Length(255, varying = true), O.Default(None))
    /** Database column maxcost SqlType(float8) */
    val maxcost: Rep[Double] = column[Double]("maxcost")
  }

  /** Collection-like TableQuery object for table Purchases */
  lazy val Purchases = new TableQuery(tag => new Purchases(tag))

  /** Entity class storing rows of table RegistrationKo
    * @param regnum Database column regnum SqlType(varchar), PrimaryKey, Length(255,true)
    * @param fullname Database column fullname SqlType(varchar), Length(255,true), Default(None) */
  case class RegistrationKoRow(regnum: String, fullname: Option[String] = None)

  /** GetResult implicit for fetching RegistrationKoRow objects using plain SQL queries */
  implicit def GetResultRegistrationKoRow(implicit e0: GR[String], e1: GR[Option[String]]): GR[RegistrationKoRow] = GR {
    prs => import prs._
      RegistrationKoRow.tupled((<<[String], <<?[String]))
  }

  /** Table description of table registration_ko. Objects of this class serve as prototypes for rows in queries. */
  class RegistrationKo(_tableTag: Tag) extends Table[RegistrationKoRow](_tableTag, "registration_ko") {
    def * = (regnum, fullname) <>(RegistrationKoRow.tupled, RegistrationKoRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(regnum), fullname).shaped.<>({ r => import r._; _1.map(_ => RegistrationKoRow.tupled((_1.get, _2))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column regnum SqlType(varchar), PrimaryKey, Length(255,true) */
    val regnum: Rep[String] = column[String]("regnum", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column fullname SqlType(varchar), Length(255,true), Default(None) */
    val fullname: Rep[Option[String]] = column[Option[String]]("fullname", O.Length(255, varying = true), O.Default(None))
  }

  /** Collection-like TableQuery object for table RegistrationKo */
  lazy val RegistrationKo = new TableQuery(tag => new RegistrationKo(tag))

  /** Entity class storing rows of table TenderSuspensions
    * @param id Database column id SqlType(int4), PrimaryKey
    * @param action Database column action SqlType(varchar), Length(255,true), Default(None)
    * @param complaintnumber Database column complaintnumber SqlType(varchar), Length(255,true), Default(None)
    * @param regdate Database column regdate SqlType(timestamp), Default(None) */
  case class TenderSuspensionsRow(id: Int, action: Option[String] = None, complaintnumber: Option[String] = None, regdate: Option[java.sql.Timestamp] = None)

  /** GetResult implicit for fetching TenderSuspensionsRow objects using plain SQL queries */
  implicit def GetResultTenderSuspensionsRow(implicit e0: GR[Int], e1: GR[Option[String]], e2: GR[Option[java.sql.Timestamp]]): GR[TenderSuspensionsRow] = GR {
    prs => import prs._
      TenderSuspensionsRow.tupled((<<[Int], <<?[String], <<?[String], <<?[java.sql.Timestamp]))
  }

  /** Table description of table tender_suspensions. Objects of this class serve as prototypes for rows in queries. */
  class TenderSuspensions(_tableTag: Tag) extends Table[TenderSuspensionsRow](_tableTag, "tender_suspensions") {
    def * = (id, action, complaintnumber, regdate) <>(TenderSuspensionsRow.tupled, TenderSuspensionsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(id), action, complaintnumber, regdate).shaped.<>({ r => import r._; _1.map(_ => TenderSuspensionsRow.tupled((_1.get, _2, _3, _4))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column id SqlType(int4), PrimaryKey */
    val id: Rep[Int] = column[Int]("id", O.PrimaryKey)
    /** Database column action SqlType(varchar), Length(255,true), Default(None) */
    val action: Rep[Option[String]] = column[Option[String]]("action", O.Length(255, varying = true), O.Default(None))
    /** Database column complaintnumber SqlType(varchar), Length(255,true), Default(None) */
    val complaintnumber: Rep[Option[String]] = column[Option[String]]("complaintnumber", O.Length(255, varying = true), O.Default(None))
    /** Database column regdate SqlType(timestamp), Default(None) */
    val regdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("regdate", O.Default(None))
  }

  /** Collection-like TableQuery object for table TenderSuspensions */
  lazy val TenderSuspensions = new TableQuery(tag => new TenderSuspensions(tag))

  /** Entity class storing rows of table UnfairSuppliers
    * @param registrynum Database column registrynum SqlType(varchar), PrimaryKey, Length(255,true)
    * @param fullname Database column fullname SqlType(varchar), Length(255,true), Default(None)
    * @param inn Database column inn SqlType(varchar), Length(255,true), Default(None)
    * @param publishdate Database column publishdate SqlType(timestamp), Default(None)
    * @param purchasenumber Database column purchasenumber SqlType(varchar), Length(255,true), Default(None)
    * @param reason Database column reason SqlType(varchar), Length(255,true), Default(None)
    * @param state Database column state SqlType(varchar), Length(255,true), Default(None)
    * @param publishOrgId Database column publish_org_id SqlType(varchar), Length(255,true), Default(None) */
  case class UnfairSuppliersRow(registrynum: String, fullname: Option[String] = None, inn: Option[String] = None, publishdate: Option[java.sql.Timestamp] = None, purchasenumber: Option[String] = None, reason: Option[String] = None, state: Option[String] = None, publishOrgId: Option[String] = None)

  /** GetResult implicit for fetching UnfairSuppliersRow objects using plain SQL queries */
  implicit def GetResultUnfairSuppliersRow(implicit e0: GR[String], e1: GR[Option[String]], e2: GR[Option[java.sql.Timestamp]]): GR[UnfairSuppliersRow] = GR {
    prs => import prs._
      UnfairSuppliersRow.tupled((<<[String], <<?[String], <<?[String], <<?[java.sql.Timestamp], <<?[String], <<?[String], <<?[String], <<?[String]))
  }

  /** Table description of table unfair_suppliers. Objects of this class serve as prototypes for rows in queries. */
  class UnfairSuppliers(_tableTag: Tag) extends Table[UnfairSuppliersRow](_tableTag, "unfair_suppliers") {
    def * = (registrynum, fullname, inn, publishdate, purchasenumber, reason, state, publishOrgId) <>(UnfairSuppliersRow.tupled, UnfairSuppliersRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(registrynum), fullname, inn, publishdate, purchasenumber, reason, state, publishOrgId).shaped.<>({ r => import r._; _1.map(_ => UnfairSuppliersRow.tupled((_1.get, _2, _3, _4, _5, _6, _7, _8))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column registrynum SqlType(varchar), PrimaryKey, Length(255,true) */
    val registrynum: Rep[String] = column[String]("registrynum", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column fullname SqlType(varchar), Length(255,true), Default(None) */
    val fullname: Rep[Option[String]] = column[Option[String]]("fullname", O.Length(255, varying = true), O.Default(None))
    /** Database column inn SqlType(varchar), Length(255,true), Default(None) */
    val inn: Rep[Option[String]] = column[Option[String]]("inn", O.Length(255, varying = true), O.Default(None))
    /** Database column publishdate SqlType(timestamp), Default(None) */
    val publishdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("publishdate", O.Default(None))
    /** Database column purchasenumber SqlType(varchar), Length(255,true), Default(None) */
    val purchasenumber: Rep[Option[String]] = column[Option[String]]("purchasenumber", O.Length(255, varying = true), O.Default(None))
    /** Database column reason SqlType(varchar), Length(255,true), Default(None) */
    val reason: Rep[Option[String]] = column[Option[String]]("reason", O.Length(255, varying = true), O.Default(None))
    /** Database column state SqlType(varchar), Length(255,true), Default(None) */
    val state: Rep[Option[String]] = column[Option[String]]("state", O.Length(255, varying = true), O.Default(None))
    /** Database column publish_org_id SqlType(varchar), Length(255,true), Default(None) */
    val publishOrgId: Rep[Option[String]] = column[Option[String]]("publish_org_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing PublishOrg (database name fk_h0fkww2qunm83wog6dyjpy9db) */
    lazy val publishOrgFk = foreignKey("fk_h0fkww2qunm83wog6dyjpy9db", publishOrgId, PublishOrg)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table UnfairSuppliers */
  lazy val UnfairSuppliers = new TableQuery(tag => new UnfairSuppliers(tag))

  /** Entity class storing rows of table UnplannedCheckResults
    * @param checkresultnumber Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true)
    * @param createdate Database column createdate SqlType(timestamp), Default(None)
    * @param result Database column result SqlType(varchar), Length(255,true), Default(None)
    * @param subjectClientId Database column subject_client_id SqlType(varchar), Length(255,true), Default(None)
    * @param ownerId Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
  case class UnplannedCheckResultsRow(checkresultnumber: String, createdate: Option[java.sql.Timestamp] = None, result: Option[String] = None, subjectClientId: Option[String] = None, ownerId: Option[String] = None)

  /** GetResult implicit for fetching UnplannedCheckResultsRow objects using plain SQL queries */
  implicit def GetResultUnplannedCheckResultsRow(implicit e0: GR[String], e1: GR[Option[java.sql.Timestamp]], e2: GR[Option[String]]): GR[UnplannedCheckResultsRow] = GR {
    prs => import prs._
      UnplannedCheckResultsRow.tupled((<<[String], <<?[java.sql.Timestamp], <<?[String], <<?[String], <<?[String]))
  }

  /** Table description of table unplanned_check_results. Objects of this class serve as prototypes for rows in queries. */
  class UnplannedCheckResults(_tableTag: Tag) extends Table[UnplannedCheckResultsRow](_tableTag, "unplanned_check_results") {
    def * = (checkresultnumber, createdate, result, subjectClientId, ownerId) <>(UnplannedCheckResultsRow.tupled, UnplannedCheckResultsRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(checkresultnumber), createdate, result, subjectClientId, ownerId).shaped.<>({ r => import r._; _1.map(_ => UnplannedCheckResultsRow.tupled((_1.get, _2, _3, _4, _5))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column checkresultnumber SqlType(varchar), PrimaryKey, Length(255,true) */
    val checkresultnumber: Rep[String] = column[String]("checkresultnumber", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column createdate SqlType(timestamp), Default(None) */
    val createdate: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("createdate", O.Default(None))
    /** Database column result SqlType(varchar), Length(255,true), Default(None) */
    val result: Rep[Option[String]] = column[Option[String]]("result", O.Length(255, varying = true), O.Default(None))
    /** Database column subject_client_id SqlType(varchar), Length(255,true), Default(None) */
    val subjectClientId: Rep[Option[String]] = column[Option[String]]("subject_client_id", O.Length(255, varying = true), O.Default(None))
    /** Database column owner_id SqlType(varchar), Length(255,true), Default(None) */
    val ownerId: Rep[Option[String]] = column[Option[String]]("owner_id", O.Length(255, varying = true), O.Default(None))

    /** Foreign key referencing Clients (database name fk_kqwhooj6tfaq56tkqupjfelfm) */
    lazy val clientsFk = foreignKey("fk_kqwhooj6tfaq56tkqupjfelfm", subjectClientId, Clients)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
    /** Foreign key referencing Owners (database name fk_pmjd4dopavvrqnevbadk2e26l) */
    lazy val ownersFk = foreignKey("fk_pmjd4dopavvrqnevbadk2e26l", ownerId, Owners)(r => Rep.Some(r.regnum), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table UnplannedCheckResults */
  lazy val UnplannedCheckResults = new TableQuery(tag => new UnplannedCheckResults(tag))
}
