package alex.zakupki.complaint.utils

import java.io.File
import java.net.URL

import org.hibernate.cfg.Configuration
import org.hibernate.tool.hbm2ddl.SchemaExport

//todo: move to maven task
object SchemaGenerator {

  @throws(classOf[Exception])
  def main(args: Array[String]) {
    val gen: SchemaGenerator = new SchemaGenerator("alex.zakupki.complaint.api.model")
    gen.generate(Dialects.POSTGRE)
  }

  /**
   * Holds the classnames of hibernate dialects for easy reference.
   */
  object Dialects {

    sealed abstract class Dialect(val name: String, val dialectClass: String)

    case object ORACLE extends Dialect("oracle", "org.hibernate.dialect.Oracle10gDialect")

    case object MYSQL extends Dialect("mysql", "org.hibernate.dialect.MySQLDialect")

    case object HSQL extends Dialect("hsql", "org.hibernate.dialect.HSQLDialect")

    case object POSTGRE extends Dialect("postgre", "org.hibernate.dialect.PostgreSQL9Dialect")

  }

}

class SchemaGenerator {

  import SchemaGenerator.Dialects._

  private var cfg: Configuration = null

  @throws(classOf[Exception])
  def this(packageName: String) {
    this()
    cfg = new Configuration
    cfg.setProperty("hibernate.hbm2ddl.auto", "create-drop")
    getClasses(packageName).foreach(cfg.addAnnotatedClass)
  }

  private def generate(dialect: Dialect) {
    cfg.setProperty("hibernate.dialect", dialect.dialectClass)
    val export: SchemaExport = new SchemaExport(cfg)
    export.setDelimiter(";")
    export.setOutputFile("ddl_" + dialect.name.toLowerCase + ".sql")
    export.execute(true, false, false, false)
  }

  /**
   * Utility method used to fetch Class list based on a package name.
   *
   * @param packageName (should be the package containing your annotated beans.
   */
  @throws(classOf[Exception])
  private def getClasses(packageName: String): Vector[Class[_]] = {
    var directory: File = null
    try {
      val cld: ClassLoader = Thread.currentThread.getContextClassLoader
      if (cld == null) {
        throw new ClassNotFoundException("Can't get class loader.")
      }
      val path: String = packageName.replace('.', '/')
      val resource: URL = cld.getResource(path)
      if (resource == null) {
        throw new ClassNotFoundException("No resource for " + path)
      }
      directory = new File(resource.getFile)
    } catch {
      case x: NullPointerException =>
        throw new ClassNotFoundException(packageName + " (" + directory + ") does not appear to be a valid package")
    }
    if (directory.exists) {
      val classes: Vector[Class[_]] = directory.list.filter(_.endsWith(".class"))
        .map(fileName => Class.forName(packageName + '.' + fileName.substring(0, fileName.length - 6))).toVector
      classes
    } else {
      throw new ClassNotFoundException(packageName + " is not a valid package")
    }
  }
}