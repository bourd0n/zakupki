import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnplannedCheckResultComponent } from './unplanned-check-result.component';
import { UnplannedCheckResultDetailComponent } from './unplanned-check-result-detail.component';
import { UnplannedCheckResultPopupComponent } from './unplanned-check-result-dialog.component';
import { UnplannedCheckResultDeletePopupComponent } from './unplanned-check-result-delete-dialog.component';

export const unplannedCheckResultRoute: Routes = [
    {
        path: 'unplanned-check-result',
        component: UnplannedCheckResultComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unplannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'unplanned-check-result/:id',
        component: UnplannedCheckResultDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unplannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unplannedCheckResultPopupRoute: Routes = [
    {
        path: 'unplanned-check-result-new',
        component: UnplannedCheckResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unplannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unplanned-check-result/:id/edit',
        component: UnplannedCheckResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unplannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unplanned-check-result/:id/delete',
        component: UnplannedCheckResultDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unplannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
