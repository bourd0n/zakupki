package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Purchase.
 */
@Entity
@Table(name = "purchase")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "purchase")
public class Purchase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "purchase_number", nullable = false)
    private String purchaseNumber;

    @Column(name = "determ_supplier_method")
    private String determSupplierMethod;

    @Column(name = "electronic_platform")
    private String electronicPlatform;

    @Column(name = "max_cost")
    private Double maxCost;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPurchaseNumber() {
        return purchaseNumber;
    }

    public Purchase purchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
        return this;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public String getDetermSupplierMethod() {
        return determSupplierMethod;
    }

    public Purchase determSupplierMethod(String determSupplierMethod) {
        this.determSupplierMethod = determSupplierMethod;
        return this;
    }

    public void setDetermSupplierMethod(String determSupplierMethod) {
        this.determSupplierMethod = determSupplierMethod;
    }

    public String getElectronicPlatform() {
        return electronicPlatform;
    }

    public Purchase electronicPlatform(String electronicPlatform) {
        this.electronicPlatform = electronicPlatform;
        return this;
    }

    public void setElectronicPlatform(String electronicPlatform) {
        this.electronicPlatform = electronicPlatform;
    }

    public Double getMaxCost() {
        return maxCost;
    }

    public Purchase maxCost(Double maxCost) {
        this.maxCost = maxCost;
        return this;
    }

    public void setMaxCost(Double maxCost) {
        this.maxCost = maxCost;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Purchase purchase = (Purchase) o;
        if (purchase.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), purchase.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Purchase{" +
            "id=" + getId() +
            ", purchaseNumber='" + getPurchaseNumber() + "'" +
            ", determSupplierMethod='" + getDetermSupplierMethod() + "'" +
            ", electronicPlatform='" + getElectronicPlatform() + "'" +
            ", maxCost='" + getMaxCost() + "'" +
            "}";
    }
}
