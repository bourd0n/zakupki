package com.alex.zakupki.service.mapper.xml

import java.util.concurrent.TimeUnit
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general.{Purchase => XmlPurchase}
import com.alex.zakupki.domain.Purchase
import com.alex.zakupki.repository.PurchaseRepository
import com.google.common.cache.{Cache, CacheBuilder}
import org.springframework.stereotype.Component

@Component
class PurchaseXmlToDataMapper @Inject()(val purchaseRepository: PurchaseRepository)
    extends XmlToDataMapper[XmlPurchase, Purchase] {

    private val cache: Cache[String, Purchase] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, Purchase]]

    override def map(from: XmlPurchase): Option[Purchase] = Option(from) map {
        xmlPurchase => {
            cache.get(xmlPurchase.purchaseNumber, () => {
                purchaseRepository
                    .findOneByPurchaseNumber(xmlPurchase.purchaseNumber)
                    .orElseGet(() => {
                        val purchase = new Purchase()
                            .purchaseNumber(xmlPurchase.purchaseNumber)
                            .determSupplierMethod(xmlPurchase.determSupplierMethod)
                            .electronicPlatform(xmlPurchase.electronicPlatform)
                            .maxCost(xmlPurchase.maxCost)

                        purchaseRepository.save(purchase)
                    })
            })
        }
    }
}
