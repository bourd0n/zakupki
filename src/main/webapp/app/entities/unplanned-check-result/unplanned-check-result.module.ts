import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    UnplannedCheckResultService,
    UnplannedCheckResultPopupService,
    UnplannedCheckResultComponent,
    UnplannedCheckResultDetailComponent,
    UnplannedCheckResultDialogComponent,
    UnplannedCheckResultPopupComponent,
    UnplannedCheckResultDeletePopupComponent,
    UnplannedCheckResultDeleteDialogComponent,
    unplannedCheckResultRoute,
    unplannedCheckResultPopupRoute,
} from './';

const ENTITY_STATES = [
    ...unplannedCheckResultRoute,
    ...unplannedCheckResultPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UnplannedCheckResultComponent,
        UnplannedCheckResultDetailComponent,
        UnplannedCheckResultDialogComponent,
        UnplannedCheckResultDeleteDialogComponent,
        UnplannedCheckResultPopupComponent,
        UnplannedCheckResultDeletePopupComponent,
    ],
    entryComponents: [
        UnplannedCheckResultComponent,
        UnplannedCheckResultDialogComponent,
        UnplannedCheckResultPopupComponent,
        UnplannedCheckResultDeleteDialogComponent,
        UnplannedCheckResultDeletePopupComponent,
    ],
    providers: [
        UnplannedCheckResultService,
        UnplannedCheckResultPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiUnplannedCheckResultModule {}
