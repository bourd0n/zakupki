package com.alex.zakupki.complaint.api.model.versions.v6_2

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import com.google.common.base.MoreObjects

//@Entity
//@Table(name = "purchases")
@XmlAccessorType(XmlAccessType.FIELD)
case class Purchase(@xmlPath("oos:purchaseNumber/text()")
                    purchaseNumber: String,
                    determSupplierMethod: String,
                    electronicPlatform: String,
                    maxCost: Double) extends GeneralizedEntity[general.Purchase] {
    private def this() = this(null, null, null, Double.NaN)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("purchaseNumber", purchaseNumber).add("determSupplierMethod", determSupplierMethod).toString
    }

    override def mapToGeneral(): general.Purchase = {
        general.Purchase(purchaseNumber, determSupplierMethod, electronicPlatform, maxCost)
    }
}
