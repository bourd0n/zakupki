package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence.{Entity, Table}
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

@Entity
@Table(name = "complaints_cancel")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class ComplaintCancel(@id @xmlPath("complaintCancel[1]/oos:complaintNumber/text()")
                           complaintNumber: String,
                           @xmlPath("complaintCancel[1]/oos:regDate/text()")
                           @xmlJavaTypeAdapter(classOf[DateAdapter])
                           regDate: Date,
                           @manyToOne
                           @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                           @joinColumn(name = "registration_ko_id")
                           @xmlPath("complaintCancel[1]/oos:registrationKO")
                           registrationKO: RegistrationKO,
                           @column(length = 4000)
                           @xmlPath("complaintCancel[1]/oos:text/text()")
                           text: String) {
  private def this() = this(null, null, null, null)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
  }
}