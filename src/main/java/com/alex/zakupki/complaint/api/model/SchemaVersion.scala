package com.alex.zakupki.complaint.api.model

trait SchemaVersion {

    val map: Map[Class[_], Class[_ <: GeneralizedEntity[_]]]

    def mapGeneralClassToVersionClass[T](traitClass: Class[T]): Class[_ <: GeneralizedEntity[T]] = {
        map.get(traitClass).orNull.asInstanceOf[Class[GeneralizedEntity[T]]]
    }
}
