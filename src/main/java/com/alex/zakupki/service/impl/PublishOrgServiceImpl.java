package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.PublishOrgService;
import com.alex.zakupki.domain.PublishOrg;
import com.alex.zakupki.repository.PublishOrgRepository;
import com.alex.zakupki.repository.search.PublishOrgSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PublishOrg.
 */
@Service
@Transactional
public class PublishOrgServiceImpl implements PublishOrgService{

    private final Logger log = LoggerFactory.getLogger(PublishOrgServiceImpl.class);

    private final PublishOrgRepository publishOrgRepository;

    private final PublishOrgSearchRepository publishOrgSearchRepository;

    public PublishOrgServiceImpl(PublishOrgRepository publishOrgRepository, PublishOrgSearchRepository publishOrgSearchRepository) {
        this.publishOrgRepository = publishOrgRepository;
        this.publishOrgSearchRepository = publishOrgSearchRepository;
    }

    /**
     * Save a publishOrg.
     *
     * @param publishOrg the entity to save
     * @return the persisted entity
     */
    @Override
    public PublishOrg save(PublishOrg publishOrg) {
        log.debug("Request to save PublishOrg : {}", publishOrg);
        PublishOrg result = publishOrgRepository.save(publishOrg);
        publishOrgSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the publishOrgs.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PublishOrg> findAll(Pageable pageable) {
        log.debug("Request to get all PublishOrgs");
        return publishOrgRepository.findAll(pageable);
    }

    /**
     *  Get one publishOrg by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PublishOrg findOne(Long id) {
        log.debug("Request to get PublishOrg : {}", id);
        return publishOrgRepository.findOne(id);
    }

    /**
     *  Delete the  publishOrg by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PublishOrg : {}", id);
        publishOrgRepository.delete(id);
        publishOrgSearchRepository.delete(id);
    }

    /**
     * Search for the publishOrg corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PublishOrg> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PublishOrgs for query {}", query);
        Page<PublishOrg> result = publishOrgSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
