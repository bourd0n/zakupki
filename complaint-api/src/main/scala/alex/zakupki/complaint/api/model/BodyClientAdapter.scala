package alex.zakupki.complaint.api.model

import javax.xml.bind.annotation.adapters.XmlAdapter

import com.jcabi.xml.XMLDocument
import org.w3c.dom.Element

sealed class BodyClientAdapter extends XmlAdapter[Body, Client] {

  def unmarshal(v: Body): Client = {
    val element: Element = v.getBodyVal.asInstanceOf[Element]
    val clientType: String = element.getLocalName
    val middleNode = clientType match {
      case "commission94" | "commission44" =>
        "[local-name()='organization']//*"
      case _ => ""
    }
    val xmlDoc = new XMLDocument(element)
    val xpathExprRegNum = s"//*[local-name()='$clientType']//*$middleNode[local-name()='regNum']/text()"
    val xpathExprFullName = s"//*[local-name()='$clientType']//*$middleNode[local-name()='fullName']/text()"
    val regNum = evalAndSetResult(xmlDoc, xpathExprRegNum, clientType)
    val fullName = evalAndSetResult(xmlDoc, xpathExprFullName, clientType)
    Client(regNum, fullName, clientType)
  }

  def evalAndSetResult(xmlDoc: XMLDocument, xpathExpr: String, clientType: String): String = {
    xmlDoc.xpath(xpathExpr).toArray.headOption match {
      case Some(value) => value.asInstanceOf[String]
      case None => throw new IllegalArgumentException(s"Problem with client type '$clientType'")
    }
  }

  def marshal(v: Client): Body = {
    null
  }
}