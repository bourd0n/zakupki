package com.alex.zakupki.repository;

import com.alex.zakupki.domain.ControlCheckResult;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ControlCheckResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ControlCheckResultRepository extends JpaRepository<ControlCheckResult, Long> {

}
