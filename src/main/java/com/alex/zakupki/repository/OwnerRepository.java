package com.alex.zakupki.repository;

import com.alex.zakupki.domain.Owner;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Owner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {

    Optional<Owner> findOneByRegNum(String regNum);
}
