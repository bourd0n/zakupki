package alex.zakupki.complaint.logic

import _root_.akka.actor.ActorSystem
import alex.zakupki.complaint.logic.akka.StartDownload
import alex.zakupki.complaint.logic.akka.spring.SpringExtension
import alex.zakupki.complaint.logic.ftp.FTPTypes
import com.typesafe.scalalogging.LazyLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
class RefreshDataImpl @Autowired()(implicit val ctx: ApplicationContext, val actorSystem: ActorSystem) extends RefreshData with LazyLogging {

  val prop = SpringExtension(actorSystem).props("processFilesActor")

  val processFilesActor = actorSystem.actorOf(prop, "processFilesActor")

  private var _isCurrentlyRefresh = false

  def isCurrentlyRefresh = _isCurrentlyRefresh

  def refresh() = {
    logger.debug("Start refreshing")
    _isCurrentlyRefresh = true
    try {
      //      FTPLoader.download(FTPTypes.ComplaintFTPType, 1)
      //      xmlProcessor.processFromFiles()
      processFilesActor ! StartDownload(FTPTypes.ComplaintFTPType, 1)
    } catch {
      case e: Throwable => logger.error("Exception during refresh data", e)
    }
    logger.debug("Finish refreshing")
  }

}
