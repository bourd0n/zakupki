import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ComplaintCancel } from './complaint-cancel.model';
import { ComplaintCancelService } from './complaint-cancel.service';

@Injectable()
export class ComplaintCancelPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private complaintCancelService: ComplaintCancelService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.complaintCancelService.find(id).subscribe((complaintCancel) => {
                    if (complaintCancel.regDate) {
                        complaintCancel.regDate = {
                            year: complaintCancel.regDate.getFullYear(),
                            month: complaintCancel.regDate.getMonth() + 1,
                            day: complaintCancel.regDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.complaintCancelModalRef(component, complaintCancel);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.complaintCancelModalRef(component, new ComplaintCancel());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    complaintCancelModalRef(component: Component, complaintCancel: ComplaintCancel): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.complaintCancel = complaintCancel;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
