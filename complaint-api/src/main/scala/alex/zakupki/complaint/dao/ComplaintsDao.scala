package alex.zakupki.complaint.dao

import scala.concurrent.Future

trait ComplaintsDao {
  def countAllComplaints() : Future[Int]
}
