package com.alex.zakupki.repository;

import com.alex.zakupki.domain.ReturnedComplaint;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ReturnedComplaint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReturnedComplaintRepository extends JpaRepository<ReturnedComplaint, Long> {

}
