package alex.zakupki.complaint.config

import java.util.Properties
import javax.sql.DataSource

import akka.actor.ActorSystem
import alex.zakupki.complaint.logic.akka.spring.SpringExt
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.{PropertySource, Bean, Configuration}
import org.springframework.core.env.Environment
import org.springframework.jdbc.datasource.DriverManagerDataSource
import org.springframework.orm.hibernate5.{HibernateTransactionManager, LocalSessionFactoryBean}

@Configuration
@PropertySource(Array("classpath:/application.properties"))
class ComplaintsConfiguration {

  @Autowired
  val environment: Environment = null

  @Autowired
  implicit var ctx: ApplicationContext = _

  @Bean
  def sessionFactory(): LocalSessionFactoryBean = {
    val sessionFactory = new LocalSessionFactoryBean()
    sessionFactory.setDataSource(dataSource())
    sessionFactory.setPackagesToScan("alex.zakupki.complaint.api.model")
    sessionFactory.setHibernateProperties(hibernateProperties())
    sessionFactory
  }

  @Bean
  def dataSource(): DataSource = {
    val dataSource = new DriverManagerDataSource()
    dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"))
    dataSource.setUrl(environment.getRequiredProperty("jdbc.url"))
    dataSource.setUsername(environment.getRequiredProperty("jdbc.username"))
    dataSource.setPassword(environment.getRequiredProperty("jdbc.password"))
    dataSource
  }

  def hibernateProperties(): Properties = {
    val properties = new Properties()
    properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"))
    properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"))
    properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"))
    properties
  }

  @Bean
  @Autowired
  def transactionManager(s: SessionFactory): HibernateTransactionManager = {
    val txManager = new HibernateTransactionManager()
    txManager.setSessionFactory(s)
    txManager
  }

  /**
    * Actor system singleton for this application.
    */
  @Bean
  def actorSystem() = {
    val system = ActorSystem("AkkaComplaints")
    // initialize the application context in the Akka Spring Extension
    SpringExt(system)
    system
  }
}
