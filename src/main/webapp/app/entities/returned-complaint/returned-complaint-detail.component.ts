import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ReturnedComplaint } from './returned-complaint.model';
import { ReturnedComplaintService } from './returned-complaint.service';

@Component({
    selector: 'jhi-returned-complaint-detail',
    templateUrl: './returned-complaint-detail.component.html'
})
export class ReturnedComplaintDetailComponent implements OnInit, OnDestroy {

    returnedComplaint: ReturnedComplaint;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private returnedComplaintService: ReturnedComplaintService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReturnedComplaints();
    }

    load(id) {
        this.returnedComplaintService.find(id).subscribe((returnedComplaint) => {
            this.returnedComplaint = returnedComplaint;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReturnedComplaints() {
        this.eventSubscriber = this.eventManager.subscribe(
            'returnedComplaintListModification',
            (response) => this.load(this.returnedComplaint.id)
        );
    }
}
