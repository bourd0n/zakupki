package com.alex.zakupki.domain

import java.time.LocalDate
import java.util.Objects
import javax.persistence.{Column, Entity, Id, Table}

import org.hibernate.annotations.{Cache, CacheConcurrencyStrategy}

import scala.beans.BeanProperty

@Entity
@Table(name = "sync_info")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
class SyncInfo extends Serializable {
    @Id
    @BeanProperty
    var name: String = _

    @Column(name = "sync_date")
    @BeanProperty
    var syncDate: LocalDate = _

    override def equals(o: scala.Any): Boolean = {
        if (this == o) return true
        if (o == null || (getClass ne o.getClass)) return false
        val other: SyncInfo = o.asInstanceOf[SyncInfo]
        name.eq(other.name)
    }

    override def hashCode(): Int = Objects.hashCode(name)

    override def toString = s"SyncInfo{name='$name', syncDate='$syncDate'}"
}
