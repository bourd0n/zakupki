package com.alex.zakupki.web.rest

import java.text.SimpleDateFormat
import java.util
import javax.inject.Inject

import com.alex.zakupki.service.AnalyticsService
import com.alex.zakupki.service.dto.{ClientStats, ComplaintsYearStats}
import com.alex.zakupki.web.rest.util.PaginationUtil
import com.codahale.metrics.annotation.Timed
import io.github.jhipster.domain.util.JSR310DateConverters.DateToLocalDateConverter
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Pageable
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.{GetMapping, RequestMapping, RequestParam, RestController}


@RestController
@RequestMapping(Array("/api"))
class AnalyticsResource @Inject()(val analyticsService: AnalyticsService) {

    private val log = LoggerFactory.getLogger(classOf[AnalyticsResource])

    @GetMapping(Array("/client-complaints-stats"))
    @Timed
    def getClientsComplaintsStats(@RequestParam(required = false) from: String,
                                  @RequestParam(required = false) to: String,
                                  pageable: Pageable): ResponseEntity[java.util.List[ClientStats]] = {
        log.debug(s"REST request to getClientsToComplaintsCount from $from to $to")
        val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        val fromDate = if (from == null) null else DateToLocalDateConverter.INSTANCE.convert(dateFormat.parse(from))
        val toDate = if (to == null) null else DateToLocalDateConverter.INSTANCE.convert(dateFormat.parse(to))
        val page = analyticsService.getClientsComplaintsStats(fromDate, toDate, pageable)
        val headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/clients-with-complaints")
        new ResponseEntity[java.util.List[ClientStats]](page.getContent, headers, HttpStatus.OK)

    }

    @GetMapping(Array("/complaints-stats"))
    @Timed
    def getComplaintsStats(@RequestParam(required = false) clientId: String,
                           pageable: Pageable): ResponseEntity[java.util.List[ComplaintsYearStats]] = {
        log.debug(s"REST request to getComplaintsStats. ClientId {}", clientId)
        val list: java.util.List[ComplaintsYearStats] = analyticsService.getComplaintsStats(clientId, pageable.getSort)
        new ResponseEntity[java.util.List[ComplaintsYearStats]](list, HttpStatus.OK)
    }
}
