/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ReturnedComplaintDetailComponent } from '../../../../../../main/webapp/app/entities/returned-complaint/returned-complaint-detail.component';
import { ReturnedComplaintService } from '../../../../../../main/webapp/app/entities/returned-complaint/returned-complaint.service';
import { ReturnedComplaint } from '../../../../../../main/webapp/app/entities/returned-complaint/returned-complaint.model';

describe('Component Tests', () => {

    describe('ReturnedComplaint Management Detail Component', () => {
        let comp: ReturnedComplaintDetailComponent;
        let fixture: ComponentFixture<ReturnedComplaintDetailComponent>;
        let service: ReturnedComplaintService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [ReturnedComplaintDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ReturnedComplaintService,
                    JhiEventManager
                ]
            }).overrideTemplate(ReturnedComplaintDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ReturnedComplaintDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ReturnedComplaintService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ReturnedComplaint(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.returnedComplaint).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
