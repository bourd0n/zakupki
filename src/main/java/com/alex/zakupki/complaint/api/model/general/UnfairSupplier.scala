package com.alex.zakupki.complaint.api.model.general

import java.util.Date

import com.google.common.base.MoreObjects

case class UnfairSupplier(registryNum: String,
                          publishDate: Date,
                          state: String,
                          publishOrg: PublishOrg,
                          reason: String,
                          fullName: String,
                          inn: String,
                          purchaseNumber: String) {

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("registryNum", registryNum).add("state", state).add("reason", reason).toString
  }
}
