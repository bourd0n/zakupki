package com.alex.zakupki.service;

import com.alex.zakupki.domain.ControlCheckResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ControlCheckResult.
 */
public interface ControlCheckResultService {

    /**
     * Save a controlCheckResult.
     *
     * @param controlCheckResult the entity to save
     * @return the persisted entity
     */
    ControlCheckResult save(ControlCheckResult controlCheckResult);

    /**
     *  Get all the controlCheckResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ControlCheckResult> findAll(Pageable pageable);

    /**
     *  Get the "id" controlCheckResult.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ControlCheckResult findOne(Long id);

    /**
     *  Delete the "id" controlCheckResult.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the controlCheckResult corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ControlCheckResult> search(String query, Pageable pageable);
}
