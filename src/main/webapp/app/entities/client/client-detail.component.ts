import { Component,  OnDestroy , OnInit} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs/Rx";
import {JhiEventManager, JhiLanguageService} from "ng-jhipster";
import {Response} from "@angular/http";
import { Client } from "./client.model";
import { ClientService } from "./client.service";
import {ComplaintsStats} from "../../analytics/complaints-stats/complaints-stats.model";
import {ComplaintStatsService} from "../../analytics/complaints-stats/complaints-stats.service";

@Component({
    selector: 'jhi-client-detail',
    templateUrl: './client-detail.component.html'
})
export class ClientDetailComponent implements OnInit, OnDestroy {

    client: Client;
    complaintsStats: ComplaintsStats[];
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jhiLanguageService: JhiLanguageService,
        private clientService: ClientService,
        private route: ActivatedRoute,
        private complaintStatsService: ComplaintStatsService
    ) {
        this.complaintsStats = [];
        // this.jhiLanguageService.setLocations(['client', 'complaints-stats']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInClients();
    }

    load(id) {
        this.clientService.find(id).subscribe((client) => {
            this.client = client;
        });
        this.complaintStatsService.query({
            client: id
        }).subscribe(
            (res: Response) => this.onSuccess(res.json())
        )
    }

    private onSuccess(data) {
        for (let i = 0; i < data.length; i++) {
            if (data[i].year) {
                this.complaintsStats.push(data[i]);
            }
        }
    }

    trackYear(index: number, item: ComplaintsStats) {
        return item.year;
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInClients() {
        this.eventSubscriber = this.eventManager.subscribe(
            'clientListModification',
            (response) => this.load(this.client.id)
        );
    }
}
