SELECT
    o.id,
    o.full_name,
    res."VIOLATIONS",
    res."NO_VIOLATIONS",
    res.unplanned_count,
    res.planned_count
FROM owner o, (SELECT
                   res.ko_id             ko_id_1,
                   unplanned_check.ko_id ko_id_2,
                   planned_check.ko_id   ko_id_3,
                   res."VIOLATIONS",
                   res."NO_VIOLATIONS",
                   unplanned_check.count unplanned_count,
                   planned_check.count   planned_count
               FROM (SELECT *
                     FROM crosstab('SELECT subq.ko_id, subq.checkResult, subq.complaints_count FROM ( SELECT
                              ucr.owner_id ko_id,
                              COALESCE (ucr.result, '''') checkResult,
                              count(*) complaints_count
                              FROM unplanned_check_result ucr
                              WHERE date_trunc(''year'', ucr.created_date) = to_date(''2014'', ''yyyy'')
                              GROUP BY ucr.owner_id, checkResult) subq ORDER BY 1, 2',
                                   $$VALUES ('VIOLATIONS'::text), ('NO_VIOLATIONS'::text)$$)
                         AS ct ("ko_id" INT, "VIOLATIONS" INT, "NO_VIOLATIONS" INT)) res
                   FULL OUTER JOIN (SELECT
                                        unc.owner_id          ko_id,
                                        coalesce(count(*), 0) count
                                    FROM
                                        unplanned_check_result unc
                                    WHERE date_trunc('year', unc.created_date) = to_date('2014', 'yyyy')
                                    GROUP BY unc.owner_id) unplanned_check ON (res.ko_id = unplanned_check.ko_id)
                   FULL OUTER JOIN (SELECT
                                        pcr.owner_id          ko_id,
                                        coalesce(count(*), 0) count
                                    FROM
                                        planned_check_result pcr
                                    WHERE date_trunc('year', pcr.created_date) = to_date('2014', 'yyyy')
                                    GROUP BY pcr.owner_id) planned_check ON (res.ko_id = planned_check.ko_id)) AS res
WHERE o.id = coalesce(res.ko_id_1, res.ko_id_2, res.ko_id_3)
