package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.RegistrationKoService;
import com.alex.zakupki.domain.RegistrationKo;
import com.alex.zakupki.repository.RegistrationKoRepository;
import com.alex.zakupki.repository.search.RegistrationKoSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RegistrationKo.
 */
@Service
@Transactional
public class RegistrationKoServiceImpl implements RegistrationKoService{

    private final Logger log = LoggerFactory.getLogger(RegistrationKoServiceImpl.class);

    private final RegistrationKoRepository registrationKoRepository;

    private final RegistrationKoSearchRepository registrationKoSearchRepository;

    public RegistrationKoServiceImpl(RegistrationKoRepository registrationKoRepository, RegistrationKoSearchRepository registrationKoSearchRepository) {
        this.registrationKoRepository = registrationKoRepository;
        this.registrationKoSearchRepository = registrationKoSearchRepository;
    }

    /**
     * Save a registrationKo.
     *
     * @param registrationKo the entity to save
     * @return the persisted entity
     */
    @Override
    public RegistrationKo save(RegistrationKo registrationKo) {
        log.debug("Request to save RegistrationKo : {}", registrationKo);
        RegistrationKo result = registrationKoRepository.save(registrationKo);
        registrationKoSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the registrationKos.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RegistrationKo> findAll(Pageable pageable) {
        log.debug("Request to get all RegistrationKos");
        return registrationKoRepository.findAll(pageable);
    }

    /**
     *  Get one registrationKo by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RegistrationKo findOne(Long id) {
        log.debug("Request to get RegistrationKo : {}", id);
        return registrationKoRepository.findOne(id);
    }

    /**
     *  Delete the  registrationKo by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RegistrationKo : {}", id);
        registrationKoRepository.delete(id);
        registrationKoSearchRepository.delete(id);
    }

    /**
     * Search for the registrationKo corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RegistrationKo> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RegistrationKos for query {}", query);
        Page<RegistrationKo> result = registrationKoSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
