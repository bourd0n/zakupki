import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PublishOrgComponent } from './publish-org.component';
import { PublishOrgDetailComponent } from './publish-org-detail.component';
import { PublishOrgPopupComponent } from './publish-org-dialog.component';
import { PublishOrgDeletePopupComponent } from './publish-org-delete-dialog.component';

export const publishOrgRoute: Routes = [
    {
        path: 'publish-org',
        component: PublishOrgComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.publishOrg.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'publish-org/:id',
        component: PublishOrgDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.publishOrg.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const publishOrgPopupRoute: Routes = [
    {
        path: 'publish-org-new',
        component: PublishOrgPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.publishOrg.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'publish-org/:id/edit',
        component: PublishOrgPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.publishOrg.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'publish-org/:id/delete',
        component: PublishOrgDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.publishOrg.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
