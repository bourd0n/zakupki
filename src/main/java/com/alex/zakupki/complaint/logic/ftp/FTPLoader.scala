package com.alex.zakupki.complaint.logic.ftp

import java.io._
import java.time.LocalDate
import java.util.{Calendar, GregorianCalendar}

import com.alex.zakupki.complaint.logic.config.ServerConfig
import com.alex.zakupki.complaint.temp.Consts._
import org.apache.commons.net.ftp.{FTP, FTPClient, FTPFile, FTPReply}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object FTPLoader {


    protected lazy val logger: Logger = LoggerFactory.getLogger(getClass.getName)

    val makeFiles = new File(ServerConfig.server_path + "/zakupki_tmp/unpacked/").mkdirs()

    //    def download(ftpType: FTPTypes.FTPType, limit: Int): Boolean =
    //        downloadFilesFrom(ftpType.ftpPath, limit)(_.getName.startsWith(ftpType.fileNameRegx))

    private def downloadFilesFrom(path: String, limit: Int)(filterFunc: FTPFile => Boolean) = {
        val ftp: FTPClient = new FTPClient
        try {
            var reply: Int = 0
            ftp.connect(FTP_URL_ZAKUPKI)
            ftp.login("free", "free")
            ftp.enterLocalPassiveMode()
            reply = ftp.getReplyCode
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect()
                throw new RuntimeException("FTP server refused connection.")
            }
            ftp.setFileType(FTP.BINARY_FILE_TYPE)
            val files = ftp.listFiles(path).take(limit).filter(filterFunc)
            for (file: FTPFile <- files) {
                val name = file.getName
                val link: String = s"$path$name"
                val downloadFile: File = new File(ServerConfig.server_path + "/zakupki_tmp/" + name)
                //val absPath = downloadFile.getAbsolutePath
                //println(s"Current path: $absPath")
                val outputStream: OutputStream = new BufferedOutputStream(new FileOutputStream(downloadFile))
                ftp.retrieveFile(link, outputStream)
                println("File retrieved")
                outputStream.flush()
                outputStream.close()
            }
            ftp.logout
        } finally {
            if (ftp.isConnected) ftp.disconnect()
        }
    }

    def downloadInmemory(ftpType: FTPTypes.FTPType, limit: Int = 0): Array[Array[Byte]] =
        downloadInmemoryFilesFrom(ftpType.ftpPath, limit)(_.getName.startsWith(ftpType.fileNameRegx))

    private def downloadInmemoryFilesFrom(path: String, limit: Int)(filterFunc: FTPFile => Boolean) = {
        val ftp: FTPClient = new FTPClient
        try {
            var reply: Int = 0
            ftp.connect(FTP_URL_ZAKUPKI)
            ftp.login("free", "free")
            ftp.enterLocalPassiveMode()
            reply = ftp.getReplyCode
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect()
                throw new RuntimeException("FTP server refused connection.")
            }
            ftp.setFileType(FTP.BINARY_FILE_TYPE)
            val listFiles =
                if (limit > 0)
                    ftp.listFiles(path).take(limit)
                else
                    ftp.listFiles(path)
            listFiles.filter(filterFunc).map(file => {
                val name = file.getName
                val link: String = s"$path$name"
                val outputStream: ByteArrayOutputStream = new ByteArrayOutputStream()
                ftp.retrieveFile(link, outputStream)
                logger.debug(s"File $link retrieved")
                outputStream.flush()
                outputStream.toByteArray
            })
        } finally {
            ftp.logout
            if (ftp.isConnected) ftp.disconnect()
        }
    }

    def downloadByLastSyncDate(ftpType: FTPTypes.FTPType, lastSyncDate: LocalDate): Tuple2[Array[Array[Byte]], Int] =
        download(ftpType.ftpPath)(ftpFile => {
            ftpFile.getName.startsWith(ftpType.fileNameRegx) && {
                lastSyncDate == null || {
                    val ftpFileModifyTime: Calendar = ftpFile.getTimestamp
                    val c = new GregorianCalendar()
                    c.setTimeInMillis(ftpFileModifyTime.getTimeInMillis)
                    lastSyncDate.isBefore(LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH)))
                }
            }
        })

    private def download(path: String)(filterFunc: FTPFile => Boolean): Tuple2[Array[Array[Byte]], Int] = {
        val ftp: FTPClient = new FTPClient
        try {
            var reply: Int = 0
            ftp.connect(FTP_URL_ZAKUPKI)
            ftp.login("free", "free")
            ftp.enterLocalPassiveMode()
            reply = ftp.getReplyCode
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect()
                throw new RuntimeException("FTP server refused connection.")
            }
            ftp.setFileType(FTP.BINARY_FILE_TYPE)
            val listFiles = ftp.listFiles(path)
            //todo: make constant
            var inmemoryLimit = 10
            var inmemoryFiles = ArrayBuffer.newBuilder[Array[Byte]]
            var countOfDownloadedToFileSystem = 0
            //todo: beautify
            listFiles.filter(filterFunc).foreach(file => {
                if (inmemoryLimit > 0) {
                    val name = file.getName
                    val link: String = s"$path$name"
                    val outputStream: ByteArrayOutputStream = new ByteArrayOutputStream()
                    ftp.retrieveFile(link, outputStream)
                    logger.debug(s"File $link retrieved inmemory")
                    outputStream.flush()
                    val bytes: Array[Byte] = outputStream.toByteArray
                    inmemoryFiles += bytes
                    inmemoryLimit -= 1
                } else {
                    val name = file.getName
                    val link: String = s"$path$name"
                    //todo: remove server path
                    val downloadFile: File = new File(ServerConfig.server_path + "/zakupki_tmp/" + name)
                    val outputStream: OutputStream = new BufferedOutputStream(new FileOutputStream(downloadFile))
                    ftp.retrieveFile(link, outputStream)
                    println(s"File $link retrieved in FS")
                    outputStream.flush()
                    outputStream.close()
                    countOfDownloadedToFileSystem += 1
                }
            })
            (inmemoryFiles.result.toArray, countOfDownloadedToFileSystem)
        } finally {
            ftp.logout
            if (ftp.isConnected) ftp.disconnect()
        }
    }
}
