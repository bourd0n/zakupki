package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence._
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

@Entity
@Table(name = "unfair_suppliers")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class UnfairSupplier(@id @xmlPath("unfairSupplier/oos:registryNum/text()")
                          registryNum: String,
                          @xmlPath("unfairSupplier/oos:publishDate/text()")
                          publishDate: Date,
                          @xmlPath("unfairSupplier/oos:state/text()")
                          state: String,
                          @manyToOne
                          @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                          @joinColumn(name = "publish_org_id")
                          @xmlPath("unfairSupplier/oos:publishOrg")
                          publishOrg: PublishOrg,
                          @xmlPath("unfairSupplier/oos:reason/text()")
                          reason: String,
                          @xmlPath("unfairSupplier/oos:unfairSupplier/oos:fullName/text()")
                          fullName: String,
                          @xmlPath("unfairSupplier/oos:unfairSupplier/oos:inn/text()")
                          inn: String,
                          @xmlPath("unfairSupplier/oos:purchase/oos:purchaseNumber/text()")
                          purchaseNumber: String) {
  private def this() = this(null, null, null, null, null, null, null, null)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("registryNum", registryNum).add("state", state).add("reason", reason).toString
  }
}