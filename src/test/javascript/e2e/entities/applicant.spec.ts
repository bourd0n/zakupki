import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Applicant e2e test', () => {

    let navBarPage: NavBarPage;
    let applicantDialogPage: ApplicantDialogPage;
    let applicantComponentsPage: ApplicantComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Applicants', () => {
        navBarPage.goToEntity('applicant');
        applicantComponentsPage = new ApplicantComponentsPage();
        expect(applicantComponentsPage.getTitle()).toMatch(/zakupkiApp.applicant.home.title/);

    });

    it('should load create Applicant dialog', () => {
        applicantComponentsPage.clickOnCreateButton();
        applicantDialogPage = new ApplicantDialogPage();
        expect(applicantDialogPage.getModalTitle()).toMatch(/zakupkiApp.applicant.home.createOrEditLabel/);
        applicantDialogPage.close();
    });

    it('should create and save Applicants', () => {
        applicantComponentsPage.clickOnCreateButton();
        applicantDialogPage.setOrganizationNameInput('organizationName');
        expect(applicantDialogPage.getOrganizationNameInput()).toMatch('organizationName');
        applicantDialogPage.setApplicantTypeInput('applicantType');
        expect(applicantDialogPage.getApplicantTypeInput()).toMatch('applicantType');
        applicantDialogPage.save();
        expect(applicantDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ApplicantComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-applicant div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ApplicantDialogPage {
    modalTitle = element(by.css('h4#myApplicantLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    organizationNameInput = element(by.css('input#field_organizationName'));
    applicantTypeInput = element(by.css('input#field_applicantType'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setOrganizationNameInput = function (organizationName) {
        this.organizationNameInput.sendKeys(organizationName);
    }

    getOrganizationNameInput = function () {
        return this.organizationNameInput.getAttribute('value');
    }

    setApplicantTypeInput = function (applicantType) {
        this.applicantTypeInput.sendKeys(applicantType);
    }

    getApplicantTypeInput = function () {
        return this.applicantTypeInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
