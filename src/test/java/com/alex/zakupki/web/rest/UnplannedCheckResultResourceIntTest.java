package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.UnplannedCheckResult;
import com.alex.zakupki.repository.UnplannedCheckResultRepository;
import com.alex.zakupki.service.UnplannedCheckResultService;
import com.alex.zakupki.repository.search.UnplannedCheckResultSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UnplannedCheckResultResource REST controller.
 *
 * @see UnplannedCheckResultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class UnplannedCheckResultResourceIntTest {

    private static final String DEFAULT_CHECK_RESULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CHECK_RESULT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_UNPLANNED_CHECK_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_UNPLANNED_CHECK_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_RESULT = "AAAAAAAAAA";
    private static final String UPDATED_RESULT = "BBBBBBBBBB";

    @Autowired
    private UnplannedCheckResultRepository unplannedCheckResultRepository;

    @Autowired
    private UnplannedCheckResultService unplannedCheckResultService;

    @Autowired
    private UnplannedCheckResultSearchRepository unplannedCheckResultSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUnplannedCheckResultMockMvc;

    private UnplannedCheckResult unplannedCheckResult;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UnplannedCheckResultResource unplannedCheckResultResource = new UnplannedCheckResultResource(unplannedCheckResultService);
        this.restUnplannedCheckResultMockMvc = MockMvcBuilders.standaloneSetup(unplannedCheckResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnplannedCheckResult createEntity(EntityManager em) {
        UnplannedCheckResult unplannedCheckResult = new UnplannedCheckResult()
            .checkResultNumber(DEFAULT_CHECK_RESULT_NUMBER)
            .unplannedCheckNumber(DEFAULT_UNPLANNED_CHECK_NUMBER)
            .createdDate(DEFAULT_CREATED_DATE)
            .result(DEFAULT_RESULT);
        return unplannedCheckResult;
    }

    @Before
    public void initTest() {
        unplannedCheckResultSearchRepository.deleteAll();
        unplannedCheckResult = createEntity(em);
    }

    @Test
    @Transactional
    public void createUnplannedCheckResult() throws Exception {
        int databaseSizeBeforeCreate = unplannedCheckResultRepository.findAll().size();

        // Create the UnplannedCheckResult
        restUnplannedCheckResultMockMvc.perform(post("/api/unplanned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unplannedCheckResult)))
            .andExpect(status().isCreated());

        // Validate the UnplannedCheckResult in the database
        List<UnplannedCheckResult> unplannedCheckResultList = unplannedCheckResultRepository.findAll();
        assertThat(unplannedCheckResultList).hasSize(databaseSizeBeforeCreate + 1);
        UnplannedCheckResult testUnplannedCheckResult = unplannedCheckResultList.get(unplannedCheckResultList.size() - 1);
        assertThat(testUnplannedCheckResult.getCheckResultNumber()).isEqualTo(DEFAULT_CHECK_RESULT_NUMBER);
        assertThat(testUnplannedCheckResult.getUnplannedCheckNumber()).isEqualTo(DEFAULT_UNPLANNED_CHECK_NUMBER);
        assertThat(testUnplannedCheckResult.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUnplannedCheckResult.getResult()).isEqualTo(DEFAULT_RESULT);

        // Validate the UnplannedCheckResult in Elasticsearch
        UnplannedCheckResult unplannedCheckResultEs = unplannedCheckResultSearchRepository.findOne(testUnplannedCheckResult.getId());
        assertThat(unplannedCheckResultEs).isEqualToComparingFieldByField(testUnplannedCheckResult);
    }

    @Test
    @Transactional
    public void createUnplannedCheckResultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = unplannedCheckResultRepository.findAll().size();

        // Create the UnplannedCheckResult with an existing ID
        unplannedCheckResult.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnplannedCheckResultMockMvc.perform(post("/api/unplanned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unplannedCheckResult)))
            .andExpect(status().isBadRequest());

        // Validate the UnplannedCheckResult in the database
        List<UnplannedCheckResult> unplannedCheckResultList = unplannedCheckResultRepository.findAll();
        assertThat(unplannedCheckResultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCheckResultNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = unplannedCheckResultRepository.findAll().size();
        // set the field null
        unplannedCheckResult.setCheckResultNumber(null);

        // Create the UnplannedCheckResult, which fails.

        restUnplannedCheckResultMockMvc.perform(post("/api/unplanned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unplannedCheckResult)))
            .andExpect(status().isBadRequest());

        List<UnplannedCheckResult> unplannedCheckResultList = unplannedCheckResultRepository.findAll();
        assertThat(unplannedCheckResultList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUnplannedCheckResults() throws Exception {
        // Initialize the database
        unplannedCheckResultRepository.saveAndFlush(unplannedCheckResult);

        // Get all the unplannedCheckResultList
        restUnplannedCheckResultMockMvc.perform(get("/api/unplanned-check-results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unplannedCheckResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].unplannedCheckNumber").value(hasItem(DEFAULT_UNPLANNED_CHECK_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void getUnplannedCheckResult() throws Exception {
        // Initialize the database
        unplannedCheckResultRepository.saveAndFlush(unplannedCheckResult);

        // Get the unplannedCheckResult
        restUnplannedCheckResultMockMvc.perform(get("/api/unplanned-check-results/{id}", unplannedCheckResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(unplannedCheckResult.getId().intValue()))
            .andExpect(jsonPath("$.checkResultNumber").value(DEFAULT_CHECK_RESULT_NUMBER.toString()))
            .andExpect(jsonPath("$.unplannedCheckNumber").value(DEFAULT_UNPLANNED_CHECK_NUMBER.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUnplannedCheckResult() throws Exception {
        // Get the unplannedCheckResult
        restUnplannedCheckResultMockMvc.perform(get("/api/unplanned-check-results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUnplannedCheckResult() throws Exception {
        // Initialize the database
        unplannedCheckResultService.save(unplannedCheckResult);

        int databaseSizeBeforeUpdate = unplannedCheckResultRepository.findAll().size();

        // Update the unplannedCheckResult
        UnplannedCheckResult updatedUnplannedCheckResult = unplannedCheckResultRepository.findOne(unplannedCheckResult.getId());
        updatedUnplannedCheckResult
            .checkResultNumber(UPDATED_CHECK_RESULT_NUMBER)
            .unplannedCheckNumber(UPDATED_UNPLANNED_CHECK_NUMBER)
            .createdDate(UPDATED_CREATED_DATE)
            .result(UPDATED_RESULT);

        restUnplannedCheckResultMockMvc.perform(put("/api/unplanned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUnplannedCheckResult)))
            .andExpect(status().isOk());

        // Validate the UnplannedCheckResult in the database
        List<UnplannedCheckResult> unplannedCheckResultList = unplannedCheckResultRepository.findAll();
        assertThat(unplannedCheckResultList).hasSize(databaseSizeBeforeUpdate);
        UnplannedCheckResult testUnplannedCheckResult = unplannedCheckResultList.get(unplannedCheckResultList.size() - 1);
        assertThat(testUnplannedCheckResult.getCheckResultNumber()).isEqualTo(UPDATED_CHECK_RESULT_NUMBER);
        assertThat(testUnplannedCheckResult.getUnplannedCheckNumber()).isEqualTo(UPDATED_UNPLANNED_CHECK_NUMBER);
        assertThat(testUnplannedCheckResult.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUnplannedCheckResult.getResult()).isEqualTo(UPDATED_RESULT);

        // Validate the UnplannedCheckResult in Elasticsearch
        UnplannedCheckResult unplannedCheckResultEs = unplannedCheckResultSearchRepository.findOne(testUnplannedCheckResult.getId());
        assertThat(unplannedCheckResultEs).isEqualToComparingFieldByField(testUnplannedCheckResult);
    }

    @Test
    @Transactional
    public void updateNonExistingUnplannedCheckResult() throws Exception {
        int databaseSizeBeforeUpdate = unplannedCheckResultRepository.findAll().size();

        // Create the UnplannedCheckResult

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUnplannedCheckResultMockMvc.perform(put("/api/unplanned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unplannedCheckResult)))
            .andExpect(status().isCreated());

        // Validate the UnplannedCheckResult in the database
        List<UnplannedCheckResult> unplannedCheckResultList = unplannedCheckResultRepository.findAll();
        assertThat(unplannedCheckResultList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUnplannedCheckResult() throws Exception {
        // Initialize the database
        unplannedCheckResultService.save(unplannedCheckResult);

        int databaseSizeBeforeDelete = unplannedCheckResultRepository.findAll().size();

        // Get the unplannedCheckResult
        restUnplannedCheckResultMockMvc.perform(delete("/api/unplanned-check-results/{id}", unplannedCheckResult.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean unplannedCheckResultExistsInEs = unplannedCheckResultSearchRepository.exists(unplannedCheckResult.getId());
        assertThat(unplannedCheckResultExistsInEs).isFalse();

        // Validate the database is empty
        List<UnplannedCheckResult> unplannedCheckResultList = unplannedCheckResultRepository.findAll();
        assertThat(unplannedCheckResultList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUnplannedCheckResult() throws Exception {
        // Initialize the database
        unplannedCheckResultService.save(unplannedCheckResult);

        // Search the unplannedCheckResult
        restUnplannedCheckResultMockMvc.perform(get("/api/_search/unplanned-check-results?query=id:" + unplannedCheckResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unplannedCheckResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].unplannedCheckNumber").value(hasItem(DEFAULT_UNPLANNED_CHECK_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UnplannedCheckResult.class);
        UnplannedCheckResult unplannedCheckResult1 = new UnplannedCheckResult();
        unplannedCheckResult1.setId(1L);
        UnplannedCheckResult unplannedCheckResult2 = new UnplannedCheckResult();
        unplannedCheckResult2.setId(unplannedCheckResult1.getId());
        assertThat(unplannedCheckResult1).isEqualTo(unplannedCheckResult2);
        unplannedCheckResult2.setId(2L);
        assertThat(unplannedCheckResult1).isNotEqualTo(unplannedCheckResult2);
        unplannedCheckResult1.setId(null);
        assertThat(unplannedCheckResult1).isNotEqualTo(unplannedCheckResult2);
    }
}
