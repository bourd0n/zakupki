package com.alex.zakupki.complaint.api.model.versions.v6_2

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}


//@Entity
//@Table(name = "registration_ko")
@XmlAccessorType(XmlAccessType.FIELD)
case class RegistrationKO(@xmlPath("oos:regNum/text()")
                          regNum: String,
                          @xmlPath("oos:fullName/text()")
                          fullName: String) extends GeneralizedEntity[general.RegistrationKO] {
    private def this() = this(null, null)

    override def mapToGeneral(): general.RegistrationKO = {
        general.RegistrationKO(regNum, fullName.trim)
    }
}
