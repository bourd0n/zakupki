import {Component, OnDestroy, OnInit} from '@angular/core';
import {Response} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {
    JhiAlertService,
    JhiEventManager,
    JhiLanguageService,
    JhiParseLinks
} from 'ng-jhipster';

import {ComplaintsStats} from './complaints-stats.model';
import {ComplaintStatsService} from './complaints-stats.service';
import {ITEMS_PER_PAGE, Principal} from '../../shared';

@Component({
    selector: 'jhi-complaints-stats',
    templateUrl: './complaints-stats.component.html'
})
export class ComplaintsStatsComponent implements OnInit, OnDestroy {

    complaintsStats: ComplaintsStats[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;

    constructor(private jhiLanguageService: JhiLanguageService,
                private complaintStatsService: ComplaintStatsService,
                private alertService: JhiAlertService,
                private eventManager: JhiEventManager,
                private parseLinks: JhiParseLinks,
                private activatedRoute: ActivatedRoute,
                private principal: Principal) {
        this.complaintsStats = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'complaintsCount';
        this.reverse = false;
        // this.jhiLanguageService.setLocations(['complaints-stats']);
    }

    loadAll() {
        this.complaintStatsService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: Response) => this.onSuccess(res.json(), res.headers),
            (res: Response) => this.onError(res.json())
        );
    }

    reset() {
        this.page = 0;
        this.complaintsStats = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    clear() {
        this.complaintsStats = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'complaintsCount';
        this.reverse = false;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInClients();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackYear(index: number, item: ComplaintsStats) {
        return item.year;
    }

    registerChangeInClients() {
        this.eventSubscriber = this.eventManager.subscribe('clientListModification', (response) => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'complaintsCount') {
            result.push('complaintsCount');
        }
        return result;
    }

    private onSuccess(data, headers) {
        // this.links = this.parseLinks.parse(headers.get('link'));
        // this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            if (data[i].year) {
                this.complaintsStats.push(data[i]);
            }
        }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
