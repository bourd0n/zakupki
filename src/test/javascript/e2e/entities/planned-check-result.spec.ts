import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('PlannedCheckResult e2e test', () => {

    let navBarPage: NavBarPage;
    let plannedCheckResultDialogPage: PlannedCheckResultDialogPage;
    let plannedCheckResultComponentsPage: PlannedCheckResultComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PlannedCheckResults', () => {
        navBarPage.goToEntity('planned-check-result');
        plannedCheckResultComponentsPage = new PlannedCheckResultComponentsPage();
        expect(plannedCheckResultComponentsPage.getTitle()).toMatch(/zakupkiApp.plannedCheckResult.home.title/);

    });

    it('should load create PlannedCheckResult dialog', () => {
        plannedCheckResultComponentsPage.clickOnCreateButton();
        plannedCheckResultDialogPage = new PlannedCheckResultDialogPage();
        expect(plannedCheckResultDialogPage.getModalTitle()).toMatch(/zakupkiApp.plannedCheckResult.home.createOrEditLabel/);
        plannedCheckResultDialogPage.close();
    });

    it('should create and save PlannedCheckResults', () => {
        plannedCheckResultComponentsPage.clickOnCreateButton();
        plannedCheckResultDialogPage.setCheckResultNumberInput('checkResultNumber');
        expect(plannedCheckResultDialogPage.getCheckResultNumberInput()).toMatch('checkResultNumber');
        plannedCheckResultDialogPage.setCheckNumberInput('checkNumber');
        expect(plannedCheckResultDialogPage.getCheckNumberInput()).toMatch('checkNumber');
        plannedCheckResultDialogPage.setCreatedDateInput('2000-12-31');
        expect(plannedCheckResultDialogPage.getCreatedDateInput()).toMatch('2000-12-31');
        plannedCheckResultDialogPage.setActDateInput('2000-12-31');
        expect(plannedCheckResultDialogPage.getActDateInput()).toMatch('2000-12-31');
        plannedCheckResultDialogPage.setActPrescriptionDateInput('2000-12-31');
        expect(plannedCheckResultDialogPage.getActPrescriptionDateInput()).toMatch('2000-12-31');
        plannedCheckResultDialogPage.ownerSelectLastOption();
        plannedCheckResultDialogPage.checkSubjectSelectLastOption();
        plannedCheckResultDialogPage.save();
        expect(plannedCheckResultDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PlannedCheckResultComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-planned-check-result div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PlannedCheckResultDialogPage {
    modalTitle = element(by.css('h4#myPlannedCheckResultLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    checkResultNumberInput = element(by.css('input#field_checkResultNumber'));
    checkNumberInput = element(by.css('input#field_checkNumber'));
    createdDateInput = element(by.css('input#field_createdDate'));
    actDateInput = element(by.css('input#field_actDate'));
    actPrescriptionDateInput = element(by.css('input#field_actPrescriptionDate'));
    ownerSelect = element(by.css('select#field_owner'));
    checkSubjectSelect = element(by.css('select#field_checkSubject'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCheckResultNumberInput = function (checkResultNumber) {
        this.checkResultNumberInput.sendKeys(checkResultNumber);
    }

    getCheckResultNumberInput = function () {
        return this.checkResultNumberInput.getAttribute('value');
    }

    setCheckNumberInput = function (checkNumber) {
        this.checkNumberInput.sendKeys(checkNumber);
    }

    getCheckNumberInput = function () {
        return this.checkNumberInput.getAttribute('value');
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setActDateInput = function (actDate) {
        this.actDateInput.sendKeys(actDate);
    }

    getActDateInput = function () {
        return this.actDateInput.getAttribute('value');
    }

    setActPrescriptionDateInput = function (actPrescriptionDate) {
        this.actPrescriptionDateInput.sendKeys(actPrescriptionDate);
    }

    getActPrescriptionDateInput = function () {
        return this.actPrescriptionDateInput.getAttribute('value');
    }

    ownerSelectLastOption = function () {
        this.ownerSelect.all(by.tagName('option')).last().click();
    }

    ownerSelectOption = function (option) {
        this.ownerSelect.sendKeys(option);
    }

    getOwnerSelect = function () {
        return this.ownerSelect;
    }

    getOwnerSelectedOption = function () {
        return this.ownerSelect.element(by.css('option:checked')).getText();
    }

    checkSubjectSelectLastOption = function () {
        this.checkSubjectSelect.all(by.tagName('option')).last().click();
    }

    checkSubjectSelectOption = function (option) {
        this.checkSubjectSelect.sendKeys(option);
    }

    getCheckSubjectSelect = function () {
        return this.checkSubjectSelect;
    }

    getCheckSubjectSelectedOption = function () {
        return this.checkSubjectSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
