import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Complaint } from './complaint.model';
import { ComplaintPopupService } from './complaint-popup.service';
import { ComplaintService } from './complaint.service';
import { RegistrationKo, RegistrationKoService } from '../registration-ko';
import { Purchase, PurchaseService } from '../purchase';
import { Applicant, ApplicantService } from '../applicant';
import { Client, ClientService } from '../client';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-complaint-dialog',
    templateUrl: './complaint-dialog.component.html'
})
export class ComplaintDialogComponent implements OnInit {

    complaint: Complaint;
    isSaving: boolean;

    registrationkos: RegistrationKo[];

    purchases: Purchase[];

    applicants: Applicant[];

    clients: Client[];
    regDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private complaintService: ComplaintService,
        private registrationKoService: RegistrationKoService,
        private purchaseService: PurchaseService,
        private applicantService: ApplicantService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.registrationKoService.query()
            .subscribe((res: ResponseWrapper) => { this.registrationkos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.purchaseService.query()
            .subscribe((res: ResponseWrapper) => { this.purchases = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.applicantService.query()
            .subscribe((res: ResponseWrapper) => { this.applicants = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clientService.query()
            .subscribe((res: ResponseWrapper) => { this.clients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.complaint.id !== undefined) {
            this.subscribeToSaveResponse(
                this.complaintService.update(this.complaint));
        } else {
            this.subscribeToSaveResponse(
                this.complaintService.create(this.complaint));
        }
    }

    private subscribeToSaveResponse(result: Observable<Complaint>) {
        result.subscribe((res: Complaint) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Complaint) {
        this.eventManager.broadcast({ name: 'complaintListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRegistrationKoById(index: number, item: RegistrationKo) {
        return item.id;
    }

    trackPurchaseById(index: number, item: Purchase) {
        return item.id;
    }

    trackApplicantById(index: number, item: Applicant) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-complaint-popup',
    template: ''
})
export class ComplaintPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private complaintPopupService: ComplaintPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.complaintPopupService
                    .open(ComplaintDialogComponent as Component, params['id']);
            } else {
                this.complaintPopupService
                    .open(ComplaintDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
