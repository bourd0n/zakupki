package com.alex.zakupki.complaint.api.model.versions.v6_2

import javax.xml.bind.annotation.XmlAccessType


@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
@deprecated
case class CheckResultNumber(@xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                             checkResultNumber: String) {
  private def this() = this(null)
}
