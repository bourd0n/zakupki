import { BaseEntity } from './../../shared';

export class UnfairSupplier implements BaseEntity {
    constructor(
        public id?: number,
        public registryNum?: string,
        public publishDate?: any,
        public state?: string,
        public reason?: string,
        public fullName?: string,
        public inn?: string,
        public purchaseNumber?: string,
        public publishOrg?: BaseEntity,
    ) {
    }
}
