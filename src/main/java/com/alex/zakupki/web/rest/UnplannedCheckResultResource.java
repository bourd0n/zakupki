package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.UnplannedCheckResult;
import com.alex.zakupki.service.UnplannedCheckResultService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnplannedCheckResult.
 */
@RestController
@RequestMapping("/api")
public class UnplannedCheckResultResource {

    private final Logger log = LoggerFactory.getLogger(UnplannedCheckResultResource.class);

    private static final String ENTITY_NAME = "unplannedCheckResult";

    private final UnplannedCheckResultService unplannedCheckResultService;

    public UnplannedCheckResultResource(UnplannedCheckResultService unplannedCheckResultService) {
        this.unplannedCheckResultService = unplannedCheckResultService;
    }

    /**
     * POST  /unplanned-check-results : Create a new unplannedCheckResult.
     *
     * @param unplannedCheckResult the unplannedCheckResult to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unplannedCheckResult, or with status 400 (Bad Request) if the unplannedCheckResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unplanned-check-results")
    @Timed
    public ResponseEntity<UnplannedCheckResult> createUnplannedCheckResult(@Valid @RequestBody UnplannedCheckResult unplannedCheckResult) throws URISyntaxException {
        log.debug("REST request to save UnplannedCheckResult : {}", unplannedCheckResult);
        if (unplannedCheckResult.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unplannedCheckResult cannot already have an ID")).body(null);
        }
        UnplannedCheckResult result = unplannedCheckResultService.save(unplannedCheckResult);
        return ResponseEntity.created(new URI("/api/unplanned-check-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /unplanned-check-results : Updates an existing unplannedCheckResult.
     *
     * @param unplannedCheckResult the unplannedCheckResult to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unplannedCheckResult,
     * or with status 400 (Bad Request) if the unplannedCheckResult is not valid,
     * or with status 500 (Internal Server Error) if the unplannedCheckResult couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unplanned-check-results")
    @Timed
    public ResponseEntity<UnplannedCheckResult> updateUnplannedCheckResult(@Valid @RequestBody UnplannedCheckResult unplannedCheckResult) throws URISyntaxException {
        log.debug("REST request to update UnplannedCheckResult : {}", unplannedCheckResult);
        if (unplannedCheckResult.getId() == null) {
            return createUnplannedCheckResult(unplannedCheckResult);
        }
        UnplannedCheckResult result = unplannedCheckResultService.save(unplannedCheckResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unplannedCheckResult.getId().toString()))
            .body(result);
    }

    /**
     * GET  /unplanned-check-results : get all the unplannedCheckResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unplannedCheckResults in body
     */
    @GetMapping("/unplanned-check-results")
    @Timed
    public ResponseEntity<List<UnplannedCheckResult>> getAllUnplannedCheckResults(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnplannedCheckResults");
        Page<UnplannedCheckResult> page = unplannedCheckResultService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unplanned-check-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unplanned-check-results/:id : get the "id" unplannedCheckResult.
     *
     * @param id the id of the unplannedCheckResult to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unplannedCheckResult, or with status 404 (Not Found)
     */
    @GetMapping("/unplanned-check-results/{id}")
    @Timed
    public ResponseEntity<UnplannedCheckResult> getUnplannedCheckResult(@PathVariable Long id) {
        log.debug("REST request to get UnplannedCheckResult : {}", id);
        UnplannedCheckResult unplannedCheckResult = unplannedCheckResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unplannedCheckResult));
    }

    /**
     * DELETE  /unplanned-check-results/:id : delete the "id" unplannedCheckResult.
     *
     * @param id the id of the unplannedCheckResult to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unplanned-check-results/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnplannedCheckResult(@PathVariable Long id) {
        log.debug("REST request to delete UnplannedCheckResult : {}", id);
        unplannedCheckResultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unplanned-check-results?query=:query : search for the unplannedCheckResult corresponding
     * to the query.
     *
     * @param query the query of the unplannedCheckResult search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unplanned-check-results")
    @Timed
    public ResponseEntity<List<UnplannedCheckResult>> searchUnplannedCheckResults(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UnplannedCheckResults for query {}", query);
        Page<UnplannedCheckResult> page = unplannedCheckResultService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unplanned-check-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
