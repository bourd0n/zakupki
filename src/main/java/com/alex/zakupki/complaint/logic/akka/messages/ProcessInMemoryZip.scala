package com.alex.zakupki.complaint.logic.akka.messages

case class ProcessInMemoryZip(zipFileBytes: Array[Byte])
