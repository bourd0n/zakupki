package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence._
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

@Entity
@Table(name = "complaints")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class Complaint(@id @xmlPath("complaint/oos:commonInfo/oos:complaintNumber/text()")
                     complaintNumber: String,
                     @manyToOne
                     @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                     @joinColumn(name = "registration_ko_id")
                     @xmlPath("complaint/oos:commonInfo/oos:registrationKO")
                     registrationKO: RegistrationKO,
                     @xmlPath("complaint/oos:commonInfo/oos:regDate/text()")
                     var regDate: Date,
                     @manyToOne
                     @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                     @joinColumn(name = "purchase_id")
                     @xmlPath("complaint/oos:object/oos:purchase")
                     var purchase: Purchase,
                     @manyToOne
                     @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                     @joinColumn(name = "applicant_id")
                     @xmlPath("complaint/oos:applicant")
                     var applicant: Applicant,
                     @column(length = 4000)
                     @xmlPath("complaint/oos:text/text()")
                     var text: String,
                     @manyToOne
                     @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                     @joinColumn(name = "indicted_client_id")
                     @xmlPath("complaint/oos:indicted")
                     @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                     var indictedClient: Client) {
  private def this() = this(null, null, null, null, null, null, null)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
  }

}