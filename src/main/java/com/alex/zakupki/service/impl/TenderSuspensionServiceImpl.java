package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.TenderSuspensionService;
import com.alex.zakupki.domain.TenderSuspension;
import com.alex.zakupki.repository.TenderSuspensionRepository;
import com.alex.zakupki.repository.search.TenderSuspensionSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TenderSuspension.
 */
@Service
@Transactional
public class TenderSuspensionServiceImpl implements TenderSuspensionService{

    private final Logger log = LoggerFactory.getLogger(TenderSuspensionServiceImpl.class);

    private final TenderSuspensionRepository tenderSuspensionRepository;

    private final TenderSuspensionSearchRepository tenderSuspensionSearchRepository;

    public TenderSuspensionServiceImpl(TenderSuspensionRepository tenderSuspensionRepository, TenderSuspensionSearchRepository tenderSuspensionSearchRepository) {
        this.tenderSuspensionRepository = tenderSuspensionRepository;
        this.tenderSuspensionSearchRepository = tenderSuspensionSearchRepository;
    }

    /**
     * Save a tenderSuspension.
     *
     * @param tenderSuspension the entity to save
     * @return the persisted entity
     */
    @Override
    public TenderSuspension save(TenderSuspension tenderSuspension) {
        log.debug("Request to save TenderSuspension : {}", tenderSuspension);
        TenderSuspension result = tenderSuspensionRepository.save(tenderSuspension);
        tenderSuspensionSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the tenderSuspensions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TenderSuspension> findAll(Pageable pageable) {
        log.debug("Request to get all TenderSuspensions");
        return tenderSuspensionRepository.findAll(pageable);
    }

    /**
     *  Get one tenderSuspension by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TenderSuspension findOne(Long id) {
        log.debug("Request to get TenderSuspension : {}", id);
        return tenderSuspensionRepository.findOne(id);
    }

    /**
     *  Delete the  tenderSuspension by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TenderSuspension : {}", id);
        tenderSuspensionRepository.delete(id);
        tenderSuspensionSearchRepository.delete(id);
    }

    /**
     * Search for the tenderSuspension corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TenderSuspension> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of TenderSuspensions for query {}", query);
        Page<TenderSuspension> result = tenderSuspensionSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
