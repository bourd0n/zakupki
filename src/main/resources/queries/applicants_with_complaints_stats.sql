-- todo: fix to full outer join
SELECT
    a.id,
    a.organization_name,
    a.applicant_type,
    res."COMPLAINT_NO_VIOLATIONS",
    res."COMPLAINT_VIOLATIONS",
    res."COMPLAINT_PARTLY_VALID",
    res."COMPLAINT_NOT_MATCHED",
    all_count.count complaints_sum,
    res."EMPTY"
FROM applicant a, (SELECT *
                   FROM crosstab('SELECT
                              subq.applicant_id,
                              subq.checkResult,
                              subq.complaints_count
                              FROM (SELECT
                                    c.applicant_id           applicant_id,
                                    COALESCE(ccr.result, '''') checkResult,
                                    count(*) complaints_count
                                  FROM complaint c, check_complaint_result ccr
                                  WHERE ccr.complaint_id = c.id
                                  GROUP BY c.applicant_id, checkResult) subq
                              ORDER BY 1, 2',
                                 $$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$)
                       AS ct ("applicant_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res,
    (SELECT
         c.applicant_id        applicant_id,
         coalesce(count(*), 0) count
     FROM
         complaint c
     GROUP BY c.applicant_id) all_count
WHERE a.id = res.applicant_id
      AND a.id = all_count.applicant_id
ORDER BY complaints_sum DESC;
