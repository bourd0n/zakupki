import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ClientsComplaintsStatsModule} from './client-complaints-stats/client-complaints-stats.module';
import {ComplaintsStatsModule} from "./complaints-stats/complaints-stats.module";

@NgModule({
    imports: [
        ClientsComplaintsStatsModule,
        ComplaintsStatsModule
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AnalyticsModule {}
