package com.alex.zakupki.service.mapper.xml

trait XmlToDataMapper[F, T] {
    def map(from: F): Option[T]
}
