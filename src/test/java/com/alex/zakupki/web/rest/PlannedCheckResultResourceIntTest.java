package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.PlannedCheckResult;
import com.alex.zakupki.repository.PlannedCheckResultRepository;
import com.alex.zakupki.service.PlannedCheckResultService;
import com.alex.zakupki.repository.search.PlannedCheckResultSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlannedCheckResultResource REST controller.
 *
 * @see PlannedCheckResultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class PlannedCheckResultResourceIntTest {

    private static final String DEFAULT_CHECK_RESULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CHECK_RESULT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CHECK_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CHECK_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACT_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_ACT_PRESCRIPTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ACT_PRESCRIPTION_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private PlannedCheckResultRepository plannedCheckResultRepository;

    @Autowired
    private PlannedCheckResultService plannedCheckResultService;

    @Autowired
    private PlannedCheckResultSearchRepository plannedCheckResultSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlannedCheckResultMockMvc;

    private PlannedCheckResult plannedCheckResult;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlannedCheckResultResource plannedCheckResultResource = new PlannedCheckResultResource(plannedCheckResultService);
        this.restPlannedCheckResultMockMvc = MockMvcBuilders.standaloneSetup(plannedCheckResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlannedCheckResult createEntity(EntityManager em) {
        PlannedCheckResult plannedCheckResult = new PlannedCheckResult()
            .checkResultNumber(DEFAULT_CHECK_RESULT_NUMBER)
            .checkNumber(DEFAULT_CHECK_NUMBER)
            .createdDate(DEFAULT_CREATED_DATE)
            .actDate(DEFAULT_ACT_DATE)
            .actPrescriptionDate(DEFAULT_ACT_PRESCRIPTION_DATE);
        return plannedCheckResult;
    }

    @Before
    public void initTest() {
        plannedCheckResultSearchRepository.deleteAll();
        plannedCheckResult = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlannedCheckResult() throws Exception {
        int databaseSizeBeforeCreate = plannedCheckResultRepository.findAll().size();

        // Create the PlannedCheckResult
        restPlannedCheckResultMockMvc.perform(post("/api/planned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannedCheckResult)))
            .andExpect(status().isCreated());

        // Validate the PlannedCheckResult in the database
        List<PlannedCheckResult> plannedCheckResultList = plannedCheckResultRepository.findAll();
        assertThat(plannedCheckResultList).hasSize(databaseSizeBeforeCreate + 1);
        PlannedCheckResult testPlannedCheckResult = plannedCheckResultList.get(plannedCheckResultList.size() - 1);
        assertThat(testPlannedCheckResult.getCheckResultNumber()).isEqualTo(DEFAULT_CHECK_RESULT_NUMBER);
        assertThat(testPlannedCheckResult.getCheckNumber()).isEqualTo(DEFAULT_CHECK_NUMBER);
        assertThat(testPlannedCheckResult.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPlannedCheckResult.getActDate()).isEqualTo(DEFAULT_ACT_DATE);
        assertThat(testPlannedCheckResult.getActPrescriptionDate()).isEqualTo(DEFAULT_ACT_PRESCRIPTION_DATE);

        // Validate the PlannedCheckResult in Elasticsearch
        PlannedCheckResult plannedCheckResultEs = plannedCheckResultSearchRepository.findOne(testPlannedCheckResult.getId());
        assertThat(plannedCheckResultEs).isEqualToComparingFieldByField(testPlannedCheckResult);
    }

    @Test
    @Transactional
    public void createPlannedCheckResultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = plannedCheckResultRepository.findAll().size();

        // Create the PlannedCheckResult with an existing ID
        plannedCheckResult.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlannedCheckResultMockMvc.perform(post("/api/planned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannedCheckResult)))
            .andExpect(status().isBadRequest());

        // Validate the PlannedCheckResult in the database
        List<PlannedCheckResult> plannedCheckResultList = plannedCheckResultRepository.findAll();
        assertThat(plannedCheckResultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCheckResultNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = plannedCheckResultRepository.findAll().size();
        // set the field null
        plannedCheckResult.setCheckResultNumber(null);

        // Create the PlannedCheckResult, which fails.

        restPlannedCheckResultMockMvc.perform(post("/api/planned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannedCheckResult)))
            .andExpect(status().isBadRequest());

        List<PlannedCheckResult> plannedCheckResultList = plannedCheckResultRepository.findAll();
        assertThat(plannedCheckResultList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlannedCheckResults() throws Exception {
        // Initialize the database
        plannedCheckResultRepository.saveAndFlush(plannedCheckResult);

        // Get all the plannedCheckResultList
        restPlannedCheckResultMockMvc.perform(get("/api/planned-check-results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plannedCheckResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].checkNumber").value(hasItem(DEFAULT_CHECK_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].actDate").value(hasItem(DEFAULT_ACT_DATE.toString())))
            .andExpect(jsonPath("$.[*].actPrescriptionDate").value(hasItem(DEFAULT_ACT_PRESCRIPTION_DATE.toString())));
    }

    @Test
    @Transactional
    public void getPlannedCheckResult() throws Exception {
        // Initialize the database
        plannedCheckResultRepository.saveAndFlush(plannedCheckResult);

        // Get the plannedCheckResult
        restPlannedCheckResultMockMvc.perform(get("/api/planned-check-results/{id}", plannedCheckResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(plannedCheckResult.getId().intValue()))
            .andExpect(jsonPath("$.checkResultNumber").value(DEFAULT_CHECK_RESULT_NUMBER.toString()))
            .andExpect(jsonPath("$.checkNumber").value(DEFAULT_CHECK_NUMBER.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.actDate").value(DEFAULT_ACT_DATE.toString()))
            .andExpect(jsonPath("$.actPrescriptionDate").value(DEFAULT_ACT_PRESCRIPTION_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlannedCheckResult() throws Exception {
        // Get the plannedCheckResult
        restPlannedCheckResultMockMvc.perform(get("/api/planned-check-results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlannedCheckResult() throws Exception {
        // Initialize the database
        plannedCheckResultService.save(plannedCheckResult);

        int databaseSizeBeforeUpdate = plannedCheckResultRepository.findAll().size();

        // Update the plannedCheckResult
        PlannedCheckResult updatedPlannedCheckResult = plannedCheckResultRepository.findOne(plannedCheckResult.getId());
        updatedPlannedCheckResult
            .checkResultNumber(UPDATED_CHECK_RESULT_NUMBER)
            .checkNumber(UPDATED_CHECK_NUMBER)
            .createdDate(UPDATED_CREATED_DATE)
            .actDate(UPDATED_ACT_DATE)
            .actPrescriptionDate(UPDATED_ACT_PRESCRIPTION_DATE);

        restPlannedCheckResultMockMvc.perform(put("/api/planned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPlannedCheckResult)))
            .andExpect(status().isOk());

        // Validate the PlannedCheckResult in the database
        List<PlannedCheckResult> plannedCheckResultList = plannedCheckResultRepository.findAll();
        assertThat(plannedCheckResultList).hasSize(databaseSizeBeforeUpdate);
        PlannedCheckResult testPlannedCheckResult = plannedCheckResultList.get(plannedCheckResultList.size() - 1);
        assertThat(testPlannedCheckResult.getCheckResultNumber()).isEqualTo(UPDATED_CHECK_RESULT_NUMBER);
        assertThat(testPlannedCheckResult.getCheckNumber()).isEqualTo(UPDATED_CHECK_NUMBER);
        assertThat(testPlannedCheckResult.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPlannedCheckResult.getActDate()).isEqualTo(UPDATED_ACT_DATE);
        assertThat(testPlannedCheckResult.getActPrescriptionDate()).isEqualTo(UPDATED_ACT_PRESCRIPTION_DATE);

        // Validate the PlannedCheckResult in Elasticsearch
        PlannedCheckResult plannedCheckResultEs = plannedCheckResultSearchRepository.findOne(testPlannedCheckResult.getId());
        assertThat(plannedCheckResultEs).isEqualToComparingFieldByField(testPlannedCheckResult);
    }

    @Test
    @Transactional
    public void updateNonExistingPlannedCheckResult() throws Exception {
        int databaseSizeBeforeUpdate = plannedCheckResultRepository.findAll().size();

        // Create the PlannedCheckResult

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlannedCheckResultMockMvc.perform(put("/api/planned-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannedCheckResult)))
            .andExpect(status().isCreated());

        // Validate the PlannedCheckResult in the database
        List<PlannedCheckResult> plannedCheckResultList = plannedCheckResultRepository.findAll();
        assertThat(plannedCheckResultList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlannedCheckResult() throws Exception {
        // Initialize the database
        plannedCheckResultService.save(plannedCheckResult);

        int databaseSizeBeforeDelete = plannedCheckResultRepository.findAll().size();

        // Get the plannedCheckResult
        restPlannedCheckResultMockMvc.perform(delete("/api/planned-check-results/{id}", plannedCheckResult.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean plannedCheckResultExistsInEs = plannedCheckResultSearchRepository.exists(plannedCheckResult.getId());
        assertThat(plannedCheckResultExistsInEs).isFalse();

        // Validate the database is empty
        List<PlannedCheckResult> plannedCheckResultList = plannedCheckResultRepository.findAll();
        assertThat(plannedCheckResultList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPlannedCheckResult() throws Exception {
        // Initialize the database
        plannedCheckResultService.save(plannedCheckResult);

        // Search the plannedCheckResult
        restPlannedCheckResultMockMvc.perform(get("/api/_search/planned-check-results?query=id:" + plannedCheckResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plannedCheckResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].checkNumber").value(hasItem(DEFAULT_CHECK_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].actDate").value(hasItem(DEFAULT_ACT_DATE.toString())))
            .andExpect(jsonPath("$.[*].actPrescriptionDate").value(hasItem(DEFAULT_ACT_PRESCRIPTION_DATE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlannedCheckResult.class);
        PlannedCheckResult plannedCheckResult1 = new PlannedCheckResult();
        plannedCheckResult1.setId(1L);
        PlannedCheckResult plannedCheckResult2 = new PlannedCheckResult();
        plannedCheckResult2.setId(plannedCheckResult1.getId());
        assertThat(plannedCheckResult1).isEqualTo(plannedCheckResult2);
        plannedCheckResult2.setId(2L);
        assertThat(plannedCheckResult1).isNotEqualTo(plannedCheckResult2);
        plannedCheckResult1.setId(null);
        assertThat(plannedCheckResult1).isNotEqualTo(plannedCheckResult2);
    }
}
