package com.alex.zakupki.complaint.api.model.general

import java.util.Date

trait CheckResult {
    var checkResultNumber: String
    var owner : Owner
    val createDate : Date
}
