package com.alex.zakupki.complaint.api.model.versions.v6_4

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}

@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class UnplannedCheckResult_AsEventResult(checkResultNumber: String,
                                              @xmlPath("ns2:eventResult/base/unplannedCheck/regNumber/text()")
                                              unplannedCheckNumber: String,
                                              @xmlPath("ns2:eventResult/commonInfo/createDate/text()")
                                              createDate: Date,
                                              @xmlPath("ns2:eventResult/commonInfo/owner")
                                              owner: Owner,
                                              @xmlPath("ns2:eventResult/base/unplannedCheck/checkResult/text()")
                                              result: String,
                                              @xmlPath("ns2:eventResult/base/unplannedCheck/checkSubjects/")
                                              @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                              checkSubject: Client) extends GeneralizedEntity[general.UnplannedCheckResult] {
    private def this() = this(null, null, null, null, null, null)

    override def mapToGeneral(): general.UnplannedCheckResult = general.UnplannedCheckResult(
        checkResultNumber,
        unplannedCheckNumber,
        createDate,
        owner.mapToGeneral(),
        result,
        Option(checkSubject).map(_.mapToGeneral()).orNull
    )
}
