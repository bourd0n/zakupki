package com.alex.zakupki.service.mapper.xml

import java.util.Optional
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general
import com.alex.zakupki.complaint.api.model.general.UnplannedCheckResult
import com.alex.zakupki.domain
import com.alex.zakupki.domain.{Client, Owner}
import com.alex.zakupki.repository.UnplannedCheckResultRepository
import org.springframework.stereotype.Component

@Component
class UnplannedCheckResultXmlToDataMapper @Inject()(override val repository: UnplannedCheckResultRepository,
                                                    val ownerXmlToDataMapper: XmlToDataMapper[general.Owner, Owner],
                                                    val clientXmlToDataMapper: XmlToDataMapper[general.Client, Client])
    extends CachedXmlToDataMapper[UnplannedCheckResult, domain.UnplannedCheckResult] with DateToLocalDateConversion {

    override def extractIdentificator(from: UnplannedCheckResult): String = from.checkResultNumber

    override def findByIdentificator(id: String): Optional[domain.UnplannedCheckResult] = repository.findOneByCheckResultNumber(id)

    override def createNew(from: UnplannedCheckResult): domain.UnplannedCheckResult = {
        new domain.UnplannedCheckResult()
            .unplannedCheckNumber(from.unplannedCheckNumber)
            .checkResultNumber(from.checkResultNumber)
            .checkSubject(clientXmlToDataMapper.map(from.checkSubject).orNull)
            .createdDate(from.createDate)
            .result(from.result)
            .owner(ownerXmlToDataMapper.map(from.owner).orNull)
    }
}
