/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import {OnInit, Sanitizer} from '@angular/core';
import { DatePipe } from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs/Rx';
import {JhiDateUtils, JhiDataUtils, JhiEventManager, JhiPaginationUtil, JhiParseLinks} from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import {MockActivatedRoute, MockRouter} from '../../../helpers/mock-route.service';
import { RegistrationKoDetailComponent } from '../../../../../../main/webapp/app/entities/registration-ko/registration-ko-detail.component';
import { RegistrationKoService } from '../../../../../../main/webapp/app/entities/registration-ko/registration-ko.service';
import { RegistrationKo } from '../../../../../../main/webapp/app/entities/registration-ko/registration-ko.model';
import {ComplaintService} from '../../../../../../main/webapp/app/entities/complaint/complaint.service';
import {PaginationConfig} from '../../../../../../main/webapp/app/blocks/config/uib-pagination.config';
import {NgbPaginationConfig} from '@ng-bootstrap/ng-bootstrap';

describe('Component Tests', () => {

    describe('RegistrationKo Management Detail Component', () => {
        let comp: RegistrationKoDetailComponent;
        let fixture: ComponentFixture<RegistrationKoDetailComponent>;
        let service: RegistrationKoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [RegistrationKoDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    JhiParseLinks,
                    JhiPaginationUtil,
                    PaginationConfig,
                    NgbPaginationConfig,
                    {
                        provide: Router,
                        useClass: MockRouter
                    },
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RegistrationKoService,
                    JhiEventManager,
                    ComplaintService
                ]
            }).overrideTemplate(RegistrationKoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegistrationKoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegistrationKoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RegistrationKo(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.registrationKo).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
