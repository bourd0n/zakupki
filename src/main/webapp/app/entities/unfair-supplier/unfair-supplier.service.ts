import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UnfairSupplier } from './unfair-supplier.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnfairSupplierService {

    private resourceUrl = SERVER_API_URL + 'api/unfair-suppliers';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/unfair-suppliers';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(unfairSupplier: UnfairSupplier): Observable<UnfairSupplier> {
        const copy = this.convert(unfairSupplier);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(unfairSupplier: UnfairSupplier): Observable<UnfairSupplier> {
        const copy = this.convert(unfairSupplier);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<UnfairSupplier> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UnfairSupplier.
     */
    private convertItemFromServer(json: any): UnfairSupplier {
        const entity: UnfairSupplier = Object.assign(new UnfairSupplier(), json);
        entity.publishDate = this.dateUtils
            .convertLocalDateFromServer(json.publishDate);
        return entity;
    }

    /**
     * Convert a UnfairSupplier to a JSON which can be sent to the server.
     */
    private convert(unfairSupplier: UnfairSupplier): UnfairSupplier {
        const copy: UnfairSupplier = Object.assign({}, unfairSupplier);
        copy.publishDate = this.dateUtils
            .convertLocalDateToServer(unfairSupplier.publishDate);
        return copy;
    }
}
