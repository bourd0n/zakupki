import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RegistrationKoComponent } from './registration-ko.component';
import { RegistrationKoDetailComponent } from './registration-ko-detail.component';
import { RegistrationKoPopupComponent } from './registration-ko-dialog.component';
import { RegistrationKoDeletePopupComponent } from './registration-ko-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class RegistrationKoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const registrationKoRoute: Routes = [
  {
    path: 'registration-ko',
    component: RegistrationKoComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'zakupkiApp.registrationKo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }, {
    path: 'registration-ko/:id',
    component: RegistrationKoDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'zakupkiApp.registrationKo.home.title'
    },
    resolve: {
        'pagingParams': RegistrationKoResolvePagingParams
    }, canActivate: [UserRouteAccessService]
  }
];

export const registrationKoPopupRoute: Routes = [
    {
        path: 'registration-ko-new',
        component: RegistrationKoPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'zakupkiApp.registrationKo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'registration-ko/:id/edit',
        component: RegistrationKoPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'zakupkiApp.registrationKo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'registration-ko/:id/delete',
        component: RegistrationKoDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN'],
            pageTitle: 'zakupkiApp.registrationKo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
