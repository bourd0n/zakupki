import { BaseEntity } from './../../shared';

export class Applicant implements BaseEntity {
    constructor(
        public id?: number,
        public organizationName?: string,
        public applicantType?: string,
    ) {
    }
}
