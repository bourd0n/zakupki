package alex.zakupki.complaint.api

import javax.persistence._
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

import org.eclipse.persistence.oxm.annotations.XmlPath
import org.hibernate.annotations.Cascade

import scala.annotation.meta.field

package object model {
  type id = Id @field
  type xmlPath = XmlPath @field
  type manyToOne = ManyToOne @field
  type cascade = Cascade @field
  type joinColumn = JoinColumn @field
  type xmlJavaTypeAdapter = XmlJavaTypeAdapter @field
  type column =  Column @field
  type generatedValue = GeneratedValue @field
}
