package com.alex.zakupki.complaint.api.model.versions

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

import org.eclipse.persistence.oxm.annotations.XmlPath

import scala.annotation.meta.field

package object v6_4 {
    type xmlPath = XmlPath@field
    type xmlJavaTypeAdapter = XmlJavaTypeAdapter@field
    type XmlAccessorType = javax.xml.bind.annotation.XmlAccessorType @field
    type XmlRootElement = javax.xml.bind.annotation.XmlRootElement @field
}
