package alex.zakupki.complaint.api.model

import javax.xml.bind.annotation.adapters.XmlAdapter
import java.text.SimpleDateFormat
import java.util.Date

sealed class DateAdapter extends XmlAdapter[String, Date] {
  private val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")

  @throws(classOf[Exception])
  def marshal(v: Date): String = {
    dateFormat.format(v)
  }

  @throws(classOf[Exception])
  def unmarshal(v: String): Date = {
    dateFormat.parse(v)
  }
}