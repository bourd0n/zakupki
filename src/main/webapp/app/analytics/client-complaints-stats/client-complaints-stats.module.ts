import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ZakupkiSharedModule} from '../../shared';
import {
    ClientComplaintStatsService,
    ClientComplaintsStatsComponent,
    clientComplaintsStatsRoute,
} from './';

const ENTITY_STATES = [
    ...clientComplaintsStatsRoute
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    ],
    declarations: [
        ClientComplaintsStatsComponent
    ],
    entryComponents: [
        ClientComplaintsStatsComponent,
    ],
    providers: [
        ClientComplaintStatsService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ClientsComplaintsStatsModule {
}
