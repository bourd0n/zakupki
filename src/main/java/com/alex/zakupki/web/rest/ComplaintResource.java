package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.Complaint;
import com.alex.zakupki.service.ComplaintService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.github.jhipster.domain.util.JSR310DateConverters;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Complaint.
 */
@RestController
@RequestMapping("/api")
public class ComplaintResource {

    private final Logger log = LoggerFactory.getLogger(ComplaintResource.class);

    private static final String ENTITY_NAME = "complaint";

    private final ComplaintService complaintService;

    public ComplaintResource(ComplaintService complaintService) {
        this.complaintService = complaintService;
    }

    /**
     * POST  /complaints : Create a new complaint.
     *
     * @param complaint the complaint to create
     * @return the ResponseEntity with status 201 (Created) and with body the new complaint, or with status 400 (Bad Request) if the complaint has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/complaints")
    @Timed
    public ResponseEntity<Complaint> createComplaint(@Valid @RequestBody Complaint complaint) throws URISyntaxException {
        log.debug("REST request to save Complaint : {}", complaint);
        if (complaint.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new complaint cannot already have an ID")).body(null);
        }
        Complaint result = complaintService.save(complaint);
        return ResponseEntity.created(new URI("/api/complaints/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /complaints : Updates an existing complaint.
     *
     * @param complaint the complaint to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated complaint,
     * or with status 400 (Bad Request) if the complaint is not valid,
     * or with status 500 (Internal Server Error) if the complaint couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/complaints")
    @Timed
    public ResponseEntity<Complaint> updateComplaint(@Valid @RequestBody Complaint complaint) throws URISyntaxException {
        log.debug("REST request to update Complaint : {}", complaint);
        if (complaint.getId() == null) {
            return createComplaint(complaint);
        }
        Complaint result = complaintService.save(complaint);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, complaint.getId().toString()))
            .body(result);
    }

    /**
     * GET  /complaints : get all the complaints.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of complaints in body
     */
    @GetMapping("/complaints")
    @Timed
    public ResponseEntity<List<Complaint>> getAllComplaints(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Complaints");
        Page<Complaint> page = complaintService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/complaints");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/complaints/registrationKo")
    @Timed
    public ResponseEntity<List<Complaint>> getAllComplaintsByRegistrationKo(
        @RequestParam(required = false) String from,
        @RequestParam(required = false) String to,
        @RequestParam String regKoId,
        @ApiParam Pageable pageable) throws ParseException {
        log.debug("REST request to get a page of Complaints");
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        LocalDate fromDate = (from == null) ? null : JSR310DateConverters.DateToLocalDateConverter.INSTANCE.convert(dateFormat.parse(from));
        LocalDate toDate = (to == null) ? null
            : JSR310DateConverters.DateToLocalDateConverter.INSTANCE.convert(dateFormat.parse(to));
        Page<Complaint> page = complaintService.findAllByRegistrationKoIdAndRegDateBetween(Long.valueOf(regKoId),
            fromDate,
            toDate,
            pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/complaints/registrationKo");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


    /**
     * GET  /complaints/:id : get the "id" complaint.
     *
     * @param id the id of the complaint to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the complaint, or with status 404 (Not Found)
     */
    @GetMapping("/complaints/{id}")
    @Timed
    public ResponseEntity<Complaint> getComplaint(@PathVariable Long id) {
        log.debug("REST request to get Complaint : {}", id);
        Complaint complaint = complaintService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(complaint));
    }

    /**
     * DELETE  /complaints/:id : delete the "id" complaint.
     *
     * @param id the id of the complaint to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/complaints/{id}")
    @Timed
    public ResponseEntity<Void> deleteComplaint(@PathVariable Long id) {
        log.debug("REST request to delete Complaint : {}", id);
        complaintService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/complaints?query=:query : search for the complaint corresponding
     * to the query.
     *
     * @param query    the query of the complaint search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/complaints")
    @Timed
    public ResponseEntity<List<Complaint>> searchComplaints(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Complaints for query {}", query);
        Page<Complaint> page = complaintService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/complaints");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
