import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { UnplannedCheckResult } from './unplanned-check-result.model';
import { UnplannedCheckResultService } from './unplanned-check-result.service';

@Component({
    selector: 'jhi-unplanned-check-result-detail',
    templateUrl: './unplanned-check-result-detail.component.html'
})
export class UnplannedCheckResultDetailComponent implements OnInit, OnDestroy {

    unplannedCheckResult: UnplannedCheckResult;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private unplannedCheckResultService: UnplannedCheckResultService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUnplannedCheckResults();
    }

    load(id) {
        this.unplannedCheckResultService.find(id).subscribe((unplannedCheckResult) => {
            this.unplannedCheckResult = unplannedCheckResult;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUnplannedCheckResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'unplannedCheckResultListModification',
            (response) => this.load(this.unplannedCheckResult.id)
        );
    }
}
