package alex.zakupki.complaint.app.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.{EnableWebSecurity, WebSecurityConfigurerAdapter}

@Configuration
@EnableWebSecurity
class AppSecurityConfig extends WebSecurityConfigurerAdapter {
  @Autowired
  def configureGlobal(auth: AuthenticationManagerBuilder): Unit = {
    auth.inMemoryAuthentication().withUser("admin@admin.com").password("admin").roles("ADMIN")
  }

  override def configure(http: HttpSecurity) = {
    http.authorizeRequests().antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
    http.formLogin().usernameParameter("email").passwordParameter("password").loginPage("/login").defaultSuccessUrl("/", false)
    //todo : fix !
    http.csrf().disable()
  }
}
