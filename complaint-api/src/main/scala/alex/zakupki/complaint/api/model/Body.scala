package alex.zakupki.complaint.api.model

import javax.xml.bind.annotation.XmlAnyElement

import scala.beans.BeanProperty

sealed class Body {
  @XmlAnyElement(lax = true)
  @BeanProperty
  var bodyVal: AnyRef = null
}