import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UnfairSupplier } from './unfair-supplier.model';
import { UnfairSupplierPopupService } from './unfair-supplier-popup.service';
import { UnfairSupplierService } from './unfair-supplier.service';

@Component({
    selector: 'jhi-unfair-supplier-delete-dialog',
    templateUrl: './unfair-supplier-delete-dialog.component.html'
})
export class UnfairSupplierDeleteDialogComponent {

    unfairSupplier: UnfairSupplier;

    constructor(
        private unfairSupplierService: UnfairSupplierService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.unfairSupplierService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'unfairSupplierListModification',
                content: 'Deleted an unfairSupplier'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-unfair-supplier-delete-popup',
    template: ''
})
export class UnfairSupplierDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unfairSupplierPopupService: UnfairSupplierPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.unfairSupplierPopupService
                .open(UnfairSupplierDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
