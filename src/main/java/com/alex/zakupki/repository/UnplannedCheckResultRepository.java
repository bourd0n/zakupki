package com.alex.zakupki.repository;

import com.alex.zakupki.domain.UnplannedCheckResult;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the UnplannedCheckResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnplannedCheckResultRepository extends JpaRepository<UnplannedCheckResult, Long> {

    Optional<UnplannedCheckResult> findOneByCheckResultNumber(String checkResultNumber);

}
