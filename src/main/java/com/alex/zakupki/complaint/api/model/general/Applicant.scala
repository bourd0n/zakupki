package com.alex.zakupki.complaint.api.model.general

case class Applicant(organizationName: String,
                     applicantType: String)
