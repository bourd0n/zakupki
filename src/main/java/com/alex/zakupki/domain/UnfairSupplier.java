package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A UnfairSupplier.
 */
@Entity
@Table(name = "unfair_supplier")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "unfairsupplier")
public class UnfairSupplier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "registry_num", nullable = false)
    private String registryNum;

    @Column(name = "publish_date")
    private LocalDate publishDate;

    @Column(name = "state")
    private String state;

    @Column(name = "reason")
    private String reason;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "inn")
    private String inn;

    @Column(name = "purchase_number")
    private String purchaseNumber;

    @ManyToOne
    private PublishOrg publishOrg;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegistryNum() {
        return registryNum;
    }

    public UnfairSupplier registryNum(String registryNum) {
        this.registryNum = registryNum;
        return this;
    }

    public void setRegistryNum(String registryNum) {
        this.registryNum = registryNum;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public UnfairSupplier publishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
        return this;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public String getState() {
        return state;
    }

    public UnfairSupplier state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReason() {
        return reason;
    }

    public UnfairSupplier reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getFullName() {
        return fullName;
    }

    public UnfairSupplier fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getInn() {
        return inn;
    }

    public UnfairSupplier inn(String inn) {
        this.inn = inn;
        return this;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getPurchaseNumber() {
        return purchaseNumber;
    }

    public UnfairSupplier purchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
        return this;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public PublishOrg getPublishOrg() {
        return publishOrg;
    }

    public UnfairSupplier publishOrg(PublishOrg publishOrg) {
        this.publishOrg = publishOrg;
        return this;
    }

    public void setPublishOrg(PublishOrg publishOrg) {
        this.publishOrg = publishOrg;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UnfairSupplier unfairSupplier = (UnfairSupplier) o;
        if (unfairSupplier.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), unfairSupplier.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UnfairSupplier{" +
            "id=" + getId() +
            ", registryNum='" + getRegistryNum() + "'" +
            ", publishDate='" + getPublishDate() + "'" +
            ", state='" + getState() + "'" +
            ", reason='" + getReason() + "'" +
            ", fullName='" + getFullName() + "'" +
            ", inn='" + getInn() + "'" +
            ", purchaseNumber='" + getPurchaseNumber() + "'" +
            "}";
    }
}
