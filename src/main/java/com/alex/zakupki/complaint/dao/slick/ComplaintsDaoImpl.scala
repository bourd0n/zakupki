package com.alex.zakupki.complaint.dao.slick

import com.alex.zakupki.complaint.dao.ComplaintsDao
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.stereotype.Component
import slick.driver.PostgresDriver.api._
import slick.lifted.Rep

import scala.concurrent.Future


@Component
class ComplaintsDaoImpl extends ComplaintsDao {

    protected lazy val logger: Logger = LoggerFactory.getLogger(getClass.getName)

  import DB._

  def countAllComplaints(): Future[Int] = {
    logger.debug("countAllComplaints called")
    val length: Rep[Int] = Tables.Complaints.length
    db.run(length.result)
  }

}
