import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    UnfairSupplierService,
    UnfairSupplierPopupService,
    UnfairSupplierComponent,
    UnfairSupplierDetailComponent,
    UnfairSupplierDialogComponent,
    UnfairSupplierPopupComponent,
    UnfairSupplierDeletePopupComponent,
    UnfairSupplierDeleteDialogComponent,
    unfairSupplierRoute,
    unfairSupplierPopupRoute,
} from './';

const ENTITY_STATES = [
    ...unfairSupplierRoute,
    ...unfairSupplierPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        UnfairSupplierComponent,
        UnfairSupplierDetailComponent,
        UnfairSupplierDialogComponent,
        UnfairSupplierDeleteDialogComponent,
        UnfairSupplierPopupComponent,
        UnfairSupplierDeletePopupComponent,
    ],
    entryComponents: [
        UnfairSupplierComponent,
        UnfairSupplierDialogComponent,
        UnfairSupplierPopupComponent,
        UnfairSupplierDeleteDialogComponent,
        UnfairSupplierDeletePopupComponent,
    ],
    providers: [
        UnfairSupplierService,
        UnfairSupplierPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiUnfairSupplierModule {}
