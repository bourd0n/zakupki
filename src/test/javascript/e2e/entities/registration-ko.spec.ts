import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('RegistrationKo e2e test', () => {

    let navBarPage: NavBarPage;
    let registrationKoDialogPage: RegistrationKoDialogPage;
    let registrationKoComponentsPage: RegistrationKoComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load RegistrationKos', () => {
        navBarPage.goToEntity('registration-ko');
        registrationKoComponentsPage = new RegistrationKoComponentsPage();
        expect(registrationKoComponentsPage.getTitle()).toMatch(/zakupkiApp.registrationKo.home.title/);

    });

    it('should load create RegistrationKo dialog', () => {
        registrationKoComponentsPage.clickOnCreateButton();
        registrationKoDialogPage = new RegistrationKoDialogPage();
        expect(registrationKoDialogPage.getModalTitle()).toMatch(/zakupkiApp.registrationKo.home.createOrEditLabel/);
        registrationKoDialogPage.close();
    });

    it('should create and save RegistrationKos', () => {
        registrationKoComponentsPage.clickOnCreateButton();
        registrationKoDialogPage.setRegNumInput('regNum');
        expect(registrationKoDialogPage.getRegNumInput()).toMatch('regNum');
        registrationKoDialogPage.setFullNameInput('fullName');
        expect(registrationKoDialogPage.getFullNameInput()).toMatch('fullName');
        registrationKoDialogPage.save();
        expect(registrationKoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RegistrationKoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-registration-ko div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class RegistrationKoDialogPage {
    modalTitle = element(by.css('h4#myRegistrationKoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    regNumInput = element(by.css('input#field_regNum'));
    fullNameInput = element(by.css('input#field_fullName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setRegNumInput = function (regNum) {
        this.regNumInput.sendKeys(regNum);
    }

    getRegNumInput = function () {
        return this.regNumInput.getAttribute('value');
    }

    setFullNameInput = function (fullName) {
        this.fullNameInput.sendKeys(fullName);
    }

    getFullNameInput = function () {
        return this.fullNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
