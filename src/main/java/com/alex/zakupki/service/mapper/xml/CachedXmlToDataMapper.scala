package com.alex.zakupki.service.mapper.xml

import java.util.Optional
import java.util.concurrent.TimeUnit

import com.google.common.cache.{Cache, CacheBuilder}
import org.springframework.data.jpa.repository.JpaRepository

trait CachedXmlToDataMapper[F, T] extends XmlToDataMapper[F, T] {
    private val cache: Cache[String, T] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, T]]

    val repository: JpaRepository[T, _]

    override def map(from: F): Option[T] = Option(from) map {
        from => {
            cache.get(extractIdentificator(from), () => {
                findByIdentificator(extractIdentificator(from))
                    .orElseGet(() => {
                        val newObject = createNew(from)
                        repository.save(newObject)
                    })
            })
        }
    }

    def extractIdentificator(from: F): String

    def findByIdentificator(id: String): Optional[T]

    def createNew(from: F): T

}
