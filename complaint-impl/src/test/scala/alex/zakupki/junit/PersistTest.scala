package alex.zakupki.junit

import java.io.File
import javax.xml.bind.{JAXBContext, Unmarshaller}

import alex.zakupki.complaint.api.model._
import org.hibernate.{Session, SessionFactory}
import org.hibernate.cfg.Configuration
import org.junit.{Ignore, Test}

class PersistTest {
  @Ignore
  @Test def testApp() {
    val sessionFactory: SessionFactory = new Configuration().configure.buildSessionFactory
    val session: Session = sessionFactory.openSession
    session.beginTransaction
    val user: RegistrationKO = new RegistrationKO("2", "Привет")
    session.save(user)
    session.getTransaction.commit()
    session.close()
  }

  @Ignore
  @Test
  def testSaveComplaint() {
    val jc: JAXBContext = JAXBContext.newInstance(classOf[Complaint])
    val unmarshaller: Unmarshaller = jc.createUnmarshaller
    val xml: File = new File(getClass.getResource("/test_xmls/complaint_01521000080_10112014_2097_1501051.xml").getFile)
    val response: Complaint = unmarshaller.unmarshal(xml).asInstanceOf[Complaint]
    val sessionFactory: SessionFactory = new Configuration().configure.buildSessionFactory
    val session: Session = sessionFactory.openSession
    session.beginTransaction
    session.persist(response)
    session.getTransaction.commit()
    session.close()
  }
}