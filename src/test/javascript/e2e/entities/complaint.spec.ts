import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Complaint e2e test', () => {

    let navBarPage: NavBarPage;
    let complaintDialogPage: ComplaintDialogPage;
    let complaintComponentsPage: ComplaintComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Complaints', () => {
        navBarPage.goToEntity('complaint');
        complaintComponentsPage = new ComplaintComponentsPage();
        expect(complaintComponentsPage.getTitle()).toMatch(/zakupkiApp.complaint.home.title/);

    });

    it('should load create Complaint dialog', () => {
        complaintComponentsPage.clickOnCreateButton();
        complaintDialogPage = new ComplaintDialogPage();
        expect(complaintDialogPage.getModalTitle()).toMatch(/zakupkiApp.complaint.home.createOrEditLabel/);
        complaintDialogPage.close();
    });

    it('should create and save Complaints', () => {
        complaintComponentsPage.clickOnCreateButton();
        complaintDialogPage.setComplaintNumberInput('complaintNumber');
        expect(complaintDialogPage.getComplaintNumberInput()).toMatch('complaintNumber');
        complaintDialogPage.setRegDateInput('2000-12-31');
        expect(complaintDialogPage.getRegDateInput()).toMatch('2000-12-31');
        complaintDialogPage.setTextInput('text');
        expect(complaintDialogPage.getTextInput()).toMatch('text');
        complaintDialogPage.registrationKoSelectLastOption();
        complaintDialogPage.purchaseSelectLastOption();
        complaintDialogPage.applicantSelectLastOption();
        complaintDialogPage.indictedClientSelectLastOption();
        complaintDialogPage.save();
        expect(complaintDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ComplaintComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-complaint div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ComplaintDialogPage {
    modalTitle = element(by.css('h4#myComplaintLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    complaintNumberInput = element(by.css('input#field_complaintNumber'));
    regDateInput = element(by.css('input#field_regDate'));
    textInput = element(by.css('input#field_text'));
    registrationKoSelect = element(by.css('select#field_registrationKo'));
    purchaseSelect = element(by.css('select#field_purchase'));
    applicantSelect = element(by.css('select#field_applicant'));
    indictedClientSelect = element(by.css('select#field_indictedClient'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setComplaintNumberInput = function (complaintNumber) {
        this.complaintNumberInput.sendKeys(complaintNumber);
    }

    getComplaintNumberInput = function () {
        return this.complaintNumberInput.getAttribute('value');
    }

    setRegDateInput = function (regDate) {
        this.regDateInput.sendKeys(regDate);
    }

    getRegDateInput = function () {
        return this.regDateInput.getAttribute('value');
    }

    setTextInput = function (text) {
        this.textInput.sendKeys(text);
    }

    getTextInput = function () {
        return this.textInput.getAttribute('value');
    }

    registrationKoSelectLastOption = function () {
        this.registrationKoSelect.all(by.tagName('option')).last().click();
    }

    registrationKoSelectOption = function (option) {
        this.registrationKoSelect.sendKeys(option);
    }

    getRegistrationKoSelect = function () {
        return this.registrationKoSelect;
    }

    getRegistrationKoSelectedOption = function () {
        return this.registrationKoSelect.element(by.css('option:checked')).getText();
    }

    purchaseSelectLastOption = function () {
        this.purchaseSelect.all(by.tagName('option')).last().click();
    }

    purchaseSelectOption = function (option) {
        this.purchaseSelect.sendKeys(option);
    }

    getPurchaseSelect = function () {
        return this.purchaseSelect;
    }

    getPurchaseSelectedOption = function () {
        return this.purchaseSelect.element(by.css('option:checked')).getText();
    }

    applicantSelectLastOption = function () {
        this.applicantSelect.all(by.tagName('option')).last().click();
    }

    applicantSelectOption = function (option) {
        this.applicantSelect.sendKeys(option);
    }

    getApplicantSelect = function () {
        return this.applicantSelect;
    }

    getApplicantSelectedOption = function () {
        return this.applicantSelect.element(by.css('option:checked')).getText();
    }

    indictedClientSelectLastOption = function () {
        this.indictedClientSelect.all(by.tagName('option')).last().click();
    }

    indictedClientSelectOption = function (option) {
        this.indictedClientSelect.sendKeys(option);
    }

    getIndictedClientSelect = function () {
        return this.indictedClientSelect;
    }

    getIndictedClientSelectedOption = function () {
        return this.indictedClientSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
