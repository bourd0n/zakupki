import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PlannedCheckResult } from './planned-check-result.model';
import { PlannedCheckResultService } from './planned-check-result.service';

@Component({
    selector: 'jhi-planned-check-result-detail',
    templateUrl: './planned-check-result-detail.component.html'
})
export class PlannedCheckResultDetailComponent implements OnInit, OnDestroy {

    plannedCheckResult: PlannedCheckResult;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private plannedCheckResultService: PlannedCheckResultService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPlannedCheckResults();
    }

    load(id) {
        this.plannedCheckResultService.find(id).subscribe((plannedCheckResult) => {
            this.plannedCheckResult = plannedCheckResult;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPlannedCheckResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'plannedCheckResultListModification',
            (response) => this.load(this.plannedCheckResult.id)
        );
    }
}
