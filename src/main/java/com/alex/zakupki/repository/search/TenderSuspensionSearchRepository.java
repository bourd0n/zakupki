package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.TenderSuspension;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TenderSuspension entity.
 */
public interface TenderSuspensionSearchRepository extends ElasticsearchRepository<TenderSuspension, Long> {
}
