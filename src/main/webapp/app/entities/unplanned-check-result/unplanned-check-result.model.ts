import { BaseEntity } from './../../shared';

export class UnplannedCheckResult implements BaseEntity {
    constructor(
        public id?: number,
        public checkResultNumber?: string,
        public unplannedCheckNumber?: string,
        public createdDate?: any,
        public result?: string,
        public owner?: BaseEntity,
        public checkSubject?: BaseEntity,
    ) {
    }
}
