package alex.zakupki.complaint.api.model

import javax.persistence.{Entity, Table}
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType}

@Entity
@Table(name = "registration_ko")
@XmlAccessorType(XmlAccessType.FIELD)
case class RegistrationKO(@id @xmlPath("oos:regNum/text()")
                          regNum: String,
                          @xmlPath("oos:fullName/text()")
                          fullName: String) {
  private def this() = this(null, null)
}