import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReturnedComplaint } from './returned-complaint.model';
import { ReturnedComplaintPopupService } from './returned-complaint-popup.service';
import { ReturnedComplaintService } from './returned-complaint.service';
import { Complaint, ComplaintService } from '../complaint';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-returned-complaint-dialog',
    templateUrl: './returned-complaint-dialog.component.html'
})
export class ReturnedComplaintDialogComponent implements OnInit {

    returnedComplaint: ReturnedComplaint;
    isSaving: boolean;

    complaints: Complaint[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private returnedComplaintService: ReturnedComplaintService,
        private complaintService: ComplaintService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.complaintService.query()
            .subscribe((res: ResponseWrapper) => { this.complaints = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.returnedComplaint.id !== undefined) {
            this.subscribeToSaveResponse(
                this.returnedComplaintService.update(this.returnedComplaint));
        } else {
            this.subscribeToSaveResponse(
                this.returnedComplaintService.create(this.returnedComplaint));
        }
    }

    private subscribeToSaveResponse(result: Observable<ReturnedComplaint>) {
        result.subscribe((res: ReturnedComplaint) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ReturnedComplaint) {
        this.eventManager.broadcast({ name: 'returnedComplaintListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackComplaintById(index: number, item: Complaint) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-returned-complaint-popup',
    template: ''
})
export class ReturnedComplaintPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returnedComplaintPopupService: ReturnedComplaintPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.returnedComplaintPopupService
                    .open(ReturnedComplaintDialogComponent as Component, params['id']);
            } else {
                this.returnedComplaintPopupService
                    .open(ReturnedComplaintDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
