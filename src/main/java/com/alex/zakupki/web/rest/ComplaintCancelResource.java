package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.ComplaintCancel;
import com.alex.zakupki.service.ComplaintCancelService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ComplaintCancel.
 */
@RestController
@RequestMapping("/api")
public class ComplaintCancelResource {

    private final Logger log = LoggerFactory.getLogger(ComplaintCancelResource.class);

    private static final String ENTITY_NAME = "complaintCancel";

    private final ComplaintCancelService complaintCancelService;

    public ComplaintCancelResource(ComplaintCancelService complaintCancelService) {
        this.complaintCancelService = complaintCancelService;
    }

    /**
     * POST  /complaint-cancels : Create a new complaintCancel.
     *
     * @param complaintCancel the complaintCancel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new complaintCancel, or with status 400 (Bad Request) if the complaintCancel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/complaint-cancels")
    @Timed
    public ResponseEntity<ComplaintCancel> createComplaintCancel(@Valid @RequestBody ComplaintCancel complaintCancel) throws URISyntaxException {
        log.debug("REST request to save ComplaintCancel : {}", complaintCancel);
        if (complaintCancel.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new complaintCancel cannot already have an ID")).body(null);
        }
        ComplaintCancel result = complaintCancelService.save(complaintCancel);
        return ResponseEntity.created(new URI("/api/complaint-cancels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /complaint-cancels : Updates an existing complaintCancel.
     *
     * @param complaintCancel the complaintCancel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated complaintCancel,
     * or with status 400 (Bad Request) if the complaintCancel is not valid,
     * or with status 500 (Internal Server Error) if the complaintCancel couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/complaint-cancels")
    @Timed
    public ResponseEntity<ComplaintCancel> updateComplaintCancel(@Valid @RequestBody ComplaintCancel complaintCancel) throws URISyntaxException {
        log.debug("REST request to update ComplaintCancel : {}", complaintCancel);
        if (complaintCancel.getId() == null) {
            return createComplaintCancel(complaintCancel);
        }
        ComplaintCancel result = complaintCancelService.save(complaintCancel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, complaintCancel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /complaint-cancels : get all the complaintCancels.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of complaintCancels in body
     */
    @GetMapping("/complaint-cancels")
    @Timed
    public ResponseEntity<List<ComplaintCancel>> getAllComplaintCancels(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ComplaintCancels");
        Page<ComplaintCancel> page = complaintCancelService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/complaint-cancels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /complaint-cancels/:id : get the "id" complaintCancel.
     *
     * @param id the id of the complaintCancel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the complaintCancel, or with status 404 (Not Found)
     */
    @GetMapping("/complaint-cancels/{id}")
    @Timed
    public ResponseEntity<ComplaintCancel> getComplaintCancel(@PathVariable Long id) {
        log.debug("REST request to get ComplaintCancel : {}", id);
        ComplaintCancel complaintCancel = complaintCancelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(complaintCancel));
    }

    /**
     * DELETE  /complaint-cancels/:id : delete the "id" complaintCancel.
     *
     * @param id the id of the complaintCancel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/complaint-cancels/{id}")
    @Timed
    public ResponseEntity<Void> deleteComplaintCancel(@PathVariable Long id) {
        log.debug("REST request to delete ComplaintCancel : {}", id);
        complaintCancelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/complaint-cancels?query=:query : search for the complaintCancel corresponding
     * to the query.
     *
     * @param query the query of the complaintCancel search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/complaint-cancels")
    @Timed
    public ResponseEntity<List<ComplaintCancel>> searchComplaintCancels(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ComplaintCancels for query {}", query);
        Page<ComplaintCancel> page = complaintCancelService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/complaint-cancels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
