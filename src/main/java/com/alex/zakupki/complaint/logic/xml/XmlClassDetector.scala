package com.alex.zakupki.complaint.logic.xml

import com.alex.zakupki.complaint.api.model.versions.v5_0.SchemaVersion5_0
import com.alex.zakupki.complaint.api.model.versions.v6_2.SchemaVersion6_2
import com.alex.zakupki.complaint.api.model.versions.v6_3.SchemaVersion6_3
import com.alex.zakupki.complaint.api.model.versions.v6_4.SchemaVersion6_4
import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, SchemaVersion}
import org.slf4j.{Logger, LoggerFactory}

import scala.util.matching.Regex

object XmlClassDetector {

    val logger: Logger = LoggerFactory.getLogger(XmlClassDetector.getClass)

    //todo: add auto-fill
    val versionsMap: Map[String, SchemaVersion] = Map(
        "4.6" -> SchemaVersion5_0,
        "5.0" -> SchemaVersion5_0,
        "5.1" -> SchemaVersion5_0,
        "5.2" -> SchemaVersion5_0,
        "6.0" -> SchemaVersion5_0,
        "6.1" -> SchemaVersion5_0,
        "6.2" -> SchemaVersion6_2,
        "6.3" -> SchemaVersion6_3,
        "6.4" -> SchemaVersion6_4
    )

    def findSchemeVersion(xml: String): Option[SchemaVersion] = {
        val r: Regex = "schemeVersion=\"(.*?)\"".r
        val version = r.findFirstMatchIn(xml).map(_ group 1).get
        versionsMap.get(version).orElse({
            logger.warn("Version '{}' was not found", version)
            throw new IllegalArgumentException(s"Version $version not supported")
        })
    }

    def findClass[T](version: Option[SchemaVersion], traitClass: Class[T]): Option[Class[_ <: GeneralizedEntity[T]]] = {
        version.map(_.mapGeneralClassToVersionClass(traitClass))
    }
}
