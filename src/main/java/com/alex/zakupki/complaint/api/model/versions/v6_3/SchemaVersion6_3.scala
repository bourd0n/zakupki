package com.alex.zakupki.complaint.api.model.versions.v6_3

import com.alex.zakupki.complaint.api.model.versions.v6_4
import com.alex.zakupki.complaint.api.model.versions.v6_2
import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, SchemaVersion, general}

object SchemaVersion6_3 extends SchemaVersion{
    override val map: Map[Class[_], Class[_ <: GeneralizedEntity[_]]] = Map(
        classOf[general.Complaint] -> classOf[v6_4.Complaint],
        classOf[general.CheckComplaintResult] -> classOf[v6_2.CheckComplaintResult],
        classOf[general.UnplannedCheckResult] -> classOf[v6_4.UnplannedCheckResult],
        classOf[general.PlannedCheckResult] -> classOf[v6_4.PlannedCheckResult]
    )
}
