import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CheckComplaintResult } from './check-complaint-result.model';
import { CheckComplaintResultService } from './check-complaint-result.service';

@Component({
    selector: 'jhi-check-complaint-result-detail',
    templateUrl: './check-complaint-result-detail.component.html'
})
export class CheckComplaintResultDetailComponent implements OnInit, OnDestroy {

    checkComplaintResult: CheckComplaintResult;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private checkComplaintResultService: CheckComplaintResultService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCheckComplaintResults();
    }

    load(id) {
        this.checkComplaintResultService.find(id).subscribe((checkComplaintResult) => {
            this.checkComplaintResult = checkComplaintResult;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCheckComplaintResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'checkComplaintResultListModification',
            (response) => this.load(this.checkComplaintResult.id)
        );
    }
}
