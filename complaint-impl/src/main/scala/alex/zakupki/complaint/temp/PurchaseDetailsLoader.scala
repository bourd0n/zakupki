package alex.zakupki.complaint.temp

import java.text.NumberFormat
import java.util.Locale

import alex.zakupki.complaint.api.model.{Complaint, Purchase}
import org.apache.commons.lang3.StringUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.slf4j.{Logger, LoggerFactory}

/**
  * Not should be used in future
  */
@deprecated
object PurchaseDetailsLoader {

  private lazy val log: Logger = LoggerFactory.getLogger(PurchaseDetailsLoader.getClass)

  def loadPurchaseDetails(complaint: Complaint): Complaint = {
    val purchase: Purchase = complaint.purchase
    if (purchase != null) {
      val purchaseNew = loadPurchaseDetails(purchase)
      complaint.copy(purchase = purchaseNew)
    } else
      complaint
  }

  def loadPurchaseDetails(purchase: Purchase): Purchase = {
    val purchaseNumber: String = purchase.purchaseNumber
    val doc: Document = Jsoup.connect(Consts.PURCHASE_DETAILS_URL + purchaseNumber).timeout(6000).get
    val determSupplierMethod: String = getValueByContains(doc, "Способ определения поставщика")
    val electronicPlatform: String = getValueByContains(doc, "Наименование электронной площадки")
    val maxCost: String = getValueByContains(doc, "Начальная (максимальная) цена контракта")
    val maxCostFinal: Double = if (StringUtils.isNotEmpty(maxCost)) {
      NumberFormat.getInstance(Locale.FRANCE).parse(maxCost).doubleValue
    } else Double.NaN
    Purchase(purchaseNumber, determSupplierMethod, electronicPlatform, maxCostFinal)
  }

  private def getValueByContains(doc: Document, containText: String): String = {
    try {
      doc.getElementsContainingOwnText(containText).select("td.noticeTdFirst").parents.get(0).select("td").get(1).text
    }
    catch {
      case e: Exception =>
        log.error("Exception during getValueByContains", e)
        null
    }
  }

}
