# This is a comment
# see http://stackoverflow.com/questions/35132568/caching-jar-dependencies-for-maven-based-docker-builds
# https://github.com/carlossg/docker-maven (Packaging a local repository with the image)
FROM maven:3.3.9-jdk-8
MAINTAINER Nikita Aleksandrov <andone74@gmail.com>

RUN mkdir --parents /usr/src/app

WORKDIR /usr/src/app

ADD pom.xml /usr/src/app/
ADD src /usr/src/app/src

RUN mvn verify clean --fail-never -DskipTests

RUN mvn -B -T 4 -s /usr/share/maven/ref/settings-docker.xml dependency:copy-dependencies -Dmdep.copyPom=true dependency:resolve-plugins dependency:go-offline clean package -DskipTests
