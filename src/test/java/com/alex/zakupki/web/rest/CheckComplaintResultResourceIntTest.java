package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.CheckComplaintResult;
import com.alex.zakupki.domain.Complaint;
import com.alex.zakupki.repository.CheckComplaintResultRepository;
import com.alex.zakupki.service.CheckComplaintResultService;
import com.alex.zakupki.repository.search.CheckComplaintResultSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CheckComplaintResultResource REST controller.
 *
 * @see CheckComplaintResultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class CheckComplaintResultResourceIntTest {

    private static final String DEFAULT_CHECK_RESULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CHECK_RESULT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_RESULT = "AAAAAAAAAA";
    private static final String UPDATED_RESULT = "BBBBBBBBBB";

    @Autowired
    private CheckComplaintResultRepository checkComplaintResultRepository;

    @Autowired
    private CheckComplaintResultService checkComplaintResultService;

    @Autowired
    private CheckComplaintResultSearchRepository checkComplaintResultSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCheckComplaintResultMockMvc;

    private CheckComplaintResult checkComplaintResult;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CheckComplaintResultResource checkComplaintResultResource = new CheckComplaintResultResource(checkComplaintResultService);
        this.restCheckComplaintResultMockMvc = MockMvcBuilders.standaloneSetup(checkComplaintResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CheckComplaintResult createEntity(EntityManager em) {
        Complaint complaint = new Complaint().complaintNumber("ComplaintNumber");
        em.persist(complaint);
        CheckComplaintResult checkComplaintResult = new CheckComplaintResult()
            .checkResultNumber(DEFAULT_CHECK_RESULT_NUMBER)
            .createDate(DEFAULT_CREATE_DATE)
            .complaint(complaint)
            .result(DEFAULT_RESULT);
        return checkComplaintResult;
    }

    @Before
    public void initTest() {
        checkComplaintResultSearchRepository.deleteAll();
        checkComplaintResult = createEntity(em);
    }

    @Test
    @Transactional
    public void createCheckComplaintResult() throws Exception {
        int databaseSizeBeforeCreate = checkComplaintResultRepository.findAll().size();

        // Create the CheckComplaintResult
        restCheckComplaintResultMockMvc.perform(post("/api/check-complaint-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkComplaintResult)))
            .andExpect(status().isCreated());

        // Validate the CheckComplaintResult in the database
        List<CheckComplaintResult> checkComplaintResultList = checkComplaintResultRepository.findAll();
        assertThat(checkComplaintResultList).hasSize(databaseSizeBeforeCreate + 1);
        CheckComplaintResult testCheckComplaintResult = checkComplaintResultList.get(checkComplaintResultList.size() - 1);
        assertThat(testCheckComplaintResult.getCheckResultNumber()).isEqualTo(DEFAULT_CHECK_RESULT_NUMBER);
        assertThat(testCheckComplaintResult.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testCheckComplaintResult.getResult()).isEqualTo(DEFAULT_RESULT);

        // Validate the CheckComplaintResult in Elasticsearch
        CheckComplaintResult checkComplaintResultEs = checkComplaintResultSearchRepository.findOne(testCheckComplaintResult.getId());
        assertThat(checkComplaintResultEs).isEqualToComparingFieldByField(testCheckComplaintResult);
    }

    @Test
    @Transactional
    public void createCheckComplaintResultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = checkComplaintResultRepository.findAll().size();

        // Create the CheckComplaintResult with an existing ID
        checkComplaintResult.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCheckComplaintResultMockMvc.perform(post("/api/check-complaint-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkComplaintResult)))
            .andExpect(status().isBadRequest());

        // Validate the CheckComplaintResult in the database
        List<CheckComplaintResult> checkComplaintResultList = checkComplaintResultRepository.findAll();
        assertThat(checkComplaintResultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCheckResultNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = checkComplaintResultRepository.findAll().size();
        // set the field null
        checkComplaintResult.setCheckResultNumber(null);

        // Create the CheckComplaintResult, which fails.

        restCheckComplaintResultMockMvc.perform(post("/api/check-complaint-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkComplaintResult)))
            .andExpect(status().isBadRequest());

        List<CheckComplaintResult> checkComplaintResultList = checkComplaintResultRepository.findAll();
        assertThat(checkComplaintResultList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCheckComplaintResults() throws Exception {
        // Initialize the database
        checkComplaintResultRepository.saveAndFlush(checkComplaintResult);

        // Get all the checkComplaintResultList
        restCheckComplaintResultMockMvc.perform(get("/api/check-complaint-results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(checkComplaintResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void getCheckComplaintResult() throws Exception {
        // Initialize the database
        checkComplaintResultRepository.saveAndFlush(checkComplaintResult);

        // Get the checkComplaintResult
        restCheckComplaintResultMockMvc.perform(get("/api/check-complaint-results/{id}", checkComplaintResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(checkComplaintResult.getId().intValue()))
            .andExpect(jsonPath("$.checkResultNumber").value(DEFAULT_CHECK_RESULT_NUMBER.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCheckComplaintResult() throws Exception {
        // Get the checkComplaintResult
        restCheckComplaintResultMockMvc.perform(get("/api/check-complaint-results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCheckComplaintResult() throws Exception {
        // Initialize the database
        checkComplaintResultService.save(checkComplaintResult);

        int databaseSizeBeforeUpdate = checkComplaintResultRepository.findAll().size();

        // Update the checkComplaintResult
        CheckComplaintResult updatedCheckComplaintResult = checkComplaintResultRepository.findOne(checkComplaintResult.getId());
        updatedCheckComplaintResult
            .checkResultNumber(UPDATED_CHECK_RESULT_NUMBER)
            .createDate(UPDATED_CREATE_DATE)
            .result(UPDATED_RESULT);

        restCheckComplaintResultMockMvc.perform(put("/api/check-complaint-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCheckComplaintResult)))
            .andExpect(status().isOk());

        // Validate the CheckComplaintResult in the database
        List<CheckComplaintResult> checkComplaintResultList = checkComplaintResultRepository.findAll();
        assertThat(checkComplaintResultList).hasSize(databaseSizeBeforeUpdate);
        CheckComplaintResult testCheckComplaintResult = checkComplaintResultList.get(checkComplaintResultList.size() - 1);
        assertThat(testCheckComplaintResult.getCheckResultNumber()).isEqualTo(UPDATED_CHECK_RESULT_NUMBER);
        assertThat(testCheckComplaintResult.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testCheckComplaintResult.getResult()).isEqualTo(UPDATED_RESULT);

        // Validate the CheckComplaintResult in Elasticsearch
        CheckComplaintResult checkComplaintResultEs = checkComplaintResultSearchRepository.findOne(testCheckComplaintResult.getId());
        assertThat(checkComplaintResultEs).isEqualToComparingFieldByField(testCheckComplaintResult);
    }

    @Test
    @Transactional
    public void updateNonExistingCheckComplaintResult() throws Exception {
        int databaseSizeBeforeUpdate = checkComplaintResultRepository.findAll().size();

        // Create the CheckComplaintResult

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCheckComplaintResultMockMvc.perform(put("/api/check-complaint-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(checkComplaintResult)))
            .andExpect(status().isCreated());

        // Validate the CheckComplaintResult in the database
        List<CheckComplaintResult> checkComplaintResultList = checkComplaintResultRepository.findAll();
        assertThat(checkComplaintResultList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCheckComplaintResult() throws Exception {
        // Initialize the database
        checkComplaintResultService.save(checkComplaintResult);

        int databaseSizeBeforeDelete = checkComplaintResultRepository.findAll().size();

        // Get the checkComplaintResult
        restCheckComplaintResultMockMvc.perform(delete("/api/check-complaint-results/{id}", checkComplaintResult.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean checkComplaintResultExistsInEs = checkComplaintResultSearchRepository.exists(checkComplaintResult.getId());
        assertThat(checkComplaintResultExistsInEs).isFalse();

        // Validate the database is empty
        List<CheckComplaintResult> checkComplaintResultList = checkComplaintResultRepository.findAll();
        assertThat(checkComplaintResultList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCheckComplaintResult() throws Exception {
        // Initialize the database
        checkComplaintResultService.save(checkComplaintResult);

        // Search the checkComplaintResult
        restCheckComplaintResultMockMvc.perform(get("/api/_search/check-complaint-results?query=id:" + checkComplaintResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(checkComplaintResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CheckComplaintResult.class);
        CheckComplaintResult checkComplaintResult1 = new CheckComplaintResult();
        checkComplaintResult1.setId(1L);
        CheckComplaintResult checkComplaintResult2 = new CheckComplaintResult();
        checkComplaintResult2.setId(checkComplaintResult1.getId());
        assertThat(checkComplaintResult1).isEqualTo(checkComplaintResult2);
        checkComplaintResult2.setId(2L);
        assertThat(checkComplaintResult1).isNotEqualTo(checkComplaintResult2);
        checkComplaintResult1.setId(null);
        assertThat(checkComplaintResult1).isNotEqualTo(checkComplaintResult2);
    }
}
