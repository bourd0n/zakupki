package com.alex.zakupki.service;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.*;
import com.alex.zakupki.repository.*;
import com.alex.zakupki.repository.search.*;
import org.elasticsearch.indices.IndexAlreadyExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.lang.reflect.Method;
import java.util.List;

@Service
public class ElasticsearchIndexService {

    private final Logger log = LoggerFactory.getLogger(ElasticsearchIndexService.class);

    private final ApplicantRepository applicantRepository;

    private final ApplicantSearchRepository applicantSearchRepository;

    private final CheckComplaintResultRepository checkComplaintResultRepository;

    private final CheckComplaintResultSearchRepository checkComplaintResultSearchRepository;

    private final ClientRepository clientRepository;

    private final ClientSearchRepository clientSearchRepository;

    private final ComplaintRepository complaintRepository;

    private final ComplaintSearchRepository complaintSearchRepository;

    private final ComplaintCancelRepository complaintCancelRepository;

    private final ComplaintCancelSearchRepository complaintCancelSearchRepository;

    private final ControlCheckResultRepository controlCheckResultRepository;

    private final ControlCheckResultSearchRepository controlCheckResultSearchRepository;

    private final OwnerRepository ownerRepository;

    private final OwnerSearchRepository ownerSearchRepository;

    private final PlannedCheckResultRepository plannedCheckResultRepository;

    private final PlannedCheckResultSearchRepository plannedCheckResultSearchRepository;

    private final PublishOrgRepository publishOrgRepository;

    private final PublishOrgSearchRepository publishOrgSearchRepository;

    private final PurchaseRepository purchaseRepository;

    private final PurchaseSearchRepository purchaseSearchRepository;

    private final RegistrationKoRepository registrationKoRepository;

    private final RegistrationKoSearchRepository registrationKoSearchRepository;

    private final TenderSuspensionRepository tenderSuspensionRepository;

    private final TenderSuspensionSearchRepository tenderSuspensionSearchRepository;

    private final UnfairSupplierRepository unfairSupplierRepository;

    private final UnfairSupplierSearchRepository unfairSupplierSearchRepository;

    private final UnplannedCheckResultRepository unplannedCheckResultRepository;

    private final UnplannedCheckResultSearchRepository unplannedCheckResultSearchRepository;

    private final UserRepository userRepository;

    private final UserSearchRepository userSearchRepository;

    private final ElasticsearchTemplate elasticsearchTemplate;

    @Inject
    public ElasticsearchIndexService(ApplicantSearchRepository applicantSearchRepository, ApplicantRepository applicantRepository, CheckComplaintResultRepository checkComplaintResultRepository, UnplannedCheckResultSearchRepository unplannedCheckResultSearchRepository, ElasticsearchTemplate elasticsearchTemplate, PurchaseRepository purchaseRepository, UnfairSupplierSearchRepository unfairSupplierSearchRepository, PurchaseSearchRepository purchaseSearchRepository, OwnerSearchRepository ownerSearchRepository, PlannedCheckResultSearchRepository plannedCheckResultSearchRepository, CheckComplaintResultSearchRepository checkComplaintResultSearchRepository, UserRepository userRepository, ControlCheckResultSearchRepository controlCheckResultSearchRepository, PublishOrgSearchRepository publishOrgSearchRepository, ClientRepository clientRepository, TenderSuspensionSearchRepository tenderSuspensionSearchRepository, RegistrationKoSearchRepository registrationKoSearchRepository, UnplannedCheckResultRepository unplannedCheckResultRepository, ClientSearchRepository clientSearchRepository, PlannedCheckResultRepository plannedCheckResultRepository, PublishOrgRepository publishOrgRepository, RegistrationKoRepository registrationKoRepository, TenderSuspensionRepository tenderSuspensionRepository, ComplaintRepository complaintRepository, UserSearchRepository userSearchRepository, ComplaintSearchRepository complaintSearchRepository, ComplaintCancelRepository complaintCancelRepository, UnfairSupplierRepository unfairSupplierRepository, ComplaintCancelSearchRepository complaintCancelSearchRepository, ControlCheckResultRepository controlCheckResultRepository, OwnerRepository ownerRepository) {
        this.applicantSearchRepository = applicantSearchRepository;
        this.applicantRepository = applicantRepository;
        this.checkComplaintResultRepository = checkComplaintResultRepository;
        this.unplannedCheckResultSearchRepository = unplannedCheckResultSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.purchaseRepository = purchaseRepository;
        this.unfairSupplierSearchRepository = unfairSupplierSearchRepository;
        this.purchaseSearchRepository = purchaseSearchRepository;
        this.ownerSearchRepository = ownerSearchRepository;
        this.plannedCheckResultSearchRepository = plannedCheckResultSearchRepository;
        this.checkComplaintResultSearchRepository = checkComplaintResultSearchRepository;
        this.userRepository = userRepository;
        this.controlCheckResultSearchRepository = controlCheckResultSearchRepository;
        this.publishOrgSearchRepository = publishOrgSearchRepository;
        this.clientRepository = clientRepository;
        this.tenderSuspensionSearchRepository = tenderSuspensionSearchRepository;
        this.registrationKoSearchRepository = registrationKoSearchRepository;
        this.unplannedCheckResultRepository = unplannedCheckResultRepository;
        this.clientSearchRepository = clientSearchRepository;
        this.plannedCheckResultRepository = plannedCheckResultRepository;
        this.publishOrgRepository = publishOrgRepository;
        this.registrationKoRepository = registrationKoRepository;
        this.tenderSuspensionRepository = tenderSuspensionRepository;
        this.complaintRepository = complaintRepository;
        this.userSearchRepository = userSearchRepository;
        this.complaintSearchRepository = complaintSearchRepository;
        this.complaintCancelRepository = complaintCancelRepository;
        this.unfairSupplierRepository = unfairSupplierRepository;
        this.complaintCancelSearchRepository = complaintCancelSearchRepository;
        this.controlCheckResultRepository = controlCheckResultRepository;
        this.ownerRepository = ownerRepository;
    }


    @Async
    @Timed
    public void reindexAll() {
        reindexForClass(Applicant.class, applicantRepository, applicantSearchRepository);
        reindexForClass(CheckComplaintResult.class, checkComplaintResultRepository, checkComplaintResultSearchRepository);
        reindexForClass(Client.class, clientRepository, clientSearchRepository);
        reindexForClass(Complaint.class, complaintRepository, complaintSearchRepository);
        reindexForClass(ComplaintCancel.class, complaintCancelRepository, complaintCancelSearchRepository);
        reindexForClass(ControlCheckResult.class, controlCheckResultRepository, controlCheckResultSearchRepository);
        reindexForClass(Owner.class, ownerRepository, ownerSearchRepository);
        reindexForClass(PlannedCheckResult.class, plannedCheckResultRepository, plannedCheckResultSearchRepository);
        reindexForClass(PublishOrg.class, publishOrgRepository, publishOrgSearchRepository);
        reindexForClass(Purchase.class, purchaseRepository, purchaseSearchRepository);
        reindexForClass(RegistrationKo.class, registrationKoRepository, registrationKoSearchRepository);
        reindexForClass(TenderSuspension.class, tenderSuspensionRepository, tenderSuspensionSearchRepository);
        reindexForClass(UnfairSupplier.class, unfairSupplierRepository, unfairSupplierSearchRepository);
        reindexForClass(UnplannedCheckResult.class, unplannedCheckResultRepository, unplannedCheckResultSearchRepository);
        reindexForClass(User.class, userRepository, userSearchRepository);

        log.info("Elasticsearch: Successfully performed reindexing");
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    private <T> void reindexForClass(Class<T> entityClass, JpaRepository<T, ?> jpaRepository,
                                                          ElasticsearchRepository<T, ?> elasticsearchRepository) {
        elasticsearchTemplate.deleteIndex(entityClass);
        try {
            elasticsearchTemplate.createIndex(entityClass);
        } catch (IndexAlreadyExistsException e) {
            // Do nothing. Index was already concurrently recreated by some other service.
        }
        elasticsearchTemplate.putMapping(entityClass);
        if (jpaRepository.count() > 0) {
            try {
                Method m = jpaRepository.getClass().getMethod("findAllWithEagerRelationships");
                elasticsearchRepository.save((List<T>) m.invoke(jpaRepository));
            } catch (Exception e) {
                elasticsearchRepository.save(jpaRepository.findAll());
            }
        }
        log.info("Elasticsearch: Indexed all rows for " + entityClass.getSimpleName());
    }
}
