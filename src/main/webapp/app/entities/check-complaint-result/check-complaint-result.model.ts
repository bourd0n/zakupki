import { BaseEntity } from './../../shared';

export class CheckComplaintResult implements BaseEntity {
    constructor(
        public id?: number,
        public checkResultNumber?: string,
        public createDate?: any,
        public result?: string,
        public owner?: BaseEntity,
        public checkSubject?: BaseEntity,
        public complaint?: BaseEntity,
    ) {
    }
}
