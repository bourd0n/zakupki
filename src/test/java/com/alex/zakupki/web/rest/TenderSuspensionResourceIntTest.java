package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.TenderSuspension;
import com.alex.zakupki.repository.TenderSuspensionRepository;
import com.alex.zakupki.service.TenderSuspensionService;
import com.alex.zakupki.repository.search.TenderSuspensionSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TenderSuspensionResource REST controller.
 *
 * @see TenderSuspensionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class TenderSuspensionResourceIntTest {

    private static final String DEFAULT_COMPLAINT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_COMPLAINT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_REG_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REG_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    @Autowired
    private TenderSuspensionRepository tenderSuspensionRepository;

    @Autowired
    private TenderSuspensionService tenderSuspensionService;

    @Autowired
    private TenderSuspensionSearchRepository tenderSuspensionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTenderSuspensionMockMvc;

    private TenderSuspension tenderSuspension;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TenderSuspensionResource tenderSuspensionResource = new TenderSuspensionResource(tenderSuspensionService);
        this.restTenderSuspensionMockMvc = MockMvcBuilders.standaloneSetup(tenderSuspensionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TenderSuspension createEntity(EntityManager em) {
        TenderSuspension tenderSuspension = new TenderSuspension()
            .complaintNumber(DEFAULT_COMPLAINT_NUMBER)
            .regDate(DEFAULT_REG_DATE)
            .action(DEFAULT_ACTION);
        return tenderSuspension;
    }

    @Before
    public void initTest() {
        tenderSuspensionSearchRepository.deleteAll();
        tenderSuspension = createEntity(em);
    }

    @Test
    @Transactional
    public void createTenderSuspension() throws Exception {
        int databaseSizeBeforeCreate = tenderSuspensionRepository.findAll().size();

        // Create the TenderSuspension
        restTenderSuspensionMockMvc.perform(post("/api/tender-suspensions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tenderSuspension)))
            .andExpect(status().isCreated());

        // Validate the TenderSuspension in the database
        List<TenderSuspension> tenderSuspensionList = tenderSuspensionRepository.findAll();
        assertThat(tenderSuspensionList).hasSize(databaseSizeBeforeCreate + 1);
        TenderSuspension testTenderSuspension = tenderSuspensionList.get(tenderSuspensionList.size() - 1);
        assertThat(testTenderSuspension.getComplaintNumber()).isEqualTo(DEFAULT_COMPLAINT_NUMBER);
        assertThat(testTenderSuspension.getRegDate()).isEqualTo(DEFAULT_REG_DATE);
        assertThat(testTenderSuspension.getAction()).isEqualTo(DEFAULT_ACTION);

        // Validate the TenderSuspension in Elasticsearch
        TenderSuspension tenderSuspensionEs = tenderSuspensionSearchRepository.findOne(testTenderSuspension.getId());
        assertThat(tenderSuspensionEs).isEqualToComparingFieldByField(testTenderSuspension);
    }

    @Test
    @Transactional
    public void createTenderSuspensionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tenderSuspensionRepository.findAll().size();

        // Create the TenderSuspension with an existing ID
        tenderSuspension.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTenderSuspensionMockMvc.perform(post("/api/tender-suspensions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tenderSuspension)))
            .andExpect(status().isBadRequest());

        // Validate the TenderSuspension in the database
        List<TenderSuspension> tenderSuspensionList = tenderSuspensionRepository.findAll();
        assertThat(tenderSuspensionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTenderSuspensions() throws Exception {
        // Initialize the database
        tenderSuspensionRepository.saveAndFlush(tenderSuspension);

        // Get all the tenderSuspensionList
        restTenderSuspensionMockMvc.perform(get("/api/tender-suspensions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tenderSuspension.getId().intValue())))
            .andExpect(jsonPath("$.[*].complaintNumber").value(hasItem(DEFAULT_COMPLAINT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].regDate").value(hasItem(DEFAULT_REG_DATE.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())));
    }

    @Test
    @Transactional
    public void getTenderSuspension() throws Exception {
        // Initialize the database
        tenderSuspensionRepository.saveAndFlush(tenderSuspension);

        // Get the tenderSuspension
        restTenderSuspensionMockMvc.perform(get("/api/tender-suspensions/{id}", tenderSuspension.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tenderSuspension.getId().intValue()))
            .andExpect(jsonPath("$.complaintNumber").value(DEFAULT_COMPLAINT_NUMBER.toString()))
            .andExpect(jsonPath("$.regDate").value(DEFAULT_REG_DATE.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTenderSuspension() throws Exception {
        // Get the tenderSuspension
        restTenderSuspensionMockMvc.perform(get("/api/tender-suspensions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTenderSuspension() throws Exception {
        // Initialize the database
        tenderSuspensionService.save(tenderSuspension);

        int databaseSizeBeforeUpdate = tenderSuspensionRepository.findAll().size();

        // Update the tenderSuspension
        TenderSuspension updatedTenderSuspension = tenderSuspensionRepository.findOne(tenderSuspension.getId());
        updatedTenderSuspension
            .complaintNumber(UPDATED_COMPLAINT_NUMBER)
            .regDate(UPDATED_REG_DATE)
            .action(UPDATED_ACTION);

        restTenderSuspensionMockMvc.perform(put("/api/tender-suspensions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTenderSuspension)))
            .andExpect(status().isOk());

        // Validate the TenderSuspension in the database
        List<TenderSuspension> tenderSuspensionList = tenderSuspensionRepository.findAll();
        assertThat(tenderSuspensionList).hasSize(databaseSizeBeforeUpdate);
        TenderSuspension testTenderSuspension = tenderSuspensionList.get(tenderSuspensionList.size() - 1);
        assertThat(testTenderSuspension.getComplaintNumber()).isEqualTo(UPDATED_COMPLAINT_NUMBER);
        assertThat(testTenderSuspension.getRegDate()).isEqualTo(UPDATED_REG_DATE);
        assertThat(testTenderSuspension.getAction()).isEqualTo(UPDATED_ACTION);

        // Validate the TenderSuspension in Elasticsearch
        TenderSuspension tenderSuspensionEs = tenderSuspensionSearchRepository.findOne(testTenderSuspension.getId());
        assertThat(tenderSuspensionEs).isEqualToComparingFieldByField(testTenderSuspension);
    }

    @Test
    @Transactional
    public void updateNonExistingTenderSuspension() throws Exception {
        int databaseSizeBeforeUpdate = tenderSuspensionRepository.findAll().size();

        // Create the TenderSuspension

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTenderSuspensionMockMvc.perform(put("/api/tender-suspensions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tenderSuspension)))
            .andExpect(status().isCreated());

        // Validate the TenderSuspension in the database
        List<TenderSuspension> tenderSuspensionList = tenderSuspensionRepository.findAll();
        assertThat(tenderSuspensionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTenderSuspension() throws Exception {
        // Initialize the database
        tenderSuspensionService.save(tenderSuspension);

        int databaseSizeBeforeDelete = tenderSuspensionRepository.findAll().size();

        // Get the tenderSuspension
        restTenderSuspensionMockMvc.perform(delete("/api/tender-suspensions/{id}", tenderSuspension.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tenderSuspensionExistsInEs = tenderSuspensionSearchRepository.exists(tenderSuspension.getId());
        assertThat(tenderSuspensionExistsInEs).isFalse();

        // Validate the database is empty
        List<TenderSuspension> tenderSuspensionList = tenderSuspensionRepository.findAll();
        assertThat(tenderSuspensionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTenderSuspension() throws Exception {
        // Initialize the database
        tenderSuspensionService.save(tenderSuspension);

        // Search the tenderSuspension
        restTenderSuspensionMockMvc.perform(get("/api/_search/tender-suspensions?query=id:" + tenderSuspension.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tenderSuspension.getId().intValue())))
            .andExpect(jsonPath("$.[*].complaintNumber").value(hasItem(DEFAULT_COMPLAINT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].regDate").value(hasItem(DEFAULT_REG_DATE.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TenderSuspension.class);
        TenderSuspension tenderSuspension1 = new TenderSuspension();
        tenderSuspension1.setId(1L);
        TenderSuspension tenderSuspension2 = new TenderSuspension();
        tenderSuspension2.setId(tenderSuspension1.getId());
        assertThat(tenderSuspension1).isEqualTo(tenderSuspension2);
        tenderSuspension2.setId(2L);
        assertThat(tenderSuspension1).isNotEqualTo(tenderSuspension2);
        tenderSuspension1.setId(null);
        assertThat(tenderSuspension1).isNotEqualTo(tenderSuspension2);
    }
}
