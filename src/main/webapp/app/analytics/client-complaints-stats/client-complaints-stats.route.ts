import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';

import {ClientComplaintsStatsComponent} from './client-complaints-stats.component';

export const clientComplaintsStatsRoute: Routes = [
    {
        path: 'client-complaints-stats',
        component: ClientComplaintsStatsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'global.menu.analytics.clientComplaintsStats'
        },
        canActivate: [UserRouteAccessService]
    }
];
