export * from './returned-complaint.model';
export * from './returned-complaint-popup.service';
export * from './returned-complaint.service';
export * from './returned-complaint-dialog.component';
export * from './returned-complaint-delete-dialog.component';
export * from './returned-complaint-detail.component';
export * from './returned-complaint.component';
export * from './returned-complaint.route';
