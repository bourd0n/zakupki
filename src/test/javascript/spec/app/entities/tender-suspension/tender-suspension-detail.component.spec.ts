/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TenderSuspensionDetailComponent } from '../../../../../../main/webapp/app/entities/tender-suspension/tender-suspension-detail.component';
import { TenderSuspensionService } from '../../../../../../main/webapp/app/entities/tender-suspension/tender-suspension.service';
import { TenderSuspension } from '../../../../../../main/webapp/app/entities/tender-suspension/tender-suspension.model';

describe('Component Tests', () => {

    describe('TenderSuspension Management Detail Component', () => {
        let comp: TenderSuspensionDetailComponent;
        let fixture: ComponentFixture<TenderSuspensionDetailComponent>;
        let service: TenderSuspensionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [TenderSuspensionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TenderSuspensionService,
                    JhiEventManager
                ]
            }).overrideTemplate(TenderSuspensionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TenderSuspensionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TenderSuspensionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TenderSuspension(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.tenderSuspension).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
