package com.alex.zakupki.service

import javax.inject.Inject

import com.alex.zakupki.domain.SyncInfo
import com.alex.zakupki.repository.SyncInfoRepository
import com.alex.zakupki.web.rest.vm.SyncInfoVM
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import scala.collection.JavaConverters._

@Service
@Transactional
class SyncInfoService {

    @Inject
    private val syncInfoRepository: SyncInfoRepository = null

    def getAllSyncInfos: List[SyncInfoVM] = {
        syncInfoRepository.findAll().asScala
            .map(syncInfo => SyncInfoVM(syncInfo.name, syncInfo.syncDate))
            .toList
    }

    def updateSyncInfo(syncInfoVM: SyncInfoVM): SyncInfo = {
        val syncInfo = new SyncInfo
        syncInfo.name = syncInfoVM.name
        syncInfo.syncDate = syncInfoVM.syncDate
        syncInfoRepository.save(syncInfo)
    }
}
