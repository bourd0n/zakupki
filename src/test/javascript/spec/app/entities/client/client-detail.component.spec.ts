/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ClientDetailComponent } from '../../../../../../main/webapp/app/entities/client/client-detail.component';
import { ClientService } from '../../../../../../main/webapp/app/entities/client/client.service';
import { Client } from '../../../../../../main/webapp/app/entities/client/client.model';
import {ComplaintStatsService} from '../../../../../../main/webapp/app/analytics/complaints-stats/complaints-stats.service';
import {Response, ResponseOptions} from '@angular/http';

describe('Component Tests', () => {

    describe('Client Management Detail Component', () => {
        let comp: ClientDetailComponent;
        let fixture: ComponentFixture<ClientDetailComponent>;
        let service: ClientService;
        let complaintStatsService: ComplaintStatsService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [ClientDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ClientService,
                    ComplaintStatsService,
                    JhiEventManager
                ]
            }).overrideTemplate(ClientDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ClientDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ClientService);
            complaintStatsService = fixture.debugElement.injector.get(ComplaintStatsService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Client(10)));

            spyOn(complaintStatsService, 'query').and.returnValue(Observable.of(new Response(new ResponseOptions({
                   body: '{}'
                 }))));
            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.client).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
