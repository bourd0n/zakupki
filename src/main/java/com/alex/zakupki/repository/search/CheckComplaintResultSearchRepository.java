package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.CheckComplaintResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CheckComplaintResult entity.
 */
public interface CheckComplaintResultSearchRepository extends ElasticsearchRepository<CheckComplaintResult, Long> {
}
