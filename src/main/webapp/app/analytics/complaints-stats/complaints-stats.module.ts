import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';

import {ZakupkiSharedModule} from '../../shared';
import {
    ComplaintStatsService,
    ComplaintsStatsComponent,
    complaintsStatsRoute,
} from './';

const ENTITY_STATES = [
    ...complaintsStatsRoute
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, {useHash: true})
    ],
    declarations: [
        ComplaintsStatsComponent
    ],
    entryComponents: [
        ComplaintsStatsComponent,
    ],
    providers: [
        ComplaintStatsService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComplaintsStatsModule {
}
