package com.alex.zakupki.complaint.api.model.versions.v5_0

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.GeneralizedEntity
import com.alex.zakupki.complaint.api.model.general

@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class UnplannedCheckResult(@xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                                checkResultNumber: String,
                                @xmlPath("checkResult/oos:base/oos:unplannedCheck/oos:unplannedCheckNumber/text()")
                                unplannedCheckNumber: String,
                                @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                                createDate: Date,
                                @xmlPath("checkResult/oos:commonInfo/oos:owner")
                                owner: Owner,
                                @xmlPath("checkResult/oos:commonInfo/oos:result/text()")
                                result: String,
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                @xmlPath("checkResult/oos:base/oos:unplannedCheck/oos:checkSubjects/")
                                checkSubject: Client) extends GeneralizedEntity[general.UnplannedCheckResult]{
    private def this() = this(null, null, null, null, null, null)

    override def mapToGeneral(): general.UnplannedCheckResult = general.UnplannedCheckResult(
        checkResultNumber,
        unplannedCheckNumber,
        createDate,
        owner.mapToGeneral(),
        result,
        Option(checkSubject).map(_.mapToGeneral()).orNull
    )
}
