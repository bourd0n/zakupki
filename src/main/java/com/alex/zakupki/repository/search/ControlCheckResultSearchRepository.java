package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.ControlCheckResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ControlCheckResult entity.
 */
public interface ControlCheckResultSearchRepository extends ElasticsearchRepository<ControlCheckResult, Long> {
}
