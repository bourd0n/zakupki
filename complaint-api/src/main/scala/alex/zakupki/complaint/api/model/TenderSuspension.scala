package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence._
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects

@Entity
@Table(name = "tender_suspensions")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class TenderSuspension(@id @GeneratedValue(strategy = GenerationType.SEQUENCE)
                            var id: Integer,
                            @xmlPath("tenderSuspension/oos:complaintNumber/text()")
                            var complaintNumber: String,
                            @xmlPath("tenderSuspension/oos:regDate/text()")
                            var regDate: Date,
                            @xmlPath("tenderSuspension/oos:action/text()")
                            var action: String) {
  private def this() = this(null, null, null, null)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).add("action", action).toString
  }
}