package com.alex.zakupki.config

import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.springframework.context.annotation.{Bean, Configuration}

@Configuration
class ScalaWebHookConfiguration {

    /**
      * For serializing scala lists. Autodetect by spring-boot. See https://spring.io/blog/2014/12/02/latest-jackson-integration-improvements-in-spring
      */
    @Bean
    def jacksonScalaModule(): Module = new DefaultScalaModule

}
