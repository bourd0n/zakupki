package com.alex.zakupki.service;

import com.alex.zakupki.domain.PlannedCheckResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PlannedCheckResult.
 */
public interface PlannedCheckResultService {

    /**
     * Save a plannedCheckResult.
     *
     * @param plannedCheckResult the entity to save
     * @return the persisted entity
     */
    PlannedCheckResult save(PlannedCheckResult plannedCheckResult);

    /**
     *  Get all the plannedCheckResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PlannedCheckResult> findAll(Pageable pageable);

    /**
     *  Get the "id" plannedCheckResult.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlannedCheckResult findOne(Long id);

    /**
     *  Delete the "id" plannedCheckResult.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the plannedCheckResult corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PlannedCheckResult> search(String query, Pageable pageable);
}
