package com.alex.zakupki.repository;

import com.alex.zakupki.domain.Complaint;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Complaint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComplaintRepository extends JpaRepository<Complaint, Long> {

    Optional<Complaint> findOneByComplaintNumber(String complaintNumber);

    Page<Complaint> findAllByRegistrationKoIdAndRegDateBetween(Long registrationKoId,
                                                               LocalDate fromDate,
                                                               LocalDate toDate,
                                                               Pageable pageable);

    Page<Complaint> findAllByRegistrationKoIdAndRegDateBefore(Long registrationKoId,
                                                              LocalDate toDate,
                                                              Pageable pageable);

    Page<Complaint> findAllByRegistrationKoIdAndRegDateAfter(Long registrationKoId,
                                                              LocalDate fromDate,
                                                              Pageable pageable);
}
