package alex.zakupki.complaint.logic.akka

import akka.actor.Actor
import alex.zakupki.complaint.logic.xml.XMLProcessor
import com.typesafe.scalalogging.StrictLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component("zipProcessorActor")
@Scope("prototype")
class ZipProcessorActor @Autowired()(xmlProcessor: XMLProcessor) extends Actor with StrictLogging {
  override def receive: Receive = {
    case ProcessDownloadedZip(zipFile) =>
      logger.debug("{} start process file inmemory", self.path.name)
      xmlProcessor.processInmemory(zipFile)
      logger.debug("{} finish process file inmemory", self.path.name)
  }
}
