package com.alex.zakupki.complaint.logic.xml

import java.io._
import java.nio.file.{Files, Path, Paths}
import javax.xml.bind.{JAXBContext, Unmarshaller}

import com.alex.zakupki.complaint.api.model.general._
import com.alex.zakupki.complaint.api.model.versions.v5_0.SchemaVersion5_0
import com.alex.zakupki.complaint.api.model.versions.v6_2.SchemaVersion6_2
import com.alex.zakupki.complaint.api.model.versions.v6_3.SchemaVersion6_3
import com.alex.zakupki.complaint.api.model.versions.v6_4.{PlannedCheckResult_AsCheckResult, SchemaVersion6_4, UnplannedCheckResult_AsEventResult}
import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, SchemaVersion}
import com.jcabi.xml.XMLDocument
import org.apache.commons.io.IOUtils
import org.slf4j.{Logger, LoggerFactory}

//trait EntityProcessorImplicitConversation {
//  implicit def string2InputStream(s: String) : InputStream = new FileInputStream(s)
//}

object EntityProcessor {
    private lazy val log: Logger = LoggerFactory.getLogger(getClass)

    def unmarshalComplaint(fileName: String, inputStream: InputStream, xmlFilePath: Path): Option[Complaint] = {
        val stringVal: String = IOUtils.toString(inputStream, "UTF-8")
        val version: Option[SchemaVersion] = XmlClassDetector.findSchemeVersion(stringVal)
        val clazz: Option[Class[_ <: GeneralizedEntity[Complaint]]] = XmlClassDetector.findClass(version, classOf[Complaint])
        val unmarshalled: Option[GeneralizedEntity[Complaint]] = clazz
            .flatMap(unmarshal(new BufferedInputStream(new ByteArrayInputStream(stringVal.getBytes("UTF-8"))), _))
        val complaint = unmarshalled.map(_.mapToGeneral())
        version match {
            case Some(SchemaVersion6_4) | Some(SchemaVersion6_2) | Some(SchemaVersion6_3) =>
                val complaintNumber = fileName.split("_")(1)
                complaint.get.complaintNumber = complaintNumber
            case _ =>
        }
        //        if (complaint.get.indictedClient == null
        //            && !stringVal.contains("<oos:indicted></oos:indicted>")
        //            && !stringVal.contains("<oos:indicted><oos:customer><oos:regNum></oos:regNum></oos:customer></oos:indicted>")) {
        //            log.warn(s"WARN! $complaint has no typical construction for indicted. Xml ${xmlFilePath.getFileName.toString}")
        //            Files.copy(xmlFilePath, Paths.get("tmp/null_complaint/" + xmlFilePath.getFileName.toString))
        //        }
        complaint
    }

    def unmarshalCheckResult(fileName: String, inputStream: InputStream, xmlFilePath: Path): Option[CheckResult] = {
        val stringVal: String = IOUtils.toString(inputStream, "UTF-8")
        val version = XmlClassDetector.findSchemeVersion(stringVal)
        val bytes = stringVal.getBytes("UTF-8")
        val entityClass: Option[Class[_ <: GeneralizedEntity[_ <: CheckResult]]] = version match {
            case Some(SchemaVersion5_0) =>
                val xml = new XMLDocument(stringVal)
                val strings = xml.xpath("//*[local-name()='checkResultNumber']/text()")
                val checkResultNumber: String = strings.get(0)
                Option(checkResultNumber.charAt(0)) match {
                    //because russian is sometime goes instead of russian
                    case Some('Ж') | Some('К') | Some('K') | Some('R') | Some('C') | Some('С') =>
                        XmlClassDetector.findClass(version, classOf[CheckComplaintResult])
                    case Some('В') | Some('B') =>
                        XmlClassDetector.findClass(version, classOf[UnplannedCheckResult])
                    case Some('П') =>
                        XmlClassDetector.findClass(version, classOf[PlannedCheckResult])
                    case Some(someCharAt0) =>
                        log.warn(s"Not unplanned, planned or complaint '$someCharAt0' for xml $fileName")
                        Files.copy(xmlFilePath, Paths.get("tmp/unknow/" + xmlFilePath.getFileName.toString))
                        None
                    case _ =>
                        log.warn("Not unplanned, planned or complaint for xml {}", fileName)
                        Files.copy(xmlFilePath, Paths.get("tmp/unknow/" + xmlFilePath.getFileName.toString))
                        None
                }
            case Some(SchemaVersion6_2) =>
                val xml = new XMLDocument(stringVal)
                val unplannedCheckComplaintRegNumber = xml.xpath("//*[local-name()='unplannedCheckComplaint']//text()")
                if (!unplannedCheckComplaintRegNumber.isEmpty) {
                    XmlClassDetector.findClass(version, classOf[CheckComplaintResult])
                } else {
                    val unplannedCheckNumber = xml.xpath("//*[local-name()='unplannedCheck']//text()")
                    if (!unplannedCheckNumber.isEmpty) {
                        XmlClassDetector.findClass(version, classOf[UnplannedCheckResult])
                    } else {
                        val plannedCheckNumber = xml.xpath("//*[local-name()='plannedCheck']//text()")
                        if (!plannedCheckNumber.isEmpty) {
                            XmlClassDetector.findClass(version, classOf[PlannedCheckResult])
                        } else {
                            log.warn(s"Not unplanned, planned or complaint result for xml $fileName")
                            Files.copy(xmlFilePath, Paths.get("tmp/unknow/" + xmlFilePath.getFileName.toString))
                            None
                        }
                    }
                }
            case Some(SchemaVersion6_3) =>
                val xml = new XMLDocument(stringVal)
                val complaintRegNum = xml.xpath("//*[local-name()='complaint']//text()")
                val unplannedCheckComplaintRegNumber = xml.xpath("//*[local-name()='checkResult']//*[local-name()='unplannedCheckComplaint']//text()")
                if (!complaintRegNum.isEmpty || !unplannedCheckComplaintRegNumber.isEmpty) {
                    XmlClassDetector.findClass(version, classOf[CheckComplaintResult])
                } else {
                    val unplannedCheckNumber = xml.xpath("//*[local-name()='checkResult']//*[local-name()='unplannedCheck']//text()")
                    if (!unplannedCheckNumber.isEmpty) {
                        XmlClassDetector.findClass(version, classOf[UnplannedCheckResult])
                    } else {
                        val plannedCheckNumber = xml.xpath("//*[local-name()='eventResult']//*[local-name()='plannedCheck']//text()")
                        if (!plannedCheckNumber.isEmpty) {
                            XmlClassDetector.findClass(version, classOf[PlannedCheckResult])
                        } else {
                            val plannedCheckNumber = xml.xpath("//*[local-name()='checkResult']//*[local-name()='plannedCheck']//text()")
                            if (!plannedCheckNumber.isEmpty) {
                                //todo - remove simple classOf
                                Some(classOf[PlannedCheckResult_AsCheckResult])
                            } else {
                                val unplannedCheckNumber = xml.xpath("//*[local-name()='eventResult']//*[local-name()='unplannedCheck']//text()")
                                if (!unplannedCheckNumber.isEmpty) {
                                    Some(classOf[UnplannedCheckResult_AsEventResult])
                                } else {
                                    log.warn(s"Not unplanned, planned or complaint result for xml $fileName")
                                    Files.copy(xmlFilePath, Paths.get("tmp/unknow/" + xmlFilePath.getFileName.toString))
                                    None
                                }
                            }
                        }
                    }
                }
            case Some(SchemaVersion6_4) =>
                val xml = new XMLDocument(stringVal)
                val complaintRegNum = xml.xpath("//*[local-name()='complaint']//text()")
                val unplannedCheckComplaintRegNumber = xml.xpath("//*[local-name()='checkResult']//*[local-name()='unplannedCheckComplaint']//text()")
                if (!complaintRegNum.isEmpty || !unplannedCheckComplaintRegNumber.isEmpty) {
                    XmlClassDetector.findClass(version, classOf[CheckComplaintResult])
                } else {
                    val unplannedCheckNumber = xml.xpath("//*[local-name()='checkResult']//*[local-name()='unplannedCheck']//text()")
                    if (!unplannedCheckNumber.isEmpty) {
                        XmlClassDetector.findClass(version, classOf[UnplannedCheckResult])
                    } else {
                        val plannedCheckNumber = xml.xpath("//*[local-name()='eventResult']//*[local-name()='plannedCheck']//text()")
                        if (!plannedCheckNumber.isEmpty) {
                            XmlClassDetector.findClass(version, classOf[PlannedCheckResult])
                        } else {
                            val plannedCheckNumber = xml.xpath("//*[local-name()='checkResult']//*[local-name()='plannedCheck']//text()")
                            if (!plannedCheckNumber.isEmpty) {
                                //todo - remove simple classOf
                                Some(classOf[PlannedCheckResult_AsCheckResult])
                            } else {
                                val unplannedCheckNumber = xml.xpath("//*[local-name()='eventResult']//*[local-name()='unplannedCheck']//text()")
                                if (!unplannedCheckNumber.isEmpty) {
                                    Some(classOf[UnplannedCheckResult_AsEventResult])
                                } else {
                                    log.warn(s"Not unplanned, planned or complaint result for xml $fileName")
                                    Files.copy(xmlFilePath, Paths.get("tmp/unknow/" + xmlFilePath.getFileName.toString))
                                    None
                                }
                            }
                        }
                    }
                }
        }

        val unmarshalled: Option[GeneralizedEntity[_ <: CheckResult]] = entityClass
            .flatMap(unmarshal(new BufferedInputStream(new ByteArrayInputStream(bytes)), _))
        val generalCheckResult: Option[CheckResult] = unmarshalled.map(_.mapToGeneral())

        version match {
            case Some(SchemaVersion6_4) | Some(SchemaVersion6_3) =>
                val checkResultNumber = fileName.split("_")(1)
                if (generalCheckResult.isDefined) {
                    generalCheckResult.get.checkResultNumber = checkResultNumber
                }
            case _ =>
        }

        generalCheckResult
    }

    def unmarshalComplaintCancel(inputStream: InputStream): Option[ComplaintCancel] = {
        unmarshal(inputStream, classOf[ComplaintCancel])
    }

    def unmarshal[T](inputStream: InputStream, clazz: Class[T]): Option[T] = {
        val jc: JAXBContext = JAXBContext.newInstance(clazz)
        val unmarshaller: Unmarshaller = jc.createUnmarshaller
        val unmarshalled = unmarshaller.unmarshal(inputStream).asInstanceOf[T]
        Some(unmarshalled)
    }

    def unmarshalTenderSuspension(inputStream: InputStream): Option[TenderSuspension] = {
        unmarshal(inputStream, classOf[TenderSuspension])
    }

    def unmarshalUnfairSupplier(inputStream: InputStream): Option[UnfairSupplier] = {
        unmarshal(inputStream, classOf[UnfairSupplier])
    }

}
