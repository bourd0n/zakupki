/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CheckComplaintResultDetailComponent } from '../../../../../../main/webapp/app/entities/check-complaint-result/check-complaint-result-detail.component';
import { CheckComplaintResultService } from '../../../../../../main/webapp/app/entities/check-complaint-result/check-complaint-result.service';
import { CheckComplaintResult } from '../../../../../../main/webapp/app/entities/check-complaint-result/check-complaint-result.model';

describe('Component Tests', () => {

    describe('CheckComplaintResult Management Detail Component', () => {
        let comp: CheckComplaintResultDetailComponent;
        let fixture: ComponentFixture<CheckComplaintResultDetailComponent>;
        let service: CheckComplaintResultService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [CheckComplaintResultDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CheckComplaintResultService,
                    JhiEventManager
                ]
            }).overrideTemplate(CheckComplaintResultDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CheckComplaintResultDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CheckComplaintResultService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CheckComplaintResult(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.checkComplaintResult).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
