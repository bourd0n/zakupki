package com.alex.zakupki.complaint.logic.akka.actors

import java.nio.file.{Files, Paths}

import akka.actor.{Actor, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import com.alex.zakupki.complaint.logic.akka.messages.{ProcessDownloadedFiles, ProcessDownloadedZip}
import com.alex.zakupki.complaint.logic.akka.spring.SpringExtension
import com.alex.zakupki.complaint.logic.config.ServerConfig
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component("downloadedZipProcessorActor")
@Scope("prototype")
@deprecated
class DownloadedZipProcessorActor @Autowired()(implicit ctx: ApplicationContext) extends Actor {

    private val log: Logger = LoggerFactory.getLogger(getClass)

    override def receive: Receive = {
        case ProcessDownloadedFiles(filesCount) =>
            log.debug("{} start process downloaded files", self.path.name)
            val path = s"${ServerConfig.server_path}/zakupki_tmp"
            val router = createRouter(filesCount / 10)
            Files.walk(Paths.get(path))
                .filter(Files.isRegularFile(_))
                .filter(_.getFileName.toString.endsWith("zip"))
                .forEach(filePath => {
                    router.route(ProcessDownloadedZip(filePath), self)
                })
            log.debug("{} finish process downloaded files", self.path.name)
        case Terminated(routee) =>
            //todo: add terminate action
            log.warn("{} was terminated", routee)
    }

    private def createRouter(numActors: Int): Router = {
        val zipProcessorActorProps = SpringExtension(context.system).props("zipProcessorActor")
        val routees = for (i <- 1 to numActors) yield {
            val actor = context.actorOf(zipProcessorActorProps, name = s"zip-worker-$i")
            context watch actor
            ActorRefRoutee(actor)
        }
        Router(RoundRobinRoutingLogic(), routees)
    }
}

