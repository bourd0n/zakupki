import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PublishOrg } from './publish-org.model';
import { PublishOrgPopupService } from './publish-org-popup.service';
import { PublishOrgService } from './publish-org.service';

@Component({
    selector: 'jhi-publish-org-dialog',
    templateUrl: './publish-org-dialog.component.html'
})
export class PublishOrgDialogComponent implements OnInit {

    publishOrg: PublishOrg;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private publishOrgService: PublishOrgService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.publishOrg.id !== undefined) {
            this.subscribeToSaveResponse(
                this.publishOrgService.update(this.publishOrg));
        } else {
            this.subscribeToSaveResponse(
                this.publishOrgService.create(this.publishOrg));
        }
    }

    private subscribeToSaveResponse(result: Observable<PublishOrg>) {
        result.subscribe((res: PublishOrg) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PublishOrg) {
        this.eventManager.broadcast({ name: 'publishOrgListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-publish-org-popup',
    template: ''
})
export class PublishOrgPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private publishOrgPopupService: PublishOrgPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.publishOrgPopupService
                    .open(PublishOrgDialogComponent as Component, params['id']);
            } else {
                this.publishOrgPopupService
                    .open(PublishOrgDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
