import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ComplaintCancel } from './complaint-cancel.model';
import { ComplaintCancelService } from './complaint-cancel.service';

@Component({
    selector: 'jhi-complaint-cancel-detail',
    templateUrl: './complaint-cancel-detail.component.html'
})
export class ComplaintCancelDetailComponent implements OnInit, OnDestroy {

    complaintCancel: ComplaintCancel;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private complaintCancelService: ComplaintCancelService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInComplaintCancels();
    }

    load(id) {
        this.complaintCancelService.find(id).subscribe((complaintCancel) => {
            this.complaintCancel = complaintCancel;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInComplaintCancels() {
        this.eventSubscriber = this.eventManager.subscribe(
            'complaintCancelListModification',
            (response) => this.load(this.complaintCancel.id)
        );
    }
}
