package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.ComplaintService;
import com.alex.zakupki.domain.Complaint;
import com.alex.zakupki.repository.ComplaintRepository;
import com.alex.zakupki.repository.search.ComplaintSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Complaint.
 */
@Service
@Transactional
public class ComplaintServiceImpl implements ComplaintService{

    private final Logger log = LoggerFactory.getLogger(ComplaintServiceImpl.class);

    private final ComplaintRepository complaintRepository;

    private final ComplaintSearchRepository complaintSearchRepository;

    public ComplaintServiceImpl(ComplaintRepository complaintRepository, ComplaintSearchRepository complaintSearchRepository) {
        this.complaintRepository = complaintRepository;
        this.complaintSearchRepository = complaintSearchRepository;
    }

    /**
     * Save a complaint.
     *
     * @param complaint the entity to save
     * @return the persisted entity
     */
    @Override
    public Complaint save(Complaint complaint) {
        log.debug("Request to save Complaint : {}", complaint);
        Complaint result = complaintRepository.save(complaint);
        complaintSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the complaints.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Complaint> findAll(Pageable pageable) {
        log.debug("Request to get all Complaints");
        return complaintRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Complaint> findAllByRegistrationKoIdAndRegDateBetween(Long registrationKoId,
                                                                      LocalDate fromDate,
                                                                      LocalDate toDate,
                                                                      Pageable pageable) {
        if (fromDate != null && toDate !=null) {
            return complaintRepository.findAllByRegistrationKoIdAndRegDateBetween(registrationKoId,
                fromDate, toDate, pageable);
        } else if (fromDate != null) {
            return complaintRepository.findAllByRegistrationKoIdAndRegDateAfter(registrationKoId, fromDate, pageable);
        } else if (toDate != null) {
            return complaintRepository.findAllByRegistrationKoIdAndRegDateBefore(registrationKoId, toDate, pageable);
        } else {
            throw new IllegalStateException("Not expecting fromDate and toDate both to be null");
        }
    }

    /**
     *  Get one complaint by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Complaint findOne(Long id) {
        log.debug("Request to get Complaint : {}", id);
        return complaintRepository.findOne(id);
    }

    /**
     *  Delete the  complaint by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Complaint : {}", id);
        complaintRepository.delete(id);
        complaintSearchRepository.delete(id);
    }

    /**
     * Search for the complaint corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Complaint> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Complaints for query {}", query);
        Page<Complaint> result = complaintSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
