package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.UnfairSupplier;
import com.alex.zakupki.repository.UnfairSupplierRepository;
import com.alex.zakupki.service.UnfairSupplierService;
import com.alex.zakupki.repository.search.UnfairSupplierSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UnfairSupplierResource REST controller.
 *
 * @see UnfairSupplierResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class UnfairSupplierResourceIntTest {

    private static final String DEFAULT_REGISTRY_NUM = "AAAAAAAAAA";
    private static final String UPDATED_REGISTRY_NUM = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_PUBLISH_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PUBLISH_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_INN = "AAAAAAAAAA";
    private static final String UPDATED_INN = "BBBBBBBBBB";

    private static final String DEFAULT_PURCHASE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PURCHASE_NUMBER = "BBBBBBBBBB";

    @Autowired
    private UnfairSupplierRepository unfairSupplierRepository;

    @Autowired
    private UnfairSupplierService unfairSupplierService;

    @Autowired
    private UnfairSupplierSearchRepository unfairSupplierSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUnfairSupplierMockMvc;

    private UnfairSupplier unfairSupplier;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UnfairSupplierResource unfairSupplierResource = new UnfairSupplierResource(unfairSupplierService);
        this.restUnfairSupplierMockMvc = MockMvcBuilders.standaloneSetup(unfairSupplierResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UnfairSupplier createEntity(EntityManager em) {
        UnfairSupplier unfairSupplier = new UnfairSupplier()
            .registryNum(DEFAULT_REGISTRY_NUM)
            .publishDate(DEFAULT_PUBLISH_DATE)
            .state(DEFAULT_STATE)
            .reason(DEFAULT_REASON)
            .fullName(DEFAULT_FULL_NAME)
            .inn(DEFAULT_INN)
            .purchaseNumber(DEFAULT_PURCHASE_NUMBER);
        return unfairSupplier;
    }

    @Before
    public void initTest() {
        unfairSupplierSearchRepository.deleteAll();
        unfairSupplier = createEntity(em);
    }

    @Test
    @Transactional
    public void createUnfairSupplier() throws Exception {
        int databaseSizeBeforeCreate = unfairSupplierRepository.findAll().size();

        // Create the UnfairSupplier
        restUnfairSupplierMockMvc.perform(post("/api/unfair-suppliers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unfairSupplier)))
            .andExpect(status().isCreated());

        // Validate the UnfairSupplier in the database
        List<UnfairSupplier> unfairSupplierList = unfairSupplierRepository.findAll();
        assertThat(unfairSupplierList).hasSize(databaseSizeBeforeCreate + 1);
        UnfairSupplier testUnfairSupplier = unfairSupplierList.get(unfairSupplierList.size() - 1);
        assertThat(testUnfairSupplier.getRegistryNum()).isEqualTo(DEFAULT_REGISTRY_NUM);
        assertThat(testUnfairSupplier.getPublishDate()).isEqualTo(DEFAULT_PUBLISH_DATE);
        assertThat(testUnfairSupplier.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testUnfairSupplier.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testUnfairSupplier.getFullName()).isEqualTo(DEFAULT_FULL_NAME);
        assertThat(testUnfairSupplier.getInn()).isEqualTo(DEFAULT_INN);
        assertThat(testUnfairSupplier.getPurchaseNumber()).isEqualTo(DEFAULT_PURCHASE_NUMBER);

        // Validate the UnfairSupplier in Elasticsearch
        UnfairSupplier unfairSupplierEs = unfairSupplierSearchRepository.findOne(testUnfairSupplier.getId());
        assertThat(unfairSupplierEs).isEqualToComparingFieldByField(testUnfairSupplier);
    }

    @Test
    @Transactional
    public void createUnfairSupplierWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = unfairSupplierRepository.findAll().size();

        // Create the UnfairSupplier with an existing ID
        unfairSupplier.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUnfairSupplierMockMvc.perform(post("/api/unfair-suppliers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unfairSupplier)))
            .andExpect(status().isBadRequest());

        // Validate the UnfairSupplier in the database
        List<UnfairSupplier> unfairSupplierList = unfairSupplierRepository.findAll();
        assertThat(unfairSupplierList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkRegistryNumIsRequired() throws Exception {
        int databaseSizeBeforeTest = unfairSupplierRepository.findAll().size();
        // set the field null
        unfairSupplier.setRegistryNum(null);

        // Create the UnfairSupplier, which fails.

        restUnfairSupplierMockMvc.perform(post("/api/unfair-suppliers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unfairSupplier)))
            .andExpect(status().isBadRequest());

        List<UnfairSupplier> unfairSupplierList = unfairSupplierRepository.findAll();
        assertThat(unfairSupplierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUnfairSuppliers() throws Exception {
        // Initialize the database
        unfairSupplierRepository.saveAndFlush(unfairSupplier);

        // Get all the unfairSupplierList
        restUnfairSupplierMockMvc.perform(get("/api/unfair-suppliers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unfairSupplier.getId().intValue())))
            .andExpect(jsonPath("$.[*].registryNum").value(hasItem(DEFAULT_REGISTRY_NUM.toString())))
            .andExpect(jsonPath("$.[*].publishDate").value(hasItem(DEFAULT_PUBLISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].inn").value(hasItem(DEFAULT_INN.toString())))
            .andExpect(jsonPath("$.[*].purchaseNumber").value(hasItem(DEFAULT_PURCHASE_NUMBER.toString())));
    }

    @Test
    @Transactional
    public void getUnfairSupplier() throws Exception {
        // Initialize the database
        unfairSupplierRepository.saveAndFlush(unfairSupplier);

        // Get the unfairSupplier
        restUnfairSupplierMockMvc.perform(get("/api/unfair-suppliers/{id}", unfairSupplier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(unfairSupplier.getId().intValue()))
            .andExpect(jsonPath("$.registryNum").value(DEFAULT_REGISTRY_NUM.toString()))
            .andExpect(jsonPath("$.publishDate").value(DEFAULT_PUBLISH_DATE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()))
            .andExpect(jsonPath("$.inn").value(DEFAULT_INN.toString()))
            .andExpect(jsonPath("$.purchaseNumber").value(DEFAULT_PURCHASE_NUMBER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUnfairSupplier() throws Exception {
        // Get the unfairSupplier
        restUnfairSupplierMockMvc.perform(get("/api/unfair-suppliers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUnfairSupplier() throws Exception {
        // Initialize the database
        unfairSupplierService.save(unfairSupplier);

        int databaseSizeBeforeUpdate = unfairSupplierRepository.findAll().size();

        // Update the unfairSupplier
        UnfairSupplier updatedUnfairSupplier = unfairSupplierRepository.findOne(unfairSupplier.getId());
        updatedUnfairSupplier
            .registryNum(UPDATED_REGISTRY_NUM)
            .publishDate(UPDATED_PUBLISH_DATE)
            .state(UPDATED_STATE)
            .reason(UPDATED_REASON)
            .fullName(UPDATED_FULL_NAME)
            .inn(UPDATED_INN)
            .purchaseNumber(UPDATED_PURCHASE_NUMBER);

        restUnfairSupplierMockMvc.perform(put("/api/unfair-suppliers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUnfairSupplier)))
            .andExpect(status().isOk());

        // Validate the UnfairSupplier in the database
        List<UnfairSupplier> unfairSupplierList = unfairSupplierRepository.findAll();
        assertThat(unfairSupplierList).hasSize(databaseSizeBeforeUpdate);
        UnfairSupplier testUnfairSupplier = unfairSupplierList.get(unfairSupplierList.size() - 1);
        assertThat(testUnfairSupplier.getRegistryNum()).isEqualTo(UPDATED_REGISTRY_NUM);
        assertThat(testUnfairSupplier.getPublishDate()).isEqualTo(UPDATED_PUBLISH_DATE);
        assertThat(testUnfairSupplier.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testUnfairSupplier.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testUnfairSupplier.getFullName()).isEqualTo(UPDATED_FULL_NAME);
        assertThat(testUnfairSupplier.getInn()).isEqualTo(UPDATED_INN);
        assertThat(testUnfairSupplier.getPurchaseNumber()).isEqualTo(UPDATED_PURCHASE_NUMBER);

        // Validate the UnfairSupplier in Elasticsearch
        UnfairSupplier unfairSupplierEs = unfairSupplierSearchRepository.findOne(testUnfairSupplier.getId());
        assertThat(unfairSupplierEs).isEqualToComparingFieldByField(testUnfairSupplier);
    }

    @Test
    @Transactional
    public void updateNonExistingUnfairSupplier() throws Exception {
        int databaseSizeBeforeUpdate = unfairSupplierRepository.findAll().size();

        // Create the UnfairSupplier

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUnfairSupplierMockMvc.perform(put("/api/unfair-suppliers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(unfairSupplier)))
            .andExpect(status().isCreated());

        // Validate the UnfairSupplier in the database
        List<UnfairSupplier> unfairSupplierList = unfairSupplierRepository.findAll();
        assertThat(unfairSupplierList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUnfairSupplier() throws Exception {
        // Initialize the database
        unfairSupplierService.save(unfairSupplier);

        int databaseSizeBeforeDelete = unfairSupplierRepository.findAll().size();

        // Get the unfairSupplier
        restUnfairSupplierMockMvc.perform(delete("/api/unfair-suppliers/{id}", unfairSupplier.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean unfairSupplierExistsInEs = unfairSupplierSearchRepository.exists(unfairSupplier.getId());
        assertThat(unfairSupplierExistsInEs).isFalse();

        // Validate the database is empty
        List<UnfairSupplier> unfairSupplierList = unfairSupplierRepository.findAll();
        assertThat(unfairSupplierList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchUnfairSupplier() throws Exception {
        // Initialize the database
        unfairSupplierService.save(unfairSupplier);

        // Search the unfairSupplier
        restUnfairSupplierMockMvc.perform(get("/api/_search/unfair-suppliers?query=id:" + unfairSupplier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(unfairSupplier.getId().intValue())))
            .andExpect(jsonPath("$.[*].registryNum").value(hasItem(DEFAULT_REGISTRY_NUM.toString())))
            .andExpect(jsonPath("$.[*].publishDate").value(hasItem(DEFAULT_PUBLISH_DATE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].inn").value(hasItem(DEFAULT_INN.toString())))
            .andExpect(jsonPath("$.[*].purchaseNumber").value(hasItem(DEFAULT_PURCHASE_NUMBER.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UnfairSupplier.class);
        UnfairSupplier unfairSupplier1 = new UnfairSupplier();
        unfairSupplier1.setId(1L);
        UnfairSupplier unfairSupplier2 = new UnfairSupplier();
        unfairSupplier2.setId(unfairSupplier1.getId());
        assertThat(unfairSupplier1).isEqualTo(unfairSupplier2);
        unfairSupplier2.setId(2L);
        assertThat(unfairSupplier1).isNotEqualTo(unfairSupplier2);
        unfairSupplier1.setId(null);
        assertThat(unfairSupplier1).isNotEqualTo(unfairSupplier2);
    }
}
