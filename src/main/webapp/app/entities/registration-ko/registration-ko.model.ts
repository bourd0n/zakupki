import { BaseEntity } from './../../shared';

export class RegistrationKo implements BaseEntity {
    constructor(
        public id?: number,
        public regNum?: string,
        public fullName?: string,
    ) {
    }
}
