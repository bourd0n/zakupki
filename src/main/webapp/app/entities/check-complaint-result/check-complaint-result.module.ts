import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    CheckComplaintResultService,
    CheckComplaintResultPopupService,
    CheckComplaintResultComponent,
    CheckComplaintResultDetailComponent,
    CheckComplaintResultDialogComponent,
    CheckComplaintResultPopupComponent,
    CheckComplaintResultDeletePopupComponent,
    CheckComplaintResultDeleteDialogComponent,
    checkComplaintResultRoute,
    checkComplaintResultPopupRoute,
} from './';

const ENTITY_STATES = [
    ...checkComplaintResultRoute,
    ...checkComplaintResultPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CheckComplaintResultComponent,
        CheckComplaintResultDetailComponent,
        CheckComplaintResultDialogComponent,
        CheckComplaintResultDeleteDialogComponent,
        CheckComplaintResultPopupComponent,
        CheckComplaintResultDeletePopupComponent,
    ],
    entryComponents: [
        CheckComplaintResultComponent,
        CheckComplaintResultDialogComponent,
        CheckComplaintResultPopupComponent,
        CheckComplaintResultDeleteDialogComponent,
        CheckComplaintResultDeletePopupComponent,
    ],
    providers: [
        CheckComplaintResultService,
        CheckComplaintResultPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiCheckComplaintResultModule {}
