import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ComplaintCancel } from './complaint-cancel.model';
import { ComplaintCancelPopupService } from './complaint-cancel-popup.service';
import { ComplaintCancelService } from './complaint-cancel.service';

@Component({
    selector: 'jhi-complaint-cancel-delete-dialog',
    templateUrl: './complaint-cancel-delete-dialog.component.html'
})
export class ComplaintCancelDeleteDialogComponent {

    complaintCancel: ComplaintCancel;

    constructor(
        private complaintCancelService: ComplaintCancelService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.complaintCancelService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'complaintCancelListModification',
                content: 'Deleted an complaintCancel'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-complaint-cancel-delete-popup',
    template: ''
})
export class ComplaintCancelDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private complaintCancelPopupService: ComplaintCancelPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.complaintCancelPopupService
                .open(ComplaintCancelDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
