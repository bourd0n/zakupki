package com.alex.zakupki.complaint.api.model.general

import java.util.Date

import com.google.common.base.MoreObjects

case class CheckComplaintResult(override var checkResultNumber: String,
                                override val createDate: Date,
                                override var owner: Owner,
                                result: String,
                                checkSubject: Client,
                                complaintNumber: String) extends CheckResult {
    override def toString: String = {
        MoreObjects.toStringHelper(this).add("result", result)
            .add("checkResultNumber", checkResultNumber)
            .add("complaint", complaintNumber)
            .add("createDate", createDate)
            .toString
    }
}
