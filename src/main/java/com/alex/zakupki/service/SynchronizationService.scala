package com.alex.zakupki.service

import javax.inject.Inject

import akka.actor.ActorSystem
import com.alex.zakupki.complaint.logic.akka.messages.StartSynchronization
import com.alex.zakupki.complaint.logic.akka.spring.SpringExtension
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import com.alex.zakupki.domain.SyncInfo
import com.alex.zakupki.service.SynchronizationService.log
import com.alex.zakupki.web.rest.vm.SyncInfoVM
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Service

@Service
class SynchronizationService @Inject()(implicit val ctx: ApplicationContext,
                                       val actorSystem: ActorSystem,
                                       val xmlProcessor: XMLProcessor) {

    val prop = SpringExtension(actorSystem).props("synchronizationActor")

    val synchronizationActor = actorSystem.actorOf(prop, "synchronizationActor")

    def synchronize(syncInfoVM: SyncInfoVM) = {
        log.debug("Start refreshing {}", syncInfoVM)

        val syncInfo = new SyncInfo
        syncInfo.name = syncInfoVM.name
        syncInfo.syncDate = syncInfoVM.syncDate

        try {
            synchronizationActor ! StartSynchronization(syncInfo)
        } catch {
            case e: Throwable => log.error("Exception during refresh data", e)
        }
        log.debug("Finish refreshing {}", syncInfo)
    }

    def synchronizeFailed(): Unit = {
        log.debug("Start refreshing failed")

        //todo: use akka
        xmlProcessor.processFailed()
        log.debug("Finish refreshing failed")
    }
}

object SynchronizationService {
    final val log: Logger = LoggerFactory.getLogger(classOf[SynchronizationService])
}

