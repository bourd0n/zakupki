import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UnfairSupplierComponent } from './unfair-supplier.component';
import { UnfairSupplierDetailComponent } from './unfair-supplier-detail.component';
import { UnfairSupplierPopupComponent } from './unfair-supplier-dialog.component';
import { UnfairSupplierDeletePopupComponent } from './unfair-supplier-delete-dialog.component';

export const unfairSupplierRoute: Routes = [
    {
        path: 'unfair-supplier',
        component: UnfairSupplierComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unfairSupplier.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'unfair-supplier/:id',
        component: UnfairSupplierDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unfairSupplier.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const unfairSupplierPopupRoute: Routes = [
    {
        path: 'unfair-supplier-new',
        component: UnfairSupplierPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unfairSupplier.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unfair-supplier/:id/edit',
        component: UnfairSupplierPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unfairSupplier.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'unfair-supplier/:id/delete',
        component: UnfairSupplierDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.unfairSupplier.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
