package com.alex.zakupki.complaint.logic.ftp

import java.io.{BufferedOutputStream, File, FileOutputStream, OutputStream}
import java.nio.file.Paths
import java.time.LocalDate
import java.util.{Calendar, GregorianCalendar}
import javax.annotation.{PostConstruct, PreDestroy}

import com.alex.zakupki.complaint.logic.config.ServerConfig
import com.alex.zakupki.complaint.temp.Consts.FTP_URL_ZAKUPKI
import org.apache.commons.net.ftp.{FTP, FTPClient, FTPFile, FTPReply}
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.context.annotation.{Lazy, Scope}
import org.springframework.stereotype.Component

//todo: use some akka scope?
@Component
@Lazy
@Scope("prototype")
class FtpUtils {

    private lazy val logger: Logger = LoggerFactory.getLogger(getClass)

    var ftp: FTPClient = _

    val makeFiles = new File(ServerConfig.server_path + "/zakupki_tmp/unpacked/").mkdirs()

    @PostConstruct
    def init(): Unit = {
        logger.debug("Init is called")
        ftp = new FTPClient
    }

    def connect(): Unit = {
        if (!ftp.isConnected) {
            var reply: Int = 0
            ftp.connect(FTP_URL_ZAKUPKI)
            ftp.login("free", "free")
            ftp.enterLocalPassiveMode()
            reply = ftp.getReplyCode
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect()
                throw new RuntimeException("FTP server refused connection.")
            }
            ftp.setFileType(FTP.BINARY_FILE_TYPE)
        }
    }

    @PreDestroy
    def disconnect(): Unit = {
        logger.debug("Close ftp connection")
        ftp.logout
        if (ftp.isConnected) ftp.disconnect()
    }

    def listFiles(path: String)(filterFunc: FTPFile => Boolean): Seq[FTPFile] = {
        connect()
        ftp.listFiles(path).filter(filterFunc).toStream
    }

    def downloadFile(file: FTPFile, path: String): String = {
        connect()
        val name = file.getName
        val link: String = s"$path$name"
        //todo: remove server path
        val localPath = ServerConfig.server_path + "/zakupki_tmp/" + name
        val downloadFile: File = new File(localPath)
        if (!downloadFile.exists()) {
            val outputStream: OutputStream = new BufferedOutputStream(new FileOutputStream(downloadFile))
            ftp.retrieveFile(link, outputStream)
            println(s"File $link retrieved in FS")
            outputStream.flush()
            outputStream.close()
        }
        localPath
    }

    def listFilesByLastSyncDate(ftpType: FTPTypes.FTPType, lastSyncDate: LocalDate): Seq[FTPFile] =
        listFiles(ftpType.ftpPath)(ftpFile => {
            ftpFile.getName.startsWith(ftpType.fileNameRegx) && {
                lastSyncDate == null || {
                    val ftpFileModifyTime: Calendar = ftpFile.getTimestamp
                    val c = new GregorianCalendar()
                    c.setTimeInMillis(ftpFileModifyTime.getTimeInMillis)
                    lastSyncDate.isBefore(LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH)))
                }
            }
        })


}
