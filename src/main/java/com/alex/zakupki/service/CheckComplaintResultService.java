package com.alex.zakupki.service;

import com.alex.zakupki.domain.CheckComplaintResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing CheckComplaintResult.
 */
public interface CheckComplaintResultService {

    /**
     * Save a checkComplaintResult.
     *
     * @param checkComplaintResult the entity to save
     * @return the persisted entity
     */
    CheckComplaintResult save(CheckComplaintResult checkComplaintResult);

    /**
     *  Get all the checkComplaintResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CheckComplaintResult> findAll(Pageable pageable);

    /**
     *  Get the "id" checkComplaintResult.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CheckComplaintResult findOne(Long id);

    /**
     *  Delete the "id" checkComplaintResult.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the checkComplaintResult corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CheckComplaintResult> search(String query, Pageable pageable);
}
