import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CheckComplaintResult } from './check-complaint-result.model';
import { CheckComplaintResultPopupService } from './check-complaint-result-popup.service';
import { CheckComplaintResultService } from './check-complaint-result.service';
import { Owner, OwnerService } from '../owner';
import { Client, ClientService } from '../client';
import { Complaint, ComplaintService } from '../complaint';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-check-complaint-result-dialog',
    templateUrl: './check-complaint-result-dialog.component.html'
})
export class CheckComplaintResultDialogComponent implements OnInit {

    checkComplaintResult: CheckComplaintResult;
    isSaving: boolean;

    owners: Owner[];

    clients: Client[];

    complaints: Complaint[];
    createDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private checkComplaintResultService: CheckComplaintResultService,
        private ownerService: OwnerService,
        private clientService: ClientService,
        private complaintService: ComplaintService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.ownerService.query()
            .subscribe((res: ResponseWrapper) => { this.owners = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clientService.query()
            .subscribe((res: ResponseWrapper) => { this.clients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.complaintService.query()
            .subscribe((res: ResponseWrapper) => { this.complaints = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.checkComplaintResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.checkComplaintResultService.update(this.checkComplaintResult));
        } else {
            this.subscribeToSaveResponse(
                this.checkComplaintResultService.create(this.checkComplaintResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<CheckComplaintResult>) {
        result.subscribe((res: CheckComplaintResult) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CheckComplaintResult) {
        this.eventManager.broadcast({ name: 'checkComplaintResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOwnerById(index: number, item: Owner) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }

    trackComplaintById(index: number, item: Complaint) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-check-complaint-result-popup',
    template: ''
})
export class CheckComplaintResultPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private checkComplaintResultPopupService: CheckComplaintResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.checkComplaintResultPopupService
                    .open(CheckComplaintResultDialogComponent as Component, params['id']);
            } else {
                this.checkComplaintResultPopupService
                    .open(CheckComplaintResultDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
