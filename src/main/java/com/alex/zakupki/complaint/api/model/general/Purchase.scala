package com.alex.zakupki.complaint.api.model.general

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType}

import com.google.common.base.MoreObjects

case class Purchase(purchaseNumber: String,
                    determSupplierMethod: String,
                    electronicPlatform: String,
                    maxCost: Double) {
  override def toString: String = {
    MoreObjects.toStringHelper(this).add("purchaseNumber", purchaseNumber).add("determSupplierMethod", determSupplierMethod).toString
  }
}
