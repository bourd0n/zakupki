package com.alex.zakupki.service.mapper.xml

import java.time.ZoneId
import java.util.concurrent.TimeUnit
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general
import com.alex.zakupki.domain.{CheckComplaintResult, Owner}
import com.alex.zakupki.repository.OwnerRepository
import com.google.common.cache.{Cache, CacheBuilder}
import org.springframework.stereotype.Component

@Component
class OwnerXmlToDataMapper @Inject()(val ownerRepository: OwnerRepository)
    extends XmlToDataMapper[general.Owner, Owner] {

    private val cache: Cache[String, Owner] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, Owner]]


    override def map(from: general.Owner): Option[Owner] = Option(from) map {
        fromOwner => {
            //locking is performed
            cache.get(fromOwner.regNum, () => {
                ownerRepository
                    .findOneByRegNum(from.regNum)
                    .orElseGet(() => {
                        val owner = new Owner()
                            .regNum(from.regNum)
                            .fullName(from.fullName)
                        ownerRepository.save(owner)
                    })
            })
        }
    }
}
