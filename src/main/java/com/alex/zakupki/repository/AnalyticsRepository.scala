package com.alex.zakupki.repository

import java.sql.ResultSet
import java.time.LocalDate

import com.alex.zakupki.service.dto._
import com.google.common.base.Preconditions
import org.apache.commons.lang3.StringUtils
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.data.domain.{Pageable, Sort}
import org.springframework.stereotype.Repository
import scalikejdbc.{AutoSession, ConnectionPool, DB, using}
import scalikejdbc._

@Repository
class AnalyticsRepository {

    private val log: Logger = LoggerFactory.getLogger(classOf[AnalyticsRepository])

    Class.forName("org.postgresql.Driver")
    ConnectionPool.singleton("jdbc:postgresql://192.168.99.100:5432/zakupki", "zakupki", "")

    implicit val session = AutoSession

    private val queries: Map[String, String] = Map(
        "complaints_count_by_fas" ->
            """
              |SELECT
              |  registration_ko.full_name,
              |  ko2complaints.complaints_count
              |FROM (SELECT
              |        c.registration_ko_id ko_id,
              |        count(*)             complaints_count
              |      FROM complaint c
              |      GROUP BY c.registration_ko_id) ko2complaints, registration_ko
              |WHERE ko2complaints.ko_id = registration_ko.id
              |ORDER BY complaints_count DESC
            """.stripMargin,


        "check_result_by_fas_all_time" ->
            """
              |SELECT
              |  o.full_name,
              |  res."COMPLAINT_NO_VIOLATIONS",
              |  res."COMPLAINT_VIOLATIONS",
              |  res."COMPLAINT_PARTLY_VALID",
              |  res."COMPLAINT_NOT_MATCHED",
              |  res."EMPTY"
              |FROM owner o, (SELECT *
              |               FROM crosstab('select subq.ko_id, subq.checkResult, subq.complaints_count' ||
              |                             ' from (SELECT
              |                 ccr.owner_id ko_id,
              |                 coalesce(ccr.result, '''') checkResult,
              |                 count(*) complaints_count
              |               FROM check_complaint_result ccr
              |               GROUP BY ccr.owner_id, checkResult) subq order by 1,2',
              |                             $$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$)
              |                 AS ct ("ko_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
              |WHERE o.id = res.ko_id;
              |
          """.stripMargin

    )

    //todo: rewrite to binds
    def getCheckResultByFasByYear(yearParam: String): Seq[FasComplaintsStats] = {
        Preconditions.checkArgument(yearParam.matches("\\d{4}"), "Incorrect year '%s' was specified", yearParam)
        val query =
            s"""
               |SELECT
               |    regKo.id,
               |    regKo.full_name,
               |    res."COMPLAINT_NO_VIOLATIONS",
               |    res."COMPLAINT_VIOLATIONS",
               |    res."COMPLAINT_PARTLY_VALID",
               |    res."COMPLAINT_NOT_MATCHED",
               |    res.returned_count returned_count,
               |    res.count          complaints_sum,
               |    res."EMPTY"
               |FROM
               |    (SELECT
               |         res.ko_id       ko_id_1,
               |         returned.ko_id  ko_id_2,
               |         all_count.ko_id ko_id_3,
               |         res."COMPLAINT_NO_VIOLATIONS",
               |         res."COMPLAINT_VIOLATIONS",
               |         res."COMPLAINT_PARTLY_VALID",
               |         res."COMPLAINT_NOT_MATCHED",
               |         res."EMPTY",
               |         all_count.count,
               |         returned.returned_count
               |     FROM
               |         (SELECT *
               |          FROM crosstab('SELECT
               |                      regKo.id ko_id,
               |                      subq.checkResult,
               |                      subq.complaints_count
               |                    FROM (SELECT
               |                            ccr.owner_id             ko_id,
               |                            COALESCE(ccr.result, '''') checkResult,
               |                            count(*)                 complaints_count
               |                          FROM check_complaint_result ccr
               |                          WHERE date_trunc(''year'', ccr.create_date) = to_date(''$yearParam'', ''yyyy'')
               |                          GROUP BY ccr.owner_id, checkResult) subq, registration_ko regKo, owner o
               |                          WHERE o.reg_num = regKo.reg_num
               |                                AND o.id = subq.ko_id
               |                            ORDER BY 1, 2',
               |                        $$$$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$$$)
               |              AS ct ("ko_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
               |         FULL OUTER JOIN (SELECT
               |                              C.registration_ko_id  ko_id,
               |                              COALESCE(count(*), 0) returned_count
               |                          FROM
               |                              complaint C, returned_complaint rc
               |                          WHERE rc.complaint_id = C.id
               |                                AND date_trunc('year', C.reg_date) = to_date('$yearParam', 'yyyy')
               |                          GROUP BY C.registration_ko_id) returned ON (res.ko_id = returned.ko_id)
               |         FULL OUTER JOIN
               |         (SELECT
               |              C.registration_ko_id  ko_id,
               |              COALESCE(count(*), 0) count
               |          FROM
               |              complaint C
               |          WHERE date_trunc('year', C.reg_date) = to_date('$yearParam', 'yyyy')
               |          GROUP BY C.registration_ko_id) all_count ON (res.ko_id = all_count.ko_id)) res,
               |    registration_ko regKo
               |WHERE regKo.id = coalesce(res.ko_id_1, res.ko_id_2, res.ko_id_3)
               |ORDER BY complaints_sum DESC NULLS LAST
             """.stripMargin

        using(ConnectionPool.borrow()) { connection =>
            val prepareStatement = connection.prepareStatement(query)
            val rs: ResultSet = prepareStatement.executeQuery()

            (for ((_, r) <- Iterator.continually((rs.next(), rs)).takeWhile(_._1)) yield {
                val fullName = rs.getString("full_name")
                val noViolations = rs.getInt("COMPLAINT_NO_VIOLATIONS")
                val violations = rs.getInt("COMPLAINT_VIOLATIONS")
                val partlyValid = rs.getInt("COMPLAINT_PARTLY_VALID")
                val notMatched = rs.getInt("COMPLAINT_NOT_MATCHED")
                val returned = rs.getInt("returned_count")
                val empty = rs.getInt("EMPTY")
                val complaintSum = rs.getInt("complaints_sum")
                FasComplaintsStats(fullName, violations, noViolations, partlyValid, notMatched, returned, empty, complaintSum)
            }).toList
        }
    }

    //todo: rewrite to binds
    def getUnplannedPlannedCheckByFasByYear(yearParam: String): Seq[FasPlannedUnplannedStats] = {
        Preconditions.checkArgument(yearParam.matches("\\d{4}"), "Incorrect year '%s' was specified", yearParam)
        val query =
            s"""
               |SELECT
               |    o.id,
               |    o.full_name,
               |    res."VIOLATIONS",
               |    res."NO_VIOLATIONS",
               |    res.unplanned_count,
               |    res.planned_count
               |FROM owner o, (SELECT
               |                   res.ko_id             ko_id_1,
               |                   unplanned_check.ko_id ko_id_2,
               |                   planned_check.ko_id   ko_id_3,
               |                   res."VIOLATIONS",
               |                   res."NO_VIOLATIONS",
               |                   unplanned_check.count unplanned_count,
               |                   planned_check.count   planned_count
               |               FROM (SELECT *
               |                     FROM crosstab('SELECT subq.ko_id, subq.checkResult, subq.complaints_count FROM ( SELECT
               |                              ucr.owner_id ko_id,
               |                              COALESCE (ucr.result, '''') checkResult,
               |                              count(*) complaints_count
               |                              FROM unplanned_check_result ucr
               |                              WHERE date_trunc(''year'', ucr.created_date) = to_date(''$yearParam'', ''yyyy'')
               |                              GROUP BY ucr.owner_id, checkResult) subq ORDER BY 1, 2',
               |                                   $$$$VALUES ('VIOLATIONS'::text), ('NO_VIOLATIONS'::text)$$$$)
               |                         AS ct ("ko_id" INT, "VIOLATIONS" INT, "NO_VIOLATIONS" INT)) res
               |                   FULL OUTER JOIN (SELECT
               |                                        unc.owner_id          ko_id,
               |                                        coalesce(count(*), 0) count
               |                                    FROM
               |                                        unplanned_check_result unc
               |                                    WHERE date_trunc('year', unc.created_date) = to_date('$yearParam', 'yyyy')
               |                                    GROUP BY unc.owner_id) unplanned_check ON (res.ko_id = unplanned_check.ko_id)
               |                   FULL OUTER JOIN (SELECT
               |                                        pcr.owner_id          ko_id,
               |                                        coalesce(count(*), 0) count
               |                                    FROM
               |                                        planned_check_result pcr
               |                                    WHERE date_trunc('year', pcr.created_date) = to_date('$yearParam', 'yyyy')
               |                                    GROUP BY pcr.owner_id) planned_check ON (res.ko_id = planned_check.ko_id)) AS res
               |WHERE o.id = coalesce(res.ko_id_1, res.ko_id_2, res.ko_id_3)
             """.stripMargin


        using(ConnectionPool.borrow()) { connection =>
            val prepareStatement = connection.prepareStatement(query)
            val rs: ResultSet = prepareStatement.executeQuery()

            (for ((_, r) <- Iterator.continually((rs.next(), rs)).takeWhile(_._1)) yield {
                val fullName = rs.getString("full_name")
                val noViolations = rs.getInt("NO_VIOLATIONS")
                val violations = rs.getInt("VIOLATIONS")
                val unplannedCount = rs.getInt("unplanned_count")
                val plannedCount = rs.getInt("planned_count")
                FasPlannedUnplannedStats(fullName, violations, noViolations, unplannedCount, plannedCount)
            }).toList
        }
    }

    def getApplicantStats(): Seq[ApplicantStats] = {
        DB readOnly { implicit session =>
            sql"""
                 |SELECT
                 |    a.id,
                 |    a.organization_name,
                 |    a.applicant_type,
                 |    res."COMPLAINT_NO_VIOLATIONS",
                 |    res."COMPLAINT_VIOLATIONS",
                 |    res."COMPLAINT_PARTLY_VALID",
                 |    res."COMPLAINT_NOT_MATCHED",
                 |    all_count.count complaints_sum,
                 |    res."EMPTY"
                 |FROM applicant a, (SELECT *
                 |                   FROM crosstab('SELECT
                 |                              subq.applicant_id,
                 |                              subq.checkResult,
                 |                              subq.complaints_count
                 |                              FROM (SELECT
                 |                                    c.applicant_id           applicant_id,
                 |                                    COALESCE(ccr.result, '''') checkResult,
                 |                                    count(*) complaints_count
                 |                                  FROM complaint c, check_complaint_result ccr
                 |                                  WHERE ccr.complaint_id = c.id
                 |                                  GROUP BY c.applicant_id, checkResult) subq
                 |                              ORDER BY 1, 2',
                 |                                 $$$$VALUES ('COMPLAINT_NO_VIOLATIONS'::TEXT), ('COMPLAINT_VIOLATIONS'::TEXT), ('COMPLAINT_PARTLY_VALID'::TEXT), ('COMPLAINT_NOT_MATCHED'::TEXT), (''::TEXT)$$$$)
                 |                       AS ct ("applicant_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res,
                 |    (SELECT
                 |         c.applicant_id        applicant_id,
                 |         coalesce(count(*), 0) count
                 |     FROM
                 |         complaint c
                 |     GROUP BY c.applicant_id) all_count
                 |WHERE a.id = res.applicant_id
                 |      AND a.id = all_count.applicant_id
                 |ORDER BY complaints_sum DESC
                 |"""
                .stripMargin
                .map((rs: WrappedResultSet) => {
                    val organizationName = rs.string("organization_name")
                    val applicantType = rs.stringOpt("applicant_type").getOrElse("")
                    val noViolations = rs.intOpt("COMPLAINT_NO_VIOLATIONS").getOrElse(0)
                    val violations = rs.intOpt("COMPLAINT_VIOLATIONS").getOrElse(0)
                    val partlyValid = rs.intOpt("COMPLAINT_PARTLY_VALID").getOrElse(0)
                    val notMatched = rs.intOpt("COMPLAINT_NOT_MATCHED").getOrElse(0)
                    val empty = rs.intOpt("EMPTY").getOrElse(0)
                    val complaintSum = rs.intOpt("complaints_sum").getOrElse(0)
                    ApplicantStats(organizationName, applicantType, complaintSum, noViolations, violations,
                        partlyValid, notMatched, empty)
                })
                .list()
                .apply()
        }
    }

    def getApplicantStatsByYear(yearParam: String): Seq[ApplicantStats] = {
        Preconditions.checkArgument(yearParam.matches("\\d{4}"), "Incorrect year '%s' was specified", yearParam)
        val query =
            s"""
               |SELECT
               |  a.id,
               |  a.organization_name,
               |  a.applicant_type,
               |  res_all."COMPLAINT_NO_VIOLATIONS",
               |  res_all."COMPLAINT_VIOLATIONS",
               |  res_all."COMPLAINT_PARTLY_VALID",
               |  res_all."COMPLAINT_NOT_MATCHED",
               |  res_all.count complaints_sum,
               |  res_all."EMPTY"
               |FROM applicant a, (SELECT
               |                     res.applicant_id       appl_id_1,
               |                     all_count.applicant_id appl_id_2,
               |                     res."COMPLAINT_NO_VIOLATIONS",
               |                     res."COMPLAINT_VIOLATIONS",
               |                     res."COMPLAINT_PARTLY_VALID",
               |                     res."COMPLAINT_NOT_MATCHED",
               |                     res."EMPTY",
               |                     all_count.count
               |                   FROM (SELECT *
               |                         FROM crosstab('SELECT
               |                              subq.applicant_id,
               |                              subq.checkResult,
               |                              subq.complaints_count
               |                              FROM (SELECT
               |                                    c.applicant_id           applicant_id,
               |                                    COALESCE(ccr.result, '''') checkResult,
               |                                    count(*) complaints_count
               |                                  FROM complaint c, check_complaint_result ccr
               |                                  WHERE ccr.complaint_id = c.id
               |                                    AND date_trunc(''year'', ccr.create_date) = to_date(''$yearParam'', ''yyyy'')
               |                                  GROUP BY c.applicant_id, checkResult) subq
               |                              ORDER BY 1, 2',
               |                                       $$$$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$$$)
               |                           AS ct ("applicant_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) AS res
               |                     FULL OUTER JOIN (SELECT
               |                                        c.applicant_id        applicant_id,
               |                                        coalesce(count(*), 0) count
               |                                      FROM
               |                                        complaint c
               |                                      WHERE date_trunc('year', c.reg_date) = to_date('$yearParam', 'yyyy')
               |                                      GROUP BY c.applicant_id) all_count
               |                       ON (res.applicant_id = all_count.applicant_id)
               |                  ) res_all
               |WHERE a.id = coalesce(res_all.appl_id_1, res_all.appl_id_2)
               |ORDER BY complaints_sum DESC NULLS LAST
             """.stripMargin

        using(ConnectionPool.borrow()) { connection =>
            val prepareStatement = connection.prepareStatement(query)
            val rs: ResultSet = prepareStatement.executeQuery()

            (for ((_, r) <- Iterator.continually((rs.next(), rs)).takeWhile(_._1)) yield {
                val organizationName = r.getString("organization_name")
                val applicantType = r.getString("applicant_type")
                val noViolations = r.getInt("COMPLAINT_NO_VIOLATIONS")
                val violations = r.getInt("COMPLAINT_VIOLATIONS")
                val partlyValid = r.getInt("COMPLAINT_PARTLY_VALID")
                val notMatched = r.getInt("COMPLAINT_NOT_MATCHED")
                val empty = r.getInt("EMPTY")
                val complaintSum = r.getInt("complaints_sum")
                ApplicantStats(organizationName, applicantType, complaintSum, noViolations, violations,
                    partlyValid, notMatched, empty)
            }).toList
        }
    }

    def getClientsStatsByYear(yearParam: String): Seq[ClientStats] = {
        Preconditions.checkArgument(yearParam.matches("\\d{4}"), "Incorrect year '%s' was specified", yearParam)
        val query =
            s"""
               |SELECT
               |    c.id,
               |    c.full_name,
               |    c.client_type,
               |    res."COMPLAINT_NO_VIOLATIONS",
               |    res."COMPLAINT_VIOLATIONS",
               |    res."COMPLAINT_PARTLY_VALID",
               |    res."COMPLAINT_NOT_MATCHED",
               |    res.count complaints_sum,
               |    res."EMPTY"
               |FROM client c, (SELECT
               |                    res.indicted_client_id       client_id_1,
               |                    all_count.indicted_client_id client_id_2,
               |                    res."COMPLAINT_NO_VIOLATIONS",
               |                    res."COMPLAINT_VIOLATIONS",
               |                    res."COMPLAINT_PARTLY_VALID",
               |                    res."COMPLAINT_NOT_MATCHED",
               |                    res."EMPTY",
               |                    all_count.count
               |                FROM (SELECT *
               |                      FROM crosstab('SELECT
               |                              subq.indicted_client_id,
               |                              subq.checkResult,
               |                              subq.complaints_count
               |                              FROM (SELECT
               |                                    c.indicted_client_id           indicted_client_id,
               |                                    COALESCE(ccr.result, '''') checkResult,
               |                                    count(*) complaints_count
               |                                  FROM complaint c, check_complaint_result ccr
               |                                  WHERE ccr.complaint_id = c.id
               |                                    AND date_trunc(''year'', ccr.create_date) = to_date(''$yearParam'', ''yyyy'')
               |                                  GROUP BY c.indicted_client_id, checkResult) subq
               |                              ORDER BY 1, 2',
               |                                    $$$$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$$$)
               |                          AS ct ("indicted_client_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
               |                    FULL OUTER JOIN
               |                    (SELECT
               |                         c.indicted_client_id  indicted_client_id,
               |                         coalesce(count(*), 0) count
               |                     FROM
               |                         complaint c
               |                     WHERE date_trunc('year', c.reg_date) = to_date('$yearParam', 'yyyy')
               |                     GROUP BY c.indicted_client_id) all_count
               |                        ON (res.indicted_client_id = all_count.indicted_client_id)) res
               |WHERE c.id = coalesce(res.client_id_1, res.client_id_2)
               |ORDER BY complaints_sum DESC NULLS LAST
             """.stripMargin

        using(ConnectionPool.borrow()) { connection =>
            val prepareStatement = connection.prepareStatement(query)
            val rs: ResultSet = prepareStatement.executeQuery()

            (for ((_, r) <- Iterator.continually((rs.next(), rs)).takeWhile(_._1)) yield {
                val id = r.getLong("id")
                val fullName = r.getString("full_name")
                val clientType = r.getString("client_type")
                val noViolations = r.getInt("COMPLAINT_NO_VIOLATIONS")
                val violations = r.getInt("COMPLAINT_VIOLATIONS")
                val partlyValid = r.getInt("COMPLAINT_PARTLY_VALID")
                val notMatched = r.getInt("COMPLAINT_NOT_MATCHED")
                val empty = r.getInt("EMPTY")
                val complaintSum = r.getInt("complaints_sum")
                ClientStats(id, fullName, clientType, complaintSum, noViolations, violations,
                    partlyValid, notMatched, empty)
            }).toList
        }
    }

    def getClientsStats(fromDate: LocalDate,
                        toDate: LocalDate,
                        pageable: Pageable): (Long, Seq[ClientStats]) = {

        val query = StringBuilder.newBuilder
        query.append(
            """
              |SELECT
              |    c.id,
              |    c.full_name fullName,
              |    c.client_type,
              |    res."COMPLAINT_NO_VIOLATIONS" noViolations,
              |    res."COMPLAINT_VIOLATIONS" violations,
              |    res."COMPLAINT_PARTLY_VALID" partlyValid,
              |    res."COMPLAINT_NOT_MATCHED" notMatched,
              |    res.count complaintsCount,
              |    res."EMPTY" empty,
              |    count(*) OVER() AS full_count
              |FROM client c, (SELECT
              |                    res.indicted_client_id       client_id_1,
              |                    all_count.indicted_client_id client_id_2,
              |                    res."COMPLAINT_NO_VIOLATIONS",
              |                    res."COMPLAINT_VIOLATIONS",
              |                    res."COMPLAINT_PARTLY_VALID",
              |                    res."COMPLAINT_NOT_MATCHED",
              |                    res."EMPTY",
              |                    all_count.count
              |                FROM
              |                 (SELECT *
              |                     FROM crosstab('SELECT
              |                                     subq.indicted_client_id,
              |                                     subq.checkResult,
              |                                     subq.complaints_count
              |                                    FROM (SELECT
              |                                    c.indicted_client_id           indicted_client_id,
              |                                    COALESCE(ccr.result, '''') checkResult,
              |                                    count(*) complaints_count
              |                                  FROM complaint c, check_complaint_result ccr
              |                                  WHERE ccr.complaint_id = c.id
              |                                    """.stripMargin)
            .append({
                val sbnew = StringBuilder.newBuilder
                if (fromDate != null) {
                    sbnew.append(s" AND ccr.create_date >= ''$fromDate''")
                }
                if (toDate != null) {
                    sbnew.append(s" AND ccr.create_date <= ''$toDate''")
                }
                sbnew
            })
            .append(
                """
                  |                                  GROUP BY c.indicted_client_id, checkResult) subq
                  |                              ORDER BY 1, 2',
                  |                              $$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$)
                  |                    AS ct ("indicted_client_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
                  |    FULL OUTER JOIN (SELECT
                  |         c.indicted_client_id  indicted_client_id,
                  |         coalesce(count(*), 0) count
                  |     FROM
                  |         complaint c""".stripMargin)
            .append({
                val sbnew = StringBuilder.newBuilder
                if (fromDate != null) {
                    sbnew.append(s" WHERE c.reg_date >= '$fromDate'")
                }
                if (fromDate != null && toDate != null) {
                    sbnew.append(s" AND c.reg_date <= '$toDate'")
                } else if (toDate != null) {
                    sbnew.append(s" WHERE c.reg_date <= '$toDate'")
                }
                sbnew
            })
            .append(
                """
                  |     GROUP BY c.indicted_client_id) all_count
                  |          ON (res.indicted_client_id = all_count.indicted_client_id)) res
                  |          WHERE c.id = coalesce(res.client_id_1, res.client_id_2)""".stripMargin)
            .append({
                val sbnew = StringBuilder.newBuilder
                val iterator = pageable.getSort.iterator()
                val firstSort = iterator.next()
                firstSort.getNullHandling
                sbnew.append(s" ORDER BY ${firstSort.getProperty} ${firstSort.getDirection} NULLS LAST")
                iterator.forEachRemaining(order => {
                    sbnew.append(s", ${order.getProperty} ${order.getDirection}")
                })
                sbnew
            }).
            append(s" LIMIT ${pageable.getPageSize} OFFSET ${pageable.getOffset}")

        val queryResult = query.result()
        log.debug(s"Query: $queryResult")

        using(ConnectionPool.borrow()) { connection =>
            val prepareStatement = connection.prepareStatement(queryResult)
            val rs: ResultSet = prepareStatement.executeQuery()
            var full_count: Long = 0
            val result = (for ((_, r) <- Iterator.continually((rs.next(), rs)).takeWhile(_._1)) yield {
                if (full_count == 0) {
                    full_count = rs.getLong("full_count")
                }
                val id = r.getLong("id")
                val fullName = r.getString("fullName")
                val clientType = r.getString("client_type")
                val noViolations = r.getInt("noViolations")
                val violations = r.getInt("violations")
                val partlyValid = r.getInt("partlyValid")
                val notMatched = r.getInt("notMatched")
                val empty = r.getInt("empty")
                val complaintSum = r.getInt("complaintsCount")
                ClientStats(id, fullName, clientType, complaintSum, noViolations, violations,
                    partlyValid, notMatched, empty)
            }).toList
            (full_count, result)
        }
    }

    def getComplaintsResults(): Seq[ComplaintResult] = {
        DB readOnly { implicit session =>
            sql"""
                 |SELECT
                 |  subq.id,
                 |  subq.complaint_number,
                 |  subq.reg_date,
                 |  coalesce(subq.result, 'RETURNED') result
                 |FROM (
                 |       SELECT
                 |         c.id,
                 |         c.complaint_number,
                 |         c.reg_date,
                 |         ccr.result,
                 |         rc.return_info_base
                 |       FROM complaint c
                 |         LEFT OUTER JOIN check_complaint_result ccr ON (c.id = ccr.complaint_id)
                 |         LEFT OUTER JOIN returned_complaint rc ON (c.id = rc.complaint_id)) AS subq
                 |WHERE subq.result IS NOT NULL OR subq.return_info_base IS NOT NULL
                 |ORDER BY subq.reg_date NULLS LAST
                 """
                .stripMargin
                .map((rs: WrappedResultSet) => {
                    val id = rs.long("id")
                    val complaintNumber = rs.string("complaint_number")
                    val regDate = rs.jodaLocalDateOpt("reg_date").map(jodaLocalDate =>
                        LocalDate.of(jodaLocalDate.getYear, jodaLocalDate.getMonthOfYear, jodaLocalDate.getDayOfMonth)).orNull
                    val result = rs.stringOpt("result").getOrElse("")
                    ComplaintResult(id, complaintNumber, regDate, result)
                })
                .list()
                .apply()
        }
    }


    def getComplaintsStats(clientId: String, sort: Sort): Seq[ComplaintsYearStats] = {

        val query = StringBuilder.newBuilder

        query.append(
            """
              |SELECT
              |  to_char(res.result_date, '9999') result_date,
              |  res."COMPLAINT_NO_VIOLATIONS" noViolations,
              |  res."COMPLAINT_VIOLATIONS"    violations,
              |  res."COMPLAINT_PARTLY_VALID"  partlyValid,
              |  res."COMPLAINT_NOT_MATCHED"   notMatched,
              |  res.returned_count            returned_count,
              |  res.count                     complaints_sum,
              |  res."EMPTY"                   empty
              |FROM
              |  (SELECT
              |     coalesce(res.result_date, all_count.create_date) result_date,
              |     res."COMPLAINT_NO_VIOLATIONS",
              |     res."COMPLAINT_VIOLATIONS",
              |     res."COMPLAINT_PARTLY_VALID",
              |     res."COMPLAINT_NOT_MATCHED",
              |     res."EMPTY",
              |     all_count.count,
              |     res.returned_count
              |   FROM
              |     (SELECT
              |        coalesce(res.result_date, returned.create_date) result_date,
              |        res."COMPLAINT_NO_VIOLATIONS",
              |        res."COMPLAINT_VIOLATIONS",
              |        res."COMPLAINT_PARTLY_VALID",
              |        res."COMPLAINT_NOT_MATCHED",
              |        res."EMPTY",
              |        returned.returned_count
              |      FROM
              |        (SELECT *
              |      FROM crosstab('SELECT
              |                result_date,
              |                subq.checkResult,
              |                cnt
              |              FROM (SELECT
              |                      date_part(''year'', ccr.create_date) result_date,
              |                      COALESCE(ccr.result, '''')           checkResult,
              |                      count(*)                           cnt
              |                    FROM check_complaint_result ccr""".stripMargin)
            .append({
                val subq = StringBuilder.newBuilder
                if (StringUtils.isNotEmpty(clientId)) {
                    subq.append(s" where ccr.check_subject_id = $clientId")
                }
                subq
            })
            .append(
                """
                  |                    GROUP BY result_date, checkResult) subq
                  |              ORDER BY 1, 2',
                  |                    $$VALUES ('COMPLAINT_NO_VIOLATIONS'::TEXT), ('COMPLAINT_VIOLATIONS'::TEXT), ('COMPLAINT_PARTLY_VALID'::TEXT), ('COMPLAINT_NOT_MATCHED'::TEXT), (''::TEXT)$$)
                  |        AS ct ("result_date" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
                  |     FULL OUTER JOIN (SELECT
                  |                        date_part('year', c.reg_date) create_date,
                  |                        COALESCE(count(*), 0)         returned_count
                  |                      FROM
                  |                        complaint c, returned_complaint rc
                  |                      WHERE rc.complaint_id = c.id""".stripMargin)
            .append({
                val subq = StringBuilder.newBuilder
                if (StringUtils.isNotEmpty(clientId)) {
                    subq.append(s" and c.indicted_client_id = $clientId")
                }
                subq
            })
            .append(
                """
                  |                      GROUP BY create_date) returned ON (res.result_date = returned.create_date)) AS res
                  |     FULL OUTER JOIN
                  |     (SELECT
                  |        date_part('year', c.reg_date) create_date,
                  |        COALESCE(count(*), 0) count
                  |      FROM
                  |        complaint c""".stripMargin)
            .append({
                val subq = StringBuilder.newBuilder
                if (StringUtils.isNotEmpty(clientId)) {
                    subq.append(s" where c.indicted_client_id = $clientId")
                }
                subq
            })
            .append(
                """
                  |      GROUP BY create_date) all_count ON (res.result_date = all_count.create_date)) res
                  |ORDER BY result_date ASC NULLS LAST
                """.stripMargin)
        //            .append({
        //todo: add sorting
        //            })
        val queryResult = query.result()
        log.debug(s"Query: $queryResult")

        using(ConnectionPool.borrow()) { connection =>
            val prepareStatement = connection.prepareStatement(queryResult)
            val rs: ResultSet = prepareStatement.executeQuery()
            (for ((_, r) <- Iterator.continually((rs.next(), rs)).takeWhile(_._1)) yield {
                val year = r.getString("result_date")
                val noViolations = r.getInt("noViolations")
                val violations = r.getInt("violations")
                val partlyValid = r.getInt("partlyValid")
                val notMatched = r.getInt("notMatched")
                val empty = r.getInt("empty")
                val complaintSum = r.getInt("complaints_sum")
                val returnedCount = r.getInt("returned_count")
                ComplaintsYearStats(year, complaintSum, noViolations, violations, partlyValid, notMatched, returnedCount)
            }).toList
        }
    }

}
