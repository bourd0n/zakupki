import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('ComplaintCancel e2e test', () => {

    let navBarPage: NavBarPage;
    let complaintCancelDialogPage: ComplaintCancelDialogPage;
    let complaintCancelComponentsPage: ComplaintCancelComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ComplaintCancels', () => {
        navBarPage.goToEntity('complaint-cancel');
        complaintCancelComponentsPage = new ComplaintCancelComponentsPage();
        expect(complaintCancelComponentsPage.getTitle()).toMatch(/zakupkiApp.complaintCancel.home.title/);

    });

    it('should load create ComplaintCancel dialog', () => {
        complaintCancelComponentsPage.clickOnCreateButton();
        complaintCancelDialogPage = new ComplaintCancelDialogPage();
        expect(complaintCancelDialogPage.getModalTitle()).toMatch(/zakupkiApp.complaintCancel.home.createOrEditLabel/);
        complaintCancelDialogPage.close();
    });

    it('should create and save ComplaintCancels', () => {
        complaintCancelComponentsPage.clickOnCreateButton();
        complaintCancelDialogPage.setComplaintNumberInput('complaintNumber');
        expect(complaintCancelDialogPage.getComplaintNumberInput()).toMatch('complaintNumber');
        complaintCancelDialogPage.setRegDateInput('2000-12-31');
        expect(complaintCancelDialogPage.getRegDateInput()).toMatch('2000-12-31');
        complaintCancelDialogPage.setTextInput('text');
        expect(complaintCancelDialogPage.getTextInput()).toMatch('text');
        complaintCancelDialogPage.registrationKoSelectLastOption();
        complaintCancelDialogPage.save();
        expect(complaintCancelDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ComplaintCancelComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-complaint-cancel div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ComplaintCancelDialogPage {
    modalTitle = element(by.css('h4#myComplaintCancelLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    complaintNumberInput = element(by.css('input#field_complaintNumber'));
    regDateInput = element(by.css('input#field_regDate'));
    textInput = element(by.css('input#field_text'));
    registrationKoSelect = element(by.css('select#field_registrationKo'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setComplaintNumberInput = function (complaintNumber) {
        this.complaintNumberInput.sendKeys(complaintNumber);
    }

    getComplaintNumberInput = function () {
        return this.complaintNumberInput.getAttribute('value');
    }

    setRegDateInput = function (regDate) {
        this.regDateInput.sendKeys(regDate);
    }

    getRegDateInput = function () {
        return this.regDateInput.getAttribute('value');
    }

    setTextInput = function (text) {
        this.textInput.sendKeys(text);
    }

    getTextInput = function () {
        return this.textInput.getAttribute('value');
    }

    registrationKoSelectLastOption = function () {
        this.registrationKoSelect.all(by.tagName('option')).last().click();
    }

    registrationKoSelectOption = function (option) {
        this.registrationKoSelect.sendKeys(option);
    }

    getRegistrationKoSelect = function () {
        return this.registrationKoSelect;
    }

    getRegistrationKoSelectedOption = function () {
        return this.registrationKoSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
