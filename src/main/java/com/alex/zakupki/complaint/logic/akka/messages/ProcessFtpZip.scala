package com.alex.zakupki.complaint.logic.akka.messages

import com.alex.zakupki.complaint.logic.ftp.FTPTypes.FTPType
import org.apache.commons.net.ftp.FTPFile

case class ProcessFtpZip(fTPFile: FTPFile, fTPType: FTPType)
