import {Component, OnInit, OnDestroy} from '@angular/core';
import {DataSyncService} from './data-sync.service';
import {Response, Headers} from '@angular/http';
import {SyncInfo} from './sync-info.model';
import {JhiAlertService, JhiLanguageService} from 'ng-jhipster';

@Component({
    selector: 'jhi-data-sync',
    templateUrl: './data-sync.component.html'
})
export class DataSyncComponent implements OnInit, OnDestroy {

    syncInfos: SyncInfo[];

    constructor(private dataSyncService: DataSyncService,
                private alertService: JhiAlertService,
                private jhiLanguageService: JhiLanguageService) {
        // this.jhiLanguageService.setLocations(['data-sync']);
    }

    ngOnInit(): void {
        this.refresh();
    }

    ngOnDestroy(): void {
    }

    refresh() {
        this.dataSyncService.getSyncInfos().subscribe(
            (res: Response) => this.onSuccess(res.json(), res.headers),
            (res: Response) => this.onError(res.json())
        );
    }

    getLabelClass(statusState) {
        if (statusState === 'sync') {
            return 'badge-success';
        } else {
            return 'badge-danger';
        }
    }

    performSync(syncInfo) {
        this.dataSyncService.performSync(syncInfo)
            .subscribe((res: SyncInfo) => {
                this.refresh();
            });
    }

    trackName(index: number, item: SyncInfo) {
        return item.name;
    }


    private onSuccess(data: any, headers: Headers) {
        this.syncInfos = data;
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
