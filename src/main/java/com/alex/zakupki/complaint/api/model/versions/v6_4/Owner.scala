package com.alex.zakupki.complaint.api.model.versions.v6_4

import javax.xml.bind.annotation.{XmlAccessType, XmlType}

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}

//@Entity
//@Table(name = "owners")
@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "zfcs_organizationInfoType", namespace = "http://www.w3.org/2001/XMLSchema-instance", propOrder = Array("regNum", "fullName"))
case class Owner(@xmlPath("regNum/text()")
                 regNum: String,
                 @xmlPath("fullName/text()")
                 fullName: String) extends GeneralizedEntity[general.Owner] {
    private def this() = this(null, null)

    override def mapToGeneral(): general.Owner = general.Owner(
        regNum,
        fullName.trim
    )
}
