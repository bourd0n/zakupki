package com.alex.zakupki.service.dto

case class ClientStats(id: Long,
                       fullName: String,
                       clientType: String,
                       complaintsCount: Int,
                       noViolations: Int,
                       violations: Int,
                       partlyValid: Int,
                       notMatched: Int,
                       empty: Int)
