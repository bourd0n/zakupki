package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.Purchase;
import com.alex.zakupki.repository.PurchaseRepository;
import com.alex.zakupki.service.PurchaseService;
import com.alex.zakupki.repository.search.PurchaseSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PurchaseResource REST controller.
 *
 * @see PurchaseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class PurchaseResourceIntTest {

    private static final String DEFAULT_PURCHASE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_PURCHASE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_DETERM_SUPPLIER_METHOD = "AAAAAAAAAA";
    private static final String UPDATED_DETERM_SUPPLIER_METHOD = "BBBBBBBBBB";

    private static final String DEFAULT_ELECTRONIC_PLATFORM = "AAAAAAAAAA";
    private static final String UPDATED_ELECTRONIC_PLATFORM = "BBBBBBBBBB";

    private static final Double DEFAULT_MAX_COST = 1D;
    private static final Double UPDATED_MAX_COST = 2D;

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PurchaseSearchRepository purchaseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPurchaseMockMvc;

    private Purchase purchase;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PurchaseResource purchaseResource = new PurchaseResource(purchaseService);
        this.restPurchaseMockMvc = MockMvcBuilders.standaloneSetup(purchaseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Purchase createEntity(EntityManager em) {
        Purchase purchase = new Purchase()
            .purchaseNumber(DEFAULT_PURCHASE_NUMBER)
            .determSupplierMethod(DEFAULT_DETERM_SUPPLIER_METHOD)
            .electronicPlatform(DEFAULT_ELECTRONIC_PLATFORM)
            .maxCost(DEFAULT_MAX_COST);
        return purchase;
    }

    @Before
    public void initTest() {
        purchaseSearchRepository.deleteAll();
        purchase = createEntity(em);
    }

    @Test
    @Transactional
    public void createPurchase() throws Exception {
        int databaseSizeBeforeCreate = purchaseRepository.findAll().size();

        // Create the Purchase
        restPurchaseMockMvc.perform(post("/api/purchases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(purchase)))
            .andExpect(status().isCreated());

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll();
        assertThat(purchaseList).hasSize(databaseSizeBeforeCreate + 1);
        Purchase testPurchase = purchaseList.get(purchaseList.size() - 1);
        assertThat(testPurchase.getPurchaseNumber()).isEqualTo(DEFAULT_PURCHASE_NUMBER);
        assertThat(testPurchase.getDetermSupplierMethod()).isEqualTo(DEFAULT_DETERM_SUPPLIER_METHOD);
        assertThat(testPurchase.getElectronicPlatform()).isEqualTo(DEFAULT_ELECTRONIC_PLATFORM);
        assertThat(testPurchase.getMaxCost()).isEqualTo(DEFAULT_MAX_COST);

        // Validate the Purchase in Elasticsearch
        Purchase purchaseEs = purchaseSearchRepository.findOne(testPurchase.getId());
        assertThat(purchaseEs).isEqualToComparingFieldByField(testPurchase);
    }

    @Test
    @Transactional
    public void createPurchaseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = purchaseRepository.findAll().size();

        // Create the Purchase with an existing ID
        purchase.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPurchaseMockMvc.perform(post("/api/purchases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(purchase)))
            .andExpect(status().isBadRequest());

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll();
        assertThat(purchaseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPurchaseNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchaseRepository.findAll().size();
        // set the field null
        purchase.setPurchaseNumber(null);

        // Create the Purchase, which fails.

        restPurchaseMockMvc.perform(post("/api/purchases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(purchase)))
            .andExpect(status().isBadRequest());

        List<Purchase> purchaseList = purchaseRepository.findAll();
        assertThat(purchaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPurchases() throws Exception {
        // Initialize the database
        purchaseRepository.saveAndFlush(purchase);

        // Get all the purchaseList
        restPurchaseMockMvc.perform(get("/api/purchases?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purchase.getId().intValue())))
            .andExpect(jsonPath("$.[*].purchaseNumber").value(hasItem(DEFAULT_PURCHASE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].determSupplierMethod").value(hasItem(DEFAULT_DETERM_SUPPLIER_METHOD.toString())))
            .andExpect(jsonPath("$.[*].electronicPlatform").value(hasItem(DEFAULT_ELECTRONIC_PLATFORM.toString())))
            .andExpect(jsonPath("$.[*].maxCost").value(hasItem(DEFAULT_MAX_COST.doubleValue())));
    }

    @Test
    @Transactional
    public void getPurchase() throws Exception {
        // Initialize the database
        purchaseRepository.saveAndFlush(purchase);

        // Get the purchase
        restPurchaseMockMvc.perform(get("/api/purchases/{id}", purchase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(purchase.getId().intValue()))
            .andExpect(jsonPath("$.purchaseNumber").value(DEFAULT_PURCHASE_NUMBER.toString()))
            .andExpect(jsonPath("$.determSupplierMethod").value(DEFAULT_DETERM_SUPPLIER_METHOD.toString()))
            .andExpect(jsonPath("$.electronicPlatform").value(DEFAULT_ELECTRONIC_PLATFORM.toString()))
            .andExpect(jsonPath("$.maxCost").value(DEFAULT_MAX_COST.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPurchase() throws Exception {
        // Get the purchase
        restPurchaseMockMvc.perform(get("/api/purchases/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePurchase() throws Exception {
        // Initialize the database
        purchaseService.save(purchase);

        int databaseSizeBeforeUpdate = purchaseRepository.findAll().size();

        // Update the purchase
        Purchase updatedPurchase = purchaseRepository.findOne(purchase.getId());
        updatedPurchase
            .purchaseNumber(UPDATED_PURCHASE_NUMBER)
            .determSupplierMethod(UPDATED_DETERM_SUPPLIER_METHOD)
            .electronicPlatform(UPDATED_ELECTRONIC_PLATFORM)
            .maxCost(UPDATED_MAX_COST);

        restPurchaseMockMvc.perform(put("/api/purchases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPurchase)))
            .andExpect(status().isOk());

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate);
        Purchase testPurchase = purchaseList.get(purchaseList.size() - 1);
        assertThat(testPurchase.getPurchaseNumber()).isEqualTo(UPDATED_PURCHASE_NUMBER);
        assertThat(testPurchase.getDetermSupplierMethod()).isEqualTo(UPDATED_DETERM_SUPPLIER_METHOD);
        assertThat(testPurchase.getElectronicPlatform()).isEqualTo(UPDATED_ELECTRONIC_PLATFORM);
        assertThat(testPurchase.getMaxCost()).isEqualTo(UPDATED_MAX_COST);

        // Validate the Purchase in Elasticsearch
        Purchase purchaseEs = purchaseSearchRepository.findOne(testPurchase.getId());
        assertThat(purchaseEs).isEqualToComparingFieldByField(testPurchase);
    }

    @Test
    @Transactional
    public void updateNonExistingPurchase() throws Exception {
        int databaseSizeBeforeUpdate = purchaseRepository.findAll().size();

        // Create the Purchase

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPurchaseMockMvc.perform(put("/api/purchases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(purchase)))
            .andExpect(status().isCreated());

        // Validate the Purchase in the database
        List<Purchase> purchaseList = purchaseRepository.findAll();
        assertThat(purchaseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePurchase() throws Exception {
        // Initialize the database
        purchaseService.save(purchase);

        int databaseSizeBeforeDelete = purchaseRepository.findAll().size();

        // Get the purchase
        restPurchaseMockMvc.perform(delete("/api/purchases/{id}", purchase.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean purchaseExistsInEs = purchaseSearchRepository.exists(purchase.getId());
        assertThat(purchaseExistsInEs).isFalse();

        // Validate the database is empty
        List<Purchase> purchaseList = purchaseRepository.findAll();
        assertThat(purchaseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPurchase() throws Exception {
        // Initialize the database
        purchaseService.save(purchase);

        // Search the purchase
        restPurchaseMockMvc.perform(get("/api/_search/purchases?query=id:" + purchase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purchase.getId().intValue())))
            .andExpect(jsonPath("$.[*].purchaseNumber").value(hasItem(DEFAULT_PURCHASE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].determSupplierMethod").value(hasItem(DEFAULT_DETERM_SUPPLIER_METHOD.toString())))
            .andExpect(jsonPath("$.[*].electronicPlatform").value(hasItem(DEFAULT_ELECTRONIC_PLATFORM.toString())))
            .andExpect(jsonPath("$.[*].maxCost").value(hasItem(DEFAULT_MAX_COST.doubleValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Purchase.class);
        Purchase purchase1 = new Purchase();
        purchase1.setId(1L);
        Purchase purchase2 = new Purchase();
        purchase2.setId(purchase1.getId());
        assertThat(purchase1).isEqualTo(purchase2);
        purchase2.setId(2L);
        assertThat(purchase1).isNotEqualTo(purchase2);
        purchase1.setId(null);
        assertThat(purchase1).isNotEqualTo(purchase2);
    }
}
