package com.alex.zakupki.complaint.logic.akka.actors

import akka.actor.Actor
import com.alex.zakupki.complaint.logic.akka.messages.ProcessDownloadedZip
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component(ZipProcessorActor.NAME)
@Scope("prototype")
class ZipProcessorActor @Autowired()(xmlProcessor: XMLProcessor) extends Actor {

    private val logger : Logger = LoggerFactory.getLogger(getClass)

    override def receive: Receive = {
        case ProcessDownloadedZip(zipFilePath) =>
            logger.debug(s"${self.path.name} start process file $zipFilePath")
            xmlProcessor.processFile(zipFilePath)
            logger.debug(s"${self.path.name} finish process file $zipFilePath")
    }
}

object ZipProcessorActor {
    final val NAME = "zipProcessorActor"
}
