import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { UnfairSupplier } from './unfair-supplier.model';
import { UnfairSupplierService } from './unfair-supplier.service';

@Injectable()
export class UnfairSupplierPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private unfairSupplierService: UnfairSupplierService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.unfairSupplierService.find(id).subscribe((unfairSupplier) => {
                    if (unfairSupplier.publishDate) {
                        unfairSupplier.publishDate = {
                            year: unfairSupplier.publishDate.getFullYear(),
                            month: unfairSupplier.publishDate.getMonth() + 1,
                            day: unfairSupplier.publishDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.unfairSupplierModalRef(component, unfairSupplier);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.unfairSupplierModalRef(component, new UnfairSupplier());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    unfairSupplierModalRef(component: Component, unfairSupplier: UnfairSupplier): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.unfairSupplier = unfairSupplier;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
