package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.PublishOrg;
import com.alex.zakupki.repository.PublishOrgRepository;
import com.alex.zakupki.service.PublishOrgService;
import com.alex.zakupki.repository.search.PublishOrgSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PublishOrgResource REST controller.
 *
 * @see PublishOrgResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class PublishOrgResourceIntTest {

    private static final String DEFAULT_REG_NUM = "AAAAAAAAAA";
    private static final String UPDATED_REG_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    @Autowired
    private PublishOrgRepository publishOrgRepository;

    @Autowired
    private PublishOrgService publishOrgService;

    @Autowired
    private PublishOrgSearchRepository publishOrgSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPublishOrgMockMvc;

    private PublishOrg publishOrg;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PublishOrgResource publishOrgResource = new PublishOrgResource(publishOrgService);
        this.restPublishOrgMockMvc = MockMvcBuilders.standaloneSetup(publishOrgResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PublishOrg createEntity(EntityManager em) {
        PublishOrg publishOrg = new PublishOrg()
            .regNum(DEFAULT_REG_NUM)
            .fullName(DEFAULT_FULL_NAME);
        return publishOrg;
    }

    @Before
    public void initTest() {
        publishOrgSearchRepository.deleteAll();
        publishOrg = createEntity(em);
    }

    @Test
    @Transactional
    public void createPublishOrg() throws Exception {
        int databaseSizeBeforeCreate = publishOrgRepository.findAll().size();

        // Create the PublishOrg
        restPublishOrgMockMvc.perform(post("/api/publish-orgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publishOrg)))
            .andExpect(status().isCreated());

        // Validate the PublishOrg in the database
        List<PublishOrg> publishOrgList = publishOrgRepository.findAll();
        assertThat(publishOrgList).hasSize(databaseSizeBeforeCreate + 1);
        PublishOrg testPublishOrg = publishOrgList.get(publishOrgList.size() - 1);
        assertThat(testPublishOrg.getRegNum()).isEqualTo(DEFAULT_REG_NUM);
        assertThat(testPublishOrg.getFullName()).isEqualTo(DEFAULT_FULL_NAME);

        // Validate the PublishOrg in Elasticsearch
        PublishOrg publishOrgEs = publishOrgSearchRepository.findOne(testPublishOrg.getId());
        assertThat(publishOrgEs).isEqualToComparingFieldByField(testPublishOrg);
    }

    @Test
    @Transactional
    public void createPublishOrgWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = publishOrgRepository.findAll().size();

        // Create the PublishOrg with an existing ID
        publishOrg.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPublishOrgMockMvc.perform(post("/api/publish-orgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publishOrg)))
            .andExpect(status().isBadRequest());

        // Validate the PublishOrg in the database
        List<PublishOrg> publishOrgList = publishOrgRepository.findAll();
        assertThat(publishOrgList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkRegNumIsRequired() throws Exception {
        int databaseSizeBeforeTest = publishOrgRepository.findAll().size();
        // set the field null
        publishOrg.setRegNum(null);

        // Create the PublishOrg, which fails.

        restPublishOrgMockMvc.perform(post("/api/publish-orgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publishOrg)))
            .andExpect(status().isBadRequest());

        List<PublishOrg> publishOrgList = publishOrgRepository.findAll();
        assertThat(publishOrgList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPublishOrgs() throws Exception {
        // Initialize the database
        publishOrgRepository.saveAndFlush(publishOrg);

        // Get all the publishOrgList
        restPublishOrgMockMvc.perform(get("/api/publish-orgs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(publishOrg.getId().intValue())))
            .andExpect(jsonPath("$.[*].regNum").value(hasItem(DEFAULT_REG_NUM.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())));
    }

    @Test
    @Transactional
    public void getPublishOrg() throws Exception {
        // Initialize the database
        publishOrgRepository.saveAndFlush(publishOrg);

        // Get the publishOrg
        restPublishOrgMockMvc.perform(get("/api/publish-orgs/{id}", publishOrg.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(publishOrg.getId().intValue()))
            .andExpect(jsonPath("$.regNum").value(DEFAULT_REG_NUM.toString()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPublishOrg() throws Exception {
        // Get the publishOrg
        restPublishOrgMockMvc.perform(get("/api/publish-orgs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePublishOrg() throws Exception {
        // Initialize the database
        publishOrgService.save(publishOrg);

        int databaseSizeBeforeUpdate = publishOrgRepository.findAll().size();

        // Update the publishOrg
        PublishOrg updatedPublishOrg = publishOrgRepository.findOne(publishOrg.getId());
        updatedPublishOrg
            .regNum(UPDATED_REG_NUM)
            .fullName(UPDATED_FULL_NAME);

        restPublishOrgMockMvc.perform(put("/api/publish-orgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPublishOrg)))
            .andExpect(status().isOk());

        // Validate the PublishOrg in the database
        List<PublishOrg> publishOrgList = publishOrgRepository.findAll();
        assertThat(publishOrgList).hasSize(databaseSizeBeforeUpdate);
        PublishOrg testPublishOrg = publishOrgList.get(publishOrgList.size() - 1);
        assertThat(testPublishOrg.getRegNum()).isEqualTo(UPDATED_REG_NUM);
        assertThat(testPublishOrg.getFullName()).isEqualTo(UPDATED_FULL_NAME);

        // Validate the PublishOrg in Elasticsearch
        PublishOrg publishOrgEs = publishOrgSearchRepository.findOne(testPublishOrg.getId());
        assertThat(publishOrgEs).isEqualToComparingFieldByField(testPublishOrg);
    }

    @Test
    @Transactional
    public void updateNonExistingPublishOrg() throws Exception {
        int databaseSizeBeforeUpdate = publishOrgRepository.findAll().size();

        // Create the PublishOrg

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPublishOrgMockMvc.perform(put("/api/publish-orgs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(publishOrg)))
            .andExpect(status().isCreated());

        // Validate the PublishOrg in the database
        List<PublishOrg> publishOrgList = publishOrgRepository.findAll();
        assertThat(publishOrgList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePublishOrg() throws Exception {
        // Initialize the database
        publishOrgService.save(publishOrg);

        int databaseSizeBeforeDelete = publishOrgRepository.findAll().size();

        // Get the publishOrg
        restPublishOrgMockMvc.perform(delete("/api/publish-orgs/{id}", publishOrg.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean publishOrgExistsInEs = publishOrgSearchRepository.exists(publishOrg.getId());
        assertThat(publishOrgExistsInEs).isFalse();

        // Validate the database is empty
        List<PublishOrg> publishOrgList = publishOrgRepository.findAll();
        assertThat(publishOrgList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPublishOrg() throws Exception {
        // Initialize the database
        publishOrgService.save(publishOrg);

        // Search the publishOrg
        restPublishOrgMockMvc.perform(get("/api/_search/publish-orgs?query=id:" + publishOrg.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(publishOrg.getId().intValue())))
            .andExpect(jsonPath("$.[*].regNum").value(hasItem(DEFAULT_REG_NUM.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PublishOrg.class);
        PublishOrg publishOrg1 = new PublishOrg();
        publishOrg1.setId(1L);
        PublishOrg publishOrg2 = new PublishOrg();
        publishOrg2.setId(publishOrg1.getId());
        assertThat(publishOrg1).isEqualTo(publishOrg2);
        publishOrg2.setId(2L);
        assertThat(publishOrg1).isNotEqualTo(publishOrg2);
        publishOrg1.setId(null);
        assertThat(publishOrg1).isNotEqualTo(publishOrg2);
    }
}
