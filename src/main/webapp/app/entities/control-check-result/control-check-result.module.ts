import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    ControlCheckResultService,
    ControlCheckResultPopupService,
    ControlCheckResultComponent,
    ControlCheckResultDetailComponent,
    ControlCheckResultDialogComponent,
    ControlCheckResultPopupComponent,
    ControlCheckResultDeletePopupComponent,
    ControlCheckResultDeleteDialogComponent,
    controlCheckResultRoute,
    controlCheckResultPopupRoute,
} from './';

const ENTITY_STATES = [
    ...controlCheckResultRoute,
    ...controlCheckResultPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ControlCheckResultComponent,
        ControlCheckResultDetailComponent,
        ControlCheckResultDialogComponent,
        ControlCheckResultDeleteDialogComponent,
        ControlCheckResultPopupComponent,
        ControlCheckResultDeletePopupComponent,
    ],
    entryComponents: [
        ControlCheckResultComponent,
        ControlCheckResultDialogComponent,
        ControlCheckResultPopupComponent,
        ControlCheckResultDeleteDialogComponent,
        ControlCheckResultDeletePopupComponent,
    ],
    providers: [
        ControlCheckResultService,
        ControlCheckResultPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiControlCheckResultModule {}
