package com.alex.zakupki.complaint.web.controller

import com.alex.zakupki.complaint.dao.ComplaintsDao
import com.alex.zakupki.complaint.logic.RefreshData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import org.springframework.web.servlet.ModelAndView

//@Controller
@deprecated
class DataRefreshController @Autowired() (val complaintsDao : ComplaintsDao, val refresh : RefreshData) {
//  @RequestMapping(value = Array("/refreshData"), method = Array(RequestMethod.POST))
  def refreshData: ModelAndView = {
    refresh.refresh()
    val model: ModelAndView = new ModelAndView("index")
    model.addObject("complaintNum", complaintsDao.countAllComplaints())
    model.addObject("resultOfUpdate", "Success")
    model
  }
}
