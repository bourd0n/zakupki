import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ComplaintCancel } from './complaint-cancel.model';
import { ComplaintCancelPopupService } from './complaint-cancel-popup.service';
import { ComplaintCancelService } from './complaint-cancel.service';
import { RegistrationKo, RegistrationKoService } from '../registration-ko';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-complaint-cancel-dialog',
    templateUrl: './complaint-cancel-dialog.component.html'
})
export class ComplaintCancelDialogComponent implements OnInit {

    complaintCancel: ComplaintCancel;
    isSaving: boolean;

    registrationkos: RegistrationKo[];
    regDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private complaintCancelService: ComplaintCancelService,
        private registrationKoService: RegistrationKoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.registrationKoService.query()
            .subscribe((res: ResponseWrapper) => { this.registrationkos = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.complaintCancel.id !== undefined) {
            this.subscribeToSaveResponse(
                this.complaintCancelService.update(this.complaintCancel));
        } else {
            this.subscribeToSaveResponse(
                this.complaintCancelService.create(this.complaintCancel));
        }
    }

    private subscribeToSaveResponse(result: Observable<ComplaintCancel>) {
        result.subscribe((res: ComplaintCancel) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ComplaintCancel) {
        this.eventManager.broadcast({ name: 'complaintCancelListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRegistrationKoById(index: number, item: RegistrationKo) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-complaint-cancel-popup',
    template: ''
})
export class ComplaintCancelPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private complaintCancelPopupService: ComplaintCancelPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.complaintCancelPopupService
                    .open(ComplaintCancelDialogComponent as Component, params['id']);
            } else {
                this.complaintCancelPopupService
                    .open(ComplaintCancelDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
