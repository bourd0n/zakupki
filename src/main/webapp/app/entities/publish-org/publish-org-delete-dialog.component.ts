import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PublishOrg } from './publish-org.model';
import { PublishOrgPopupService } from './publish-org-popup.service';
import { PublishOrgService } from './publish-org.service';

@Component({
    selector: 'jhi-publish-org-delete-dialog',
    templateUrl: './publish-org-delete-dialog.component.html'
})
export class PublishOrgDeleteDialogComponent {

    publishOrg: PublishOrg;

    constructor(
        private publishOrgService: PublishOrgService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.publishOrgService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'publishOrgListModification',
                content: 'Deleted an publishOrg'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-publish-org-delete-popup',
    template: ''
})
export class PublishOrgDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private publishOrgPopupService: PublishOrgPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.publishOrgPopupService
                .open(PublishOrgDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
