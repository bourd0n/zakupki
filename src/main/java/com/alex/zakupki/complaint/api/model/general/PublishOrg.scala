package com.alex.zakupki.complaint.api.model.general

case class PublishOrg(regNum: String, fullName: String)
