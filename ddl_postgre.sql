
    alter table check_complaint_results 
        drop constraint if exists FK_3go46qky36ylbu9cy9qckmt8;

    alter table check_complaint_results 
        drop constraint if exists FK_2f31doq164wreywabos71b9s1;

    alter table complaints 
        drop constraint if exists FK_hf8c3l5pdmcmtmgvat7nn6jlp;

    alter table complaints 
        drop constraint if exists FK_ahk1d1hm972m4x7a7nwwpao1e;

    alter table complaints 
        drop constraint if exists FK_2oai1b35ov3uxmpbm91w44v2l;

    alter table complaints 
        drop constraint if exists FK_2t0rgl1368cnx2j3dfo0c2a0g;

    alter table complaints_cancel 
        drop constraint if exists FK_p9d6by14qoiey1qd24qkfv2s2;

    alter table control_check_results 
        drop constraint if exists FK_4hhfdb5o1tuc3i981pnp9ur3t;

    alter table control_check_results 
        drop constraint if exists FK_bqwuj4aa49gc8pjs4w6i3lufo;

    alter table planned_check_results 
        drop constraint if exists FK_fnkjvnyk7kej4ly5d33dsj3qs;

    alter table planned_check_results 
        drop constraint if exists FK_jrhy8ot83j422q7ffdbkpx42h;

    alter table unfair_suppliers 
        drop constraint if exists FK_h0fkww2qunm83wog6dyjpy9db;

    alter table unplanned_check_results 
        drop constraint if exists FK_kqwhooj6tfaq56tkqupjfelfm;

    alter table unplanned_check_results 
        drop constraint if exists FK_pmjd4dopavvrqnevbadk2e26l;

    drop table applicants cascade;

    drop table check_complaint_results cascade;

    drop table clients cascade;

    drop table complaints cascade;

    drop table complaints_cancel cascade;

    drop table control_check_results cascade;

    drop table owners cascade;

    drop table planned_check_results cascade;

    drop table publish_org cascade;

    drop table purchases cascade;

    drop table registration_ko cascade;

    drop table tender_suspensions cascade;

    drop table unfair_suppliers cascade;

    drop table unplanned_check_results cascade;

    create table applicants (
        organizationName varchar(255) not null,
        applicantType varchar(255),
        primary key (organizationName)
    );

    create table check_complaint_results (
        checkResultNumber varchar(255) not null,
        createDate timestamp,
        result varchar(255),
        indicted_id varchar(255),
        owner_id varchar(255),
        primary key (checkResultNumber)
    );

    create table clients (
        regNum varchar(255) not null,
        clientType varchar(255),
        fullName varchar(4000),
        primary key (regNum)
    );

    create table complaints (
        complaintNumber varchar(255) not null,
        regDate timestamp,
        text varchar(4000),
        applicant_id varchar(255),
        indicted_client_id varchar(255),
        purchase_id varchar(255),
        registration_ko_id varchar(255),
        primary key (complaintNumber)
    );

    create table complaints_cancel (
        complaintNumber varchar(255) not null,
        regDate timestamp,
        text varchar(4000),
        registration_ko_id varchar(255),
        primary key (complaintNumber)
    );

    create table control_check_results (
        checkResultNumber varchar(255) not null,
        createDate timestamp,
        result varchar(255),
        subject_client_id varchar(255),
        owner_id varchar(255),
        primary key (checkResultNumber)
    );

    create table owners (
        regNum varchar(255) not null,
        fullName varchar(255),
        primary key (regNum)
    );

    create table planned_check_results (
        checkResultNumber varchar(255) not null,
        actDate timestamp,
        actPrescriptionDate timestamp,
        createDate timestamp,
        subject_customer_id varchar(255),
        owner_id varchar(255),
        primary key (checkResultNumber)
    );

    create table publish_org (
        regNum varchar(255) not null,
        fullName varchar(255),
        primary key (regNum)
    );

    create table purchases (
        purchaseNumber varchar(255) not null,
        determSupplierMethod varchar(255),
        electronicPlatform varchar(255),
        maxCost float8 not null,
        primary key (purchaseNumber)
    );

    create table registration_ko (
        regNum varchar(255) not null,
        fullName varchar(255),
        primary key (regNum)
    );

    create table tender_suspensions (
        id int4 not null,
        action varchar(255),
        complaintNumber varchar(255),
        regDate timestamp,
        primary key (id)
    );

    create table unfair_suppliers (
        registryNum varchar(255) not null,
        fullName varchar(255),
        inn varchar(255),
        publishDate timestamp,
        purchaseNumber varchar(255),
        reason varchar(255),
        state varchar(255),
        publish_org_id varchar(255),
        primary key (registryNum)
    );

    create table unplanned_check_results (
        checkResultNumber varchar(255) not null,
        createDate timestamp,
        result varchar(255),
        subject_client_id varchar(255),
        owner_id varchar(255),
        primary key (checkResultNumber)
    );

    alter table check_complaint_results 
        add constraint FK_3go46qky36ylbu9cy9qckmt8 
        foreign key (indicted_id) 
        references clients;

    alter table check_complaint_results 
        add constraint FK_2f31doq164wreywabos71b9s1 
        foreign key (owner_id) 
        references owners;

    alter table complaints 
        add constraint FK_hf8c3l5pdmcmtmgvat7nn6jlp 
        foreign key (applicant_id) 
        references applicants;

    alter table complaints 
        add constraint FK_ahk1d1hm972m4x7a7nwwpao1e 
        foreign key (indicted_client_id) 
        references clients;

    alter table complaints 
        add constraint FK_2oai1b35ov3uxmpbm91w44v2l 
        foreign key (purchase_id) 
        references purchases;

    alter table complaints 
        add constraint FK_2t0rgl1368cnx2j3dfo0c2a0g 
        foreign key (registration_ko_id) 
        references registration_ko;

    alter table complaints_cancel 
        add constraint FK_p9d6by14qoiey1qd24qkfv2s2 
        foreign key (registration_ko_id) 
        references registration_ko;

    alter table control_check_results 
        add constraint FK_4hhfdb5o1tuc3i981pnp9ur3t 
        foreign key (subject_client_id) 
        references clients;

    alter table control_check_results 
        add constraint FK_bqwuj4aa49gc8pjs4w6i3lufo 
        foreign key (owner_id) 
        references owners;

    alter table planned_check_results 
        add constraint FK_fnkjvnyk7kej4ly5d33dsj3qs 
        foreign key (subject_customer_id) 
        references clients;

    alter table planned_check_results 
        add constraint FK_jrhy8ot83j422q7ffdbkpx42h 
        foreign key (owner_id) 
        references owners;

    alter table unfair_suppliers 
        add constraint FK_h0fkww2qunm83wog6dyjpy9db 
        foreign key (publish_org_id) 
        references publish_org;

    alter table unplanned_check_results 
        add constraint FK_kqwhooj6tfaq56tkqupjfelfm 
        foreign key (subject_client_id) 
        references clients;

    alter table unplanned_check_results 
        add constraint FK_pmjd4dopavvrqnevbadk2e26l 
        foreign key (owner_id) 
        references owners;
