import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Owner e2e test', () => {

    let navBarPage: NavBarPage;
    let ownerDialogPage: OwnerDialogPage;
    let ownerComponentsPage: OwnerComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Owners', () => {
        navBarPage.goToEntity('owner');
        ownerComponentsPage = new OwnerComponentsPage();
        expect(ownerComponentsPage.getTitle()).toMatch(/zakupkiApp.owner.home.title/);

    });

    it('should load create Owner dialog', () => {
        ownerComponentsPage.clickOnCreateButton();
        ownerDialogPage = new OwnerDialogPage();
        expect(ownerDialogPage.getModalTitle()).toMatch(/zakupkiApp.owner.home.createOrEditLabel/);
        ownerDialogPage.close();
    });

    it('should create and save Owners', () => {
        ownerComponentsPage.clickOnCreateButton();
        ownerDialogPage.setRegNumInput('regNum');
        expect(ownerDialogPage.getRegNumInput()).toMatch('regNum');
        ownerDialogPage.setFullNameInput('fullName');
        expect(ownerDialogPage.getFullNameInput()).toMatch('fullName');
        ownerDialogPage.save();
        expect(ownerDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class OwnerComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-owner div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OwnerDialogPage {
    modalTitle = element(by.css('h4#myOwnerLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    regNumInput = element(by.css('input#field_regNum'));
    fullNameInput = element(by.css('input#field_fullName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setRegNumInput = function (regNum) {
        this.regNumInput.sendKeys(regNum);
    }

    getRegNumInput = function () {
        return this.regNumInput.getAttribute('value');
    }

    setFullNameInput = function (fullName) {
        this.fullNameInput.sendKeys(fullName);
    }

    getFullNameInput = function () {
        return this.fullNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
