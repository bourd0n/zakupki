package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.UnplannedCheckResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnplannedCheckResult entity.
 */
public interface UnplannedCheckResultSearchRepository extends ElasticsearchRepository<UnplannedCheckResult, Long> {
}
