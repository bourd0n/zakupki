package alex.zakupki.complaint.logic.akka.spring

import akka.actor._

object SpringExt extends AbstractExtensionId[SpringExtension] with ExtensionIdProvider {
  /**
    * Is used by Akka to instantiate the Extension identified by this
    * ExtensionId, internal use only.
    */
  override def createExtension(system: ExtendedActorSystem): SpringExtension = new SpringExtension

  /**
    * Retrieve the SpringExt extension for the given system.
    */
  override def lookup() = SpringExt
}
