package com.alex.zakupki.repository;

import com.alex.zakupki.domain.CheckComplaintResult;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;

/**
 * Spring Data JPA repository for the CheckComplaintResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CheckComplaintResultRepository extends JpaRepository<CheckComplaintResult, Long> {
    Optional<CheckComplaintResult> findOneByCheckResultNumber(String checkResultNumber);
}
