import {Routes, Route} from "@angular/router";
import {ElasticSearchReindexComponent} from "./elasticsearch-reindex.component";
import {ElasticSearchReindexPopupComponent} from "./elasticsearch-reindex-dialog.component";

export const elasticSearchReindexRoute: Route = {
    path: 'elasticsearch-reindex',
    component: ElasticSearchReindexComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'elasticsearch.reindex.title'
    }
};


export const elasticSearchReindexPopupRoute: Route = {
    path: 'elasticsearch-reindex-dialog',
    component: ElasticSearchReindexPopupComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
    },
    outlet: 'popup'
};
