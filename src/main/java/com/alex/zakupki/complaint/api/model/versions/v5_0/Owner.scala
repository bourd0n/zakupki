package com.alex.zakupki.complaint.api.model.versions.v5_0

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
;

//@Entity
//@Table(name = "owners")
@XmlAccessorType(XmlAccessType.FIELD)
case class Owner(@xmlPath("oos:regNum/text()")
                 regNum: String,
                 @xmlPath("oos:fullName/text()")
                 fullName: String) extends GeneralizedEntity[general.Owner] {
    private def this() = this(null, null)

    override def mapToGeneral(): general.Owner = general.Owner(
        regNum,
        fullName.trim
    )
}
