package com.alex.zakupki.xml

import java.nio.file.Paths

import com.alex.zakupki.complaint.api.model.general.{CheckComplaintResult, Complaint, PlannedCheckResult, UnplannedCheckResult}
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CheckResultParsingTest {

    @Test
    def parseCheckResult_complaint_v5_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_complaint_5_2.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[CheckComplaintResult])
        val checkComplaintResult = parsed.get.asInstanceOf[CheckComplaintResult]
        assertThat(checkComplaintResult.checkResultNumber).isEqualTo("Ж-01031000103/31.07.2015/3127")
        assertThat(checkComplaintResult.complaintNumber).isEqualTo("01031000103/31.07.2015/3127")
        assertThat(checkComplaintResult.result).isEqualTo("COMPLAINT_NO_VIOLATIONS")
        assertThat(checkComplaintResult.checkSubject.regNum).isEqualTo("01032000092")
        assertThat(checkComplaintResult.checkSubject.fullName).isEqualTo("Агентство по энергетике Республики Дагестан")
        assertThat(checkComplaintResult.owner.regNum).isEqualTo("01031000103")
        assertThat(checkComplaintResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО РЕСПУБЛИКЕ ДАГЕСТАН")
    }

    @Test
    def parseCheckResult_complaint_v6_1(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_01481000016_28122015_8299_180464.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[CheckComplaintResult])
        val checkComplaintResult = parsed.get.asInstanceOf[CheckComplaintResult]
        assertThat(checkComplaintResult.checkResultNumber).isEqualTo("Ж-01481000016/28.12.2015/8299")
        assertThat(checkComplaintResult.complaintNumber).isEqualTo("01481000016/28.12.2015/8299")
        assertThat(checkComplaintResult.result).isNullOrEmpty()
        //todo: multiple subjects now not supported
        assertThat(checkComplaintResult.checkSubject.regNum).isIn("1103020988", "08483000487")
        assertThat(checkComplaintResult.checkSubject.fullName).isIn(
            "ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ ЗДРАВООХРАНЕНИЯ РЕСПУБЛИКИ КОМИ \"ВОРКУТИНСКАЯ БОЛЬНИЦА СКОРОЙ МЕДИЦИНСКОЙ ПОМОЩИ\"",
            "Муниципальное казенное учреждение Павлово-Посадского муниципального района Московской области \"Центр муниципальных закупок\""
        )
        assertThat(checkComplaintResult.owner.regNum).isEqualTo("01481000016")
        assertThat(checkComplaintResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО МОСКОВСКОЙ ОБЛАСТИ")
    }

    @Test
    def parseCheckResult_complaint_v6_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201601071000027000127_220059.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[CheckComplaintResult])
        val checkComplaintResult = parsed.get.asInstanceOf[CheckComplaintResult]
        assertThat(checkComplaintResult.checkResultNumber).isEqualTo("201601071000027000127")
        assertThat(checkComplaintResult.complaintNumber).isEqualTo("201600114406000094")
        assertThat(checkComplaintResult.result).isEqualTo("COMPLAINT_NO_VIOLATIONS")
        assertThat(checkComplaintResult.checkSubject.regNum).isEqualTo("1103020988")
        assertThat(checkComplaintResult.checkSubject.fullName).isEqualTo("ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ ЗДРАВООХРАНЕНИЯ РЕСПУБЛИКИ КОМИ \"ВОРКУТИНСКАЯ БОЛЬНИЦА СКОРОЙ МЕДИЦИНСКОЙ ПОМОЩИ\"")
        assertThat(checkComplaintResult.owner.regNum).isEqualTo("01071000027")
        assertThat(checkComplaintResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО РЕСПУБЛИКЕ КОМИ")
    }

    @Test
    def parseCheckResult_complaint_v6_4(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201700147554000219_286295.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[CheckComplaintResult])
        val checkComplaintResult = parsed.get.asInstanceOf[CheckComplaintResult]
        assertThat(checkComplaintResult.checkResultNumber).isEqualTo("201700147554000219")
        assertThat(checkComplaintResult.complaintNumber).isEqualTo("201700147554000178")
        assertThat(checkComplaintResult.result).isEqualTo("COMPLAINT_PARTLY_VALID")
        assertThat(checkComplaintResult.checkSubject.regNum).isEqualTo("1627003490")
        assertThat(checkComplaintResult.checkSubject.fullName).isEqualTo("ГОСУДАРСТВЕННОЕ КАЗЕННОЕ УЧРЕЖДЕНИЕ \"ЦЕНТР ЗАНЯТОСТИ НАСЕЛЕНИЯ МЕНДЕЛЕЕВСКОГО РАЙОНА\"")
        assertThat(checkComplaintResult.owner.regNum).isEqualTo("01111000049")
        assertThat(checkComplaintResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО РЕСПУБЛИКЕ ТАТАРСТАН")
    }

    @Test
    def parseCheckResult_unplanned_v5_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_unplanned_5_2.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[UnplannedCheckResult])
        val unplannedCheckResult = parsed.get.asInstanceOf[UnplannedCheckResult]
        assertThat(unplannedCheckResult.checkResultNumber).isEqualTo("В-01181000005/24.07.2015/00087")
        assertThat(unplannedCheckResult.unplannedCheckNumber).isEqualTo("01181000005/24.07.2015/00087")
        assertThat(unplannedCheckResult.result).isEqualTo("NO_VIOLATIONS")
        assertThat(unplannedCheckResult.owner.regNum).isEqualTo("01181000005")
        assertThat(unplannedCheckResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО КРАСНОДАРСКОМУ КРАЮ")
    }

    @Test
    def parseCheckResult_unplanned_v6_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201601142000010000043_218442.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[UnplannedCheckResult])
        val unplannedCheckResult = parsed.get.asInstanceOf[UnplannedCheckResult]
        assertThat(unplannedCheckResult.checkResultNumber).isEqualTo("201601142000010000043")
        assertThat(unplannedCheckResult.unplannedCheckNumber).isEqualTo("201601142000010000008")
        assertThat(unplannedCheckResult.result).isEqualTo("VIOLATIONS")
        assertThat(unplannedCheckResult.owner.regNum).isEqualTo("01142000010")
        assertThat(unplannedCheckResult.owner.fullName).isEqualTo("ПРАВИТЕЛЬСТВО РЕСПУБЛИКИ ИНГУШЕТИЯ")
    }

    @Test
    def parseCheckResult_unplanned_v6_2_checkResultNumber(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_01011000003_02032016_00003_191262.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[UnplannedCheckResult])
        val unplannedCheckResult = parsed.get.asInstanceOf[UnplannedCheckResult]
        assertThat(unplannedCheckResult.checkResultNumber).isEqualTo("В-01011000003/02.03.2016/00003")
        assertThat(unplannedCheckResult.unplannedCheckNumber).isEqualTo("01011000003/02.03.2016/00003")
        assertThat(unplannedCheckResult.result).isEqualTo("VIOLATIONS")
        assertThat(unplannedCheckResult.owner.regNum).isEqualTo("01011000003")
        assertThat(unplannedCheckResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО РЕСПУБЛИКЕ БАШКОРТОСТАН")
    }

    @Test
    def parseCheckResult_unplanned_v6_4_checkResult(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201700115324000088_281887.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[UnplannedCheckResult])
        val unplannedCheckResult = parsed.get.asInstanceOf[UnplannedCheckResult]
        assertThat(unplannedCheckResult.checkResultNumber).isEqualTo("201700115324000088")
        assertThat(unplannedCheckResult.unplannedCheckNumber).isEqualTo("201700115324000043")
        assertThat(unplannedCheckResult.result).isEqualTo("VIOLATIONS")
        assertThat(unplannedCheckResult.owner.regNum).isEqualTo("01791000022")
        assertThat(unplannedCheckResult.owner.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО КАРАЧАЕВО-ЧЕРКЕССКОЙ РЕСПУБЛИКЕ")
    }

    @Test
    def parseCheckResult_unplanned_v6_4_eventResult(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201798200001000009_271570.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[UnplannedCheckResult])
        val unplannedCheckResult = parsed.get.asInstanceOf[UnplannedCheckResult]
        assertThat(unplannedCheckResult.checkResultNumber).isEqualTo("201798200001000009")
        assertThat(unplannedCheckResult.unplannedCheckNumber).isEqualTo("201798200001000008")
        assertThat(unplannedCheckResult.result).isEqualTo("NO_VIOLATIONS")
        assertThat(unplannedCheckResult.owner.regNum).isEqualTo("01162000002")
        assertThat(unplannedCheckResult.owner.fullName).isEqualTo("МИНИСТЕРСТВО ФИНАНСОВ РЕСПУБЛИКИ САХА (ЯКУТИЯ)")
    }

    @Test
    def parseCheckResult_planned_v5_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_planned_5_2.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[PlannedCheckResult])
        val plannedCheckResult = parsed.get.asInstanceOf[PlannedCheckResult]
        assertThat(plannedCheckResult.checkResultNumber).isEqualTo("П-0101300018700000090000004")
        assertThat(plannedCheckResult.checkNumber).isEqualTo("0101300018700000090000004")
        assertThat(plannedCheckResult.owner.regNum).isEqualTo("01013000187")
        assertThat(plannedCheckResult.owner.fullName).isEqualTo("Администрация городского округа город Уфа Республики Башкортостан")
    }

    @Test
    def parseCheckResult_planned_v6_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201601183000187000011_220023.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[PlannedCheckResult])
        val plannedCheckResult = parsed.get.asInstanceOf[PlannedCheckResult]
        assertThat(plannedCheckResult.checkResultNumber).isEqualTo("201601183000187000011")
        assertThat(plannedCheckResult.checkNumber).isEqualTo("011830001870000008")
        assertThat(plannedCheckResult.owner.regNum).isEqualTo("01183000187")
        assertThat(plannedCheckResult.owner.fullName).isEqualTo("Администрация города Сочи - исполнительно-распорядительный орган муниципального образования город-курорт Сочи")
    }

    @Test
    def parseCheckResult_planned_v6_4_eventResult(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201719316000000001_281455.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[PlannedCheckResult])
        val plannedCheckResult = parsed.get.asInstanceOf[PlannedCheckResult]
        assertThat(plannedCheckResult.checkResultNumber).isEqualTo("201719316000000001")
        assertThat(plannedCheckResult.checkNumber).isEqualTo("2016193160000000010002")
        assertThat(plannedCheckResult.owner.regNum).isEqualTo("01303000081")
        assertThat(plannedCheckResult.owner.fullName).isEqualTo("ФИНАНСОВОЕ УПРАВЛЕНИЕ АДМИНИСТРАЦИИ НЮКСЕНСКОГО МУНИЦИПАЛЬНОГО РАЙОНА")
    }

    @Test
    def parseCheckResult_planned_v6_4_checkResult(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/checkResult_201765300747000001_277174.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[PlannedCheckResult])
        val plannedCheckResult = parsed.get.asInstanceOf[PlannedCheckResult]
        assertThat(plannedCheckResult.checkResultNumber).isEqualTo("201765300747000001")
        assertThat(plannedCheckResult.checkNumber).isEqualTo("2016653007470000050002")
        assertThat(plannedCheckResult.owner.regNum).isEqualTo("01623000153")
        assertThat(plannedCheckResult.owner.fullName).isEqualTo("ОРГАН МЕСТНОГО САМОУПРАВЛЕНИЯ, УПОЛНОМОЧЕННЫЙ В СФЕРЕ ИМУЩЕСТВЕННЫХ, ПРАВОВЫХ ОТНОШЕНИЙ И НЕНАЛОГОВЫХ ДОХОДОВ, - УПРАВЛЕНИЕ ИМУЩЕСТВЕННЫХ, ПРАВОВЫХ ОТНОШЕНИЙ И НЕНАЛОГОВЫХ ДОХОДОВ")
    }



}
