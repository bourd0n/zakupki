#!/usr/bin/env bash

rm -rf ./tmp/
rm -rf ./zakupki_tmp/
docker rm -f docker_zakupki-postgresql_1
docker-compose -f src/main/docker/postgresql.yml up -d
