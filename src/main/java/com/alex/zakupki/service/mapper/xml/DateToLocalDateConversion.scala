package com.alex.zakupki.service.mapper.xml

import java.time.{LocalDate, ZoneId}
import java.util.Date

trait DateToLocalDateConversion {
    implicit def dateToLocalDate(date: Date): LocalDate = Option(date).map(_.toInstant.atZone(ZoneId.systemDefault()).toLocalDate).orNull
}
