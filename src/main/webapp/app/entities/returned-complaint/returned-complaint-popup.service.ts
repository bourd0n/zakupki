import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ReturnedComplaint } from './returned-complaint.model';
import { ReturnedComplaintService } from './returned-complaint.service';

@Injectable()
export class ReturnedComplaintPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private returnedComplaintService: ReturnedComplaintService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.returnedComplaintService.find(id).subscribe((returnedComplaint) => {
                    this.ngbModalRef = this.returnedComplaintModalRef(component, returnedComplaint);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.returnedComplaintModalRef(component, new ReturnedComplaint());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    returnedComplaintModalRef(component: Component, returnedComplaint: ReturnedComplaint): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.returnedComplaint = returnedComplaint;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
