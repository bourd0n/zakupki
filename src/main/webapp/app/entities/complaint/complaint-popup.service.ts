import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Complaint } from './complaint.model';
import { ComplaintService } from './complaint.service';

@Injectable()
export class ComplaintPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private complaintService: ComplaintService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.complaintService.find(id).subscribe((complaint) => {
                    if (complaint.regDate) {
                        complaint.regDate = {
                            year: complaint.regDate.getFullYear(),
                            month: complaint.regDate.getMonth() + 1,
                            day: complaint.regDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.complaintModalRef(component, complaint);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.complaintModalRef(component, new Complaint());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    complaintModalRef(component: Component, complaint: Complaint): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.complaint = complaint;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
