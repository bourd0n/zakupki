package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence._
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import org.hibernate.annotations.CascadeType

@Entity
@Table(name = "unplanned_check_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class UnplannedCheckResult(@id @xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                           checkResultNumber: String,
                           @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                           createDate: Date,
                           @manyToOne
                           @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                           @joinColumn(name = "owner_id")
                           @xmlPath("checkResult/oos:commonInfo/oos:owner")
                           owner: Owner,
                           @xmlPath("checkResult/oos:commonInfo/oos:result/text()")
                           result: String,
                           @manyToOne
                           @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                           @joinColumn(name = "subject_client_id")
                           @xmlPath("checkResult/oos:base/oos:unplannedCheck/oos:checkSubjects/")
                           clientComplaint: Client) {
  private def this() = this(null, null, null, null, null)

}