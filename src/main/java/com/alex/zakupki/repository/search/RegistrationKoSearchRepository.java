package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.RegistrationKo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RegistrationKo entity.
 */
public interface RegistrationKoSearchRepository extends ElasticsearchRepository<RegistrationKo, Long> {
}
