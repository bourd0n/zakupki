import { BaseEntity } from './../../shared';

export class ComplaintCancel implements BaseEntity {
    constructor(
        public id?: number,
        public complaintNumber?: string,
        public regDate?: any,
        public text?: string,
        public registrationKo?: BaseEntity,
    ) {
    }
}
