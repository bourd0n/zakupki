package com.alex.zakupki.complaint.api.model.versions.v6_4

import javax.xml.bind.annotation.XmlAnyElement

import scala.beans.BeanProperty

sealed class Body {
  @XmlAnyElement(lax = true)
  @BeanProperty
  var bodyVal: AnyRef = null
}
