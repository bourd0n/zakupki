import { BaseEntity } from './../../shared';

export class Purchase implements BaseEntity {
    constructor(
        public id?: number,
        public purchaseNumber?: string,
        public determSupplierMethod?: string,
        public electronicPlatform?: string,
        public maxCost?: number,
    ) {
    }
}
