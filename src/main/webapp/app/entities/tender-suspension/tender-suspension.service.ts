import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TenderSuspension } from './tender-suspension.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TenderSuspensionService {

    private resourceUrl = SERVER_API_URL + 'api/tender-suspensions';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/tender-suspensions';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(tenderSuspension: TenderSuspension): Observable<TenderSuspension> {
        const copy = this.convert(tenderSuspension);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(tenderSuspension: TenderSuspension): Observable<TenderSuspension> {
        const copy = this.convert(tenderSuspension);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<TenderSuspension> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TenderSuspension.
     */
    private convertItemFromServer(json: any): TenderSuspension {
        const entity: TenderSuspension = Object.assign(new TenderSuspension(), json);
        entity.regDate = this.dateUtils
            .convertLocalDateFromServer(json.regDate);
        return entity;
    }

    /**
     * Convert a TenderSuspension to a JSON which can be sent to the server.
     */
    private convert(tenderSuspension: TenderSuspension): TenderSuspension {
        const copy: TenderSuspension = Object.assign({}, tenderSuspension);
        copy.regDate = this.dateUtils
            .convertLocalDateToServer(tenderSuspension.regDate);
        return copy;
    }
}
