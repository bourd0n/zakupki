package com.alex.zakupki.service.impl

import java.time.LocalDate
import javax.inject.Inject

import com.alex.zakupki.repository.AnalyticsRepository
import com.alex.zakupki.service.AnalyticsService
import com.alex.zakupki.service.dto.{ClientStats, ComplaintsYearStats}
import org.springframework.data.domain.{Page, PageImpl, Pageable, Sort}
import org.springframework.stereotype.Service

import scala.collection.JavaConverters._

@Service
class AnalyticsServiceImpl @Inject() (val analyticsRepository: AnalyticsRepository)
    extends AnalyticsService {

    override def getClientsComplaintsStats(fromDate: LocalDate,
                                           toDate: LocalDate,
                                           pageable: Pageable): Page[ClientStats] = {
        val stats = analyticsRepository.getClientsStats(fromDate, toDate, pageable)

        new PageImpl[ClientStats](stats._2.asJava, pageable, stats._1)
    }

    override def getComplaintsStats(clientId: String, sort: Sort): java.util.List[ComplaintsYearStats] = {
        val stats = analyticsRepository.getComplaintsStats(clientId, sort)
        stats.asJava
    }
}
