import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ControlCheckResultComponent } from './control-check-result.component';
import { ControlCheckResultDetailComponent } from './control-check-result-detail.component';
import { ControlCheckResultPopupComponent } from './control-check-result-dialog.component';
import { ControlCheckResultDeletePopupComponent } from './control-check-result-delete-dialog.component';

export const controlCheckResultRoute: Routes = [
    {
        path: 'control-check-result',
        component: ControlCheckResultComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.controlCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'control-check-result/:id',
        component: ControlCheckResultDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.controlCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const controlCheckResultPopupRoute: Routes = [
    {
        path: 'control-check-result-new',
        component: ControlCheckResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.controlCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'control-check-result/:id/edit',
        component: ControlCheckResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.controlCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'control-check-result/:id/delete',
        component: ControlCheckResultDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.controlCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
