export class ComplaintsStats {
    constructor(public year?: string,
                public complaintsCount?: number,
                public noViolations?: number,
                public violations?: number,
                public partlyValid?: number,
                public notMatched?: number,
                public returned?: number) {
    }
}
