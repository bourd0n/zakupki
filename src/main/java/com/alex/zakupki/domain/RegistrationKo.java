package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A RegistrationKo.
 */
@Entity
@Table(name = "registration_ko")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "registrationko")
public class RegistrationKo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "reg_num", nullable = false)
    private String regNum;

    @Column(name = "full_name")
    private String fullName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegNum() {
        return regNum;
    }

    public RegistrationKo regNum(String regNum) {
        this.regNum = regNum;
        return this;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getFullName() {
        return fullName;
    }

    public RegistrationKo fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegistrationKo registrationKo = (RegistrationKo) o;
        if (registrationKo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), registrationKo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RegistrationKo{" +
            "id=" + getId() +
            ", regNum='" + getRegNum() + "'" +
            ", fullName='" + getFullName() + "'" +
            "}";
    }
}
