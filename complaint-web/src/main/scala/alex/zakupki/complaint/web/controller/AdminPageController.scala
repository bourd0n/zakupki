package alex.zakupki.complaint.web.controller

import java.util.Date

import alex.zakupki.complaint.dao.ComplaintsDao
import alex.zakupki.complaint.logic.RefreshData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, ResponseBody}
import org.springframework.web.servlet.ModelAndView

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Controller
class AdminPageController @Autowired() (val complaintsDao : ComplaintsDao, val refreshData : RefreshData) {

  @RequestMapping(value = Array("/admin"), method = Array(RequestMethod.GET))
  def adminPage: ModelAndView = {
    val model = new ModelAndView("admin")
    //todo: add async
    model.addObject("complaintsNumber", Await.result(complaintsDao.countAllComplaints(), Duration.Inf))
    model.addObject("lastSyncDate", new Date())
//    model.addObject("isCurrentlyRefresh", RefreshData.isCurrentlyRefresh)
    model
  }

  @RequestMapping(value = Array("/loadComplaints"), method = Array(RequestMethod.POST))
  @ResponseBody
  def loadComplaints() : Boolean = {
    refreshData.refresh()
    true
  }

  @RequestMapping(value = Array("/login"), method = Array(RequestMethod.GET))
  def loginPage: String = "login"
}
