/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { UnfairSupplierDetailComponent } from '../../../../../../main/webapp/app/entities/unfair-supplier/unfair-supplier-detail.component';
import { UnfairSupplierService } from '../../../../../../main/webapp/app/entities/unfair-supplier/unfair-supplier.service';
import { UnfairSupplier } from '../../../../../../main/webapp/app/entities/unfair-supplier/unfair-supplier.model';

describe('Component Tests', () => {

    describe('UnfairSupplier Management Detail Component', () => {
        let comp: UnfairSupplierDetailComponent;
        let fixture: ComponentFixture<UnfairSupplierDetailComponent>;
        let service: UnfairSupplierService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [UnfairSupplierDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    UnfairSupplierService,
                    JhiEventManager
                ]
            }).overrideTemplate(UnfairSupplierDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UnfairSupplierDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UnfairSupplierService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new UnfairSupplier(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.unfairSupplier).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
