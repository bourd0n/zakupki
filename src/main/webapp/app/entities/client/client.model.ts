import { BaseEntity } from './../../shared';

export class Client implements BaseEntity {
    constructor(
        public id?: number,
        public regNum?: string,
        public fullName?: string,
        public clientType?: string,
    ) {
    }
}
