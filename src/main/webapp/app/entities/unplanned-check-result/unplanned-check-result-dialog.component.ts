import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnplannedCheckResult } from './unplanned-check-result.model';
import { UnplannedCheckResultPopupService } from './unplanned-check-result-popup.service';
import { UnplannedCheckResultService } from './unplanned-check-result.service';
import { Owner, OwnerService } from '../owner';
import { Client, ClientService } from '../client';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unplanned-check-result-dialog',
    templateUrl: './unplanned-check-result-dialog.component.html'
})
export class UnplannedCheckResultDialogComponent implements OnInit {

    unplannedCheckResult: UnplannedCheckResult;
    isSaving: boolean;

    owners: Owner[];

    clients: Client[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private unplannedCheckResultService: UnplannedCheckResultService,
        private ownerService: OwnerService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.ownerService.query()
            .subscribe((res: ResponseWrapper) => { this.owners = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clientService.query()
            .subscribe((res: ResponseWrapper) => { this.clients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unplannedCheckResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.unplannedCheckResultService.update(this.unplannedCheckResult));
        } else {
            this.subscribeToSaveResponse(
                this.unplannedCheckResultService.create(this.unplannedCheckResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<UnplannedCheckResult>) {
        result.subscribe((res: UnplannedCheckResult) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: UnplannedCheckResult) {
        this.eventManager.broadcast({ name: 'unplannedCheckResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOwnerById(index: number, item: Owner) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-unplanned-check-result-popup',
    template: ''
})
export class UnplannedCheckResultPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unplannedCheckResultPopupService: UnplannedCheckResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unplannedCheckResultPopupService
                    .open(UnplannedCheckResultDialogComponent as Component, params['id']);
            } else {
                this.unplannedCheckResultPopupService
                    .open(UnplannedCheckResultDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
