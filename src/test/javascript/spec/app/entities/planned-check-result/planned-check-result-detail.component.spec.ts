/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PlannedCheckResultDetailComponent } from '../../../../../../main/webapp/app/entities/planned-check-result/planned-check-result-detail.component';
import { PlannedCheckResultService } from '../../../../../../main/webapp/app/entities/planned-check-result/planned-check-result.service';
import { PlannedCheckResult } from '../../../../../../main/webapp/app/entities/planned-check-result/planned-check-result.model';

describe('Component Tests', () => {

    describe('PlannedCheckResult Management Detail Component', () => {
        let comp: PlannedCheckResultDetailComponent;
        let fixture: ComponentFixture<PlannedCheckResultDetailComponent>;
        let service: PlannedCheckResultService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [PlannedCheckResultDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PlannedCheckResultService,
                    JhiEventManager
                ]
            }).overrideTemplate(PlannedCheckResultDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlannedCheckResultDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlannedCheckResultService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PlannedCheckResult(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.plannedCheckResult).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
