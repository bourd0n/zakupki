package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.ControlCheckResult;
import com.alex.zakupki.service.ControlCheckResultService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ControlCheckResult.
 */
@RestController
@RequestMapping("/api")
public class ControlCheckResultResource {

    private final Logger log = LoggerFactory.getLogger(ControlCheckResultResource.class);

    private static final String ENTITY_NAME = "controlCheckResult";

    private final ControlCheckResultService controlCheckResultService;

    public ControlCheckResultResource(ControlCheckResultService controlCheckResultService) {
        this.controlCheckResultService = controlCheckResultService;
    }

    /**
     * POST  /control-check-results : Create a new controlCheckResult.
     *
     * @param controlCheckResult the controlCheckResult to create
     * @return the ResponseEntity with status 201 (Created) and with body the new controlCheckResult, or with status 400 (Bad Request) if the controlCheckResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/control-check-results")
    @Timed
    public ResponseEntity<ControlCheckResult> createControlCheckResult(@Valid @RequestBody ControlCheckResult controlCheckResult) throws URISyntaxException {
        log.debug("REST request to save ControlCheckResult : {}", controlCheckResult);
        if (controlCheckResult.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new controlCheckResult cannot already have an ID")).body(null);
        }
        ControlCheckResult result = controlCheckResultService.save(controlCheckResult);
        return ResponseEntity.created(new URI("/api/control-check-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /control-check-results : Updates an existing controlCheckResult.
     *
     * @param controlCheckResult the controlCheckResult to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated controlCheckResult,
     * or with status 400 (Bad Request) if the controlCheckResult is not valid,
     * or with status 500 (Internal Server Error) if the controlCheckResult couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/control-check-results")
    @Timed
    public ResponseEntity<ControlCheckResult> updateControlCheckResult(@Valid @RequestBody ControlCheckResult controlCheckResult) throws URISyntaxException {
        log.debug("REST request to update ControlCheckResult : {}", controlCheckResult);
        if (controlCheckResult.getId() == null) {
            return createControlCheckResult(controlCheckResult);
        }
        ControlCheckResult result = controlCheckResultService.save(controlCheckResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, controlCheckResult.getId().toString()))
            .body(result);
    }

    /**
     * GET  /control-check-results : get all the controlCheckResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of controlCheckResults in body
     */
    @GetMapping("/control-check-results")
    @Timed
    public ResponseEntity<List<ControlCheckResult>> getAllControlCheckResults(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of ControlCheckResults");
        Page<ControlCheckResult> page = controlCheckResultService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/control-check-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /control-check-results/:id : get the "id" controlCheckResult.
     *
     * @param id the id of the controlCheckResult to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the controlCheckResult, or with status 404 (Not Found)
     */
    @GetMapping("/control-check-results/{id}")
    @Timed
    public ResponseEntity<ControlCheckResult> getControlCheckResult(@PathVariable Long id) {
        log.debug("REST request to get ControlCheckResult : {}", id);
        ControlCheckResult controlCheckResult = controlCheckResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(controlCheckResult));
    }

    /**
     * DELETE  /control-check-results/:id : delete the "id" controlCheckResult.
     *
     * @param id the id of the controlCheckResult to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/control-check-results/{id}")
    @Timed
    public ResponseEntity<Void> deleteControlCheckResult(@PathVariable Long id) {
        log.debug("REST request to delete ControlCheckResult : {}", id);
        controlCheckResultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/control-check-results?query=:query : search for the controlCheckResult corresponding
     * to the query.
     *
     * @param query the query of the controlCheckResult search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/control-check-results")
    @Timed
    public ResponseEntity<List<ControlCheckResult>> searchControlCheckResults(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of ControlCheckResults for query {}", query);
        Page<ControlCheckResult> page = controlCheckResultService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/control-check-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
