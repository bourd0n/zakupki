package alex.zakupki.complaint.logic.xml

import java.io._
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes
import java.util.zip.ZipInputStream

import alex.zakupki.complaint.dao.ObjectSaver
import alex.zakupki.complaint.logic.config.ServerConfig
import alex.zakupki.complaint.temp.Consts
import alex.zakupki.complaint.utils.Utils._
import com.typesafe.scalalogging.LazyLogging
import net.lingala.zip4j.core.ZipFile
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import scala.compat.java8.FunctionConverters._

@Component
class XMLProcessor @Autowired()(objectSaver: ObjectSaver) extends LazyLogging {

  private val BUFFER_SIZE: Int = 10000

  object Types {

    sealed abstract class Type(val path: String)

    case object COMPLAINTS extends Type(Consts.COMPLAINT_PATH)

    case object CHECK_RESULTS extends Type(Consts.CHECK_RESULT_PATH)

    case object UNFAIR_SUPPLIER extends Type(Consts.UNFAIR_SUPPLIER_PATH)

  }

  def processFromFiles() {
    val path = s"${ServerConfig.server_path}/zakupki_tmp"
    Files.walk(Paths.get(path)).limit(100).forEach(asJavaConsumer(filePath => {
      if (Files.isRegularFile(filePath)) {
        logger.debug("Process zip file {}", filePath.getFileName)
        val zipFile: ZipFile = new ZipFile(filePath.toString)
        zipFile.extractAll(path + "/unpacked/")

        Files.walk(Paths.get(path + "/unpacked/")).forEach(asJavaConsumer(xmlFilePath => {
          if (Files.isRegularFile(xmlFilePath)) {
            val unmarshalObject: Option[AnyRef] = processXmlFile(xmlFilePath)
            if (unmarshalObject.isDefined) {
              objectSaver.saveObject(unmarshalObject.get)
            }
          }
        }))

        Files.walkFileTree(Paths.get(path + "/unpacked/"), new SimpleFileVisitor[Path]() {
          override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
            Files.delete(file)
            FileVisitResult.CONTINUE
          }

          override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
            Files.delete(dir)
            FileVisitResult.CONTINUE
          }
        })

        Files.delete(filePath)
        logger.debug("File deleted: {}", filePath)
      }
    }))
  }

  def processInmemory(zipFileBytes: Array[Byte]): Unit = {
    logger.debug("Process zip file {}", zipFileBytes)
    val zipStream = new ZipInputStream(new BufferedInputStream(new ByteArrayInputStream(zipFileBytes)))
    cleanly(zipStream) {
      Stream.continually(zipStream.getNextEntry).takeWhile(_ != null).withFilter(!_.isDirectory).foreach(entry => {
        val data = Array.ofDim[Byte](BUFFER_SIZE)
        val outputStream = new ByteArrayOutputStream()
        val bufOut = new BufferedOutputStream(outputStream, BUFFER_SIZE)
        Stream.continually(zipStream.read(data, 0, BUFFER_SIZE)).takeWhile(_ != -1).foreach(bufOut.write(data, 0, _))
        bufOut.flush()
        bufOut.close()
        for (unmarshalObject <- processXml(entry.getName, new BufferedInputStream(new ByteArrayInputStream(outputStream.toByteArray)))) {
          objectSaver.saveObject(unmarshalObject)
        }
      })
    }
  }

  def processInmemory(zipFilesBytes: Array[Array[Byte]]): Unit = {
    logger.debug("Start processInmemory with files {}", zipFilesBytes)
    zipFilesBytes.foreach((zipFile: Array[Byte]) => {
      processInmemory(zipFile)
    })
  }

  private def processXmlFile(xmlFilePath: Path): Option[AnyRef] = {
    processXml(xmlFilePath.getFileName.toString, new FileInputStream(xmlFilePath.toString))
  }

  def processXml(xmlFileName: String, stream: InputStream): Option[AnyRef] = {
    logger.debug("Unmarshal file {}", stream)
    xmlFileName match {
      case s if s.startsWith("complaint_") =>
        val complaint = EntityProcessor.unmarshalComplaint(stream)
        if (complaint.get.indictedClient == null) {
          logger.warn("WARN! {} has no typical construction for indicted", complaint)
        }
        Some(complaint)
      case s if s.startsWith("complaintCancel_") => EntityProcessor.unmarshalComplaintCancel(stream)
      case s if s.startsWith("tenderSuspension_") => EntityProcessor.unmarshalTenderSuspension(stream)
      case s if s.startsWith("checkResult_") => EntityProcessor.unmarshalCheckResult(stream)
      case s if s.startsWith("unfairSupplier_") => EntityProcessor.unmarshalUnfairSupplier(stream)
      case _ =>
        logger.error("Xml file {} not matched", stream)
        None
    }
  }
}
