package com.alex.zakupki.complaint.logic.xml

import java.io._
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes
import java.util.zip.ZipInputStream
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general
import com.alex.zakupki.complaint.api.model.general.{Complaint => XmlComplaint}
import com.alex.zakupki.complaint.logic.config.ServerConfig
import com.alex.zakupki.complaint.temp.Consts
import com.alex.zakupki.complaint.utils.Utils._
import com.alex.zakupki.domain.{CheckComplaintResult, Complaint, PlannedCheckResult, UnplannedCheckResult}
import com.alex.zakupki.repository.ComplaintRepository
import com.alex.zakupki.service.mapper.xml.XmlToDataMapper
import net.lingala.zip4j.core.ZipFile
import net.lingala.zip4j.exception.ZipException
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.stereotype.Component

@Component
class XMLProcessor() {

    protected lazy val logger: Logger = LoggerFactory.getLogger(getClass.getName)

    private val BUFFER_SIZE: Int = 10000

    private val path = s"${ServerConfig.server_path}/zakupki_tmp"

    new File(ServerConfig.server_path + "/tmp/null_complaint/").mkdirs()
    new File(ServerConfig.server_path + "/tmp/control/").mkdirs()
    new File(ServerConfig.server_path + "/tmp/unknow/").mkdirs()

    @Inject
    private val complaintRepository: ComplaintRepository = null

    @Inject
    private val complaintXmlToDataMapper: XmlToDataMapper[XmlComplaint, Complaint] = null

    @Inject
    private val checkComplaintResultXmlToDataMapper: XmlToDataMapper[general.CheckComplaintResult, CheckComplaintResult] = null

    @Inject
    private val plannedCheckResultXmlToDataMapper: XmlToDataMapper[general.PlannedCheckResult, PlannedCheckResult] = null

    @Inject
    private val unplannedCheckResultXmlToDataMapper: XmlToDataMapper[general.UnplannedCheckResult, UnplannedCheckResult] = null

    object Types {

        sealed abstract class Type(val path: String)

        case object COMPLAINTS extends Type(Consts.COMPLAINT_PATH)

        case object CHECK_RESULTS extends Type(Consts.CHECK_RESULT_PATH)

        case object UNFAIR_SUPPLIER extends Type(Consts.UNFAIR_SUPPLIER_PATH)

    }

    def processFile(filePath: Path): Unit = {
        if (Files.isRegularFile(filePath)) {
            logger.debug("Process zip file {}", filePath.getFileName)
            val zipFile: ZipFile = new ZipFile(filePath.toString)
            val extractedDirFilePath = path + "/unpacked/" + filePath.getFileName
            try {
                zipFile.extractAll(extractedDirFilePath)
            } catch {
                case e: ZipException =>
                    logger.error(s"Error during extraction of ${filePath.getFileName}", e)
                case _: Throwable =>
            }

            Files.walk(Paths.get(extractedDirFilePath))
                .filter(Files.isRegularFile(_))
                .filter(_.toString.endsWith("xml"))
                .forEach(xmlFilePath => {
                    val unmarshalObject: Option[AnyRef] = processXmlFile(xmlFilePath)
                    try {
                        unmarshalObject.map(save)
                    } catch {
                        case e: Throwable =>
                            logger.error(s"Error while saving $unmarshalObject. File ${xmlFilePath.getFileName.toString}", e)
                            Files.copy(xmlFilePath, Paths.get("tmp/" + xmlFilePath.getFileName.toString))
                            None
                    }
                })

            Files.walkFileTree(Paths.get(extractedDirFilePath), new SimpleFileVisitor[Path]() {
                override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
                    Files.delete(file)
                    FileVisitResult.CONTINUE
                }

                override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
                    logger.debug("Delete dir {}", dir)
                    Files.delete(dir)
                    FileVisitResult.CONTINUE
                }
            })

            Files.delete(filePath)
            logger.debug("File deleted: {}", filePath)
        }
    }

    def processFailed(): Unit = {
        logger.debug("Start process failed")

        Files.walk(Paths.get("tmp"))
            .filter(Files.isRegularFile(_))
            .filter(_.getFileName.toString.endsWith("xml"))
            .parallel()
            .forEach(xmlFilePath => {
                val unmarshalObject: Option[AnyRef] = processXmlFile(xmlFilePath)
                try {
                    unmarshalObject.map(save)
                    logger.debug("Successfully process failed {}", xmlFilePath)
                    Files.delete(xmlFilePath)
                } catch {
                    case e: Throwable =>
                        logger.error(s"Error while saving $unmarshalObject. File ${xmlFilePath.getFileName.toString}", e)
                        //Files.copy(xmlFilePath, Paths.get("tmp/" + xmlFilePath.getFileName.toString))
                        None
                }
            })

    }

    def processInmemory(zipFileBytes: Array[Byte]): Unit = {
        logger.debug("Process zip file")
        val zipStream = new ZipInputStream(new BufferedInputStream(new ByteArrayInputStream(zipFileBytes)))
        cleanly(zipStream) {
            Stream.continually(zipStream.getNextEntry).takeWhile(_ != null).withFilter(!_.isDirectory).foreach(entry => {
                val data = Array.ofDim[Byte](BUFFER_SIZE)
                val outputStream = new ByteArrayOutputStream()
                val bufOut = new BufferedOutputStream(outputStream, BUFFER_SIZE)
                Stream.continually(zipStream.read(data, 0, BUFFER_SIZE)).takeWhile(_ != -1).foreach(bufOut.write(data, 0, _))
                bufOut.flush()
                bufOut.close()
                //todo: uncomment
                //                for (unmarshalObject <- processXml(entry.getName, new BufferedInputStream(new ByteArrayInputStream(outputStream.toByteArray)))) {
                //                    todo: conflict in multiple threads saving same object?
                //                    save(unmarshalObject)
                //                }
            })
        }
    }

    def save(unmarshalObject: AnyRef): Any = unmarshalObject match {
        case xmlComplaint: XmlComplaint =>
            complaintXmlToDataMapper.map(xmlComplaint)
        case checkComplaintResult: general.CheckComplaintResult =>
            checkComplaintResultXmlToDataMapper.map(checkComplaintResult)
        case plannedCheckResult: general.PlannedCheckResult =>
            plannedCheckResultXmlToDataMapper.map(plannedCheckResult)
        case unplannedCheckResult: general.UnplannedCheckResult =>
            unplannedCheckResultXmlToDataMapper.map(unplannedCheckResult)
        case _ =>
    }

    @deprecated
    def processInmemory(zipFilesBytes: Array[Array[Byte]]): Unit = {
        logger.debug("Start processInmemory with files {}", zipFilesBytes)
        zipFilesBytes.foreach((zipFile: Array[Byte]) => {
            processInmemory(zipFile)
        })
    }

    def processXmlFile(xmlFilePath: Path): Option[AnyRef] = {
        val stream = new FileInputStream(xmlFilePath.toString)
        try {
            processXml(xmlFilePath.getFileName.toString, stream, xmlFilePath)
        } catch {
            case e: Throwable =>
                logger.error(s"Error while processing $xmlFilePath", e)
                Files.copy(xmlFilePath, Paths.get("tmp/" + xmlFilePath.getFileName.toString))
                None
        } finally {
            stream.close()
        }
    }

    def processXml(xmlFileName: String, stream: InputStream, xmlFilePath: Path): Option[AnyRef] = {
        logger.trace("Unmarshal file {}", xmlFileName)
        xmlFileName match {
            case s if s.startsWith("complaint_") =>
                val complaint = EntityProcessor.unmarshalComplaint(xmlFileName, stream, xmlFilePath)
                complaint
            //            todo: temp comment
            case s if s.startsWith("complaintCancel_") =>
                logger.trace("complaintCancel temp commented")
                None
            //                EntityProcessor.unmarshalComplaintCancel(stream)
            case s if s.startsWith("tenderSuspension_") =>
                logger.trace("tenderSuspension temp commented")
                None
            //                EntityProcessor.unmarshalTenderSuspension(stream)
            case s if s.startsWith("checkResult_") => EntityProcessor.unmarshalCheckResult(xmlFileName, stream, xmlFilePath)
            case s if s.startsWith("unfairSupplier_") => EntityProcessor.unmarshalUnfairSupplier(stream)
            case _ =>
                logger.error("Xml file {} not matched", xmlFileName)
                None
        }
    }
}
