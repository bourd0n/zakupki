package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A CheckComplaintResult.
 */
@Entity
@Table(name = "check_complaint_result")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "checkcomplaintresult")
public class CheckComplaintResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "check_result_number", nullable = false)
    private String checkResultNumber;

    @Column(name = "create_date")
    private LocalDate createDate;

    @Column(name = "result")
    private String result;

    @ManyToOne
    private Owner owner;

    @ManyToOne
    private Client checkSubject;

    @ManyToOne
    @NotNull
    private Complaint complaint;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCheckResultNumber() {
        return checkResultNumber;
    }

    public CheckComplaintResult checkResultNumber(String checkResultNumber) {
        this.checkResultNumber = checkResultNumber;
        return this;
    }

    public void setCheckResultNumber(String checkResultNumber) {
        this.checkResultNumber = checkResultNumber;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public CheckComplaintResult createDate(LocalDate createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getResult() {
        return result;
    }

    public CheckComplaintResult result(String result) {
        this.result = result;
        return this;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Owner getOwner() {
        return owner;
    }

    public CheckComplaintResult owner(Owner owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Client getCheckSubject() {
        return checkSubject;
    }

    public CheckComplaintResult checkSubject(Client client) {
        this.checkSubject = client;
        return this;
    }

    public void setCheckSubject(Client client) {
        this.checkSubject = client;
    }

    public Complaint getComplaint() {
        return complaint;
    }

    public CheckComplaintResult complaint(Complaint complaint) {
        this.complaint = complaint;
        return this;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CheckComplaintResult checkComplaintResult = (CheckComplaintResult) o;
        if (checkComplaintResult.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), checkComplaintResult.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CheckComplaintResult{" +
            "id=" + getId() +
            ", checkResultNumber='" + getCheckResultNumber() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", result='" + getResult() + "'" +
            "}";
    }
}
