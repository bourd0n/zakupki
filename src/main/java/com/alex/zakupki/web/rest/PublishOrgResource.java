package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.PublishOrg;
import com.alex.zakupki.service.PublishOrgService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PublishOrg.
 */
@RestController
@RequestMapping("/api")
public class PublishOrgResource {

    private final Logger log = LoggerFactory.getLogger(PublishOrgResource.class);

    private static final String ENTITY_NAME = "publishOrg";

    private final PublishOrgService publishOrgService;

    public PublishOrgResource(PublishOrgService publishOrgService) {
        this.publishOrgService = publishOrgService;
    }

    /**
     * POST  /publish-orgs : Create a new publishOrg.
     *
     * @param publishOrg the publishOrg to create
     * @return the ResponseEntity with status 201 (Created) and with body the new publishOrg, or with status 400 (Bad Request) if the publishOrg has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/publish-orgs")
    @Timed
    public ResponseEntity<PublishOrg> createPublishOrg(@Valid @RequestBody PublishOrg publishOrg) throws URISyntaxException {
        log.debug("REST request to save PublishOrg : {}", publishOrg);
        if (publishOrg.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new publishOrg cannot already have an ID")).body(null);
        }
        PublishOrg result = publishOrgService.save(publishOrg);
        return ResponseEntity.created(new URI("/api/publish-orgs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /publish-orgs : Updates an existing publishOrg.
     *
     * @param publishOrg the publishOrg to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated publishOrg,
     * or with status 400 (Bad Request) if the publishOrg is not valid,
     * or with status 500 (Internal Server Error) if the publishOrg couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/publish-orgs")
    @Timed
    public ResponseEntity<PublishOrg> updatePublishOrg(@Valid @RequestBody PublishOrg publishOrg) throws URISyntaxException {
        log.debug("REST request to update PublishOrg : {}", publishOrg);
        if (publishOrg.getId() == null) {
            return createPublishOrg(publishOrg);
        }
        PublishOrg result = publishOrgService.save(publishOrg);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, publishOrg.getId().toString()))
            .body(result);
    }

    /**
     * GET  /publish-orgs : get all the publishOrgs.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of publishOrgs in body
     */
    @GetMapping("/publish-orgs")
    @Timed
    public ResponseEntity<List<PublishOrg>> getAllPublishOrgs(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PublishOrgs");
        Page<PublishOrg> page = publishOrgService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/publish-orgs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /publish-orgs/:id : get the "id" publishOrg.
     *
     * @param id the id of the publishOrg to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the publishOrg, or with status 404 (Not Found)
     */
    @GetMapping("/publish-orgs/{id}")
    @Timed
    public ResponseEntity<PublishOrg> getPublishOrg(@PathVariable Long id) {
        log.debug("REST request to get PublishOrg : {}", id);
        PublishOrg publishOrg = publishOrgService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(publishOrg));
    }

    /**
     * DELETE  /publish-orgs/:id : delete the "id" publishOrg.
     *
     * @param id the id of the publishOrg to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/publish-orgs/{id}")
    @Timed
    public ResponseEntity<Void> deletePublishOrg(@PathVariable Long id) {
        log.debug("REST request to delete PublishOrg : {}", id);
        publishOrgService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/publish-orgs?query=:query : search for the publishOrg corresponding
     * to the query.
     *
     * @param query the query of the publishOrg search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/publish-orgs")
    @Timed
    public ResponseEntity<List<PublishOrg>> searchPublishOrgs(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PublishOrgs for query {}", query);
        Page<PublishOrg> page = publishOrgService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/publish-orgs");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
