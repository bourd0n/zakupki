/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ControlCheckResultDetailComponent } from '../../../../../../main/webapp/app/entities/control-check-result/control-check-result-detail.component';
import { ControlCheckResultService } from '../../../../../../main/webapp/app/entities/control-check-result/control-check-result.service';
import { ControlCheckResult } from '../../../../../../main/webapp/app/entities/control-check-result/control-check-result.model';

describe('Component Tests', () => {

    describe('ControlCheckResult Management Detail Component', () => {
        let comp: ControlCheckResultDetailComponent;
        let fixture: ComponentFixture<ControlCheckResultDetailComponent>;
        let service: ControlCheckResultService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [ControlCheckResultDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ControlCheckResultService,
                    JhiEventManager
                ]
            }).overrideTemplate(ControlCheckResultDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ControlCheckResultDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ControlCheckResultService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ControlCheckResult(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.controlCheckResult).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
