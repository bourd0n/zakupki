package com.alex.zakupki.complaint.api.model.general

import java.util.Date
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

@deprecated
case class ControlCheckResult(checkResultNumber: String,
                              createDate: Date,
                              owner: Owner,
                              result: String,
                              checkSubject: Client) {
  override def toString: String = {
    MoreObjects.toStringHelper(this).add("result", result).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
  }
}
