import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TenderSuspensionComponent } from './tender-suspension.component';
import { TenderSuspensionDetailComponent } from './tender-suspension-detail.component';
import { TenderSuspensionPopupComponent } from './tender-suspension-dialog.component';
import { TenderSuspensionDeletePopupComponent } from './tender-suspension-delete-dialog.component';

export const tenderSuspensionRoute: Routes = [
    {
        path: 'tender-suspension',
        component: TenderSuspensionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.tenderSuspension.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tender-suspension/:id',
        component: TenderSuspensionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.tenderSuspension.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tenderSuspensionPopupRoute: Routes = [
    {
        path: 'tender-suspension-new',
        component: TenderSuspensionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.tenderSuspension.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tender-suspension/:id/edit',
        component: TenderSuspensionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.tenderSuspension.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tender-suspension/:id/delete',
        component: TenderSuspensionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.tenderSuspension.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
