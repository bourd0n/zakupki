package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.UnfairSupplier;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UnfairSupplier entity.
 */
public interface UnfairSupplierSearchRepository extends ElasticsearchRepository<UnfairSupplier, Long> {
}
