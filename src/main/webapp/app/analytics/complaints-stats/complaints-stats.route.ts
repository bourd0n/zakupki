import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../../shared';

import {ComplaintsStatsComponent} from './complaints-stats.component';

export const complaintsStatsRoute: Routes = [
    {
        path: 'complaints-stats',
        component: ComplaintsStatsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.complaintsStats.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
