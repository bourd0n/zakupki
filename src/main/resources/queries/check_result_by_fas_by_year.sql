SELECT
    regKo.id,
    regKo.full_name,
    res."COMPLAINT_NO_VIOLATIONS",
    res."COMPLAINT_VIOLATIONS",
    res."COMPLAINT_PARTLY_VALID",
    res."COMPLAINT_NOT_MATCHED",
    res.returned_count returned_count,
    res.count          complaints_sum,
    res."EMPTY"
FROM
    (SELECT
         res.ko_id       ko_id_1,
         returned.ko_id  ko_id_2,
         all_count.ko_id ko_id_3,
         res."COMPLAINT_NO_VIOLATIONS",
         res."COMPLAINT_VIOLATIONS",
         res."COMPLAINT_PARTLY_VALID",
         res."COMPLAINT_NOT_MATCHED",
         res."EMPTY",
         all_count.count,
         returned.returned_count
     FROM
         (SELECT *
          FROM crosstab('SELECT
                      regKo.id ko_id,
                      subq.checkResult,
                      subq.complaints_count
                    FROM (SELECT
                            ccr.owner_id             ko_id,
                            COALESCE(ccr.result, '''') checkResult,
                            count(*)                 complaints_count
                          FROM check_complaint_result ccr
                          WHERE date_trunc(''year'', ccr.create_date) = to_date(''2014'', ''yyyy'')
                          GROUP BY ccr.owner_id, checkResult) subq, registration_ko regKo, owner o
                          WHERE o.reg_num = regKo.reg_num
                                AND o.id = subq.ko_id
                            ORDER BY 1, 2',
                        $$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$)
              AS ct ("ko_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
         FULL OUTER JOIN (SELECT
                              C.registration_ko_id  ko_id,
                              COALESCE(count(*), 0) returned_count
                          FROM
                              complaint C, returned_complaint rc
                          WHERE rc.complaint_id = C.id
                                AND date_trunc('year', C.reg_date) = to_date('2014', 'yyyy')
                          GROUP BY C.registration_ko_id) returned ON (res.ko_id = returned.ko_id)
         FULL OUTER JOIN
         (SELECT
              C.registration_ko_id  ko_id,
              COALESCE(count(*), 0) count
          FROM
              complaint C
          WHERE date_trunc('year', C.reg_date) = to_date('2014', 'yyyy')
          GROUP BY C.registration_ko_id) all_count ON (res.ko_id = all_count.ko_id)) res,
    registration_ko regKo
WHERE regKo.id = coalesce(res.ko_id_1, res.ko_id_2, res.ko_id_3)
ORDER BY complaints_sum DESC NULLS LAST;
