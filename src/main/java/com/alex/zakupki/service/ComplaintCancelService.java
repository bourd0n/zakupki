package com.alex.zakupki.service;

import com.alex.zakupki.domain.ComplaintCancel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ComplaintCancel.
 */
public interface ComplaintCancelService {

    /**
     * Save a complaintCancel.
     *
     * @param complaintCancel the entity to save
     * @return the persisted entity
     */
    ComplaintCancel save(ComplaintCancel complaintCancel);

    /**
     *  Get all the complaintCancels.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ComplaintCancel> findAll(Pageable pageable);

    /**
     *  Get the "id" complaintCancel.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ComplaintCancel findOne(Long id);

    /**
     *  Delete the "id" complaintCancel.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the complaintCancel corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ComplaintCancel> search(String query, Pageable pageable);
}
