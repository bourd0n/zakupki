package com.alex.zakupki.repository

import com.alex.zakupki.domain.SyncInfo
import org.springframework.data.jpa.repository.JpaRepository

trait SyncInfoRepository extends JpaRepository[SyncInfo, java.lang.String] {}
