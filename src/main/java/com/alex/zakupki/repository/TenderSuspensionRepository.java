package com.alex.zakupki.repository;

import com.alex.zakupki.domain.TenderSuspension;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TenderSuspension entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TenderSuspensionRepository extends JpaRepository<TenderSuspension, Long> {

}
