import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TenderSuspension } from './tender-suspension.model';
import { TenderSuspensionPopupService } from './tender-suspension-popup.service';
import { TenderSuspensionService } from './tender-suspension.service';

@Component({
    selector: 'jhi-tender-suspension-dialog',
    templateUrl: './tender-suspension-dialog.component.html'
})
export class TenderSuspensionDialogComponent implements OnInit {

    tenderSuspension: TenderSuspension;
    isSaving: boolean;
    regDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tenderSuspensionService: TenderSuspensionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tenderSuspension.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tenderSuspensionService.update(this.tenderSuspension));
        } else {
            this.subscribeToSaveResponse(
                this.tenderSuspensionService.create(this.tenderSuspension));
        }
    }

    private subscribeToSaveResponse(result: Observable<TenderSuspension>) {
        result.subscribe((res: TenderSuspension) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: TenderSuspension) {
        this.eventManager.broadcast({ name: 'tenderSuspensionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-tender-suspension-popup',
    template: ''
})
export class TenderSuspensionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tenderSuspensionPopupService: TenderSuspensionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tenderSuspensionPopupService
                    .open(TenderSuspensionDialogComponent as Component, params['id']);
            } else {
                this.tenderSuspensionPopupService
                    .open(TenderSuspensionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
