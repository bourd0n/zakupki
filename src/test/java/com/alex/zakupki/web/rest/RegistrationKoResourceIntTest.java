package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.RegistrationKo;
import com.alex.zakupki.repository.RegistrationKoRepository;
import com.alex.zakupki.service.RegistrationKoService;
import com.alex.zakupki.repository.search.RegistrationKoSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RegistrationKoResource REST controller.
 *
 * @see RegistrationKoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class RegistrationKoResourceIntTest {

    private static final String DEFAULT_REG_NUM = "AAAAAAAAAA";
    private static final String UPDATED_REG_NUM = "BBBBBBBBBB";

    private static final String DEFAULT_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FULL_NAME = "BBBBBBBBBB";

    @Autowired
    private RegistrationKoRepository registrationKoRepository;

    @Autowired
    private RegistrationKoService registrationKoService;

    @Autowired
    private RegistrationKoSearchRepository registrationKoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRegistrationKoMockMvc;

    private RegistrationKo registrationKo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegistrationKoResource registrationKoResource = new RegistrationKoResource(registrationKoService);
        this.restRegistrationKoMockMvc = MockMvcBuilders.standaloneSetup(registrationKoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegistrationKo createEntity(EntityManager em) {
        RegistrationKo registrationKo = new RegistrationKo()
            .regNum(DEFAULT_REG_NUM)
            .fullName(DEFAULT_FULL_NAME);
        return registrationKo;
    }

    @Before
    public void initTest() {
        registrationKoSearchRepository.deleteAll();
        registrationKoRepository.deleteAll();
        if (registrationKo != null) {
            em.remove(registrationKo);
        }
        registrationKoRepository.flush();
        registrationKo = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegistrationKo() throws Exception {
        int databaseSizeBeforeCreate = registrationKoRepository.findAll().size();

        // Create the RegistrationKo
        restRegistrationKoMockMvc.perform(post("/api/registration-kos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationKo)))
            .andExpect(status().isCreated());

        // Validate the RegistrationKo in the database
        List<RegistrationKo> registrationKoList = registrationKoRepository.findAll();
        assertThat(registrationKoList).hasSize(databaseSizeBeforeCreate + 1);
        RegistrationKo testRegistrationKo = registrationKoList.get(registrationKoList.size() - 1);
        assertThat(testRegistrationKo.getRegNum()).isEqualTo(DEFAULT_REG_NUM);
        assertThat(testRegistrationKo.getFullName()).isEqualTo(DEFAULT_FULL_NAME);

        // Validate the RegistrationKo in Elasticsearch
        RegistrationKo registrationKoEs = registrationKoSearchRepository.findOne(testRegistrationKo.getId());
        assertThat(registrationKoEs).isEqualToComparingFieldByField(testRegistrationKo);
    }

    @Test
    @Transactional
    public void createRegistrationKoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = registrationKoRepository.findAll().size();

        // Create the RegistrationKo with an existing ID
        registrationKo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegistrationKoMockMvc.perform(post("/api/registration-kos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationKo)))
            .andExpect(status().isBadRequest());

        // Validate the RegistrationKo in the database
        List<RegistrationKo> registrationKoList = registrationKoRepository.findAll();
        assertThat(registrationKoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkRegNumIsRequired() throws Exception {
        int databaseSizeBeforeTest = registrationKoRepository.findAll().size();
        // set the field null
        registrationKo.setRegNum(null);

        // Create the RegistrationKo, which fails.

        restRegistrationKoMockMvc.perform(post("/api/registration-kos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationKo)))
            .andExpect(status().isBadRequest());

        List<RegistrationKo> registrationKoList = registrationKoRepository.findAll();
        assertThat(registrationKoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegistrationKos() throws Exception {
        // Initialize the database
        registrationKoRepository.saveAndFlush(registrationKo);

        // Get all the registrationKoList
        restRegistrationKoMockMvc.perform(get("/api/registration-kos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(registrationKo.getId().intValue())))
            .andExpect(jsonPath("$.[*].regNum").value(hasItem(DEFAULT_REG_NUM.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())));
    }

    @Test
    @Transactional
    public void getRegistrationKo() throws Exception {
        // Initialize the database
        registrationKoRepository.saveAndFlush(registrationKo);

        // Get the registrationKo
        restRegistrationKoMockMvc.perform(get("/api/registration-kos/{id}", registrationKo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(registrationKo.getId().intValue()))
            .andExpect(jsonPath("$.regNum").value(DEFAULT_REG_NUM.toString()))
            .andExpect(jsonPath("$.fullName").value(DEFAULT_FULL_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRegistrationKo() throws Exception {
        // Get the registrationKo
        restRegistrationKoMockMvc.perform(get("/api/registration-kos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegistrationKo() throws Exception {
        // Initialize the database
        registrationKoService.save(registrationKo);

        int databaseSizeBeforeUpdate = registrationKoRepository.findAll().size();

        // Update the registrationKo
        RegistrationKo updatedRegistrationKo = registrationKoRepository.findOne(registrationKo.getId());
        updatedRegistrationKo
            .regNum(UPDATED_REG_NUM)
            .fullName(UPDATED_FULL_NAME);

        restRegistrationKoMockMvc.perform(put("/api/registration-kos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRegistrationKo)))
            .andExpect(status().isOk());

        // Validate the RegistrationKo in the database
        List<RegistrationKo> registrationKoList = registrationKoRepository.findAll();
        assertThat(registrationKoList).hasSize(databaseSizeBeforeUpdate);
        RegistrationKo testRegistrationKo = registrationKoList.get(registrationKoList.size() - 1);
        assertThat(testRegistrationKo.getRegNum()).isEqualTo(UPDATED_REG_NUM);
        assertThat(testRegistrationKo.getFullName()).isEqualTo(UPDATED_FULL_NAME);

        // Validate the RegistrationKo in Elasticsearch
        RegistrationKo registrationKoEs = registrationKoSearchRepository.findOne(testRegistrationKo.getId());
        assertThat(registrationKoEs).isEqualToComparingFieldByField(testRegistrationKo);
    }

    @Test
    @Transactional
    public void updateNonExistingRegistrationKo() throws Exception {
        int databaseSizeBeforeUpdate = registrationKoRepository.findAll().size();

        // Create the RegistrationKo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRegistrationKoMockMvc.perform(put("/api/registration-kos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registrationKo)))
            .andExpect(status().isCreated());

        // Validate the RegistrationKo in the database
        List<RegistrationKo> registrationKoList = registrationKoRepository.findAll();
        assertThat(registrationKoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRegistrationKo() throws Exception {
        // Initialize the database
        registrationKoService.save(registrationKo);

        int databaseSizeBeforeDelete = registrationKoRepository.findAll().size();

        // Get the registrationKo
        restRegistrationKoMockMvc.perform(delete("/api/registration-kos/{id}", registrationKo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean registrationKoExistsInEs = registrationKoSearchRepository.exists(registrationKo.getId());
        assertThat(registrationKoExistsInEs).isFalse();

        // Validate the database is empty
        List<RegistrationKo> registrationKoList = registrationKoRepository.findAll();
        assertThat(registrationKoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchRegistrationKo() throws Exception {
        // Initialize the database
        registrationKoService.save(registrationKo);

        // Search the registrationKo
        restRegistrationKoMockMvc.perform(get("/api/_search/registration-kos?query=id:" + registrationKo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(registrationKo.getId().intValue())))
            .andExpect(jsonPath("$.[*].regNum").value(hasItem(DEFAULT_REG_NUM.toString())))
            .andExpect(jsonPath("$.[*].fullName").value(hasItem(DEFAULT_FULL_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistrationKo.class);
        RegistrationKo registrationKo1 = new RegistrationKo();
        registrationKo1.setId(1L);
        RegistrationKo registrationKo2 = new RegistrationKo();
        registrationKo2.setId(registrationKo1.getId());
        assertThat(registrationKo1).isEqualTo(registrationKo2);
        registrationKo2.setId(2L);
        assertThat(registrationKo1).isNotEqualTo(registrationKo2);
        registrationKo1.setId(null);
        assertThat(registrationKo1).isNotEqualTo(registrationKo2);
    }
}
