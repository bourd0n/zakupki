package com.alex.zakupki.complaint.logic.akka.actors

import akka.actor.Actor
import com.alex.zakupki.complaint.logic.akka.messages.ProcessInMemoryZip
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import com.alex.zakupki.complaint.utils.Logging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component("inMemoryProcessorActor")
@Scope("prototype")
@deprecated
class InMemoryZipProcessorActor @Autowired()(xmlProcessor: XMLProcessor) extends Actor with Logging {
    override def receive: Receive = {
        case ProcessInMemoryZip(zipFile) =>
            logger.debug("{} start process file in memory", self.path.name)
            xmlProcessor.processInmemory(zipFile)
            logger.debug("{} finish process file inmemory", self.path.name)
    }
}
