package com.alex.zakupki.xml

import java.nio.file.Paths

import com.alex.zakupki.complaint.api.model.general.Complaint
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import org.assertj.core.api.Assertions._
import org.junit.Test

class ComplaintParsingTest {

    @Test
    def parseComplaint_v5_0(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/complaint_5_0.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[Complaint])
        val complaint = parsed.get.asInstanceOf[Complaint]
        assertThat(complaint.complaintNumber).isEqualTo("01451000049/16.01.2015/2385")
        assertThat(complaint.text).isEqualTo("Действия Заказчика")
        assertThat(complaint.purchase.purchaseNumber).isEqualTo("0145100003314000046")
        assertThat(complaint.applicant.organizationName).isEqualTo("БФ Детские вопросы")
        assertThat(complaint.indictedClient.regNum).isEqualTo("01451000033")
        assertThat(complaint.indictedClient.fullName).isNotBlank
        assertThat(complaint.registrationKO.regNum).isEqualTo("01451000049")
        assertThat(complaint.registrationKO.fullName).isEqualTo("Управление Федеральной антимонопольной службы по Ленинградской области")
    }

    @Test
    def parseComplaint_v5_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/complaint_5_2.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[Complaint])
        val complaint = parsed.get.asInstanceOf[Complaint]
        assertThat(complaint.complaintNumber).isEqualTo("01011000003/01.06.2015/2415")
        assertThat(complaint.text).isEqualTo("Заявитель считает, что Заказчиком нарушен Закон о контрактной системе")
        assertThat(complaint.purchase.purchaseNumber).isEqualTo("0801200000615000032")
        assertThat(complaint.applicant.organizationName).isEqualTo("ООО \"Строительные технологии\"")
        assertThat(complaint.indictedClient.regNum).isEqualTo("08012000006")
        assertThat(complaint.indictedClient.fullName).isEqualTo("государственное казенное учреждение Управление дорожного хозяйства Республики Башкортостан")
        assertThat(complaint.registrationKO.regNum).isEqualTo("01011000003")
        assertThat(complaint.registrationKO.fullName).isEqualTo("Управление Федеральной антимонопольной службы по Республике Башкортостан")
    }

    @Test
    def parseComplaint_v6_2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/complaint_6_2.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[Complaint])
        val complaint = parsed.get.asInstanceOf[Complaint]
        assertThat(complaint.complaintNumber).isEqualTo("6")
        assertThat(complaint.text).isEqualTo("см. приложение")
        assertThat(complaint.purchase.purchaseNumber).isEqualTo("0148200000315000039")
        assertThat(complaint.applicant.organizationName).isEqualTo("ООО «АВТ-Строй»")
        assertThat(complaint.indictedClient.regNum).isEqualTo("01482000003")
        assertThat(complaint.indictedClient.fullName).isEqualTo("МИНИСТЕРСТВО ФИНАНСОВ МОСКОВСКОЙ ОБЛАСТИ")
        assertThat(complaint.registrationKO.regNum).isEqualTo("01481000016")
        assertThat(complaint.registrationKO.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО МОСКОВСКОЙ ОБЛАСТИ")
    }

    @Test
    def parseComplaint_v6_2_test2(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/complaint_201600100161000292_1619126.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[Complaint])
        val complaint = parsed.get.asInstanceOf[Complaint]
        assertThat(complaint.complaintNumber).isEqualTo("201600100161000292")
        assertThat(complaint.text).isEqualTo("Содержание жалобы смотрите в прикрепленной электронной версии жалобы")
        assertThat(complaint.purchase.purchaseNumber).isEqualTo("0190200000316002813")
        assertThat(complaint.applicant.organizationName).isEqualTo("ООО «Сеал урал»")
        assertThat(complaint.indictedClient.regNum).isEqualTo("7707704692")
        assertThat(complaint.indictedClient.fullName).isEqualTo("ОАО \"ЕЭТП\"")
        assertThat(complaint.registrationKO.regNum).isEqualTo("01731000120")
        assertThat(complaint.registrationKO.fullName).isEqualTo("Федеральная антимонопольная служба")
    }

    @Test
    def parseComplaint_v6_2_withIndividualBusinessMan(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/complaint_20160019274000035_1617673.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[Complaint])
        val complaint = parsed.get.asInstanceOf[Complaint]
        assertThat(complaint.complaintNumber).isEqualTo("20160019274000035")
        assertThat(complaint.text).isEqualTo("Содержание жалобы смотрите в прикрепленной электронной версии жалобы.")
        assertThat(complaint.purchase.purchaseNumber).isEqualTo("0174200001916000216")
        assertThat(complaint.applicant.organizationName).isEqualTo("Тищенко А.В.")
        assertThat(complaint.indictedClient.regNum).isEqualTo("9204021260")
        assertThat(complaint.indictedClient.fullName).isEqualTo("ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ ЗДРАВООХРАНЕНИЯ СЕВАСТОПОЛЯ \"ГОРОДСКАЯ БОЛЬНИЦА № 5 - \"ЦЕНТР ОХРАНЫ ЗДОРОВЬЯ МАТЕРИ И РЕБЕНКА\"")
        assertThat(complaint.registrationKO.regNum).isEqualTo("01751000043")
        assertThat(complaint.registrationKO.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО РЕСПУБЛИКЕ КРЫМ И ГОРОДУ СЕВАСТОПОЛЮ")
    }

    @Test
    def parseComplaint_v6_4(): Unit = {
        val resourceDirectory = Paths.get("src/test/resources/xml/complaint_6_4.xml")
        val parsed = new XMLProcessor().processXmlFile(resourceDirectory)
        assertThat(parsed.get).isInstanceOf(classOf[Complaint])
        val complaint = parsed.get.asInstanceOf[Complaint]
        assertThat(complaint.complaintNumber).isEqualTo("6")
        assertThat(complaint.text).isEqualTo("составление конкурсной документации")
        assertThat(complaint.purchase.purchaseNumber).isEqualTo("0103200008416003474")
        assertThat(complaint.applicant.organizationName).isEqualTo("ООО \"РОСсигнал\"")
        assertThat(complaint.indictedClient.regNum).isEqualTo("0543000719")
        assertThat(complaint.indictedClient.fullName).isEqualTo("Государственное бюджетное учреждение Республики Дагестан \"Буйнакская центральная городская больница\"")
        assertThat(complaint.registrationKO.regNum).isEqualTo("01031000103")
        assertThat(complaint.registrationKO.fullName).isEqualTo("УПРАВЛЕНИЕ ФЕДЕРАЛЬНОЙ АНТИМОНОПОЛЬНОЙ СЛУЖБЫ ПО РЕСПУБЛИКЕ ДАГЕСТАН")
    }


}
