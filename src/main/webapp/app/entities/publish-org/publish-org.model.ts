import { BaseEntity } from './../../shared';

export class PublishOrg implements BaseEntity {
    constructor(
        public id?: number,
        public regNum?: string,
        public fullName?: string,
    ) {
    }
}
