package com.alex.zakupki.complaint.api.model

trait GeneralizedEntity[T] {
    def mapToGeneral() : T
}
