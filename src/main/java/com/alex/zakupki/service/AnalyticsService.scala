package com.alex.zakupki.service

import java.time.LocalDate

import com.alex.zakupki.service.dto.{ClientStats, ComplaintsYearStats}
import org.springframework.data.domain.{Page, Pageable, Sort}

trait AnalyticsService {

    def getComplaintsStats(clientId: String, sort: Sort): java.util.List[ComplaintsYearStats]

    def getClientsComplaintsStats(fromDate: LocalDate,
                                  toDate: LocalDate,
                                  pageable: Pageable): Page[ClientStats]
}
