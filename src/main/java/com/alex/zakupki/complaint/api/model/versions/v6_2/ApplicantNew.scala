package com.alex.zakupki.complaint.api.model.versions.v6_2

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import org.apache.commons.lang3.{ObjectUtils, StringUtils}

//@Entity
//@Table(name = "applicants")
@XmlAccessorType(XmlAccessType.FIELD)
case class ApplicantNew(@xmlPath("oos:legalEntity/oos:fullName/text()")
                        organizationName: String,
                        //todo: rewrite to OR
                        @xmlPath("oos:individualBusinessman/oos:name/text()")
                        individualBusinessmanName: String,
                        @xmlPath("oos:individualPerson/oos:name/text()")
                        individualPersonName: String,
                        @xmlPath("oos:legalEntity/oos:legalForm/oos:singularName/text()")
                        applicantType: String) extends GeneralizedEntity[general.Applicant] {
    private def this() = this(null, null, null, null)

    override def mapToGeneral(): general.Applicant = {
        general.Applicant(ObjectUtils.firstNonNull(organizationName, individualBusinessmanName, individualPersonName).trim,
            if (StringUtils.isEmpty(applicantType)) {
                if (StringUtils.isNotEmpty(individualBusinessmanName)) {
                    "Individual Businessman"
                } else if (StringUtils.isNotEmpty(individualPersonName)) {
                    "Individual Person"
                } else {
                    ""
                }
            } else
                applicantType)
    }
}
