package com.alex.zakupki.complaint.api.model.general

case class RegistrationKO(regNum: String, fullName: String)
