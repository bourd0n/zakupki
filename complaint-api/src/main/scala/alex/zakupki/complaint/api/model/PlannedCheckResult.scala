package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence._
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

@Entity
@Table(name = "planned_check_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class PlannedCheckResult(@id @xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                              checkResultNumber: String,
                              @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                              createDate: Date,
                              @manyToOne
                              @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                              @joinColumn(name = "owner_id")
                              @xmlPath("checkResult/oos:commonInfo/oos:owner")
                              owner: Owner,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:act/oos:actDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actDate: Date,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:actPrescription/oos:prescriptionDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actPrescriptionDate: Date,
                              @manyToOne
                              @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                              @joinColumn(name = "subject_customer_id")
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:checkSubject/")
                              @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                              clientComplaint: Client) {
  private def this() = this(null, null, null, null, null, null)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
  }
}