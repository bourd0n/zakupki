package alex.zakupki.complaint.logic.akka

import alex.zakupki.complaint.logic.ftp.FTPTypes

case class StartDownload(ftpType: FTPTypes.FTPType, limit: Int)
