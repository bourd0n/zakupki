import {Injectable, Component} from "@angular/core";
import {Router} from "@angular/router";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";

@Injectable()
export class ElasticSearchReindexModalService {
    private isOpen = false;

    constructor(private modalService: NgbModal,
                private router: Router) {
    }

    open(component: Component): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        return this.userModalRef(component);
    }

    userModalRef(component: Component): NgbModalRef {
        let modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.result.then(result => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        });
        return modalRef;
    }
}
