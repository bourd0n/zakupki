package com.alex.zakupki.complaint.logic.akka.actors

import java.nio.file.Paths

import akka.actor.{Actor, ActorRef}
import com.alex.zakupki.complaint.logic.akka.messages.{ProcessDownloadedZip, ProcessFtpZip}
import com.alex.zakupki.complaint.logic.akka.spring.{SpringExt, SpringExtension}
import com.alex.zakupki.complaint.logic.ftp.FtpUtils
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component(DownloadZipActor.NAME)
@Scope("prototype")
class DownloadZipActor @Autowired()(implicit ctx: ApplicationContext, ftpUtils: FtpUtils) extends Actor {

    private val logger: Logger = LoggerFactory.getLogger(getClass)

    override def receive: Receive = {
        case ProcessFtpZip(ftpFile, ftpType) =>
            logger.debug(s"${self.path.name} start download ftp zip ${ftpFile.getName}")
            val localPath = ftpUtils.downloadFile(ftpFile, ftpType.ftpPath)
            val zipProcessorActor: ActorRef = context.actorOf(SpringExtension(context.system).props(ZipProcessorActor.NAME),
                name = s"zipProcessorActor-${ftpFile.getName}")
            zipProcessorActor ! ProcessDownloadedZip(Paths.get(localPath))
            logger.debug(s"${self.path.name} finish download ftp zip ${ftpFile.getName}")
    }
}

object DownloadZipActor {
    final val NAME = "downloadZipActor"
}
