import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { UnfairSupplier } from './unfair-supplier.model';
import { UnfairSupplierPopupService } from './unfair-supplier-popup.service';
import { UnfairSupplierService } from './unfair-supplier.service';
import { PublishOrg, PublishOrgService } from '../publish-org';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-unfair-supplier-dialog',
    templateUrl: './unfair-supplier-dialog.component.html'
})
export class UnfairSupplierDialogComponent implements OnInit {

    unfairSupplier: UnfairSupplier;
    isSaving: boolean;

    publishorgs: PublishOrg[];
    publishDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private unfairSupplierService: UnfairSupplierService,
        private publishOrgService: PublishOrgService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.publishOrgService.query()
            .subscribe((res: ResponseWrapper) => { this.publishorgs = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.unfairSupplier.id !== undefined) {
            this.subscribeToSaveResponse(
                this.unfairSupplierService.update(this.unfairSupplier));
        } else {
            this.subscribeToSaveResponse(
                this.unfairSupplierService.create(this.unfairSupplier));
        }
    }

    private subscribeToSaveResponse(result: Observable<UnfairSupplier>) {
        result.subscribe((res: UnfairSupplier) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: UnfairSupplier) {
        this.eventManager.broadcast({ name: 'unfairSupplierListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPublishOrgById(index: number, item: PublishOrg) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-unfair-supplier-popup',
    template: ''
})
export class UnfairSupplierPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unfairSupplierPopupService: UnfairSupplierPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.unfairSupplierPopupService
                    .open(UnfairSupplierDialogComponent as Component, params['id']);
            } else {
                this.unfairSupplierPopupService
                    .open(UnfairSupplierDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
