package com.alex.zakupki.complaint.dao

trait ObjectSaver {
  def saveObject(unmarshalObject: AnyRef)
}
