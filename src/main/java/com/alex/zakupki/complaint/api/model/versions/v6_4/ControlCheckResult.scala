package com.alex.zakupki.complaint.api.model.versions.v6_4

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.google.common.base.MoreObjects
;

//@Entity
//@Table(name = "control_check_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class ControlCheckResult(@xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:complaintNumber/text()")
                              checkResultNumber: String,
                              @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                              createDate: Date,
                              @xmlPath("checkResult/oos:commonInfo/oos:owner")
                              owner: Owner,
                              @xmlPath("checkResult/oos:commonInfo/oos:result/text()")
                              result: String,
                              @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:checkSubjects/oos:subjectComplaint/")
                              @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                              clientComplaint: Client) {
    private def this() = this(null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("result", result).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }
}
