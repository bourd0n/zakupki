package alex.zakupki.complaint.logic.ftp

object FTPTypes {
  sealed abstract class FTPType(val ftpPath: String, val fileNameRegx: String)

  case object ComplaintFTPType extends FTPType("/fcs_fas/complaint/", "complaint")

  case object CheckResultFTPType extends FTPType("/fcs_fas/checkResult/", "checkResult")
}
