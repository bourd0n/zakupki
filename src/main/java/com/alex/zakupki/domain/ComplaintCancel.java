package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ComplaintCancel.
 */
@Entity
@Table(name = "complaint_cancel")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "complaintcancel")
public class ComplaintCancel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "complaint_number", nullable = false)
    private String complaintNumber;

    @Column(name = "reg_date")
    private LocalDate regDate;

    @Size(max = 4000)
    @Column(name = "text", length = 4000)
    private String text;

    @ManyToOne
    private RegistrationKo registrationKo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public ComplaintCancel complaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
        return this;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public LocalDate getRegDate() {
        return regDate;
    }

    public ComplaintCancel regDate(LocalDate regDate) {
        this.regDate = regDate;
        return this;
    }

    public void setRegDate(LocalDate regDate) {
        this.regDate = regDate;
    }

    public String getText() {
        return text;
    }

    public ComplaintCancel text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public RegistrationKo getRegistrationKo() {
        return registrationKo;
    }

    public ComplaintCancel registrationKo(RegistrationKo registrationKo) {
        this.registrationKo = registrationKo;
        return this;
    }

    public void setRegistrationKo(RegistrationKo registrationKo) {
        this.registrationKo = registrationKo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ComplaintCancel complaintCancel = (ComplaintCancel) o;
        if (complaintCancel.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), complaintCancel.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ComplaintCancel{" +
            "id=" + getId() +
            ", complaintNumber='" + getComplaintNumber() + "'" +
            ", regDate='" + getRegDate() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }
}
