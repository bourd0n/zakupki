import {Component, OnInit, OnDestroy} from '@angular/core';
import {ElasticSearchReindex} from './elasticsearch-reindex-dialog.service';
import {NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {JhiLanguageService, JhiAlertService} from 'ng-jhipster';
import {Response} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {ElasticSearchReindexModalService} from './elasticsearch-reindex-modal.service';

@Component({
    selector: 'jhi-search-reindex-dialog',
    templateUrl: './elasticsearch-reindex-dialog.component.html'
})
export class ElasticSearchReindexDialogComponent {

    constructor(public activeModal: NgbActiveModal,
                private elasticSearchReindex: ElasticSearchReindex,
                private jhiLanguageService: JhiLanguageService,
                private alertService: JhiAlertService) {
        // this.jhiLanguageService.setLocations(['elasticsearch-reindex']);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmReindex() {
        this.elasticSearchReindex.reindex().subscribe(
            (res: Response) => this.onSaveSuccess(res),
            (res: Response) => this.onSaveError(res.json()));
    }

    private onSaveSuccess(res: Response) {
        this.activeModal.close(true)
    }

    private onSaveError(error: any) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-search-reindex-popup',
    template: ''
})
export class ElasticSearchReindexPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(private route: ActivatedRoute,
                private elasticSearchReindexModalService: ElasticSearchReindexModalService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.elasticSearchReindexModalService.open(ElasticSearchReindexDialogComponent as Component);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
