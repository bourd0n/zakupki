package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Complaint.
 */
@Entity
@Table(name = "complaint")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "complaint")
public class Complaint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "complaint_number", nullable = false)
    private String complaintNumber;

    @Column(name = "reg_date")
    private LocalDate regDate;

    @Size(max = 4000)
    @Column(name = "text", length = 4000)
    private String text;

    @ManyToOne
    private RegistrationKo registrationKo;

    @ManyToOne
    private Purchase purchase;

    @ManyToOne
    private Applicant applicant;

    @ManyToOne
    private Client indictedClient;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public Complaint complaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
        return this;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public LocalDate getRegDate() {
        return regDate;
    }

    public Complaint regDate(LocalDate regDate) {
        this.regDate = regDate;
        return this;
    }

    public void setRegDate(LocalDate regDate) {
        this.regDate = regDate;
    }

    public String getText() {
        return text;
    }

    public Complaint text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public RegistrationKo getRegistrationKo() {
        return registrationKo;
    }

    public Complaint registrationKo(RegistrationKo registrationKo) {
        this.registrationKo = registrationKo;
        return this;
    }

    public void setRegistrationKo(RegistrationKo registrationKo) {
        this.registrationKo = registrationKo;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public Complaint purchase(Purchase purchase) {
        this.purchase = purchase;
        return this;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public Complaint applicant(Applicant applicant) {
        this.applicant = applicant;
        return this;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Client getIndictedClient() {
        return indictedClient;
    }

    public Complaint indictedClient(Client client) {
        this.indictedClient = client;
        return this;
    }

    public void setIndictedClient(Client client) {
        this.indictedClient = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Complaint complaint = (Complaint) o;
        if (complaint.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), complaint.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Complaint{" +
            "id=" + getId() +
            ", complaintNumber='" + getComplaintNumber() + "'" +
            ", regDate='" + getRegDate() + "'" +
            ", text='" + getText() + "'" +
            "}";
    }
}
