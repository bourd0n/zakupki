package com.alex.zakupki.service.mapper.xml

import java.time.ZoneId
import java.util.concurrent.TimeUnit
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general
import com.alex.zakupki.domain.{CheckComplaintResult, Client, Complaint, Owner}
import com.alex.zakupki.repository.{CheckComplaintResultRepository, ComplaintRepository}
import com.google.common.cache.{Cache, CacheBuilder}
import org.springframework.stereotype.Component

@Component
class CheckComplaintResultXmlToDataMapper @Inject()(val checkComplaintResultRepository: CheckComplaintResultRepository,
                                                    val clientXmlToDataMapper: XmlToDataMapper[general.Client, Client],
                                                    val complaintRepository: ComplaintRepository,
                                                    val ownerXmlToDataMapper: XmlToDataMapper[general.Owner, Owner])
    extends XmlToDataMapper[general.CheckComplaintResult, CheckComplaintResult] with DateToLocalDateConversion {


    private val cache: Cache[String, CheckComplaintResult] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, CheckComplaintResult]]

    override def map(from: general.CheckComplaintResult): Option[CheckComplaintResult] = Option(from) map {
        fromCheckComplaintResult => {
            //locking is performed
            cache.get(fromCheckComplaintResult.checkResultNumber, () => {
                checkComplaintResultRepository
                    .findOneByCheckResultNumber(fromCheckComplaintResult.checkResultNumber)
                    .orElseGet(() => {
                        val checkComplaintResult = new CheckComplaintResult()
                            .checkResultNumber(fromCheckComplaintResult.checkResultNumber)
                            .checkSubject(clientXmlToDataMapper.map(from.checkSubject).orNull)
                            .result(from.result)
                            .createDate(from.createDate)
                            .owner(ownerXmlToDataMapper.map(from.owner).orNull)
                            .complaint(complaintRepository.findOneByComplaintNumber(from.complaintNumber).orElse({
                                //todo: only to not depend on complaints
                                val complaint = new Complaint()
                                complaint.complaintNumber(from.complaintNumber)
                                complaintRepository.save(complaint)
                            }))
                        checkComplaintResultRepository.save(checkComplaintResult)
                    })
            })
        }
    }
}
