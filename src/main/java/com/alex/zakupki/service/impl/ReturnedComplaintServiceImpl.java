package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.ReturnedComplaintService;
import com.alex.zakupki.domain.ReturnedComplaint;
import com.alex.zakupki.repository.ReturnedComplaintRepository;
import com.alex.zakupki.repository.search.ReturnedComplaintSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing ReturnedComplaint.
 */
@Service
@Transactional
public class ReturnedComplaintServiceImpl implements ReturnedComplaintService{

    private final Logger log = LoggerFactory.getLogger(ReturnedComplaintServiceImpl.class);

    private final ReturnedComplaintRepository returnedComplaintRepository;

    private final ReturnedComplaintSearchRepository returnedComplaintSearchRepository;

    public ReturnedComplaintServiceImpl(ReturnedComplaintRepository returnedComplaintRepository, ReturnedComplaintSearchRepository returnedComplaintSearchRepository) {
        this.returnedComplaintRepository = returnedComplaintRepository;
        this.returnedComplaintSearchRepository = returnedComplaintSearchRepository;
    }

    /**
     * Save a returnedComplaint.
     *
     * @param returnedComplaint the entity to save
     * @return the persisted entity
     */
    @Override
    public ReturnedComplaint save(ReturnedComplaint returnedComplaint) {
        log.debug("Request to save ReturnedComplaint : {}", returnedComplaint);
        ReturnedComplaint result = returnedComplaintRepository.save(returnedComplaint);
        returnedComplaintSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the returnedComplaints.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReturnedComplaint> findAll(Pageable pageable) {
        log.debug("Request to get all ReturnedComplaints");
        return returnedComplaintRepository.findAll(pageable);
    }

    /**
     *  Get one returnedComplaint by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ReturnedComplaint findOne(Long id) {
        log.debug("Request to get ReturnedComplaint : {}", id);
        return returnedComplaintRepository.findOne(id);
    }

    /**
     *  Delete the  returnedComplaint by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReturnedComplaint : {}", id);
        returnedComplaintRepository.delete(id);
        returnedComplaintSearchRepository.delete(id);
    }

    /**
     * Search for the returnedComplaint corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ReturnedComplaint> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of ReturnedComplaints for query {}", query);
        Page<ReturnedComplaint> result = returnedComplaintSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
