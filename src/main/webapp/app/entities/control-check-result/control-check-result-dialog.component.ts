import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ControlCheckResult } from './control-check-result.model';
import { ControlCheckResultPopupService } from './control-check-result-popup.service';
import { ControlCheckResultService } from './control-check-result.service';
import { Owner, OwnerService } from '../owner';
import { Client, ClientService } from '../client';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-control-check-result-dialog',
    templateUrl: './control-check-result-dialog.component.html'
})
export class ControlCheckResultDialogComponent implements OnInit {

    controlCheckResult: ControlCheckResult;
    isSaving: boolean;

    owners: Owner[];

    clients: Client[];
    createdDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private controlCheckResultService: ControlCheckResultService,
        private ownerService: OwnerService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.ownerService.query()
            .subscribe((res: ResponseWrapper) => { this.owners = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clientService.query()
            .subscribe((res: ResponseWrapper) => { this.clients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.controlCheckResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.controlCheckResultService.update(this.controlCheckResult));
        } else {
            this.subscribeToSaveResponse(
                this.controlCheckResultService.create(this.controlCheckResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<ControlCheckResult>) {
        result.subscribe((res: ControlCheckResult) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ControlCheckResult) {
        this.eventManager.broadcast({ name: 'controlCheckResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOwnerById(index: number, item: Owner) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-control-check-result-popup',
    template: ''
})
export class ControlCheckResultPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private controlCheckResultPopupService: ControlCheckResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.controlCheckResultPopupService
                    .open(ControlCheckResultDialogComponent as Component, params['id']);
            } else {
                this.controlCheckResultPopupService
                    .open(ControlCheckResultDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
