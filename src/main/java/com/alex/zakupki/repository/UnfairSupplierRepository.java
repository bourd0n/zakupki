package com.alex.zakupki.repository;

import com.alex.zakupki.domain.UnfairSupplier;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the UnfairSupplier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UnfairSupplierRepository extends JpaRepository<UnfairSupplier, Long> {

}
