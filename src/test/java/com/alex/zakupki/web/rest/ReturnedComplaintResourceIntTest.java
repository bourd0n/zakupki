package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.Complaint;
import com.alex.zakupki.domain.ReturnedComplaint;
import com.alex.zakupki.repository.ReturnedComplaintRepository;
import com.alex.zakupki.service.ReturnedComplaintService;
import com.alex.zakupki.repository.search.ReturnedComplaintSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ReturnedComplaintResource REST controller.
 *
 * @see ReturnedComplaintResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class ReturnedComplaintResourceIntTest {

    private static final String DEFAULT_RETURN_INFO_BASE = "AAAAAAAAAA";
    private static final String UPDATED_RETURN_INFO_BASE = "BBBBBBBBBB";

    @Autowired
    private ReturnedComplaintRepository returnedComplaintRepository;

    @Autowired
    private ReturnedComplaintService returnedComplaintService;

    @Autowired
    private ReturnedComplaintSearchRepository returnedComplaintSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restReturnedComplaintMockMvc;

    private ReturnedComplaint returnedComplaint;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ReturnedComplaintResource returnedComplaintResource = new ReturnedComplaintResource(returnedComplaintService);
        this.restReturnedComplaintMockMvc = MockMvcBuilders.standaloneSetup(returnedComplaintResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReturnedComplaint createEntity(EntityManager em) {
        Complaint complaint = new Complaint()
            .complaintNumber("AAAA");
        em.persist(complaint);
        em.flush();
        ReturnedComplaint returnedComplaint = new ReturnedComplaint()
            .returnInfoBase(DEFAULT_RETURN_INFO_BASE)
            .complaint(complaint);
        return returnedComplaint;
    }

    @Before
    public void initTest() {
        returnedComplaintSearchRepository.deleteAll();
        returnedComplaint = createEntity(em);
    }

    @Test
    @Transactional
    public void createReturnedComplaint() throws Exception {
        int databaseSizeBeforeCreate = returnedComplaintRepository.findAll().size();

        // Create the ReturnedComplaint
        restReturnedComplaintMockMvc.perform(post("/api/returned-complaints")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returnedComplaint)))
            .andExpect(status().isCreated());

        // Validate the ReturnedComplaint in the database
        List<ReturnedComplaint> returnedComplaintList = returnedComplaintRepository.findAll();
        assertThat(returnedComplaintList).hasSize(databaseSizeBeforeCreate + 1);
        ReturnedComplaint testReturnedComplaint = returnedComplaintList.get(returnedComplaintList.size() - 1);
        assertThat(testReturnedComplaint.getReturnInfoBase()).isEqualTo(DEFAULT_RETURN_INFO_BASE);

        // Validate the ReturnedComplaint in Elasticsearch
        ReturnedComplaint returnedComplaintEs = returnedComplaintSearchRepository.findOne(testReturnedComplaint.getId());
        assertThat(returnedComplaintEs).isEqualToComparingFieldByField(testReturnedComplaint);
    }

    @Test
    @Transactional
    public void createReturnedComplaintWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = returnedComplaintRepository.findAll().size();

        // Create the ReturnedComplaint with an existing ID
        returnedComplaint.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReturnedComplaintMockMvc.perform(post("/api/returned-complaints")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returnedComplaint)))
            .andExpect(status().isBadRequest());

        // Validate the ReturnedComplaint in the database
        List<ReturnedComplaint> returnedComplaintList = returnedComplaintRepository.findAll();
        assertThat(returnedComplaintList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllReturnedComplaints() throws Exception {
        // Initialize the database
        returnedComplaintRepository.saveAndFlush(returnedComplaint);

        // Get all the returnedComplaintList
        restReturnedComplaintMockMvc.perform(get("/api/returned-complaints?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(returnedComplaint.getId().intValue())))
            .andExpect(jsonPath("$.[*].returnInfoBase").value(hasItem(DEFAULT_RETURN_INFO_BASE.toString())));
    }

    @Test
    @Transactional
    public void getReturnedComplaint() throws Exception {
        // Initialize the database
        returnedComplaintRepository.saveAndFlush(returnedComplaint);

        // Get the returnedComplaint
        restReturnedComplaintMockMvc.perform(get("/api/returned-complaints/{id}", returnedComplaint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(returnedComplaint.getId().intValue()))
            .andExpect(jsonPath("$.returnInfoBase").value(DEFAULT_RETURN_INFO_BASE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingReturnedComplaint() throws Exception {
        // Get the returnedComplaint
        restReturnedComplaintMockMvc.perform(get("/api/returned-complaints/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReturnedComplaint() throws Exception {
        // Initialize the database
        returnedComplaintService.save(returnedComplaint);

        int databaseSizeBeforeUpdate = returnedComplaintRepository.findAll().size();

        // Update the returnedComplaint
        ReturnedComplaint updatedReturnedComplaint = returnedComplaintRepository.findOne(returnedComplaint.getId());
        updatedReturnedComplaint
            .returnInfoBase(UPDATED_RETURN_INFO_BASE);

        restReturnedComplaintMockMvc.perform(put("/api/returned-complaints")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedReturnedComplaint)))
            .andExpect(status().isOk());

        // Validate the ReturnedComplaint in the database
        List<ReturnedComplaint> returnedComplaintList = returnedComplaintRepository.findAll();
        assertThat(returnedComplaintList).hasSize(databaseSizeBeforeUpdate);
        ReturnedComplaint testReturnedComplaint = returnedComplaintList.get(returnedComplaintList.size() - 1);
        assertThat(testReturnedComplaint.getReturnInfoBase()).isEqualTo(UPDATED_RETURN_INFO_BASE);

        // Validate the ReturnedComplaint in Elasticsearch
        ReturnedComplaint returnedComplaintEs = returnedComplaintSearchRepository.findOne(testReturnedComplaint.getId());
        assertThat(returnedComplaintEs).isEqualToComparingFieldByField(testReturnedComplaint);
    }

    @Test
    @Transactional
    public void updateNonExistingReturnedComplaint() throws Exception {
        int databaseSizeBeforeUpdate = returnedComplaintRepository.findAll().size();

        // Create the ReturnedComplaint

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restReturnedComplaintMockMvc.perform(put("/api/returned-complaints")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(returnedComplaint)))
            .andExpect(status().isCreated());

        // Validate the ReturnedComplaint in the database
        List<ReturnedComplaint> returnedComplaintList = returnedComplaintRepository.findAll();
        assertThat(returnedComplaintList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteReturnedComplaint() throws Exception {
        // Initialize the database
        returnedComplaintService.save(returnedComplaint);

        int databaseSizeBeforeDelete = returnedComplaintRepository.findAll().size();

        // Get the returnedComplaint
        restReturnedComplaintMockMvc.perform(delete("/api/returned-complaints/{id}", returnedComplaint.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean returnedComplaintExistsInEs = returnedComplaintSearchRepository.exists(returnedComplaint.getId());
        assertThat(returnedComplaintExistsInEs).isFalse();

        // Validate the database is empty
        List<ReturnedComplaint> returnedComplaintList = returnedComplaintRepository.findAll();
        assertThat(returnedComplaintList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchReturnedComplaint() throws Exception {
        // Initialize the database
        returnedComplaintService.save(returnedComplaint);

        // Search the returnedComplaint
        restReturnedComplaintMockMvc.perform(get("/api/_search/returned-complaints?query=id:" + returnedComplaint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(returnedComplaint.getId().intValue())))
            .andExpect(jsonPath("$.[*].returnInfoBase").value(hasItem(DEFAULT_RETURN_INFO_BASE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReturnedComplaint.class);
        ReturnedComplaint returnedComplaint1 = new ReturnedComplaint();
        returnedComplaint1.setId(1L);
        ReturnedComplaint returnedComplaint2 = new ReturnedComplaint();
        returnedComplaint2.setId(returnedComplaint1.getId());
        assertThat(returnedComplaint1).isEqualTo(returnedComplaint2);
        returnedComplaint2.setId(2L);
        assertThat(returnedComplaint1).isNotEqualTo(returnedComplaint2);
        returnedComplaint1.setId(null);
        assertThat(returnedComplaint1).isNotEqualTo(returnedComplaint2);
    }
}
