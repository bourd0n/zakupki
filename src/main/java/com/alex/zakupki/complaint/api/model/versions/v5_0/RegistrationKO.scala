package com.alex.zakupki.complaint.api.model.versions.v5_0

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.GeneralizedEntity
import com.alex.zakupki.complaint.api.model.general


//@Entity
//@Table(name = "registration_ko")
@XmlAccessorType(XmlAccessType.FIELD)
case class RegistrationKO(@xmlPath("oos:regNum/text()")
                          regNum: String,
                          @xmlPath("oos:fullName/text()")
                          fullName: String) extends GeneralizedEntity[general.RegistrationKO] {
    private def this() = this(null, null)

    override def mapToGeneral(): general.RegistrationKO = {
        general.RegistrationKO(regNum, fullName.trim)
    }
}
