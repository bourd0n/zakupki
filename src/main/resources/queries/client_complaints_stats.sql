SELECT
    to_char(res.result_date, '9999') result_date,
    res."COMPLAINT_NO_VIOLATIONS" noViolations,
    res."COMPLAINT_VIOLATIONS"    violations,
    res."COMPLAINT_PARTLY_VALID"  partlyValid,
    res."COMPLAINT_NOT_MATCHED"   notMatched,
    res.returned_count            returned_count,
    res.count                     complaints_sum,
    res."EMPTY"                   empty
FROM
    (SELECT
         coalesce(res.result_date, all_count.create_date) result_date,
         res."COMPLAINT_NO_VIOLATIONS",
         res."COMPLAINT_VIOLATIONS",
         res."COMPLAINT_PARTLY_VALID",
         res."COMPLAINT_NOT_MATCHED",
         res."EMPTY",
         all_count.count,
         res.returned_count
     FROM
         (SELECT
              coalesce(res.result_date, returned.create_date) result_date,
              res."COMPLAINT_NO_VIOLATIONS",
              res."COMPLAINT_VIOLATIONS",
              res."COMPLAINT_PARTLY_VALID",
              res."COMPLAINT_NOT_MATCHED",
              res."EMPTY",
              returned.returned_count
          FROM
              (SELECT *
               FROM crosstab('SELECT
                result_date,
                subq.checkResult,
                cnt
              FROM (SELECT
                      date_part(''year'', ccr.create_date) result_date,
                      COALESCE(ccr.result, '''')           checkResult,
                      count(*)                           cnt
                    FROM check_complaint_result ccr where ccr.check_subject_id = 11135
                    GROUP BY result_date, checkResult) subq
              ORDER BY 1, 2',
                             $$VALUES ('COMPLAINT_NO_VIOLATIONS'::TEXT), ('COMPLAINT_VIOLATIONS'::TEXT), ('COMPLAINT_PARTLY_VALID'::TEXT), ('COMPLAINT_NOT_MATCHED'::TEXT), (''::TEXT)$$)
                   AS ct ("result_date" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
              FULL OUTER JOIN (SELECT
                                   date_part('year', c.reg_date) create_date,
                                   COALESCE(count(*), 0)                          returned_count
                               FROM
                                   complaint c, returned_complaint rc
                               WHERE rc.complaint_id = c.id AND c.indicted_client_id = 11135
                               GROUP BY create_date) returned ON (res.result_date = returned.create_date)) AS res
         FULL OUTER JOIN
         (SELECT
              date_part('year', c.reg_date) create_date,
              COALESCE(count(*), 0)                          count
          FROM
              complaint c
          WHERE c.indicted_client_id = 11135
          GROUP BY create_date) all_count
             ON (res.result_date = all_count.create_date)) res
ORDER BY result_date ASC NULLS LAST;
