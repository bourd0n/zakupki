package com.alex.zakupki.xml

import java.nio.file.{Files, Paths}
import javax.inject.Inject

import com.alex.zakupki.ZakupkiApp
import com.alex.zakupki.complaint.api.model.general._
import com.alex.zakupki.complaint.logic.config.ServerConfig
import com.alex.zakupki.complaint.logic.xml.XMLProcessor
import org.assertj.core.api.Assertions._
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(classOf[SpringRunner])
@SpringBootTest(classes = Array(classOf[ZakupkiApp]))
class FailedXmlParsingTest {
    //todo: parametrized test?

    val log: Logger = LoggerFactory.getLogger(getClass)

    @Inject
    val xmlProcessor: XMLProcessor = null

    @Test
    def testXmlWhatFasFailed(): Unit = {
        val tmpPath = Paths.get(ServerConfig.server_path + "/tmp")
        Files.walk(tmpPath)
            .filter(Files.isRegularFile(_))
            .filter(_.getFileName.toString.endsWith("xml"))
            .parallel()
            .forEach(path => {
                log.debug("Start test xml {}", path)
                val parsed = xmlProcessor.processXmlFile(path)
                if (path.getFileName.toString.startsWith("complaint_")) {
                    assertThat(parsed.isDefined).isTrue
                    assertThat(parsed.get).isInstanceOf(classOf[Complaint])
                    val complaint = parsed.get.asInstanceOf[Complaint]
                    assertThat(complaint.complaintNumber).isNotEmpty
                    if (complaint.purchase != null) {
                        assertThat(complaint.purchase.purchaseNumber).isNotEmpty
                    }
                    assertThat(complaint.applicant.organizationName).isNotEmpty
                    if (complaint.indictedClient != null) {
                        assertThat(complaint.indictedClient.fullName).isNotEmpty
                    }
                    assertThat(complaint.registrationKO.regNum).isNotEmpty
                    assertThat(complaint.registrationKO.fullName).isNotEmpty
                } else if (path.getFileName.toString.startsWith("checkResult_")) {
                    if (parsed.isEmpty) {
                        return
                    }
                    val parsedValue = parsed.get
                    assertThat(parsedValue.getClass)
                        .isIn(classOf[CheckComplaintResult], classOf[PlannedCheckResult], classOf[UnplannedCheckResult])
                    val checkResult = parsedValue.asInstanceOf[CheckResult]
                    assertThat(checkResult.checkResultNumber).isNotEmpty
                    assertThat(checkResult.createDate).isNotNull
                    assertThat(checkResult.owner).isNotNull
                    assertThat(checkResult.owner.regNum).isNotEmpty
                    assertThat(checkResult.owner.fullName).isNotEmpty
                    parsedValue match {
                        case result: CheckComplaintResult =>
                            assertThat(result.complaintNumber).isNotEmpty
                        case _ =>
                    }
                }

            })
    }
}
