package com.alex.zakupki.repository;

import com.alex.zakupki.domain.PlannedCheckResult;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the PlannedCheckResult entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlannedCheckResultRepository extends JpaRepository<PlannedCheckResult, Long> {

    Optional<PlannedCheckResult> findOneByCheckResultNumber(String checkResultNumber);

}
