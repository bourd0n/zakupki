import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Complaint } from './complaint.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ComplaintService {

    private resourceUrl = SERVER_API_URL + 'api/complaints';
    private resourceUrlRegKo = SERVER_API_URL + 'api/complaints/registrationKo';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/complaints';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(complaint: Complaint): Observable<Complaint> {
        const copy = this.convert(complaint);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(complaint: Complaint): Observable<Complaint> {
        const copy = this.convert(complaint);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Complaint> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    findComplaintsByRegistrationKoAndBetweenDates(req?: any): Observable<ResponseWrapper> {
        const options = this.createRequestOption(req);
        return this.http.get(this.resourceUrlRegKo, options)
            .map((res: any) => this.convertResponse(res))
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Complaint.
     */
    private convertItemFromServer(json: any): Complaint {
        const entity: Complaint = Object.assign(new Complaint(), json);
        entity.regDate = this.dateUtils
            .convertLocalDateFromServer(json.regDate);
        return entity;
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        const options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            const params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            if (req.from) {
                params.set('from', req.from);
            }
            if (req.to) {
                params.set('to', req.to);
            }
            if (req.regKoId) {
                params.set('regKoId', req.regKoId);
            }
            params.set('query', req.query);

            options.params = params;
        }
        return options;
    }

    /**
     * Convert a Complaint to a JSON which can be sent to the server.
     */
    private convert(complaint: Complaint): Complaint {
        const copy: Complaint = Object.assign({}, complaint);
        copy.regDate = this.dateUtils
            .convertLocalDateToServer(complaint.regDate);
        return copy;
    }
}
