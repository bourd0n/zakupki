package com.alex.zakupki.complaint.api.model.versions.v5_0

import javax.xml.bind.annotation.XmlAccessType
;

//@Entity
//@Table(name = "publish_org")
@XmlAccessorType(XmlAccessType.FIELD)
case class PublishOrg(@xmlPath("oos:regNum/text()")
                      regNum: String,
                      @xmlPath("oos:fullName/text()")
                      fullName: String) {
    private def this() = this(null, null)
}
