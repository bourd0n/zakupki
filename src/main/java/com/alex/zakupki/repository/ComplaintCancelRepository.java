package com.alex.zakupki.repository;

import com.alex.zakupki.domain.ComplaintCancel;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ComplaintCancel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ComplaintCancelRepository extends JpaRepository<ComplaintCancel, Long> {

}
