SELECT
  a.id,
  a.organization_name,
  a.applicant_type,
  res_all."COMPLAINT_NO_VIOLATIONS",
  res_all."COMPLAINT_VIOLATIONS",
  res_all."COMPLAINT_PARTLY_VALID",
  res_all."COMPLAINT_NOT_MATCHED",
  res_all.count complaints_sum,
  res_all."EMPTY"
FROM applicant a, (SELECT
                     res.applicant_id       appl_id_1,
                     all_count.applicant_id appl_id_2,
                     res."COMPLAINT_NO_VIOLATIONS",
                     res."COMPLAINT_VIOLATIONS",
                     res."COMPLAINT_PARTLY_VALID",
                     res."COMPLAINT_NOT_MATCHED",
                     res."EMPTY",
                     all_count.count
                   FROM (SELECT *
                         FROM crosstab('SELECT
                              subq.applicant_id,
                              subq.checkResult,
                              subq.complaints_count
                              FROM (SELECT
                                    c.applicant_id           applicant_id,
                                    COALESCE(ccr.result, '''') checkResult,
                                    count(*) complaints_count
                                  FROM complaint c, check_complaint_result ccr
                                  WHERE ccr.complaint_id = c.id
                                    AND date_trunc(''year'', ccr.create_date) = to_date(''2016'', ''yyyy'')
                                  GROUP BY c.applicant_id, checkResult) subq
                              ORDER BY 1, 2',
                                       $$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$)
                           AS ct ("applicant_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) AS res
                     FULL OUTER JOIN (SELECT
                                        c.applicant_id        applicant_id,
                                        coalesce(count(*), 0) count
                                      FROM
                                        complaint c
                                      WHERE date_trunc('year', c.reg_date) = to_date('2016', 'yyyy')
                                      GROUP BY c.applicant_id) all_count
                       ON (res.applicant_id = all_count.applicant_id)
                  ) res_all
WHERE a.id = coalesce(res_all.appl_id_1, res_all.appl_id_2)
ORDER BY complaints_sum DESC NULLS LAST;
