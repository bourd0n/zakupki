import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PublishOrg } from './publish-org.model';
import { PublishOrgService } from './publish-org.service';

@Component({
    selector: 'jhi-publish-org-detail',
    templateUrl: './publish-org-detail.component.html'
})
export class PublishOrgDetailComponent implements OnInit, OnDestroy {

    publishOrg: PublishOrg;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private publishOrgService: PublishOrgService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPublishOrgs();
    }

    load(id) {
        this.publishOrgService.find(id).subscribe((publishOrg) => {
            this.publishOrg = publishOrg;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPublishOrgs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'publishOrgListModification',
            (response) => this.load(this.publishOrg.id)
        );
    }
}
