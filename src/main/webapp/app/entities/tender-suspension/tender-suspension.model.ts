import { BaseEntity } from './../../shared';

export class TenderSuspension implements BaseEntity {
    constructor(
        public id?: number,
        public complaintNumber?: string,
        public regDate?: any,
        public action?: string,
    ) {
    }
}
