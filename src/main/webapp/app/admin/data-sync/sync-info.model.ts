export class SyncInfo {
    constructor(
        public name?: string,
        public status?: string,
        public syncDate?: any
    ) {}

}
