import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    ReturnedComplaintService,
    ReturnedComplaintPopupService,
    ReturnedComplaintComponent,
    ReturnedComplaintDetailComponent,
    ReturnedComplaintDialogComponent,
    ReturnedComplaintPopupComponent,
    ReturnedComplaintDeletePopupComponent,
    ReturnedComplaintDeleteDialogComponent,
    returnedComplaintRoute,
    returnedComplaintPopupRoute,
} from './';

const ENTITY_STATES = [
    ...returnedComplaintRoute,
    ...returnedComplaintPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ReturnedComplaintComponent,
        ReturnedComplaintDetailComponent,
        ReturnedComplaintDialogComponent,
        ReturnedComplaintDeleteDialogComponent,
        ReturnedComplaintPopupComponent,
        ReturnedComplaintDeletePopupComponent,
    ],
    entryComponents: [
        ReturnedComplaintComponent,
        ReturnedComplaintDialogComponent,
        ReturnedComplaintPopupComponent,
        ReturnedComplaintDeleteDialogComponent,
        ReturnedComplaintDeletePopupComponent,
    ],
    providers: [
        ReturnedComplaintService,
        ReturnedComplaintPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiReturnedComplaintModule {}
