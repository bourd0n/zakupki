package com.alex.zakupki.complaint.api.model.general

case class Owner(regNum: String, fullName: String)
