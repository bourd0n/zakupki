package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.ControlCheckResult;
import com.alex.zakupki.repository.ControlCheckResultRepository;
import com.alex.zakupki.service.ControlCheckResultService;
import com.alex.zakupki.repository.search.ControlCheckResultSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ControlCheckResultResource REST controller.
 *
 * @see ControlCheckResultResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class ControlCheckResultResourceIntTest {

    private static final String DEFAULT_CHECK_RESULT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CHECK_RESULT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_RESULT = "AAAAAAAAAA";
    private static final String UPDATED_RESULT = "BBBBBBBBBB";

    @Autowired
    private ControlCheckResultRepository controlCheckResultRepository;

    @Autowired
    private ControlCheckResultService controlCheckResultService;

    @Autowired
    private ControlCheckResultSearchRepository controlCheckResultSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restControlCheckResultMockMvc;

    private ControlCheckResult controlCheckResult;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ControlCheckResultResource controlCheckResultResource = new ControlCheckResultResource(controlCheckResultService);
        this.restControlCheckResultMockMvc = MockMvcBuilders.standaloneSetup(controlCheckResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ControlCheckResult createEntity(EntityManager em) {
        ControlCheckResult controlCheckResult = new ControlCheckResult()
            .checkResultNumber(DEFAULT_CHECK_RESULT_NUMBER)
            .createdDate(DEFAULT_CREATED_DATE)
            .result(DEFAULT_RESULT);
        return controlCheckResult;
    }

    @Before
    public void initTest() {
        controlCheckResultSearchRepository.deleteAll();
        controlCheckResult = createEntity(em);
    }

    @Test
    @Transactional
    public void createControlCheckResult() throws Exception {
        int databaseSizeBeforeCreate = controlCheckResultRepository.findAll().size();

        // Create the ControlCheckResult
        restControlCheckResultMockMvc.perform(post("/api/control-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(controlCheckResult)))
            .andExpect(status().isCreated());

        // Validate the ControlCheckResult in the database
        List<ControlCheckResult> controlCheckResultList = controlCheckResultRepository.findAll();
        assertThat(controlCheckResultList).hasSize(databaseSizeBeforeCreate + 1);
        ControlCheckResult testControlCheckResult = controlCheckResultList.get(controlCheckResultList.size() - 1);
        assertThat(testControlCheckResult.getCheckResultNumber()).isEqualTo(DEFAULT_CHECK_RESULT_NUMBER);
        assertThat(testControlCheckResult.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testControlCheckResult.getResult()).isEqualTo(DEFAULT_RESULT);

        // Validate the ControlCheckResult in Elasticsearch
        ControlCheckResult controlCheckResultEs = controlCheckResultSearchRepository.findOne(testControlCheckResult.getId());
        assertThat(controlCheckResultEs).isEqualToComparingFieldByField(testControlCheckResult);
    }

    @Test
    @Transactional
    public void createControlCheckResultWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = controlCheckResultRepository.findAll().size();

        // Create the ControlCheckResult with an existing ID
        controlCheckResult.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restControlCheckResultMockMvc.perform(post("/api/control-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(controlCheckResult)))
            .andExpect(status().isBadRequest());

        // Validate the ControlCheckResult in the database
        List<ControlCheckResult> controlCheckResultList = controlCheckResultRepository.findAll();
        assertThat(controlCheckResultList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCheckResultNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = controlCheckResultRepository.findAll().size();
        // set the field null
        controlCheckResult.setCheckResultNumber(null);

        // Create the ControlCheckResult, which fails.

        restControlCheckResultMockMvc.perform(post("/api/control-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(controlCheckResult)))
            .andExpect(status().isBadRequest());

        List<ControlCheckResult> controlCheckResultList = controlCheckResultRepository.findAll();
        assertThat(controlCheckResultList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllControlCheckResults() throws Exception {
        // Initialize the database
        controlCheckResultRepository.saveAndFlush(controlCheckResult);

        // Get all the controlCheckResultList
        restControlCheckResultMockMvc.perform(get("/api/control-check-results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(controlCheckResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void getControlCheckResult() throws Exception {
        // Initialize the database
        controlCheckResultRepository.saveAndFlush(controlCheckResult);

        // Get the controlCheckResult
        restControlCheckResultMockMvc.perform(get("/api/control-check-results/{id}", controlCheckResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(controlCheckResult.getId().intValue()))
            .andExpect(jsonPath("$.checkResultNumber").value(DEFAULT_CHECK_RESULT_NUMBER.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingControlCheckResult() throws Exception {
        // Get the controlCheckResult
        restControlCheckResultMockMvc.perform(get("/api/control-check-results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateControlCheckResult() throws Exception {
        // Initialize the database
        controlCheckResultService.save(controlCheckResult);

        int databaseSizeBeforeUpdate = controlCheckResultRepository.findAll().size();

        // Update the controlCheckResult
        ControlCheckResult updatedControlCheckResult = controlCheckResultRepository.findOne(controlCheckResult.getId());
        updatedControlCheckResult
            .checkResultNumber(UPDATED_CHECK_RESULT_NUMBER)
            .createdDate(UPDATED_CREATED_DATE)
            .result(UPDATED_RESULT);

        restControlCheckResultMockMvc.perform(put("/api/control-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedControlCheckResult)))
            .andExpect(status().isOk());

        // Validate the ControlCheckResult in the database
        List<ControlCheckResult> controlCheckResultList = controlCheckResultRepository.findAll();
        assertThat(controlCheckResultList).hasSize(databaseSizeBeforeUpdate);
        ControlCheckResult testControlCheckResult = controlCheckResultList.get(controlCheckResultList.size() - 1);
        assertThat(testControlCheckResult.getCheckResultNumber()).isEqualTo(UPDATED_CHECK_RESULT_NUMBER);
        assertThat(testControlCheckResult.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testControlCheckResult.getResult()).isEqualTo(UPDATED_RESULT);

        // Validate the ControlCheckResult in Elasticsearch
        ControlCheckResult controlCheckResultEs = controlCheckResultSearchRepository.findOne(testControlCheckResult.getId());
        assertThat(controlCheckResultEs).isEqualToComparingFieldByField(testControlCheckResult);
    }

    @Test
    @Transactional
    public void updateNonExistingControlCheckResult() throws Exception {
        int databaseSizeBeforeUpdate = controlCheckResultRepository.findAll().size();

        // Create the ControlCheckResult

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restControlCheckResultMockMvc.perform(put("/api/control-check-results")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(controlCheckResult)))
            .andExpect(status().isCreated());

        // Validate the ControlCheckResult in the database
        List<ControlCheckResult> controlCheckResultList = controlCheckResultRepository.findAll();
        assertThat(controlCheckResultList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteControlCheckResult() throws Exception {
        // Initialize the database
        controlCheckResultService.save(controlCheckResult);

        int databaseSizeBeforeDelete = controlCheckResultRepository.findAll().size();

        // Get the controlCheckResult
        restControlCheckResultMockMvc.perform(delete("/api/control-check-results/{id}", controlCheckResult.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean controlCheckResultExistsInEs = controlCheckResultSearchRepository.exists(controlCheckResult.getId());
        assertThat(controlCheckResultExistsInEs).isFalse();

        // Validate the database is empty
        List<ControlCheckResult> controlCheckResultList = controlCheckResultRepository.findAll();
        assertThat(controlCheckResultList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchControlCheckResult() throws Exception {
        // Initialize the database
        controlCheckResultService.save(controlCheckResult);

        // Search the controlCheckResult
        restControlCheckResultMockMvc.perform(get("/api/_search/control-check-results?query=id:" + controlCheckResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(controlCheckResult.getId().intValue())))
            .andExpect(jsonPath("$.[*].checkResultNumber").value(hasItem(DEFAULT_CHECK_RESULT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ControlCheckResult.class);
        ControlCheckResult controlCheckResult1 = new ControlCheckResult();
        controlCheckResult1.setId(1L);
        ControlCheckResult controlCheckResult2 = new ControlCheckResult();
        controlCheckResult2.setId(controlCheckResult1.getId());
        assertThat(controlCheckResult1).isEqualTo(controlCheckResult2);
        controlCheckResult2.setId(2L);
        assertThat(controlCheckResult1).isNotEqualTo(controlCheckResult2);
        controlCheckResult1.setId(null);
        assertThat(controlCheckResult1).isNotEqualTo(controlCheckResult2);
    }
}
