package alex.zakupki.complaint.web.controller

//import alex.zakupki.complaint.api.dao.SquerylTest

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import org.springframework.web.servlet.ModelAndView

@Controller
class StartController {

  val log = LoggerFactory.getLogger(classOf[StartController])

  @RequestMapping(value = Array("/", "/start.html"), method = Array(RequestMethod.GET))
  def startPage() = {
    log.debug("Start controller is called")
    //    val model = new ModelAndView("index")
    //    model.addObject("complaintNum", ComplaintsDAO.countAllComplaints())
    val model = new ModelAndView("homepage")
    model
  }
}
