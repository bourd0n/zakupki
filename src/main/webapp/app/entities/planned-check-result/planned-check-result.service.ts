import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PlannedCheckResult } from './planned-check-result.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PlannedCheckResultService {

    private resourceUrl = SERVER_API_URL + 'api/planned-check-results';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/planned-check-results';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(plannedCheckResult: PlannedCheckResult): Observable<PlannedCheckResult> {
        const copy = this.convert(plannedCheckResult);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(plannedCheckResult: PlannedCheckResult): Observable<PlannedCheckResult> {
        const copy = this.convert(plannedCheckResult);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PlannedCheckResult> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PlannedCheckResult.
     */
    private convertItemFromServer(json: any): PlannedCheckResult {
        const entity: PlannedCheckResult = Object.assign(new PlannedCheckResult(), json);
        entity.createdDate = this.dateUtils
            .convertLocalDateFromServer(json.createdDate);
        entity.actDate = this.dateUtils
            .convertLocalDateFromServer(json.actDate);
        entity.actPrescriptionDate = this.dateUtils
            .convertLocalDateFromServer(json.actPrescriptionDate);
        return entity;
    }

    /**
     * Convert a PlannedCheckResult to a JSON which can be sent to the server.
     */
    private convert(plannedCheckResult: PlannedCheckResult): PlannedCheckResult {
        const copy: PlannedCheckResult = Object.assign({}, plannedCheckResult);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(plannedCheckResult.createdDate);
        copy.actDate = this.dateUtils
            .convertLocalDateToServer(plannedCheckResult.actDate);
        copy.actPrescriptionDate = this.dateUtils
            .convertLocalDateToServer(plannedCheckResult.actPrescriptionDate);
        return copy;
    }
}
