package com.alex.zakupki.service.dto

case class ApplicantStats (organizationName: String,
                          applicantType: String,
                          complaintsCount: Int,
                          noViolations: Int,
                          violations: Int,
                          partlyValid: Int,
                          notMatched: Int,
                          empty: Int)
