package com.alex.zakupki.service;

import com.alex.zakupki.domain.Complaint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

/**
 * Service Interface for managing Complaint.
 */
public interface ComplaintService {

    /**
     * Save a complaint.
     *
     * @param complaint the entity to save
     * @return the persisted entity
     */
    Complaint save(Complaint complaint);

    /**
     *  Get all the complaints.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Complaint> findAll(Pageable pageable);

    Page<Complaint> findAllByRegistrationKoIdAndRegDateBetween(Long registrationKoId,
                                                               LocalDate fromDate,
                                                               LocalDate toDate,
                                                               Pageable pageable);

    /**
     *  Get the "id" complaint.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Complaint findOne(Long id);

    /**
     *  Delete the "id" complaint.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the complaint corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Complaint> search(String query, Pageable pageable);
}
