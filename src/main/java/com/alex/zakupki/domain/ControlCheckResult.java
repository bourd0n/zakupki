package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ControlCheckResult.
 */
@Entity
@Table(name = "control_check_result")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "controlcheckresult")
public class ControlCheckResult implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "check_result_number", nullable = false)
    private String checkResultNumber;

    @Column(name = "created_date")
    private LocalDate createdDate;

    @Column(name = "result")
    private String result;

    @ManyToOne
    private Owner owner;

    @ManyToOne
    private Client clientComplaint;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCheckResultNumber() {
        return checkResultNumber;
    }

    public ControlCheckResult checkResultNumber(String checkResultNumber) {
        this.checkResultNumber = checkResultNumber;
        return this;
    }

    public void setCheckResultNumber(String checkResultNumber) {
        this.checkResultNumber = checkResultNumber;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public ControlCheckResult createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getResult() {
        return result;
    }

    public ControlCheckResult result(String result) {
        this.result = result;
        return this;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Owner getOwner() {
        return owner;
    }

    public ControlCheckResult owner(Owner owner) {
        this.owner = owner;
        return this;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Client getClientComplaint() {
        return clientComplaint;
    }

    public ControlCheckResult clientComplaint(Client client) {
        this.clientComplaint = client;
        return this;
    }

    public void setClientComplaint(Client client) {
        this.clientComplaint = client;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ControlCheckResult controlCheckResult = (ControlCheckResult) o;
        if (controlCheckResult.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), controlCheckResult.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ControlCheckResult{" +
            "id=" + getId() +
            ", checkResultNumber='" + getCheckResultNumber() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", result='" + getResult() + "'" +
            "}";
    }
}
