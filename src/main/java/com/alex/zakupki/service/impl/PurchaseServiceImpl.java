package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.PurchaseService;
import com.alex.zakupki.domain.Purchase;
import com.alex.zakupki.repository.PurchaseRepository;
import com.alex.zakupki.repository.search.PurchaseSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Purchase.
 */
@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService{

    private final Logger log = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    private final PurchaseRepository purchaseRepository;

    private final PurchaseSearchRepository purchaseSearchRepository;

    public PurchaseServiceImpl(PurchaseRepository purchaseRepository, PurchaseSearchRepository purchaseSearchRepository) {
        this.purchaseRepository = purchaseRepository;
        this.purchaseSearchRepository = purchaseSearchRepository;
    }

    /**
     * Save a purchase.
     *
     * @param purchase the entity to save
     * @return the persisted entity
     */
    @Override
    public Purchase save(Purchase purchase) {
        log.debug("Request to save Purchase : {}", purchase);
        Purchase result = purchaseRepository.save(purchase);
        purchaseSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the purchases.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Purchase> findAll(Pageable pageable) {
        log.debug("Request to get all Purchases");
        return purchaseRepository.findAll(pageable);
    }

    /**
     *  Get one purchase by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Purchase findOne(Long id) {
        log.debug("Request to get Purchase : {}", id);
        return purchaseRepository.findOne(id);
    }

    /**
     *  Delete the  purchase by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Purchase : {}", id);
        purchaseRepository.delete(id);
        purchaseSearchRepository.delete(id);
    }

    /**
     * Search for the purchase corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Purchase> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Purchases for query {}", query);
        Page<Purchase> result = purchaseSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
