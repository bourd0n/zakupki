import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Purchase e2e test', () => {

    let navBarPage: NavBarPage;
    let purchaseDialogPage: PurchaseDialogPage;
    let purchaseComponentsPage: PurchaseComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Purchases', () => {
        navBarPage.goToEntity('purchase');
        purchaseComponentsPage = new PurchaseComponentsPage();
        expect(purchaseComponentsPage.getTitle()).toMatch(/zakupkiApp.purchase.home.title/);

    });

    it('should load create Purchase dialog', () => {
        purchaseComponentsPage.clickOnCreateButton();
        purchaseDialogPage = new PurchaseDialogPage();
        expect(purchaseDialogPage.getModalTitle()).toMatch(/zakupkiApp.purchase.home.createOrEditLabel/);
        purchaseDialogPage.close();
    });

    it('should create and save Purchases', () => {
        purchaseComponentsPage.clickOnCreateButton();
        purchaseDialogPage.setPurchaseNumberInput('purchaseNumber');
        expect(purchaseDialogPage.getPurchaseNumberInput()).toMatch('purchaseNumber');
        purchaseDialogPage.setDetermSupplierMethodInput('determSupplierMethod');
        expect(purchaseDialogPage.getDetermSupplierMethodInput()).toMatch('determSupplierMethod');
        purchaseDialogPage.setElectronicPlatformInput('electronicPlatform');
        expect(purchaseDialogPage.getElectronicPlatformInput()).toMatch('electronicPlatform');
        purchaseDialogPage.setMaxCostInput('5');
        expect(purchaseDialogPage.getMaxCostInput()).toMatch('5');
        purchaseDialogPage.save();
        expect(purchaseDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PurchaseComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-purchase div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PurchaseDialogPage {
    modalTitle = element(by.css('h4#myPurchaseLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    purchaseNumberInput = element(by.css('input#field_purchaseNumber'));
    determSupplierMethodInput = element(by.css('input#field_determSupplierMethod'));
    electronicPlatformInput = element(by.css('input#field_electronicPlatform'));
    maxCostInput = element(by.css('input#field_maxCost'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setPurchaseNumberInput = function (purchaseNumber) {
        this.purchaseNumberInput.sendKeys(purchaseNumber);
    }

    getPurchaseNumberInput = function () {
        return this.purchaseNumberInput.getAttribute('value');
    }

    setDetermSupplierMethodInput = function (determSupplierMethod) {
        this.determSupplierMethodInput.sendKeys(determSupplierMethod);
    }

    getDetermSupplierMethodInput = function () {
        return this.determSupplierMethodInput.getAttribute('value');
    }

    setElectronicPlatformInput = function (electronicPlatform) {
        this.electronicPlatformInput.sendKeys(electronicPlatform);
    }

    getElectronicPlatformInput = function () {
        return this.electronicPlatformInput.getAttribute('value');
    }

    setMaxCostInput = function (maxCost) {
        this.maxCostInput.sendKeys(maxCost);
    }

    getMaxCostInput = function () {
        return this.maxCostInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
