package alex.zakupki.complaint.temp

@deprecated
object Consts {
  val FTP_URL_ZAKUPKI: String = "ftp.zakupki.gov.ru"
  val DOWNLOAD_PATH: String = "i:\\zakupki\\zakupki_data\\zakupki_data\\"
  val MAIN_PATH: String = "i:\\programming\\zakupkicomplaints\\"
  val COMPLAINT_PATH: String = DOWNLOAD_PATH + "complaint\\"
  val CHECK_RESULT_PATH: String = DOWNLOAD_PATH + "checkResult\\"
  val UNFAIR_SUPPLIER_PATH: String = DOWNLOAD_PATH + "unfairSupplier\\"
  val PURCHASE_DETAILS_URL: String = "http://zakupki.gov.ru/epz/order/notice/ea44/view/common-info.html?regNumber="
}