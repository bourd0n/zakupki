import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TenderSuspension } from './tender-suspension.model';
import { TenderSuspensionService } from './tender-suspension.service';

@Injectable()
export class TenderSuspensionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private tenderSuspensionService: TenderSuspensionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.tenderSuspensionService.find(id).subscribe((tenderSuspension) => {
                    if (tenderSuspension.regDate) {
                        tenderSuspension.regDate = {
                            year: tenderSuspension.regDate.getFullYear(),
                            month: tenderSuspension.regDate.getMonth() + 1,
                            day: tenderSuspension.regDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.tenderSuspensionModalRef(component, tenderSuspension);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.tenderSuspensionModalRef(component, new TenderSuspension());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    tenderSuspensionModalRef(component: Component, tenderSuspension: TenderSuspension): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tenderSuspension = tenderSuspension;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
