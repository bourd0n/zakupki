package com.alex.zakupki.complaint.api.model.general

import java.util.Date

import com.google.common.base.MoreObjects

case class Complaint(var complaintNumber: String,
                     registrationKO: RegistrationKO,
                     regDate: Date,
                     purchase: Purchase,
                     applicant: Applicant,
                     text: String,
                     indictedClient: Client,
                     returnInfo: String) {
  override def toString: String = {
    MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
  }

}
