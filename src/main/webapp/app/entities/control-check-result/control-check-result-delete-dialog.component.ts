import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ControlCheckResult } from './control-check-result.model';
import { ControlCheckResultPopupService } from './control-check-result-popup.service';
import { ControlCheckResultService } from './control-check-result.service';

@Component({
    selector: 'jhi-control-check-result-delete-dialog',
    templateUrl: './control-check-result-delete-dialog.component.html'
})
export class ControlCheckResultDeleteDialogComponent {

    controlCheckResult: ControlCheckResult;

    constructor(
        private controlCheckResultService: ControlCheckResultService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.controlCheckResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'controlCheckResultListModification',
                content: 'Deleted an controlCheckResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-control-check-result-delete-popup',
    template: ''
})
export class ControlCheckResultDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private controlCheckResultPopupService: ControlCheckResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.controlCheckResultPopupService
                .open(ControlCheckResultDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
