SELECT
    c.id,
    c.full_name,
    c.client_type,
    res."COMPLAINT_NO_VIOLATIONS",
    res."COMPLAINT_VIOLATIONS",
    res."COMPLAINT_PARTLY_VALID",
    res."COMPLAINT_NOT_MATCHED",
    res.count complaints_sum,
    res."EMPTY"
FROM client c, (SELECT
                    res.indicted_client_id       client_id_1,
                    all_count.indicted_client_id client_id_2,
                    res."COMPLAINT_NO_VIOLATIONS",
                    res."COMPLAINT_VIOLATIONS",
                    res."COMPLAINT_PARTLY_VALID",
                    res."COMPLAINT_NOT_MATCHED",
                    res."EMPTY",
                    all_count.count
                FROM (SELECT *
                      FROM crosstab('SELECT
                              subq.indicted_client_id,
                              subq.checkResult,
                              subq.complaints_count
                              FROM (SELECT
                                    c.indicted_client_id           indicted_client_id,
                                    COALESCE(ccr.result, '''') checkResult,
                                    count(*) complaints_count
                                  FROM complaint c, check_complaint_result ccr
                                  WHERE ccr.complaint_id = c.id
                                    AND date_trunc(''year'', ccr.create_date) = to_date(''2015'', ''yyyy'')
                                  GROUP BY c.indicted_client_id, checkResult) subq
                              ORDER BY 1, 2',
                                    $$VALUES ('COMPLAINT_NO_VIOLATIONS'::text), ('COMPLAINT_VIOLATIONS'::text), ('COMPLAINT_PARTLY_VALID'::text), ('COMPLAINT_NOT_MATCHED'::text), (''::text)$$)
                          AS ct ("indicted_client_id" INT, "COMPLAINT_NO_VIOLATIONS" INT, "COMPLAINT_VIOLATIONS" INT, "COMPLAINT_PARTLY_VALID" INT, "COMPLAINT_NOT_MATCHED" INT, "EMPTY" INT)) res
                    FULL OUTER JOIN
                    (SELECT
                         c.indicted_client_id  indicted_client_id,
                         coalesce(count(*), 0) count
                     FROM
                         complaint c
                     WHERE date_trunc('year', c.reg_date) = to_date('2015', 'yyyy')
                     GROUP BY c.indicted_client_id) all_count
                        ON (res.indicted_client_id = all_count.indicted_client_id)) res
WHERE c.id = coalesce(res.client_id_1, res.client_id_2)
ORDER BY complaints_sum DESC NULLS LAST;
