import { BaseEntity } from './../../shared';

export class PlannedCheckResult implements BaseEntity {
    constructor(
        public id?: number,
        public checkResultNumber?: string,
        public checkNumber?: string,
        public createdDate?: any,
        public actDate?: any,
        public actPrescriptionDate?: any,
        public owner?: BaseEntity,
        public checkSubject?: BaseEntity,
    ) {
    }
}
