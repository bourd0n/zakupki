package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.PlannedCheckResultService;
import com.alex.zakupki.domain.PlannedCheckResult;
import com.alex.zakupki.repository.PlannedCheckResultRepository;
import com.alex.zakupki.repository.search.PlannedCheckResultSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PlannedCheckResult.
 */
@Service
@Transactional
public class PlannedCheckResultServiceImpl implements PlannedCheckResultService{

    private final Logger log = LoggerFactory.getLogger(PlannedCheckResultServiceImpl.class);

    private final PlannedCheckResultRepository plannedCheckResultRepository;

    private final PlannedCheckResultSearchRepository plannedCheckResultSearchRepository;

    public PlannedCheckResultServiceImpl(PlannedCheckResultRepository plannedCheckResultRepository, PlannedCheckResultSearchRepository plannedCheckResultSearchRepository) {
        this.plannedCheckResultRepository = plannedCheckResultRepository;
        this.plannedCheckResultSearchRepository = plannedCheckResultSearchRepository;
    }

    /**
     * Save a plannedCheckResult.
     *
     * @param plannedCheckResult the entity to save
     * @return the persisted entity
     */
    @Override
    public PlannedCheckResult save(PlannedCheckResult plannedCheckResult) {
        log.debug("Request to save PlannedCheckResult : {}", plannedCheckResult);
        PlannedCheckResult result = plannedCheckResultRepository.save(plannedCheckResult);
        plannedCheckResultSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the plannedCheckResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PlannedCheckResult> findAll(Pageable pageable) {
        log.debug("Request to get all PlannedCheckResults");
        return plannedCheckResultRepository.findAll(pageable);
    }

    /**
     *  Get one plannedCheckResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlannedCheckResult findOne(Long id) {
        log.debug("Request to get PlannedCheckResult : {}", id);
        return plannedCheckResultRepository.findOne(id);
    }

    /**
     *  Delete the  plannedCheckResult by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlannedCheckResult : {}", id);
        plannedCheckResultRepository.delete(id);
        plannedCheckResultSearchRepository.delete(id);
    }

    /**
     * Search for the plannedCheckResult corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PlannedCheckResult> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PlannedCheckResults for query {}", query);
        Page<PlannedCheckResult> result = plannedCheckResultSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
