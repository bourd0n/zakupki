package com.alex.zakupki.service.dto

case class FasComplaintsStats(fasName: String,
                              violations: Int,
                              noViolations: Int,
                              partlyValid: Int,
                              notMatched: Int,
                              returned: Int,
                              empty: Int,
                              complaintSum: Int)
