package com.alex.zakupki.complaint.api.model.general

import java.util.Date

import com.google.common.base.MoreObjects

case class PlannedCheckResult(override var checkResultNumber: String,
                              checkNumber: String,
                              override val createDate: Date,
                              override var owner: Owner,
                              actDate: Date,
                              actPrescriptionDate: Date,
                              checkSubject: Client) extends CheckResult {

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }
}
