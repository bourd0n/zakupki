import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ComplaintCancel } from './complaint-cancel.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ComplaintCancelService {

    private resourceUrl = SERVER_API_URL + 'api/complaint-cancels';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/complaint-cancels';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(complaintCancel: ComplaintCancel): Observable<ComplaintCancel> {
        const copy = this.convert(complaintCancel);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(complaintCancel: ComplaintCancel): Observable<ComplaintCancel> {
        const copy = this.convert(complaintCancel);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ComplaintCancel> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ComplaintCancel.
     */
    private convertItemFromServer(json: any): ComplaintCancel {
        const entity: ComplaintCancel = Object.assign(new ComplaintCancel(), json);
        entity.regDate = this.dateUtils
            .convertLocalDateFromServer(json.regDate);
        return entity;
    }

    /**
     * Convert a ComplaintCancel to a JSON which can be sent to the server.
     */
    private convert(complaintCancel: ComplaintCancel): ComplaintCancel {
        const copy: ComplaintCancel = Object.assign({}, complaintCancel);
        copy.regDate = this.dateUtils
            .convertLocalDateToServer(complaintCancel.regDate);
        return copy;
    }
}
