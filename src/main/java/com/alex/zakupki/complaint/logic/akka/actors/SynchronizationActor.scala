package com.alex.zakupki.complaint.logic.akka.actors

import akka.actor.{Actor, ActorRef}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import com.alex.zakupki.complaint.logic.akka.messages._
import com.alex.zakupki.complaint.logic.akka.spring.SpringExtension
import com.alex.zakupki.complaint.logic.ftp.{FTPLoader, FTPTypes, FtpUtils}
import com.alex.zakupki.complaint.utils.Logging
import com.alex.zakupki.domain.EntityType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

import scala.collection.immutable.IndexedSeq

@Component(SynchronizationActor.NAME)
@Scope("prototype")
class SynchronizationActor @Autowired()(implicit ctx: ApplicationContext, ftpUtils: FtpUtils) extends Actor with Logging {

    override def receive: Receive = {
        case StartSynchronization(syncInfo) =>
            logger.debug("Start download {}", syncInfo)
            val ftpType = syncInfo.name match {
                case EntityType.COMPLAINT => FTPTypes.ComplaintFTPType
                case EntityType.CHECK_RESULT => FTPTypes.CheckResultFTPType
            }
            val listFiles = ftpUtils.listFilesByLastSyncDate(ftpType, syncInfo.syncDate)
                //todo: tmp
//                .take(30)
            if (listFiles.nonEmpty) {
                val router = createRouter(10)
                listFiles.foreach(ftpFile => {
                    router.route(ProcessFtpZip(ftpFile, ftpType), self)
                })

                //                    logger.debug("Download to file system {} files", downloadedFilesCount)
                //                val props = SpringExtension(context.system).props("downloadedZipProcessorActor")
                //                val downloadedZipProcessorActor = context.actorOf(props, name = "worker-downloadedZipProcessor")
                //                downloadedZipProcessorActor ! ProcessDownloadedFiles(downloadedFilesCount)
            }
            logger.debug("Send all messages to workers")
    }

    private def createRouter(numActors: Int): Router = {
        val zipProcessorActorProps = SpringExtension(context.system).props(DownloadZipActor.NAME)
        val routees = for (i <- 1 to numActors) yield {
            val actor = context.actorOf(zipProcessorActorProps, name = s"download-zip-worker-$i")
            context watch actor
            ActorRefRoutee(actor)
        }
        Router(RoundRobinRoutingLogic(), routees)
    }

/*    private def createInMemoryWorkers(numActors: Int): IndexedSeq[ActorRef] = {
        val zipProcessorActorProps = SpringExtension(context.system).props("inMemoryProcessorActor")
        for (i <- 1 to numActors) yield context.actorOf(zipProcessorActorProps, name = s"worker-$i")
    }

    private def beginProcessDownloadedFiles(downloaded: Array[Array[Byte]], workers: IndexedSeq[ActorRef]): Unit = {
        downloaded.zipWithIndex.foreach((elem: (Array[Byte], Int)) => {
            workers(elem._2 % workers.size) ! ProcessInMemoryZip(elem._1)
        })
    }*/

}

object SynchronizationActor {
    final val NAME = "synchronizationActor"
}
