package com.alex.zakupki.service.mapper.xml

import java.util.concurrent.TimeUnit
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general.{Applicant => XmlApplicant}
import com.alex.zakupki.domain.Applicant
import com.alex.zakupki.repository.ApplicantRepository
import com.google.common.cache.{Cache, CacheBuilder}
import org.springframework.stereotype.Component

@Component
class ApplicantXmlToDataMapper @Inject()(val applicantRepository: ApplicantRepository)
    extends XmlToDataMapper[XmlApplicant, Applicant] {

    private val cache: Cache[String, Applicant] = CacheBuilder.newBuilder()
        .expireAfterAccess(15, TimeUnit.MINUTES)
        .build()
        .asInstanceOf[Cache[String, Applicant]]

    override def map(from: XmlApplicant): Option[Applicant] = Option(from) map {
        fromApplicant => {
            //locking is performed
            cache.get(fromApplicant.organizationName, () => {
                applicantRepository
                    .findOneByOrganizationName(fromApplicant.organizationName)
                    .orElseGet(() => {
                        val applicant = new Applicant
                        applicant.setApplicantType(from.applicantType)
                        applicant.setOrganizationName(from.organizationName)
                        applicantRepository.save(applicant)
                    })
            })
        }
    }
}
