package com.alex.zakupki.complaint.api.model.versions.v6_2

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.versions.common.DateAdapter
import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import com.google.common.base.MoreObjects
import org.apache.commons.lang3.ObjectUtils
;

//@Entity
//@Table(name = "planned_check_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class PlannedCheckResult(@xmlPath("checkResult/oos:commonInfo/oos:regNumber/text()")
                              checkResultNumber: String,
                              @xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                              checkResultNumber2: String,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:regNumber/text()")
                              checkNumber: String,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:checkNumber/text()")
                              checkNumber2: String,
                              @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                              createDate: Date,
                              @xmlPath("checkResult/oos:commonInfo/oos:owner")
                              owner: Owner,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:act/oos:actDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actDate: Date,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:actPrescription/oos:prescriptionDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actPrescriptionDate: Date,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:checkSubject/")
                              @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                              checkSubject: Client) extends GeneralizedEntity[general.PlannedCheckResult] {
    private def this() = this(null, null, null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }

    override def mapToGeneral(): general.PlannedCheckResult = general.PlannedCheckResult(
        ObjectUtils.firstNonNull(checkResultNumber, checkResultNumber2),
        ObjectUtils.firstNonNull(checkNumber, checkNumber2),
        createDate,
        owner.mapToGeneral(),
        actDate,
        actPrescriptionDate,
        Option(checkSubject).map(_.mapToGeneral()).orNull
    )
}
