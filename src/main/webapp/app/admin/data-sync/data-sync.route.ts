import {Routes, Route} from '@angular/router';
import {DataSyncComponent} from './data-sync.component';

export const dataSyncRoute: Route = {
    path: 'data-sync',
    component: DataSyncComponent,
    data: {
        authorities: ['ROLE_ADMIN'],
        pageTitle: 'data-sync.title'
    }
};
