package alex.zakupki.complaint.api.model

import java.util.Date
import javax.persistence._
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

import com.google.common.base.MoreObjects
import org.hibernate.annotations.CascadeType

@Entity
@Table(name = "check_complaint_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class CheckComplaintResult(@id @xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                                checkResultNumber: String,
                                @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                                createDate: Date,
                                @manyToOne
                                @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                                @joinColumn(name = "owner_id")
                                @xmlPath("checkResult/oos:commonInfo/oos:owner")
                                owner: Owner,
                                @xmlPath("checkResult/oos:commonInfo/oos:result/text()")
                                result: String,
                                @manyToOne
                                @cascade(Array(CascadeType.SAVE_UPDATE, CascadeType.MERGE))
                                @joinColumn(name = "indicted_id")
                                @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:checkSubjects/oos:subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                indictedComplaint: Client) {
  private def this() = this(null, null, null, null, null)

  override def toString: String = {
    MoreObjects.toStringHelper(this).add("result", result).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
  }
}