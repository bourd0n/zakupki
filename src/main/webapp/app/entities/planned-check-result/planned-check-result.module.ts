import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    PlannedCheckResultService,
    PlannedCheckResultPopupService,
    PlannedCheckResultComponent,
    PlannedCheckResultDetailComponent,
    PlannedCheckResultDialogComponent,
    PlannedCheckResultPopupComponent,
    PlannedCheckResultDeletePopupComponent,
    PlannedCheckResultDeleteDialogComponent,
    plannedCheckResultRoute,
    plannedCheckResultPopupRoute,
} from './';

const ENTITY_STATES = [
    ...plannedCheckResultRoute,
    ...plannedCheckResultPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PlannedCheckResultComponent,
        PlannedCheckResultDetailComponent,
        PlannedCheckResultDialogComponent,
        PlannedCheckResultDeleteDialogComponent,
        PlannedCheckResultPopupComponent,
        PlannedCheckResultDeletePopupComponent,
    ],
    entryComponents: [
        PlannedCheckResultComponent,
        PlannedCheckResultDialogComponent,
        PlannedCheckResultPopupComponent,
        PlannedCheckResultDeleteDialogComponent,
        PlannedCheckResultDeletePopupComponent,
    ],
    providers: [
        PlannedCheckResultService,
        PlannedCheckResultPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiPlannedCheckResultModule {}
