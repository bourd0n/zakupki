package com.alex.zakupki.web.rest

import java.time.LocalDate
import javax.inject.Inject

import com.alex.zakupki.domain.SyncInfo
import com.alex.zakupki.security.AuthoritiesConstantsScala
import com.alex.zakupki.service.{SyncInfoService, SynchronizationService}
import com.alex.zakupki.web.rest.vm.SyncInfoVM
import com.codahale.metrics.annotation.Timed
import org.slf4j.{Logger, LoggerFactory}
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation._

@RestController
@RequestMapping(value = Array("/sync-data"))
class SyncDataResource @Inject() (val syncInfoService: SyncInfoService,
                                  val synchronizationService : SynchronizationService) {

    private final val log: Logger = LoggerFactory.getLogger(classOf[SyncDataResource])

    @GetMapping(value = Array("/info"))
    @Timed
    @Secured(Array(AuthoritiesConstantsScala.ADMIN))
    def getEntitiesSyncStatus: ResponseEntity[List[SyncInfoVM]] = {
        val data: List[SyncInfoVM] = syncInfoService.getAllSyncInfos.map(syncInfoVm => {
            syncInfoVm.status = if (syncInfoVm.syncDate == null) "no-sync" else "sync"
            syncInfoVm
        })
        new ResponseEntity(data, HttpStatus.OK)
    }

    @PostMapping(value = Array("/sync"))
    @Timed
    @Secured(Array(AuthoritiesConstantsScala.ADMIN))
    def sync(@RequestBody syncInfoVM: SyncInfoVM): SyncInfo = {
        log.debug("SyncDataResource.sync invoked with {}", syncInfoVM)

        synchronizationService.synchronize(syncInfoVM)

        syncInfoVM.syncDate = LocalDate.now()
        syncInfoService.updateSyncInfo(syncInfoVM)
    }

    @PostMapping(value = Array("/sync-failed"))
    @Timed
    @Secured(Array(AuthoritiesConstantsScala.ADMIN))
    def syncFailed(): ResponseEntity[String] = {
        log.debug("SyncDataResource.syncFailed invoked")

        synchronizationService.synchronizeFailed()

        new ResponseEntity("Sync failed completed", HttpStatus.OK)
    }
}
