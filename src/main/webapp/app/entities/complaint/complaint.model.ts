import { BaseEntity } from './../../shared';

export class Complaint implements BaseEntity {
    constructor(
        public id?: number,
        public complaintNumber?: string,
        public regDate?: any,
        public text?: string,
        public registrationKo?: BaseEntity,
        public purchase?: BaseEntity,
        public applicant?: BaseEntity,
        public indictedClient?: BaseEntity,
    ) {
    }
}
