package com.alex.zakupki.service.mapper.xml

import java.time.ZoneId
import java.util.Optional
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general
import com.alex.zakupki.complaint.api.model.general.PlannedCheckResult
import com.alex.zakupki.domain
import com.alex.zakupki.domain.{Client, Owner}
import com.alex.zakupki.repository.PlannedCheckResultRepository
import org.springframework.stereotype.Component

@Component
class PlannedCheckResultXmlToDataMapper @Inject()(override val repository: PlannedCheckResultRepository,
                                                 val ownerXmlToDataMapper: XmlToDataMapper[general.Owner, Owner],
                                                 val clientXmlToDataMapper: XmlToDataMapper[general.Client, Client])
    extends CachedXmlToDataMapper[PlannedCheckResult, domain.PlannedCheckResult] with DateToLocalDateConversion {

    override def extractIdentificator(from: PlannedCheckResult): String = from.checkResultNumber

    override def findByIdentificator(id: String): Optional[domain.PlannedCheckResult] = repository.findOneByCheckResultNumber(id)

    override def createNew(from: PlannedCheckResult): domain.PlannedCheckResult = {
        new domain.PlannedCheckResult()
            .actDate(from.actDate)
            .actPrescriptionDate(from.actPrescriptionDate)
            .checkNumber(from.checkNumber)
            .checkResultNumber(from.checkResultNumber)
            .checkSubject(clientXmlToDataMapper.map(from.checkSubject).orNull)
            .createdDate(from.createDate)
            .owner(ownerXmlToDataMapper.map(from.owner).orNull)
    }
}
