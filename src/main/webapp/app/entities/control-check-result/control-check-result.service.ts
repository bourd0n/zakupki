import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ControlCheckResult } from './control-check-result.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ControlCheckResultService {

    private resourceUrl = SERVER_API_URL + 'api/control-check-results';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/control-check-results';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(controlCheckResult: ControlCheckResult): Observable<ControlCheckResult> {
        const copy = this.convert(controlCheckResult);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(controlCheckResult: ControlCheckResult): Observable<ControlCheckResult> {
        const copy = this.convert(controlCheckResult);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ControlCheckResult> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ControlCheckResult.
     */
    private convertItemFromServer(json: any): ControlCheckResult {
        const entity: ControlCheckResult = Object.assign(new ControlCheckResult(), json);
        entity.createdDate = this.dateUtils
            .convertLocalDateFromServer(json.createdDate);
        return entity;
    }

    /**
     * Convert a ControlCheckResult to a JSON which can be sent to the server.
     */
    private convert(controlCheckResult: ControlCheckResult): ControlCheckResult {
        const copy: ControlCheckResult = Object.assign({}, controlCheckResult);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(controlCheckResult.createdDate);
        return copy;
    }
}
