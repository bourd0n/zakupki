package com.alex.zakupki.service;

import com.alex.zakupki.domain.ReturnedComplaint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing ReturnedComplaint.
 */
public interface ReturnedComplaintService {

    /**
     * Save a returnedComplaint.
     *
     * @param returnedComplaint the entity to save
     * @return the persisted entity
     */
    ReturnedComplaint save(ReturnedComplaint returnedComplaint);

    /**
     *  Get all the returnedComplaints.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ReturnedComplaint> findAll(Pageable pageable);

    /**
     *  Get the "id" returnedComplaint.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ReturnedComplaint findOne(Long id);

    /**
     *  Delete the "id" returnedComplaint.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the returnedComplaint corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ReturnedComplaint> search(String query, Pageable pageable);
}
