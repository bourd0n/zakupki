package alex.zakupki.complaint.api.model

import javax.xml.bind.annotation.adapters.XmlAdapter

class StringOptionAdapter extends OptionAdapter[String](null, "")

class OptionAdapter[A](nones: A*) extends XmlAdapter[A, Option[A]] {
  def marshal(v: Option[A]): A = v.getOrElse(nones(0))
  def unmarshal(v: A) = if (nones contains v) None else Some(v)
}
