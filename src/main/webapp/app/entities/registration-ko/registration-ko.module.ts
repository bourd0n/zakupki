import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    RegistrationKoService,
    RegistrationKoPopupService,
    RegistrationKoComponent,
    RegistrationKoDetailComponent,
    RegistrationKoDialogComponent,
    RegistrationKoPopupComponent,
    RegistrationKoDeletePopupComponent,
    RegistrationKoDeleteDialogComponent,
    RegistrationKoResolvePagingParams,
    registrationKoRoute,
    registrationKoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...registrationKoRoute,
    ...registrationKoPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RegistrationKoComponent,
        RegistrationKoDetailComponent,
        RegistrationKoDialogComponent,
        RegistrationKoDeleteDialogComponent,
        RegistrationKoPopupComponent,
        RegistrationKoDeletePopupComponent,
    ],
    entryComponents: [
        RegistrationKoComponent,
        RegistrationKoDialogComponent,
        RegistrationKoPopupComponent,
        RegistrationKoDeleteDialogComponent,
        RegistrationKoDeletePopupComponent,
    ],
    providers: [
        RegistrationKoService,
        RegistrationKoPopupService,
        RegistrationKoResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiRegistrationKoModule {}
