package alex.zakupki.scalatest

import alex.zakupki.complaint.dao.ObjectSaver

import scala.collection.mutable.ArrayBuffer

class FakeObjectSaver(var arrayOfSavedObject: ArrayBuffer[AnyRef]) extends ObjectSaver {
  override def saveObject(unmarshalObject: AnyRef): Unit = arrayOfSavedObject += unmarshalObject
}
