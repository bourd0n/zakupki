package com.alex.zakupki.service.mapper.xml

import java.util

import com.alex.zakupki.ZakupkiApp
import com.alex.zakupki.complaint.api.model.general.RegistrationKO
import com.alex.zakupki.domain.RegistrationKo
import com.alex.zakupki.repository.RegistrationKoRepository
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.ListAssert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(classOf[SpringRunner])
@SpringBootTest(classes = Array(classOf[ZakupkiApp]))
class RegistrationKoXmlToDataMapperTest {

    private val DEFAULT_REG_NUM = "AAAAAAAAAA"
    private val UPDATED_REG_NUM = "BBBBBBBBBB"

    private val DEFAULT_FULL_NAME = "AAAAAAAAAA"
    private val UPDATED_FULL_NAME = "BBBBBBBBBB"

    @Autowired
    private val registrationKoXmlToDataMapper: RegistrationKoXmlToDataMapper = null

    @Autowired
    private val registrationKoRepository: RegistrationKoRepository = null

    @Test
    def testXmlToDataMapCreateEntity(): Unit = {
        val databaseSizeBeforeCreate = registrationKoRepository.findAll.size
        val registrationKo = RegistrationKO(DEFAULT_REG_NUM, DEFAULT_FULL_NAME)
        val mapped = registrationKoXmlToDataMapper.map(registrationKo)

        // Validate the RegistrationKo in the database
        val registrationKoList: util.List[RegistrationKo] = registrationKoRepository.findAll
        new ListAssert(registrationKoList).hasSize(databaseSizeBeforeCreate + 1)
        val testRegistrationKo: RegistrationKo = registrationKoList.get(registrationKoList.size - 1)
        assertThat(testRegistrationKo.getRegNum).isEqualTo(DEFAULT_REG_NUM)
        assertThat(testRegistrationKo.getFullName).isEqualTo(DEFAULT_FULL_NAME)
    }

    @Test
    def testXmlToDataMapNotCreateEntityIfItWasCreated(): Unit = {
        val databaseSizeBeforeCreate = registrationKoRepository.findAll.size
        val registrationKo = RegistrationKO(UPDATED_REG_NUM, UPDATED_FULL_NAME)
        val mapped = registrationKoXmlToDataMapper.map(registrationKo)

        // Validate the RegistrationKo in the database
        val registrationKoList: util.List[RegistrationKo] = registrationKoRepository.findAll
        new ListAssert(registrationKoList).hasSize(databaseSizeBeforeCreate + 1)
        val testRegistrationKo: RegistrationKo = registrationKoList.get(registrationKoList.size - 1)
        assertThat(testRegistrationKo.getRegNum).isEqualTo(UPDATED_REG_NUM)
        assertThat(testRegistrationKo.getFullName).isEqualTo(UPDATED_FULL_NAME)

        //second map
        val mapped2 = registrationKoXmlToDataMapper.map(registrationKo)
        assertThat(mapped2.get).isSameAs(mapped.get)
        new ListAssert(registrationKoRepository.findAll).hasSize(databaseSizeBeforeCreate + 1)
    }
}
