package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.PlannedCheckResult;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PlannedCheckResult entity.
 */
public interface PlannedCheckResultSearchRepository extends ElasticsearchRepository<PlannedCheckResult, Long> {
}
