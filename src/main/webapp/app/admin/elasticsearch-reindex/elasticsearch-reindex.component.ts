import {Component} from '@angular/core';
import {JhiLanguageService} from 'ng-jhipster';

@Component({
    selector: 'jhi-search-reindex',
    templateUrl: './elasticsearch-reindex.component.html'
})
export class ElasticSearchReindexComponent {

    constructor(private jhiLanguageService: JhiLanguageService) {
        // this.jhiLanguageService.setLocations(['elasticsearch-reindex']);
    }
}
