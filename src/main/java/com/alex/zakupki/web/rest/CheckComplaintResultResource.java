package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.CheckComplaintResult;
import com.alex.zakupki.service.CheckComplaintResultService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CheckComplaintResult.
 */
@RestController
@RequestMapping("/api")
public class CheckComplaintResultResource {

    private final Logger log = LoggerFactory.getLogger(CheckComplaintResultResource.class);

    private static final String ENTITY_NAME = "checkComplaintResult";

    private final CheckComplaintResultService checkComplaintResultService;

    public CheckComplaintResultResource(CheckComplaintResultService checkComplaintResultService) {
        this.checkComplaintResultService = checkComplaintResultService;
    }

    /**
     * POST  /check-complaint-results : Create a new checkComplaintResult.
     *
     * @param checkComplaintResult the checkComplaintResult to create
     * @return the ResponseEntity with status 201 (Created) and with body the new checkComplaintResult, or with status 400 (Bad Request) if the checkComplaintResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/check-complaint-results")
    @Timed
    public ResponseEntity<CheckComplaintResult> createCheckComplaintResult(@Valid @RequestBody CheckComplaintResult checkComplaintResult) throws URISyntaxException {
        log.debug("REST request to save CheckComplaintResult : {}", checkComplaintResult);
        if (checkComplaintResult.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new checkComplaintResult cannot already have an ID")).body(null);
        }
        CheckComplaintResult result = checkComplaintResultService.save(checkComplaintResult);
        return ResponseEntity.created(new URI("/api/check-complaint-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /check-complaint-results : Updates an existing checkComplaintResult.
     *
     * @param checkComplaintResult the checkComplaintResult to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated checkComplaintResult,
     * or with status 400 (Bad Request) if the checkComplaintResult is not valid,
     * or with status 500 (Internal Server Error) if the checkComplaintResult couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/check-complaint-results")
    @Timed
    public ResponseEntity<CheckComplaintResult> updateCheckComplaintResult(@Valid @RequestBody CheckComplaintResult checkComplaintResult) throws URISyntaxException {
        log.debug("REST request to update CheckComplaintResult : {}", checkComplaintResult);
        if (checkComplaintResult.getId() == null) {
            return createCheckComplaintResult(checkComplaintResult);
        }
        CheckComplaintResult result = checkComplaintResultService.save(checkComplaintResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, checkComplaintResult.getId().toString()))
            .body(result);
    }

    /**
     * GET  /check-complaint-results : get all the checkComplaintResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of checkComplaintResults in body
     */
    @GetMapping("/check-complaint-results")
    @Timed
    public ResponseEntity<List<CheckComplaintResult>> getAllCheckComplaintResults(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of CheckComplaintResults");
        Page<CheckComplaintResult> page = checkComplaintResultService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/check-complaint-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /check-complaint-results/:id : get the "id" checkComplaintResult.
     *
     * @param id the id of the checkComplaintResult to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the checkComplaintResult, or with status 404 (Not Found)
     */
    @GetMapping("/check-complaint-results/{id}")
    @Timed
    public ResponseEntity<CheckComplaintResult> getCheckComplaintResult(@PathVariable Long id) {
        log.debug("REST request to get CheckComplaintResult : {}", id);
        CheckComplaintResult checkComplaintResult = checkComplaintResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(checkComplaintResult));
    }

    /**
     * DELETE  /check-complaint-results/:id : delete the "id" checkComplaintResult.
     *
     * @param id the id of the checkComplaintResult to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/check-complaint-results/{id}")
    @Timed
    public ResponseEntity<Void> deleteCheckComplaintResult(@PathVariable Long id) {
        log.debug("REST request to delete CheckComplaintResult : {}", id);
        checkComplaintResultService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/check-complaint-results?query=:query : search for the checkComplaintResult corresponding
     * to the query.
     *
     * @param query the query of the checkComplaintResult search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/check-complaint-results")
    @Timed
    public ResponseEntity<List<CheckComplaintResult>> searchCheckComplaintResults(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CheckComplaintResults for query {}", query);
        Page<CheckComplaintResult> page = checkComplaintResultService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/check-complaint-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
