package com.alex.zakupki.cucumber.stepdefs;

import com.alex.zakupki.ZakupkiApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = ZakupkiApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
