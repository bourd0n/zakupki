package alex.zakupki.junit

import java.io.File
import javax.xml.bind.{JAXBContext, JAXBException, Unmarshaller}

import alex.zakupki.complaint.api.model.Complaint
import com.google.common.io.Resources
import org.junit.Test

class ComplaintTest {
  @Test def unmarshalComplaint() {
    try {
      val xmlPath: String = Resources.getResource("test_xmls/complaint_01011000003_05122014_2237_1507322.xml").getPath
      val jc: JAXBContext = JAXBContext.newInstance(classOf[Complaint])
      val unmarshaller: Unmarshaller = jc.createUnmarshaller
      val xml: File = new File(xmlPath)
      val unmarshal: AnyRef = unmarshaller.unmarshal(xml)
      System.out.println(unmarshal)
    }
    catch {
      case e: JAXBException =>
        throw new RuntimeException(e)
    }
  }
}