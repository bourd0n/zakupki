package com.alex.zakupki.complaint.api.model.versions.v5_0

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}

@deprecated
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class CheckResultNumber(@xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                             checkResultNumber: String) extends GeneralizedEntity[general.CheckResultNumber] {
  private def this() = this(null)

    override def mapToGeneral(): general.CheckResultNumber = general.CheckResultNumber(checkResultNumber)
}
