import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('ControlCheckResult e2e test', () => {

    let navBarPage: NavBarPage;
    let controlCheckResultDialogPage: ControlCheckResultDialogPage;
    let controlCheckResultComponentsPage: ControlCheckResultComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ControlCheckResults', () => {
        navBarPage.goToEntity('control-check-result');
        controlCheckResultComponentsPage = new ControlCheckResultComponentsPage();
        expect(controlCheckResultComponentsPage.getTitle()).toMatch(/zakupkiApp.controlCheckResult.home.title/);

    });

    it('should load create ControlCheckResult dialog', () => {
        controlCheckResultComponentsPage.clickOnCreateButton();
        controlCheckResultDialogPage = new ControlCheckResultDialogPage();
        expect(controlCheckResultDialogPage.getModalTitle()).toMatch(/zakupkiApp.controlCheckResult.home.createOrEditLabel/);
        controlCheckResultDialogPage.close();
    });

    it('should create and save ControlCheckResults', () => {
        controlCheckResultComponentsPage.clickOnCreateButton();
        controlCheckResultDialogPage.setCheckResultNumberInput('checkResultNumber');
        expect(controlCheckResultDialogPage.getCheckResultNumberInput()).toMatch('checkResultNumber');
        controlCheckResultDialogPage.setCreatedDateInput('2000-12-31');
        expect(controlCheckResultDialogPage.getCreatedDateInput()).toMatch('2000-12-31');
        controlCheckResultDialogPage.setResultInput('result');
        expect(controlCheckResultDialogPage.getResultInput()).toMatch('result');
        controlCheckResultDialogPage.ownerSelectLastOption();
        controlCheckResultDialogPage.clientComplaintSelectLastOption();
        controlCheckResultDialogPage.save();
        expect(controlCheckResultDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ControlCheckResultComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-control-check-result div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ControlCheckResultDialogPage {
    modalTitle = element(by.css('h4#myControlCheckResultLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    checkResultNumberInput = element(by.css('input#field_checkResultNumber'));
    createdDateInput = element(by.css('input#field_createdDate'));
    resultInput = element(by.css('input#field_result'));
    ownerSelect = element(by.css('select#field_owner'));
    clientComplaintSelect = element(by.css('select#field_clientComplaint'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCheckResultNumberInput = function (checkResultNumber) {
        this.checkResultNumberInput.sendKeys(checkResultNumber);
    }

    getCheckResultNumberInput = function () {
        return this.checkResultNumberInput.getAttribute('value');
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setResultInput = function (result) {
        this.resultInput.sendKeys(result);
    }

    getResultInput = function () {
        return this.resultInput.getAttribute('value');
    }

    ownerSelectLastOption = function () {
        this.ownerSelect.all(by.tagName('option')).last().click();
    }

    ownerSelectOption = function (option) {
        this.ownerSelect.sendKeys(option);
    }

    getOwnerSelect = function () {
        return this.ownerSelect;
    }

    getOwnerSelectedOption = function () {
        return this.ownerSelect.element(by.css('option:checked')).getText();
    }

    clientComplaintSelectLastOption = function () {
        this.clientComplaintSelect.all(by.tagName('option')).last().click();
    }

    clientComplaintSelectOption = function (option) {
        this.clientComplaintSelect.sendKeys(option);
    }

    getClientComplaintSelect = function () {
        return this.clientComplaintSelect;
    }

    getClientComplaintSelectedOption = function () {
        return this.clientComplaintSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
