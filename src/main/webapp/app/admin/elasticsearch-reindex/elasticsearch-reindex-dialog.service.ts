import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class ElasticSearchReindex {

    constructor(private http: Http) {
    }

    reindex(): Observable<Response> {
        return this.http.post('api/elasticsearch/index', {}).map((res: Response) => {
            return res
        })
    }
}
