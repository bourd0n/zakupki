package com.alex.zakupki.repository;

import com.alex.zakupki.domain.PublishOrg;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PublishOrg entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PublishOrgRepository extends JpaRepository<PublishOrg, Long> {

}
