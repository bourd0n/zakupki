package alex.zakupki.scalatest

import java.io._
import java.nio.file.{Paths, Files}
import java.util.zip.{ZipEntry, ZipInputStream}

import alex.zakupki.complaint.logic.ftp.{FTPLoader, FTPTypes}
import alex.zakupki.complaint.logic.xml.XMLProcessor
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.mutable.ArrayBuffer

class TestProcessInmemory extends FlatSpec with Matchers with AssertionsForJUnit with LazyLogging {

  val BUFFER_SIZE: Int = 10000

  "FTPLoader.downloadInmemory" should "download files" in {
    val inmemory: Array[Array[Byte]] = FTPLoader.downloadInmemory(FTPTypes.ComplaintFTPType, 1)
    val out = inmemory.flatMap(oneZipAsBytes => {
      val zipStream = new ZipInputStream(new BufferedInputStream(new ByteArrayInputStream(oneZipAsBytes)))
      val resultUnzippedFiles = ArrayBuffer[String]()
      try {
        var entry: ZipEntry = zipStream.getNextEntry
        //while (entry != null) {
        resultUnzippedFiles += entry.getName
        logger.info(entry.getName)
        val data = Array.ofDim[Byte](BUFFER_SIZE)
        val outputStream = new ByteArrayOutputStream()
        val out = new BufferedOutputStream(outputStream, BUFFER_SIZE)
        var count: Int = zipStream.read(data, 0, BUFFER_SIZE)
        while (count != -1) {
          out.write(data, 0, count)
          count = zipStream.read(data, 0, BUFFER_SIZE)
        }
        out.flush()
        out.close()
        logger.info(outputStream.toString)
        outputStream shouldNot equal(null)
        //entry = zipStream.getNextEntry
        //}
        resultUnzippedFiles
      } finally {
        zipStream.close()
      }
    })
    System.out.println(out)
  }

  "XMLProcessor" should "successfully processed all files inmemory" in {
    val inmemoryZip: Array[Array[Byte]] = Array(Files.readAllBytes(Paths.get(getClass.getResource("/complaintCancel_2014120100_2015010100_008.xml.zip").getPath)))
    val arrayOfSavedObject = ArrayBuffer[AnyRef]()
    new XMLProcessor(new FakeObjectSaver(arrayOfSavedObject)).processInmemory(inmemoryZip)
    arrayOfSavedObject should have length 668
  }


}
