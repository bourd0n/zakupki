package alex.zakupki.complaint.utils

object SlickCodeGenerator {
  def main(args: Array[String]) {
    slick.codegen.SourceCodeGenerator.main(
      Array("slick.driver.PostgresDriver",
        "org.postgresql.Driver",
        "jdbc:postgresql://localhost/zakupki_new",
        "I:\\programming\\zakupki\\complaint-api\\src\\main\\scala\\alex\\zakupki\\complaint\\api\\dao\\slick",
        "alex.zakupki.complaint.api.dao.slick",
        "postgres",
        "1")
    )
  }
}
