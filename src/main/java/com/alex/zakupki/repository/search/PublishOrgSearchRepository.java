package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.PublishOrg;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PublishOrg entity.
 */
public interface PublishOrgSearchRepository extends ElasticsearchRepository<PublishOrg, Long> {
}
