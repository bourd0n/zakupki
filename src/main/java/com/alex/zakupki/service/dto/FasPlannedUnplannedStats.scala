package com.alex.zakupki.service.dto

case class FasPlannedUnplannedStats(fasName: String,
                                    violations: Int,
                                    noViolations: Int,
                                    unplannedCount: Int,
                                    plannedCount: Int)
