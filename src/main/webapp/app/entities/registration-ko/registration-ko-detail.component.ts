import {
    JhiAlertService,
    JhiDateUtils,
    JhiEventManager,
    JhiLanguageService,
    JhiPaginationUtil,
    JhiParseLinks
} from 'ng-jhipster';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Rx';
import {Response} from '@angular/http';

import {RegistrationKo} from './registration-ko.model';
import {RegistrationKoService} from './registration-ko.service';
import {Complaint} from '../complaint/complaint.model';
import {ComplaintService} from '../complaint/complaint.service';
import {ITEMS_PER_PAGE} from '../../shared/constants/pagination.constants';

@Component({
    selector: 'jhi-registration-ko-detail',
    templateUrl: './registration-ko-detail.component.html'
})
export class RegistrationKoDetailComponent implements OnInit, OnDestroy {

    registrationKo: RegistrationKo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    private fromDate: string;
    private toDate: string;
    complaints: Complaint[];
    // complaints
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    routeData: any;
    links: any;

    constructor(private eventManager: JhiEventManager,
                private jhiLanguageService: JhiLanguageService,
                private registrationKoService: RegistrationKoService,
                private route: ActivatedRoute,
                private parseLinks: JhiParseLinks,
                private alertService: JhiAlertService,
                private complaintService: ComplaintService,
                private paginationUtil: JhiPaginationUtil,
                // private paginationConfig: PaginationConfig,
                private router: Router,
                private dateUtils: JhiDateUtils) {
        this.complaints = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.route.data.subscribe((data) => {
            this.page = data['pagingParams'].page;
            this.previousPage = data['pagingParams'].page;
            this.reverse = data['pagingParams'].ascending;
            this.predicate = data['pagingParams'].predicate;
        });
        // this.jhiLanguageService.setLocations(['registrationKo', 'complaint']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRegistrationKos();
    }

    load(id) {
        this.registrationKoService.find(id).subscribe((registrationKo) => {
            this.registrationKo = registrationKo;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.routeData.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    loadComplaints() {
        this.complaintService.findComplaintsByRegistrationKoAndBetweenDates({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort(),
            regKoId: this.registrationKo.id,
            from: this.dateUtils.convertLocalDateToServer(this.fromDate),
            to: this.dateUtils.convertLocalDateToServer(this.toDate)
        }).subscribe(
            (res: Response) => this.onSuccess(res.json(), res.headers),
            (res: Response) => this.onError(res.json())
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    trackId(index, item: Complaint) {
        return item.id;
    }

    transition() {
        this.router.navigate(['/registration-ko/' + this.registrationKo.id], {
            queryParams: {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadComplaints();
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.complaints = data;
    }

    private onError(error) {
        this.alertService.error(error.error, error.message, null);
    }

    registerChangeInRegistrationKos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'registrationKoListModification',
            (response) => this.load(this.registrationKo.id)
        );
    }
}
