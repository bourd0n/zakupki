package alex.zakupki.junit

import java.io.{File, FileInputStream}
import java.nio.file.{Files, Paths}

import alex.zakupki.complaint.api.model.{CheckComplaintResult, Complaint}
import alex.zakupki.complaint.logic.xml.EntityProcessor
import org.junit.Test
import org.scalatest.Matchers
import org.scalatest.junit.{AssertionsForJUnit, JUnitSuite}

/**
  * Unit test for simple App.
  */
class XmlMappingTest extends JUnitSuite with Matchers with AssertionsForJUnit {

  def unmarshalComplaint(str: String) = EntityProcessor.unmarshalComplaint(new FileInputStream(new File(getClass.getResource(str).getPath)))
  def unmarshalCheckResult(str: String) = EntityProcessor.unmarshalCheckResult(Files.newInputStream(Paths.get(getClass.getResource(str).getPath)))

  @Test def testMappingWithAdditionalLoggingComplaints() {
    val complaintOpt: Option[Complaint] = unmarshalComplaint("/test_xmls/complaint_01011000003_05122014_2237_1507322.xml")
    complaintOpt shouldBe a [Some[_]]
    val complaint = complaintOpt.get
    complaint.complaintNumber shouldNot equal(null)
    complaint.purchase shouldNot equal(null)
    complaint.text shouldNot equal(null)
    complaint.purchase shouldNot equal(null)
  }

    @Test def testMappingCheckResult() {
      val checkResult: Option[AnyRef] = unmarshalCheckResult("/test_xmls/checkResult_01031000103_03042015_2612_121186.xml")
      checkResult shouldBe a [Some[_]]
      val checkComplaintResult = checkResult.get.asInstanceOf[CheckComplaintResult]
      checkComplaintResult.checkResultNumber shouldNot equal(null)
    }

//  @Test
//  @throws(classOf[JAXBException])
//  def testMappingComplaints {
//    var complaint: Complaint = EntityProcessor.unmarshalComplaint(Consts.MAIN_PATH + "src\\main\\resources\\complaint_01031000103_03042015_2612_1530587.xml")
//    System.out.println(complaint)
//    System.out.println(complaint.indictedClient.regNum)
//    complaint = EntityProcessor.unmarshalComplaint(Consts.MAIN_PATH + "src\\main\\resources\\complaint_01011000003_05122014_2237_1507322.xml")
//    System.out.println(complaint)
//    System.out.println(complaint.indictedClient.regNum)
//    complaint = EntityProcessor.unmarshalComplaint(Consts.MAIN_PATH + "src\\main\\resources\\complaint_01521000080_10112014_2097_1501051.xml")
//    System.out.println(complaint)
//    System.out.println(complaint.indictedClient.regNum)
//  }
//
//
//  @Test def testMappingComplaintCancel {
//    val complaintCancel: ComplaintCancel = EntityProcessor.unmarshalComplaintCancel(Consts.MAIN_PATH + "src\\main\\resources\\complaintCancel_01011000003_22122014_2304_1510768.xml")
//    System.out.println(complaintCancel)
//  }
//
//  @Test def testMappingTenderSuspension {
//    val tenderSuspension: TenderSuspension = EntityProcessor.unmarshalTenderSuspension(Consts.MAIN_PATH + "src\\main\\resources\\tenderSuspension_01031000103_14042015_2644_49630.xml")
//    System.out.println(tenderSuspension)
//  }
//
//  @Test def testMappingUnfairSupplier {
//    val unfairSupplier: UnfairSupplier = EntityProcessor.unmarshalUnfairSupplier(Consts.MAIN_PATH + "src\\main\\resources\\unfairSupplier_31180-15_31180.xml")
//    System.out.println(unfairSupplier)
//  }
}