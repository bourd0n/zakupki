package alex.zakupki.complaint.dao.slick

import slick.driver.PostgresDriver.api._

object DB  {
  lazy val db = Database.forConfig("pg_zakupki")
}
