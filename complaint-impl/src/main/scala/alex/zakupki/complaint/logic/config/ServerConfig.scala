package alex.zakupki.complaint.logic.config

import com.typesafe.config.{ConfigFactory, Config}

object ServerConfig {
  lazy val config = ConfigFactory.load()
  lazy val server_path = config.getString("server_path")
}
