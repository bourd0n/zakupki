package alex.zakupki.complaint.logic.xml

import java.io._
import javax.xml.bind.{JAXB, JAXBContext, Unmarshaller}
import javax.xml.transform.stream.StreamSource

import alex.zakupki.complaint.api.model._
import com.typesafe.scalalogging.Logger
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory

//trait EntityProcessorImplicitConversation {
//  implicit def string2InputStream(s: String) : InputStream = new FileInputStream(s)
//}

object EntityProcessor {
  private lazy val log: Logger = Logger(LoggerFactory.getLogger(EntityProcessor.getClass))

  def unmarshalComplaint(inputStream: InputStream): Option[Complaint] = {
    unmarshal(inputStream, classOf[Complaint])
  }

  def unmarshalCheckResult(inputStream: InputStream): Option[AnyRef] = {
    val bytes = IOUtils.toByteArray(inputStream)
    val ccn = unmarshal(new BufferedInputStream(new ByteArrayInputStream(bytes)), classOf[CheckResultNumber])
    val checkResultNumber: Option[String] = ccn.map(_.checkResultNumber)
    checkResultNumber.flatMap(str => Some(str.charAt(0)).orElse(Some(""))) match {
      case Some('Ж') =>
        unmarshal(new BufferedInputStream(new ByteArrayInputStream(bytes)), classOf[CheckComplaintResult]).map(_.copy(checkResultNumber = checkResultNumber.get.substring(2)))
      case Some('В') =>
        unmarshal(new BufferedInputStream(new ByteArrayInputStream(bytes)), classOf[UnplannedCheckResult]).map(_.copy(checkResultNumber = checkResultNumber.get.substring(2)))
      case Some('П') =>
        unmarshal(new BufferedInputStream(new ByteArrayInputStream(bytes)), classOf[PlannedCheckResult]).map(_.copy(checkResultNumber = checkResultNumber.get.substring(2)))
      case Some('К') =>
        unmarshal(new BufferedInputStream(new ByteArrayInputStream(bytes)), classOf[ControlCheckResult])
      case Some(someCharAt0) =>
        log.warn(s"Not unplanned, planned or complaint '$someCharAt0' for xml $inputStream")
        None
      case _ =>
        log.warn("Not unplanned, planned or complaint for xml {}", inputStream)
        None
    }
  }

  def unmarshalComplaintCancel(inputStream: InputStream): Option[ComplaintCancel] = {
    unmarshal(inputStream, classOf[ComplaintCancel])
  }

  def unmarshal[T](inputStream: InputStream, clazz: Class[T]): Option[T] = {
    val jc: JAXBContext = JAXBContext.newInstance(clazz)
    val unmarshaller: Unmarshaller = jc.createUnmarshaller
    Some(unmarshaller.unmarshal(inputStream).asInstanceOf[T])
  }

  def unmarshalTenderSuspension(inputStream: InputStream): Option[TenderSuspension] = {
    unmarshal(inputStream, classOf[TenderSuspension])
  }

  def unmarshalUnfairSupplier(inputStream: InputStream): Option[UnfairSupplier] = {
    unmarshal(inputStream, classOf[UnfairSupplier])
  }

}