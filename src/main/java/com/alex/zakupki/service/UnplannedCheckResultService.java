package com.alex.zakupki.service;

import com.alex.zakupki.domain.UnplannedCheckResult;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing UnplannedCheckResult.
 */
public interface UnplannedCheckResultService {

    /**
     * Save a unplannedCheckResult.
     *
     * @param unplannedCheckResult the entity to save
     * @return the persisted entity
     */
    UnplannedCheckResult save(UnplannedCheckResult unplannedCheckResult);

    /**
     *  Get all the unplannedCheckResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UnplannedCheckResult> findAll(Pageable pageable);

    /**
     *  Get the "id" unplannedCheckResult.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UnplannedCheckResult findOne(Long id);

    /**
     *  Delete the "id" unplannedCheckResult.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the unplannedCheckResult corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UnplannedCheckResult> search(String query, Pageable pageable);
}
