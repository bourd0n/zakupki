/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ComplaintCancelDetailComponent } from '../../../../../../main/webapp/app/entities/complaint-cancel/complaint-cancel-detail.component';
import { ComplaintCancelService } from '../../../../../../main/webapp/app/entities/complaint-cancel/complaint-cancel.service';
import { ComplaintCancel } from '../../../../../../main/webapp/app/entities/complaint-cancel/complaint-cancel.model';

describe('Component Tests', () => {

    describe('ComplaintCancel Management Detail Component', () => {
        let comp: ComplaintCancelDetailComponent;
        let fixture: ComponentFixture<ComplaintCancelDetailComponent>;
        let service: ComplaintCancelService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [ComplaintCancelDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ComplaintCancelService,
                    JhiEventManager
                ]
            }).overrideTemplate(ComplaintCancelDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ComplaintCancelDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ComplaintCancelService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ComplaintCancel(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.complaintCancel).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
