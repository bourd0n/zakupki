package com.alex.zakupki.complaint.api.model.versions.v6_4

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import org.apache.commons.lang3.{ObjectUtils, StringUtils}

//@Entity
//@Table(name = "applicants")
@XmlAccessorType(XmlAccessType.FIELD)
case class Applicant(@xmlPath("legalEntity/fullName/text()")
                     organizationName: String,
                     //todo: rewrite to OR
                     @xmlPath("individualBusinessman/name/text()")
                     individualBusinessmanName: String,
                     @xmlPath("individualPerson/name/text()")
                     individualPersonName: String,
                     @xmlPath("legalEntity/legalForm/singularName/text()")
                     applicantType: String) extends GeneralizedEntity[general.Applicant] {
    private def this() = this(null, null, null, null)

    override def mapToGeneral(): general.Applicant = {
        general.Applicant(ObjectUtils.firstNonNull(organizationName, individualBusinessmanName, individualPersonName).trim,
            if (StringUtils.isEmpty(applicantType)) {
                if (StringUtils.isNotEmpty(individualBusinessmanName)) {
                    "Individual Businessman"
                } else if (StringUtils.isNotEmpty(individualPersonName)) {
                    "Individual Person"
                } else {
                    ""
                }
            } else
                applicantType)
    }
}
