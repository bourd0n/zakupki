SELECT
  registration_ko.full_name,
  ko2complaints.complaints_count
FROM (SELECT
        c.registration_ko_id ko_id,
        count(*)             complaints_count
      FROM complaint c
      GROUP BY c.registration_ko_id) ko2complaints, registration_ko
WHERE ko2complaints.ko_id = registration_ko.id
ORDER BY complaints_count DESC;

