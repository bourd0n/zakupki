import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('CheckComplaintResult e2e test', () => {

    let navBarPage: NavBarPage;
    let checkComplaintResultDialogPage: CheckComplaintResultDialogPage;
    let checkComplaintResultComponentsPage: CheckComplaintResultComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load CheckComplaintResults', () => {
        navBarPage.goToEntity('check-complaint-result');
        checkComplaintResultComponentsPage = new CheckComplaintResultComponentsPage();
        expect(checkComplaintResultComponentsPage.getTitle()).toMatch(/zakupkiApp.checkComplaintResult.home.title/);

    });

    it('should load create CheckComplaintResult dialog', () => {
        checkComplaintResultComponentsPage.clickOnCreateButton();
        checkComplaintResultDialogPage = new CheckComplaintResultDialogPage();
        expect(checkComplaintResultDialogPage.getModalTitle()).toMatch(/zakupkiApp.checkComplaintResult.home.createOrEditLabel/);
        checkComplaintResultDialogPage.close();
    });

    it('should create and save CheckComplaintResults', () => {
        checkComplaintResultComponentsPage.clickOnCreateButton();
        checkComplaintResultDialogPage.setCheckResultNumberInput('checkResultNumber');
        expect(checkComplaintResultDialogPage.getCheckResultNumberInput()).toMatch('checkResultNumber');
        checkComplaintResultDialogPage.setCreateDateInput('2000-12-31');
        expect(checkComplaintResultDialogPage.getCreateDateInput()).toMatch('2000-12-31');
        checkComplaintResultDialogPage.setResultInput('result');
        expect(checkComplaintResultDialogPage.getResultInput()).toMatch('result');
        checkComplaintResultDialogPage.ownerSelectLastOption();
        checkComplaintResultDialogPage.checkSubjectSelectLastOption();
        checkComplaintResultDialogPage.complaintSelectLastOption();
        checkComplaintResultDialogPage.save();
        expect(checkComplaintResultDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class CheckComplaintResultComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-check-complaint-result div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class CheckComplaintResultDialogPage {
    modalTitle = element(by.css('h4#myCheckComplaintResultLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    checkResultNumberInput = element(by.css('input#field_checkResultNumber'));
    createDateInput = element(by.css('input#field_createDate'));
    resultInput = element(by.css('input#field_result'));
    ownerSelect = element(by.css('select#field_owner'));
    checkSubjectSelect = element(by.css('select#field_checkSubject'));
    complaintSelect = element(by.css('select#field_complaint'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCheckResultNumberInput = function (checkResultNumber) {
        this.checkResultNumberInput.sendKeys(checkResultNumber);
    }

    getCheckResultNumberInput = function () {
        return this.checkResultNumberInput.getAttribute('value');
    }

    setCreateDateInput = function (createDate) {
        this.createDateInput.sendKeys(createDate);
    }

    getCreateDateInput = function () {
        return this.createDateInput.getAttribute('value');
    }

    setResultInput = function (result) {
        this.resultInput.sendKeys(result);
    }

    getResultInput = function () {
        return this.resultInput.getAttribute('value');
    }

    ownerSelectLastOption = function () {
        this.ownerSelect.all(by.tagName('option')).last().click();
    }

    ownerSelectOption = function (option) {
        this.ownerSelect.sendKeys(option);
    }

    getOwnerSelect = function () {
        return this.ownerSelect;
    }

    getOwnerSelectedOption = function () {
        return this.ownerSelect.element(by.css('option:checked')).getText();
    }

    checkSubjectSelectLastOption = function () {
        this.checkSubjectSelect.all(by.tagName('option')).last().click();
    }

    checkSubjectSelectOption = function (option) {
        this.checkSubjectSelect.sendKeys(option);
    }

    getCheckSubjectSelect = function () {
        return this.checkSubjectSelect;
    }

    getCheckSubjectSelectedOption = function () {
        return this.checkSubjectSelect.element(by.css('option:checked')).getText();
    }

    complaintSelectLastOption = function () {
        this.complaintSelect.all(by.tagName('option')).last().click();
    }

    complaintSelectOption = function (option) {
        this.complaintSelect.sendKeys(option);
    }

    getComplaintSelect = function () {
        return this.complaintSelect;
    }

    getComplaintSelectedOption = function () {
        return this.complaintSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
