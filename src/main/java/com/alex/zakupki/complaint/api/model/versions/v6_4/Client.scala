package com.alex.zakupki.complaint.api.model.versions.v6_4

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}

/**
  * customer, authority, commission94/44 others
  */
//@Entity
//@Table(name = "clients")
@XmlAccessorType(XmlAccessType.FIELD)
case class Client(regNum: String,
                  fullName: String,
                  clientType: String) extends GeneralizedEntity[general.Client] {
    private def this() = this(null, null, null)

    override def mapToGeneral(): general.Client = {
        general.Client(regNum, fullName.trim, clientType)
    }
}
