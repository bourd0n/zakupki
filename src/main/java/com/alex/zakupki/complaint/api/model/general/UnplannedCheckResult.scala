package com.alex.zakupki.complaint.api.model.general

import java.util.Date

case class UnplannedCheckResult(override var checkResultNumber: String,
                                unplannedCheckNumber: String,
                                override val createDate: Date,
                                override var owner: Owner,
                                result: String,
                                checkSubject: Client) extends CheckResult
