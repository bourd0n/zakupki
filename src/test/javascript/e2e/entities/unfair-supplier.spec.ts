import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('UnfairSupplier e2e test', () => {

    let navBarPage: NavBarPage;
    let unfairSupplierDialogPage: UnfairSupplierDialogPage;
    let unfairSupplierComponentsPage: UnfairSupplierComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load UnfairSuppliers', () => {
        navBarPage.goToEntity('unfair-supplier');
        unfairSupplierComponentsPage = new UnfairSupplierComponentsPage();
        expect(unfairSupplierComponentsPage.getTitle()).toMatch(/zakupkiApp.unfairSupplier.home.title/);

    });

    it('should load create UnfairSupplier dialog', () => {
        unfairSupplierComponentsPage.clickOnCreateButton();
        unfairSupplierDialogPage = new UnfairSupplierDialogPage();
        expect(unfairSupplierDialogPage.getModalTitle()).toMatch(/zakupkiApp.unfairSupplier.home.createOrEditLabel/);
        unfairSupplierDialogPage.close();
    });

    it('should create and save UnfairSuppliers', () => {
        unfairSupplierComponentsPage.clickOnCreateButton();
        unfairSupplierDialogPage.setRegistryNumInput('registryNum');
        expect(unfairSupplierDialogPage.getRegistryNumInput()).toMatch('registryNum');
        unfairSupplierDialogPage.setPublishDateInput('2000-12-31');
        expect(unfairSupplierDialogPage.getPublishDateInput()).toMatch('2000-12-31');
        unfairSupplierDialogPage.setStateInput('state');
        expect(unfairSupplierDialogPage.getStateInput()).toMatch('state');
        unfairSupplierDialogPage.setReasonInput('reason');
        expect(unfairSupplierDialogPage.getReasonInput()).toMatch('reason');
        unfairSupplierDialogPage.setFullNameInput('fullName');
        expect(unfairSupplierDialogPage.getFullNameInput()).toMatch('fullName');
        unfairSupplierDialogPage.setInnInput('inn');
        expect(unfairSupplierDialogPage.getInnInput()).toMatch('inn');
        unfairSupplierDialogPage.setPurchaseNumberInput('purchaseNumber');
        expect(unfairSupplierDialogPage.getPurchaseNumberInput()).toMatch('purchaseNumber');
        unfairSupplierDialogPage.publishOrgSelectLastOption();
        unfairSupplierDialogPage.save();
        expect(unfairSupplierDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class UnfairSupplierComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-unfair-supplier div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class UnfairSupplierDialogPage {
    modalTitle = element(by.css('h4#myUnfairSupplierLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    registryNumInput = element(by.css('input#field_registryNum'));
    publishDateInput = element(by.css('input#field_publishDate'));
    stateInput = element(by.css('input#field_state'));
    reasonInput = element(by.css('input#field_reason'));
    fullNameInput = element(by.css('input#field_fullName'));
    innInput = element(by.css('input#field_inn'));
    purchaseNumberInput = element(by.css('input#field_purchaseNumber'));
    publishOrgSelect = element(by.css('select#field_publishOrg'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setRegistryNumInput = function (registryNum) {
        this.registryNumInput.sendKeys(registryNum);
    }

    getRegistryNumInput = function () {
        return this.registryNumInput.getAttribute('value');
    }

    setPublishDateInput = function (publishDate) {
        this.publishDateInput.sendKeys(publishDate);
    }

    getPublishDateInput = function () {
        return this.publishDateInput.getAttribute('value');
    }

    setStateInput = function (state) {
        this.stateInput.sendKeys(state);
    }

    getStateInput = function () {
        return this.stateInput.getAttribute('value');
    }

    setReasonInput = function (reason) {
        this.reasonInput.sendKeys(reason);
    }

    getReasonInput = function () {
        return this.reasonInput.getAttribute('value');
    }

    setFullNameInput = function (fullName) {
        this.fullNameInput.sendKeys(fullName);
    }

    getFullNameInput = function () {
        return this.fullNameInput.getAttribute('value');
    }

    setInnInput = function (inn) {
        this.innInput.sendKeys(inn);
    }

    getInnInput = function () {
        return this.innInput.getAttribute('value');
    }

    setPurchaseNumberInput = function (purchaseNumber) {
        this.purchaseNumberInput.sendKeys(purchaseNumber);
    }

    getPurchaseNumberInput = function () {
        return this.purchaseNumberInput.getAttribute('value');
    }

    publishOrgSelectLastOption = function () {
        this.publishOrgSelect.all(by.tagName('option')).last().click();
    }

    publishOrgSelectOption = function (option) {
        this.publishOrgSelect.sendKeys(option);
    }

    getPublishOrgSelect = function () {
        return this.publishOrgSelect;
    }

    getPublishOrgSelectedOption = function () {
        return this.publishOrgSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
