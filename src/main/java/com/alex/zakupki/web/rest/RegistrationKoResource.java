package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.RegistrationKo;
import com.alex.zakupki.service.RegistrationKoService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RegistrationKo.
 */
@RestController
@RequestMapping("/api")
public class RegistrationKoResource {

    private final Logger log = LoggerFactory.getLogger(RegistrationKoResource.class);

    private static final String ENTITY_NAME = "registrationKo";

    private final RegistrationKoService registrationKoService;

    public RegistrationKoResource(RegistrationKoService registrationKoService) {
        this.registrationKoService = registrationKoService;
    }

    /**
     * POST  /registration-kos : Create a new registrationKo.
     *
     * @param registrationKo the registrationKo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new registrationKo, or with status 400 (Bad Request) if the registrationKo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/registration-kos")
    @Timed
    public ResponseEntity<RegistrationKo> createRegistrationKo(@Valid @RequestBody RegistrationKo registrationKo) throws URISyntaxException {
        log.debug("REST request to save RegistrationKo : {}", registrationKo);
        if (registrationKo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new registrationKo cannot already have an ID")).body(null);
        }
        RegistrationKo result = registrationKoService.save(registrationKo);
        return ResponseEntity.created(new URI("/api/registration-kos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /registration-kos : Updates an existing registrationKo.
     *
     * @param registrationKo the registrationKo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated registrationKo,
     * or with status 400 (Bad Request) if the registrationKo is not valid,
     * or with status 500 (Internal Server Error) if the registrationKo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/registration-kos")
    @Timed
    public ResponseEntity<RegistrationKo> updateRegistrationKo(@Valid @RequestBody RegistrationKo registrationKo) throws URISyntaxException {
        log.debug("REST request to update RegistrationKo : {}", registrationKo);
        if (registrationKo.getId() == null) {
            return createRegistrationKo(registrationKo);
        }
        RegistrationKo result = registrationKoService.save(registrationKo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, registrationKo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /registration-kos : get all the registrationKos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of registrationKos in body
     */
    @GetMapping("/registration-kos")
    @Timed
    public ResponseEntity<List<RegistrationKo>> getAllRegistrationKos(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RegistrationKos");
        Page<RegistrationKo> page = registrationKoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/registration-kos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /registration-kos/:id : get the "id" registrationKo.
     *
     * @param id the id of the registrationKo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the registrationKo, or with status 404 (Not Found)
     */
    @GetMapping("/registration-kos/{id}")
    @Timed
    public ResponseEntity<RegistrationKo> getRegistrationKo(@PathVariable Long id) {
        log.debug("REST request to get RegistrationKo : {}", id);
        RegistrationKo registrationKo = registrationKoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(registrationKo));
    }

    /**
     * DELETE  /registration-kos/:id : delete the "id" registrationKo.
     *
     * @param id the id of the registrationKo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/registration-kos/{id}")
    @Timed
    public ResponseEntity<Void> deleteRegistrationKo(@PathVariable Long id) {
        log.debug("REST request to delete RegistrationKo : {}", id);
        registrationKoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/registration-kos?query=:query : search for the registrationKo corresponding
     * to the query.
     *
     * @param query the query of the registrationKo search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/registration-kos")
    @Timed
    public ResponseEntity<List<RegistrationKo>> searchRegistrationKos(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RegistrationKos for query {}", query);
        Page<RegistrationKo> page = registrationKoService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/registration-kos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
