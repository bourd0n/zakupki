package com.alex.zakupki.complaint.api.model.versions.v6_2

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.versions.common.DateAdapter
import com.google.common.base.MoreObjects

//@Entity
//@Table(name = "complaints_cancel")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class ComplaintCancel(@xmlPath("complaintCancel[1]/oos:complaintNumber/text()")
                           complaintNumber: String,
                           @xmlPath("complaintCancel[1]/oos:regDate/text()")
                           @xmlJavaTypeAdapter(classOf[DateAdapter])
                           regDate: Date,
                           @xmlPath("complaintCancel[1]/oos:registrationKO")
                           registrationKO: RegistrationKO,
                           @xmlPath("complaintCancel[1]/oos:text/text()")
                           text: String) {
    private def this() = this(null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
    }
}
