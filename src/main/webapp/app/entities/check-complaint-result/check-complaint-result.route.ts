import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CheckComplaintResultComponent } from './check-complaint-result.component';
import { CheckComplaintResultDetailComponent } from './check-complaint-result-detail.component';
import { CheckComplaintResultPopupComponent } from './check-complaint-result-dialog.component';
import { CheckComplaintResultDeletePopupComponent } from './check-complaint-result-delete-dialog.component';

export const checkComplaintResultRoute: Routes = [
    {
        path: 'check-complaint-result',
        component: CheckComplaintResultComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.checkComplaintResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'check-complaint-result/:id',
        component: CheckComplaintResultDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.checkComplaintResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const checkComplaintResultPopupRoute: Routes = [
    {
        path: 'check-complaint-result-new',
        component: CheckComplaintResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.checkComplaintResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'check-complaint-result/:id/edit',
        component: CheckComplaintResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.checkComplaintResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'check-complaint-result/:id/delete',
        component: CheckComplaintResultDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.checkComplaintResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
