package com.alex.zakupki.service.mapper.xml

import java.util.Optional
import javax.inject.Inject

import com.alex.zakupki.complaint.api.model.general.RegistrationKO
import com.alex.zakupki.domain.RegistrationKo
import com.alex.zakupki.repository.RegistrationKoRepository
import org.springframework.stereotype.Component

@Component
class RegistrationKoXmlToDataMapper @Inject()(override val repository: RegistrationKoRepository)
    extends CachedXmlToDataMapper[RegistrationKO, RegistrationKo] {

    override def extractIdentificator(from: RegistrationKO): String = from.regNum

    override def findByIdentificator(id: String): Optional[RegistrationKo] = repository.findOneByRegNum(id)

    override def createNew(from: RegistrationKO): RegistrationKo =
        new RegistrationKo()
            .fullName(from.fullName)
            .regNum(from.regNum)
}
