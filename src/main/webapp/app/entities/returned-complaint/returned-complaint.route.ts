import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReturnedComplaintComponent } from './returned-complaint.component';
import { ReturnedComplaintDetailComponent } from './returned-complaint-detail.component';
import { ReturnedComplaintPopupComponent } from './returned-complaint-dialog.component';
import { ReturnedComplaintDeletePopupComponent } from './returned-complaint-delete-dialog.component';

export const returnedComplaintRoute: Routes = [
    {
        path: 'returned-complaint',
        component: ReturnedComplaintComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.returnedComplaint.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'returned-complaint/:id',
        component: ReturnedComplaintDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.returnedComplaint.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const returnedComplaintPopupRoute: Routes = [
    {
        path: 'returned-complaint-new',
        component: ReturnedComplaintPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.returnedComplaint.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'returned-complaint/:id/edit',
        component: ReturnedComplaintPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.returnedComplaint.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'returned-complaint/:id/delete',
        component: ReturnedComplaintDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.returnedComplaint.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
