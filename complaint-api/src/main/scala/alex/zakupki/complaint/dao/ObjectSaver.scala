package alex.zakupki.complaint.dao

trait ObjectSaver {
  def saveObject(unmarshalObject: AnyRef)
}
