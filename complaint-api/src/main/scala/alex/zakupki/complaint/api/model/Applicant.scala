package alex.zakupki.complaint.api.model

import javax.persistence.{Entity, Table}
import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType}

@Entity
@Table(name = "applicants")
@XmlAccessorType(XmlAccessType.FIELD)
case class Applicant(@id @xmlPath("oos:organizationName/text()")
                     organizationName: String,
                     @xmlPath("oos:applicantType/text()")
                     applicantType: String) {
  private def this() = this(null, null)
}