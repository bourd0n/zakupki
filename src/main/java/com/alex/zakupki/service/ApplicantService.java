package com.alex.zakupki.service;

import com.alex.zakupki.domain.Applicant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Applicant.
 */
public interface ApplicantService {

    /**
     * Save a applicant.
     *
     * @param applicant the entity to save
     * @return the persisted entity
     */
    Applicant save(Applicant applicant);

    /**
     *  Get all the applicants.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Applicant> findAll(Pageable pageable);

    /**
     *  Get the "id" applicant.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Applicant findOne(Long id);

    /**
     *  Delete the "id" applicant.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the applicant corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Applicant> search(String query, Pageable pageable);
}
