import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('TenderSuspension e2e test', () => {

    let navBarPage: NavBarPage;
    let tenderSuspensionDialogPage: TenderSuspensionDialogPage;
    let tenderSuspensionComponentsPage: TenderSuspensionComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TenderSuspensions', () => {
        navBarPage.goToEntity('tender-suspension');
        tenderSuspensionComponentsPage = new TenderSuspensionComponentsPage();
        expect(tenderSuspensionComponentsPage.getTitle()).toMatch(/zakupkiApp.tenderSuspension.home.title/);

    });

    it('should load create TenderSuspension dialog', () => {
        tenderSuspensionComponentsPage.clickOnCreateButton();
        tenderSuspensionDialogPage = new TenderSuspensionDialogPage();
        expect(tenderSuspensionDialogPage.getModalTitle()).toMatch(/zakupkiApp.tenderSuspension.home.createOrEditLabel/);
        tenderSuspensionDialogPage.close();
    });

    it('should create and save TenderSuspensions', () => {
        tenderSuspensionComponentsPage.clickOnCreateButton();
        tenderSuspensionDialogPage.setComplaintNumberInput('complaintNumber');
        expect(tenderSuspensionDialogPage.getComplaintNumberInput()).toMatch('complaintNumber');
        tenderSuspensionDialogPage.setRegDateInput('2000-12-31');
        expect(tenderSuspensionDialogPage.getRegDateInput()).toMatch('2000-12-31');
        tenderSuspensionDialogPage.setActionInput('action');
        expect(tenderSuspensionDialogPage.getActionInput()).toMatch('action');
        tenderSuspensionDialogPage.save();
        expect(tenderSuspensionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TenderSuspensionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-tender-suspension div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TenderSuspensionDialogPage {
    modalTitle = element(by.css('h4#myTenderSuspensionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    complaintNumberInput = element(by.css('input#field_complaintNumber'));
    regDateInput = element(by.css('input#field_regDate'));
    actionInput = element(by.css('input#field_action'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setComplaintNumberInput = function (complaintNumber) {
        this.complaintNumberInput.sendKeys(complaintNumber);
    }

    getComplaintNumberInput = function () {
        return this.complaintNumberInput.getAttribute('value');
    }

    setRegDateInput = function (regDate) {
        this.regDateInput.sendKeys(regDate);
    }

    getRegDateInput = function () {
        return this.regDateInput.getAttribute('value');
    }

    setActionInput = function (action) {
        this.actionInput.sendKeys(action);
    }

    getActionInput = function () {
        return this.actionInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
