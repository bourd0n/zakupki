package alex.zakupki.complaint.logic.akka.spring

import akka.actor.{Actor, IndirectActorProducer}
import org.springframework.context.ApplicationContext

case class SpringActorProducer(ctx: ApplicationContext, actorBeanName: String) extends IndirectActorProducer {
  override def produce(): Actor = ctx.getBean(actorBeanName, classOf[Actor])

  override def actorClass: Class[_ <: Actor] = ctx.getType(actorBeanName).asInstanceOf[Class[Actor]]
}
