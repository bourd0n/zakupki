import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UnplannedCheckResult } from './unplanned-check-result.model';
import { UnplannedCheckResultPopupService } from './unplanned-check-result-popup.service';
import { UnplannedCheckResultService } from './unplanned-check-result.service';

@Component({
    selector: 'jhi-unplanned-check-result-delete-dialog',
    templateUrl: './unplanned-check-result-delete-dialog.component.html'
})
export class UnplannedCheckResultDeleteDialogComponent {

    unplannedCheckResult: UnplannedCheckResult;

    constructor(
        private unplannedCheckResultService: UnplannedCheckResultService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.unplannedCheckResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'unplannedCheckResultListModification',
                content: 'Deleted an unplannedCheckResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-unplanned-check-result-delete-popup',
    template: ''
})
export class UnplannedCheckResultDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private unplannedCheckResultPopupService: UnplannedCheckResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.unplannedCheckResultPopupService
                .open(UnplannedCheckResultDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
