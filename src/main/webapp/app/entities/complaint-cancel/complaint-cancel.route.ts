import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ComplaintCancelComponent } from './complaint-cancel.component';
import { ComplaintCancelDetailComponent } from './complaint-cancel-detail.component';
import { ComplaintCancelPopupComponent } from './complaint-cancel-dialog.component';
import { ComplaintCancelDeletePopupComponent } from './complaint-cancel-delete-dialog.component';

export const complaintCancelRoute: Routes = [
    {
        path: 'complaint-cancel',
        component: ComplaintCancelComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.complaintCancel.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'complaint-cancel/:id',
        component: ComplaintCancelDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.complaintCancel.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const complaintCancelPopupRoute: Routes = [
    {
        path: 'complaint-cancel-new',
        component: ComplaintCancelPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.complaintCancel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'complaint-cancel/:id/edit',
        component: ComplaintCancelPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.complaintCancel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'complaint-cancel/:id/delete',
        component: ComplaintCancelDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.complaintCancel.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
