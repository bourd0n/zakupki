import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CheckComplaintResult } from './check-complaint-result.model';
import { CheckComplaintResultService } from './check-complaint-result.service';

@Injectable()
export class CheckComplaintResultPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private checkComplaintResultService: CheckComplaintResultService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.checkComplaintResultService.find(id).subscribe((checkComplaintResult) => {
                    if (checkComplaintResult.createDate) {
                        checkComplaintResult.createDate = {
                            year: checkComplaintResult.createDate.getFullYear(),
                            month: checkComplaintResult.createDate.getMonth() + 1,
                            day: checkComplaintResult.createDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.checkComplaintResultModalRef(component, checkComplaintResult);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.checkComplaintResultModalRef(component, new CheckComplaintResult());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    checkComplaintResultModalRef(component: Component, checkComplaintResult: CheckComplaintResult): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.checkComplaintResult = checkComplaintResult;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
