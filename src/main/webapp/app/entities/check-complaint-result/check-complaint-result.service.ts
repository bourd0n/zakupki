import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CheckComplaintResult } from './check-complaint-result.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CheckComplaintResultService {

    private resourceUrl = SERVER_API_URL + 'api/check-complaint-results';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/check-complaint-results';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(checkComplaintResult: CheckComplaintResult): Observable<CheckComplaintResult> {
        const copy = this.convert(checkComplaintResult);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(checkComplaintResult: CheckComplaintResult): Observable<CheckComplaintResult> {
        const copy = this.convert(checkComplaintResult);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<CheckComplaintResult> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to CheckComplaintResult.
     */
    private convertItemFromServer(json: any): CheckComplaintResult {
        const entity: CheckComplaintResult = Object.assign(new CheckComplaintResult(), json);
        entity.createDate = this.dateUtils
            .convertLocalDateFromServer(json.createDate);
        return entity;
    }

    /**
     * Convert a CheckComplaintResult to a JSON which can be sent to the server.
     */
    private convert(checkComplaintResult: CheckComplaintResult): CheckComplaintResult {
        const copy: CheckComplaintResult = Object.assign({}, checkComplaintResult);
        copy.createDate = this.dateUtils
            .convertLocalDateToServer(checkComplaintResult.createDate);
        return copy;
    }
}
