package com.alex.zakupki.complaint.api.model.versions.common

import java.text.SimpleDateFormat
import java.util.Date
import javax.xml.bind.annotation.adapters.XmlAdapter

sealed class DateAdapter extends XmlAdapter[String, Date] {
  private val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")

  @throws(classOf[Exception])
  def marshal(v: Date): String = {
    dateFormat.format(v)
  }

  @throws(classOf[Exception])
  def unmarshal(v: String): Date = {
    dateFormat.parse(v)
  }
}
