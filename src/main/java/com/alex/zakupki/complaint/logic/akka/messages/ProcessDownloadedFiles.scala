package com.alex.zakupki.complaint.logic.akka.messages

case class ProcessDownloadedFiles(filesCount: Int)
