package com.alex.zakupki.web.rest.vm

import java.time.LocalDate
import java.util.Objects

import scala.beans.BeanProperty

class SyncInfoVM(@BeanProperty var name: String,
                 @BeanProperty var syncDate: LocalDate) {

    @BeanProperty
    var status : String = _

    override def equals(o: scala.Any): Boolean = {
        if (this == o) return true
        if (o == null || (getClass ne o.getClass)) return false
        val other: SyncInfoVM = o.asInstanceOf[SyncInfoVM]
        name.eq(other.name)
    }

    override def hashCode(): Int = Objects.hashCode(name)

    override def toString = s"SyncInfoVM{name='$name', syncDate='$syncDate'}"

}

object SyncInfoVM {
    def apply(name: String, syncDate: LocalDate): SyncInfoVM = new SyncInfoVM(name, syncDate)
}
