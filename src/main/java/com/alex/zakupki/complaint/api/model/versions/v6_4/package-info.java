@XmlSchema(
        namespace = "http://zakupki.gov.ru/oos/types/1",
        xmlns = {
                @XmlNs(prefix = "ns2", namespaceURI = "http://zakupki.gov.ru/oos/export/1")
        },
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlAccessorType(XmlAccessType.FIELD)
package com.alex.zakupki.complaint.api.model.versions.v6_4;
import javax.xml.bind.annotation.*;
