package com.alex.zakupki.service.impl;

import com.alex.zakupki.service.CheckComplaintResultService;
import com.alex.zakupki.domain.CheckComplaintResult;
import com.alex.zakupki.repository.CheckComplaintResultRepository;
import com.alex.zakupki.repository.search.CheckComplaintResultSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CheckComplaintResult.
 */
@Service
@Transactional
public class CheckComplaintResultServiceImpl implements CheckComplaintResultService{

    private final Logger log = LoggerFactory.getLogger(CheckComplaintResultServiceImpl.class);

    private final CheckComplaintResultRepository checkComplaintResultRepository;

    private final CheckComplaintResultSearchRepository checkComplaintResultSearchRepository;

    public CheckComplaintResultServiceImpl(CheckComplaintResultRepository checkComplaintResultRepository, CheckComplaintResultSearchRepository checkComplaintResultSearchRepository) {
        this.checkComplaintResultRepository = checkComplaintResultRepository;
        this.checkComplaintResultSearchRepository = checkComplaintResultSearchRepository;
    }

    /**
     * Save a checkComplaintResult.
     *
     * @param checkComplaintResult the entity to save
     * @return the persisted entity
     */
    @Override
    public CheckComplaintResult save(CheckComplaintResult checkComplaintResult) {
        log.debug("Request to save CheckComplaintResult : {}", checkComplaintResult);
        CheckComplaintResult result = checkComplaintResultRepository.save(checkComplaintResult);
        checkComplaintResultSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the checkComplaintResults.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CheckComplaintResult> findAll(Pageable pageable) {
        log.debug("Request to get all CheckComplaintResults");
        return checkComplaintResultRepository.findAll(pageable);
    }

    /**
     *  Get one checkComplaintResult by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CheckComplaintResult findOne(Long id) {
        log.debug("Request to get CheckComplaintResult : {}", id);
        return checkComplaintResultRepository.findOne(id);
    }

    /**
     *  Delete the  checkComplaintResult by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CheckComplaintResult : {}", id);
        checkComplaintResultRepository.delete(id);
        checkComplaintResultSearchRepository.delete(id);
    }

    /**
     * Search for the checkComplaintResult corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CheckComplaintResult> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CheckComplaintResults for query {}", query);
        Page<CheckComplaintResult> result = checkComplaintResultSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
