package com.alex.zakupki.complaint.api.model.versions.v6_4

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import com.google.common.base.MoreObjects

//@Entity
//@Table(name = "complaints")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class Complaint(complaintNumber: String,
                     @xmlPath("ns2:complaint/commonInfo/registrationKO")
                     registrationKO: RegistrationKO,
                     @xmlPath("ns2:complaint/commonInfo/regDate/text()")
                     var regDate: Date,
                     @xmlPath("ns2:complaint/object/purchase")
                     var purchase: Purchase,
                     @xmlPath("ns2:complaint/applicantNew")
                     var applicant: Applicant,
                     @xmlPath("ns2:complaint/text/text()")
                     var text: String,
                     @xmlPath("ns2:complaint/indicted")
                     @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                     var indictedClient: Client,
                     @xmlPath("ns2:complaint/returnInfo/base/text()")
                     var returnInfo: String) extends GeneralizedEntity[general.Complaint] {
    private def this() = this(null, null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
    }

    override def mapToGeneral(): general.Complaint = {
        general.Complaint(complaintNumber,
            if (registrationKO == null) null else registrationKO.mapToGeneral(),
            regDate,
            if (purchase == null) null else purchase.mapToGeneral(),
            if (applicant == null) null else applicant.mapToGeneral(),
            text,
            if (indictedClient == null) null else indictedClient.mapToGeneral(),
            returnInfo)
    }
}
