import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PlannedCheckResult } from './planned-check-result.model';
import { PlannedCheckResultPopupService } from './planned-check-result-popup.service';
import { PlannedCheckResultService } from './planned-check-result.service';

@Component({
    selector: 'jhi-planned-check-result-delete-dialog',
    templateUrl: './planned-check-result-delete-dialog.component.html'
})
export class PlannedCheckResultDeleteDialogComponent {

    plannedCheckResult: PlannedCheckResult;

    constructor(
        private plannedCheckResultService: PlannedCheckResultService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.plannedCheckResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'plannedCheckResultListModification',
                content: 'Deleted an plannedCheckResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-planned-check-result-delete-popup',
    template: ''
})
export class PlannedCheckResultDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private plannedCheckResultPopupService: PlannedCheckResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.plannedCheckResultPopupService
                .open(PlannedCheckResultDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
