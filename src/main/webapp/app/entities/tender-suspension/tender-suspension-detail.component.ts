import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TenderSuspension } from './tender-suspension.model';
import { TenderSuspensionService } from './tender-suspension.service';

@Component({
    selector: 'jhi-tender-suspension-detail',
    templateUrl: './tender-suspension-detail.component.html'
})
export class TenderSuspensionDetailComponent implements OnInit, OnDestroy {

    tenderSuspension: TenderSuspension;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tenderSuspensionService: TenderSuspensionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTenderSuspensions();
    }

    load(id) {
        this.tenderSuspensionService.find(id).subscribe((tenderSuspension) => {
            this.tenderSuspension = tenderSuspension;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTenderSuspensions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tenderSuspensionListModification',
            (response) => this.load(this.tenderSuspension.id)
        );
    }
}
