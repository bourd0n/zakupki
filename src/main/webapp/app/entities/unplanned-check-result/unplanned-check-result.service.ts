import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { UnplannedCheckResult } from './unplanned-check-result.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class UnplannedCheckResultService {

    private resourceUrl = SERVER_API_URL + 'api/unplanned-check-results';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/unplanned-check-results';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(unplannedCheckResult: UnplannedCheckResult): Observable<UnplannedCheckResult> {
        const copy = this.convert(unplannedCheckResult);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(unplannedCheckResult: UnplannedCheckResult): Observable<UnplannedCheckResult> {
        const copy = this.convert(unplannedCheckResult);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<UnplannedCheckResult> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to UnplannedCheckResult.
     */
    private convertItemFromServer(json: any): UnplannedCheckResult {
        const entity: UnplannedCheckResult = Object.assign(new UnplannedCheckResult(), json);
        entity.createdDate = this.dateUtils
            .convertLocalDateFromServer(json.createdDate);
        return entity;
    }

    /**
     * Convert a UnplannedCheckResult to a JSON which can be sent to the server.
     */
    private convert(unplannedCheckResult: UnplannedCheckResult): UnplannedCheckResult {
        const copy: UnplannedCheckResult = Object.assign({}, unplannedCheckResult);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(unplannedCheckResult.createdDate);
        return copy;
    }
}
