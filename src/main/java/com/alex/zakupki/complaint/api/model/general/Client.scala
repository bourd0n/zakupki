package com.alex.zakupki.complaint.api.model.general

/**
 * customer, authority, commission94/44 others
 */
case class Client(regNum: String,
                  fullName: String,
                  clientType: String)
