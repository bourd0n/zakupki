import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PlannedCheckResultComponent } from './planned-check-result.component';
import { PlannedCheckResultDetailComponent } from './planned-check-result-detail.component';
import { PlannedCheckResultPopupComponent } from './planned-check-result-dialog.component';
import { PlannedCheckResultDeletePopupComponent } from './planned-check-result-delete-dialog.component';

export const plannedCheckResultRoute: Routes = [
    {
        path: 'planned-check-result',
        component: PlannedCheckResultComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.plannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'planned-check-result/:id',
        component: PlannedCheckResultDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.plannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const plannedCheckResultPopupRoute: Routes = [
    {
        path: 'planned-check-result-new',
        component: PlannedCheckResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.plannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'planned-check-result/:id/edit',
        component: PlannedCheckResultPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.plannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'planned-check-result/:id/delete',
        component: PlannedCheckResultDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'zakupkiApp.plannedCheckResult.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
