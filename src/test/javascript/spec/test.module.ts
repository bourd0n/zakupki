import { NgModule } from '@angular/core';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import {JhiAlertService, JhiLanguageService} from 'ng-jhipster';
import { MockLanguageService } from './helpers/mock-language.service';
import {MockAlertService} from './helpers/mock-alert.service';

@NgModule({
    providers: [
        MockBackend,
        BaseRequestOptions,
        {
            provide: JhiLanguageService,
            useClass: MockLanguageService
        },
        {
            provide: JhiAlertService,
            useClass: MockAlertService
        },
        {
            provide: Http,
            useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                return new Http(backendInstance, defaultOptions);
            },
            deps: [MockBackend, BaseRequestOptions]
        }
    ]
})
export class ZakupkiTestModule {}
