import { BaseEntity } from './../../shared';

export class ControlCheckResult implements BaseEntity {
    constructor(
        public id?: number,
        public checkResultNumber?: string,
        public createdDate?: any,
        public result?: string,
        public owner?: BaseEntity,
        public clientComplaint?: BaseEntity,
    ) {
    }
}
