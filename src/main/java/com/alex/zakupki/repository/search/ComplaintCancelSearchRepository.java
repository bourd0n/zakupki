package com.alex.zakupki.repository.search;

import com.alex.zakupki.domain.ComplaintCancel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the ComplaintCancel entity.
 */
public interface ComplaintCancelSearchRepository extends ElasticsearchRepository<ComplaintCancel, Long> {
}
