/**
 * View Models used by Spring MVC REST controllers.
 */
package com.alex.zakupki.web.rest.vm;
