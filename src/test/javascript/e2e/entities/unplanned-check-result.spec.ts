import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('UnplannedCheckResult e2e test', () => {

    let navBarPage: NavBarPage;
    let unplannedCheckResultDialogPage: UnplannedCheckResultDialogPage;
    let unplannedCheckResultComponentsPage: UnplannedCheckResultComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load UnplannedCheckResults', () => {
        navBarPage.goToEntity('unplanned-check-result');
        unplannedCheckResultComponentsPage = new UnplannedCheckResultComponentsPage();
        expect(unplannedCheckResultComponentsPage.getTitle()).toMatch(/zakupkiApp.unplannedCheckResult.home.title/);

    });

    it('should load create UnplannedCheckResult dialog', () => {
        unplannedCheckResultComponentsPage.clickOnCreateButton();
        unplannedCheckResultDialogPage = new UnplannedCheckResultDialogPage();
        expect(unplannedCheckResultDialogPage.getModalTitle()).toMatch(/zakupkiApp.unplannedCheckResult.home.createOrEditLabel/);
        unplannedCheckResultDialogPage.close();
    });

    it('should create and save UnplannedCheckResults', () => {
        unplannedCheckResultComponentsPage.clickOnCreateButton();
        unplannedCheckResultDialogPage.setCheckResultNumberInput('checkResultNumber');
        expect(unplannedCheckResultDialogPage.getCheckResultNumberInput()).toMatch('checkResultNumber');
        unplannedCheckResultDialogPage.setUnplannedCheckNumberInput('unplannedCheckNumber');
        expect(unplannedCheckResultDialogPage.getUnplannedCheckNumberInput()).toMatch('unplannedCheckNumber');
        unplannedCheckResultDialogPage.setCreatedDateInput('2000-12-31');
        expect(unplannedCheckResultDialogPage.getCreatedDateInput()).toMatch('2000-12-31');
        unplannedCheckResultDialogPage.setResultInput('result');
        expect(unplannedCheckResultDialogPage.getResultInput()).toMatch('result');
        unplannedCheckResultDialogPage.ownerSelectLastOption();
        unplannedCheckResultDialogPage.checkSubjectSelectLastOption();
        unplannedCheckResultDialogPage.save();
        expect(unplannedCheckResultDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class UnplannedCheckResultComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-unplanned-check-result div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class UnplannedCheckResultDialogPage {
    modalTitle = element(by.css('h4#myUnplannedCheckResultLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    checkResultNumberInput = element(by.css('input#field_checkResultNumber'));
    unplannedCheckNumberInput = element(by.css('input#field_unplannedCheckNumber'));
    createdDateInput = element(by.css('input#field_createdDate'));
    resultInput = element(by.css('input#field_result'));
    ownerSelect = element(by.css('select#field_owner'));
    checkSubjectSelect = element(by.css('select#field_checkSubject'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCheckResultNumberInput = function (checkResultNumber) {
        this.checkResultNumberInput.sendKeys(checkResultNumber);
    }

    getCheckResultNumberInput = function () {
        return this.checkResultNumberInput.getAttribute('value');
    }

    setUnplannedCheckNumberInput = function (unplannedCheckNumber) {
        this.unplannedCheckNumberInput.sendKeys(unplannedCheckNumber);
    }

    getUnplannedCheckNumberInput = function () {
        return this.unplannedCheckNumberInput.getAttribute('value');
    }

    setCreatedDateInput = function (createdDate) {
        this.createdDateInput.sendKeys(createdDate);
    }

    getCreatedDateInput = function () {
        return this.createdDateInput.getAttribute('value');
    }

    setResultInput = function (result) {
        this.resultInput.sendKeys(result);
    }

    getResultInput = function () {
        return this.resultInput.getAttribute('value');
    }

    ownerSelectLastOption = function () {
        this.ownerSelect.all(by.tagName('option')).last().click();
    }

    ownerSelectOption = function (option) {
        this.ownerSelect.sendKeys(option);
    }

    getOwnerSelect = function () {
        return this.ownerSelect;
    }

    getOwnerSelectedOption = function () {
        return this.ownerSelect.element(by.css('option:checked')).getText();
    }

    checkSubjectSelectLastOption = function () {
        this.checkSubjectSelect.all(by.tagName('option')).last().click();
    }

    checkSubjectSelectOption = function (option) {
        this.checkSubjectSelect.sendKeys(option);
    }

    getCheckSubjectSelect = function () {
        return this.checkSubjectSelect;
    }

    getCheckSubjectSelectedOption = function () {
        return this.checkSubjectSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
