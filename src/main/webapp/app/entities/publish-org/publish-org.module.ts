import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ZakupkiSharedModule } from '../../shared';
import {
    PublishOrgService,
    PublishOrgPopupService,
    PublishOrgComponent,
    PublishOrgDetailComponent,
    PublishOrgDialogComponent,
    PublishOrgPopupComponent,
    PublishOrgDeletePopupComponent,
    PublishOrgDeleteDialogComponent,
    publishOrgRoute,
    publishOrgPopupRoute,
} from './';

const ENTITY_STATES = [
    ...publishOrgRoute,
    ...publishOrgPopupRoute,
];

@NgModule({
    imports: [
        ZakupkiSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PublishOrgComponent,
        PublishOrgDetailComponent,
        PublishOrgDialogComponent,
        PublishOrgDeleteDialogComponent,
        PublishOrgPopupComponent,
        PublishOrgDeletePopupComponent,
    ],
    entryComponents: [
        PublishOrgComponent,
        PublishOrgDialogComponent,
        PublishOrgPopupComponent,
        PublishOrgDeleteDialogComponent,
        PublishOrgDeletePopupComponent,
    ],
    providers: [
        PublishOrgService,
        PublishOrgPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiPublishOrgModule {}
