package com.alex.zakupki.complaint.api.model.general

import java.util.Date

import com.google.common.base.MoreObjects

case class TenderSuspension(var id: Integer,
                            var complaintNumber: String,
                            var regDate: Date,
                            var action: String) {
  override def toString: String = {
    MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).add("action", action).toString
  }
}
