import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ControlCheckResult } from './control-check-result.model';
import { ControlCheckResultService } from './control-check-result.service';

@Injectable()
export class ControlCheckResultPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private controlCheckResultService: ControlCheckResultService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.controlCheckResultService.find(id).subscribe((controlCheckResult) => {
                    if (controlCheckResult.createdDate) {
                        controlCheckResult.createdDate = {
                            year: controlCheckResult.createdDate.getFullYear(),
                            month: controlCheckResult.createdDate.getMonth() + 1,
                            day: controlCheckResult.createdDate.getDate()
                        };
                    }
                    this.ngbModalRef = this.controlCheckResultModalRef(component, controlCheckResult);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.controlCheckResultModalRef(component, new ControlCheckResult());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    controlCheckResultModalRef(component: Component, controlCheckResult: ControlCheckResult): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.controlCheckResult = controlCheckResult;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
