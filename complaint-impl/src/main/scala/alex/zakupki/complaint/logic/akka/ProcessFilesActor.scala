package alex.zakupki.complaint.logic.akka

import akka.actor.{Actor, ActorRef}
import alex.zakupki.complaint.logic.akka.spring.SpringExtension
import alex.zakupki.complaint.logic.ftp.FTPLoader
import alex.zakupki.complaint.utils.Logging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

import scala.collection.immutable.IndexedSeq

@Component("processFilesActor")
@Scope("prototype")
class ProcessFilesActor @Autowired() (implicit ctx: ApplicationContext) extends Actor with Logging {

  override def receive: Receive = {
    case StartDownload(ftpType, limit) =>
      logger.debug("Start download {} with limit {}", ftpType, limit)
      val downloaded = FTPLoader.downloadInmemory(ftpType, limit)
      logger.debug("Download {} files", downloaded.length)
      val workers = createWorkers(downloaded.length)
      beginProcessDownloadedFiles(downloaded, workers)
      logger.debug("Send all messages to workers")
  }

  private def createWorkers(numActors: Int): IndexedSeq[ActorRef] = {
    val zipProcessorActorProps = SpringExtension(context.system).props("zipProcessorActor")
    for (i <- 0 to numActors) yield context.actorOf(zipProcessorActorProps, name = s"worker-$i")
  }

  private def beginProcessDownloadedFiles(downloaded: Array[Array[Byte]], workers: IndexedSeq[ActorRef]): Unit = {
    downloaded.zipWithIndex.foreach((elem: (Array[Byte], Int)) => {
      workers(elem._2 % workers.size) ! ProcessDownloadedZip(elem._1)
    })
  }

}
