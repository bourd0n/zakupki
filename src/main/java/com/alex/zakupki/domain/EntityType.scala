package com.alex.zakupki.domain

object EntityType {
    final val COMPLAINT = "complaint"
    final val CHECK_RESULT = "check_result"
}
