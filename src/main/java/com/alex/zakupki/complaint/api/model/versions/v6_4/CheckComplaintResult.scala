package com.alex.zakupki.complaint.api.model.versions.v6_4

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.GeneralizedEntity
import com.alex.zakupki.complaint.api.model.general
import com.google.common.base.MoreObjects
import org.apache.commons.lang3.ObjectUtils
;

@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class CheckComplaintResult(checkResultNumber: String,
                                @xmlPath("ns2:checkResult/complaint/regNumber/text()")
                                complaintNumber: String,
                                @xmlPath("ns2:checkResult/base/unplannedCheckComplaint/regNumber/text()")
                                complaintNumber2: String,
                                @xmlPath("ns2:checkResult/commonInfo/createDate/text()")
                                createDate: Date,
                                @xmlPath("ns2:checkResult/commonInfo/owner")
                                owner: Owner,
                                @xmlPath("ns2:checkResult/complaint/complaintResult/text()")
                                result: String,
                                @xmlPath("ns2:checkResult/base/unplannedCheckComplaint/complaintResult/text()")
                                result2: String,
                                @xmlPath("ns2:checkResult/complaint/checkSubjects/subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                checkSubject: Client,
                                @xmlPath("ns2:checkResult/base/unplannedCheckComplaint/checkSubjects/subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                checkSubject2: Client) extends GeneralizedEntity[general.CheckComplaintResult] {
    private def this() = this(null, null, null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("result", result).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }

    override def mapToGeneral(): general.CheckComplaintResult = general.CheckComplaintResult(
        checkResultNumber,
        createDate,
        owner.mapToGeneral(),
        ObjectUtils.firstNonNull(result, result2),
        Option(ObjectUtils.firstNonNull(checkSubject, checkSubject2)).map(_.mapToGeneral()).orNull,
        ObjectUtils.firstNonNull(complaintNumber, complaintNumber2)
    )
}
