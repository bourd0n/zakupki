import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CheckComplaintResult } from './check-complaint-result.model';
import { CheckComplaintResultPopupService } from './check-complaint-result-popup.service';
import { CheckComplaintResultService } from './check-complaint-result.service';

@Component({
    selector: 'jhi-check-complaint-result-delete-dialog',
    templateUrl: './check-complaint-result-delete-dialog.component.html'
})
export class CheckComplaintResultDeleteDialogComponent {

    checkComplaintResult: CheckComplaintResult;

    constructor(
        private checkComplaintResultService: CheckComplaintResultService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.checkComplaintResultService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'checkComplaintResultListModification',
                content: 'Deleted an checkComplaintResult'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-check-complaint-result-delete-popup',
    template: ''
})
export class CheckComplaintResultDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private checkComplaintResultPopupService: CheckComplaintResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.checkComplaintResultPopupService
                .open(CheckComplaintResultDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
