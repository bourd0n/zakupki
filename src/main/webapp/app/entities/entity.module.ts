import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ZakupkiPlannedCheckResultModule } from './planned-check-result/planned-check-result.module';
import { ZakupkiApplicantModule } from './applicant/applicant.module';
import { ZakupkiClientModule } from './client/client.module';
import { ZakupkiComplaintModule } from './complaint/complaint.module';
import { ZakupkiComplaintCancelModule } from './complaint-cancel/complaint-cancel.module';
import { ZakupkiControlCheckResultModule } from './control-check-result/control-check-result.module';
import { ZakupkiOwnerModule } from './owner/owner.module';
import { ZakupkiCheckComplaintResultModule } from './check-complaint-result/check-complaint-result.module';
import { ZakupkiPublishOrgModule } from './publish-org/publish-org.module';
import { ZakupkiPurchaseModule } from './purchase/purchase.module';
import { ZakupkiRegistrationKoModule } from './registration-ko/registration-ko.module';
import { ZakupkiTenderSuspensionModule } from './tender-suspension/tender-suspension.module';
import { ZakupkiUnfairSupplierModule } from './unfair-supplier/unfair-supplier.module';
import { ZakupkiUnplannedCheckResultModule } from './unplanned-check-result/unplanned-check-result.module';
import { ZakupkiReturnedComplaintModule } from './returned-complaint/returned-complaint.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ZakupkiPlannedCheckResultModule,
        ZakupkiApplicantModule,
        ZakupkiClientModule,
        ZakupkiComplaintModule,
        ZakupkiComplaintCancelModule,
        ZakupkiControlCheckResultModule,
        ZakupkiOwnerModule,
        ZakupkiCheckComplaintResultModule,
        ZakupkiPublishOrgModule,
        ZakupkiPurchaseModule,
        ZakupkiRegistrationKoModule,
        ZakupkiTenderSuspensionModule,
        ZakupkiUnfairSupplierModule,
        ZakupkiUnplannedCheckResultModule,
        ZakupkiReturnedComplaintModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ZakupkiEntityModule {}
