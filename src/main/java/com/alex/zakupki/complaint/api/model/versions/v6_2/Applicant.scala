package com.alex.zakupki.complaint.api.model.versions.v6_2

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}

//@Entity
//@Table(name = "applicants")
@XmlAccessorType(XmlAccessType.FIELD)
case class Applicant(@xmlPath("oos:organizationName/text()")
                     organizationName: String,
                     @xmlPath("oos:applicantType/text()")
                     applicantType: String) extends GeneralizedEntity[general.Applicant] {
    private def this() = this(null, null)

    override def mapToGeneral(): general.Applicant = {
        //todo: parse applicant Type
        general.Applicant(organizationName.trim, applicantType)
    }
}
