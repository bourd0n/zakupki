package com.alex.zakupki.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A ReturnedComplaint.
 */
@Entity
@Table(name = "returned_complaint")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "returnedcomplaint")
public class ReturnedComplaint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Size(max = 4000)
    @Column(name = "return_info_base", length = 4000)
    private String returnInfoBase;

    @ManyToOne
    @NotNull
    private Complaint complaint;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReturnInfoBase() {
        return returnInfoBase;
    }

    public ReturnedComplaint returnInfoBase(String returnInfoBase) {
        this.returnInfoBase = returnInfoBase;
        return this;
    }

    public void setReturnInfoBase(String returnInfoBase) {
        this.returnInfoBase = returnInfoBase;
    }

    public Complaint getComplaint() {
        return complaint;
    }

    public ReturnedComplaint complaint(Complaint complaint) {
        this.complaint = complaint;
        return this;
    }

    public void setComplaint(Complaint complaint) {
        this.complaint = complaint;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReturnedComplaint returnedComplaint = (ReturnedComplaint) o;
        if (returnedComplaint.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), returnedComplaint.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReturnedComplaint{" +
            "id=" + getId() +
            ", returnInfoBase='" + getReturnInfoBase() + "'" +
            "}";
    }
}
