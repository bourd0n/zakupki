package com.alex.zakupki.complaint.api.model.versions.v6_2

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import com.google.common.base.MoreObjects

//@Entity
//@Table(name = "complaints")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class Complaint(complaintNumber: String,
                     @xmlPath("complaint/oos:commonInfo/oos:registrationKO")
                     registrationKO: RegistrationKO,
                     @xmlPath("complaint/oos:commonInfo/oos:regDate/text()")
                     var regDate: Date,
                     @xmlPath("complaint/oos:object/oos:purchase")
                     var purchase: Purchase,
                     @xmlPath("complaint/oos:applicant")
                     var applicant: Applicant,
                     @xmlPath("complaint/oos:applicantNew")
                     var applicantNew: ApplicantNew,
                     @xmlPath("complaint/oos:text/text()")
                     var text: String,
                     @xmlPath("complaint/oos:indicted")
                     @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                     var indictedClient: Client,
                     @xmlPath("complaint/oos:returnInfo/oos:base/text()")
                     var returnInfo: String) extends GeneralizedEntity[general.Complaint] {
    private def this() = this(null, null, null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("complaintNumber", complaintNumber).add("regDate", regDate).toString
    }

    override def mapToGeneral(): general.Complaint = {
        general.Complaint(complaintNumber,
            if (registrationKO == null) null else registrationKO.mapToGeneral(),
            regDate,
            if (purchase == null) null else purchase.mapToGeneral(),
            if (applicant != null) {
                applicant.mapToGeneral()
            } else {
                if (applicantNew != null)
                    applicantNew.mapToGeneral()
                else
                    null
            },
            text,
            if (indictedClient == null) null else indictedClient.mapToGeneral(),
            returnInfo)
    }
}
