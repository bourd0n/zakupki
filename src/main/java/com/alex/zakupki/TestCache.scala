package com.alex.zakupki

import java.util.concurrent.TimeUnit

import com.alex.zakupki.domain.Applicant
import com.google.common.cache.{Cache, CacheBuilder, LoadingCache}


//todo: delete
@deprecated
object TestCache {

    val cache: Cache[String, String] = CacheBuilder.newBuilder()
        .expireAfterAccess(1, TimeUnit.SECONDS)
        .build()
        .asInstanceOf[Cache[String, String]]


    def asd(args: Array[String]): Unit = {

        cache.get("abc", () => {
            val t = new Thread(() => {

                cache.get("abc", () => "bbb")
            })
            t.start()
            println("wait")
            t.join(5000)
            println("continue")
            "aaa"
        })

        println(cache.size())
        println(cache.getIfPresent("abc"))

        Thread.sleep(10000)

        println(cache.size())
        println(cache.getIfPresent("abc"))

    }
}
