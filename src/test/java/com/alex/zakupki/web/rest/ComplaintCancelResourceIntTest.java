package com.alex.zakupki.web.rest;

import com.alex.zakupki.ZakupkiApp;

import com.alex.zakupki.domain.ComplaintCancel;
import com.alex.zakupki.repository.ComplaintCancelRepository;
import com.alex.zakupki.service.ComplaintCancelService;
import com.alex.zakupki.repository.search.ComplaintCancelSearchRepository;
import com.alex.zakupki.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ComplaintCancelResource REST controller.
 *
 * @see ComplaintCancelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZakupkiApp.class)
public class ComplaintCancelResourceIntTest {

    private static final String DEFAULT_COMPLAINT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_COMPLAINT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_REG_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_REG_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    @Autowired
    private ComplaintCancelRepository complaintCancelRepository;

    @Autowired
    private ComplaintCancelService complaintCancelService;

    @Autowired
    private ComplaintCancelSearchRepository complaintCancelSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restComplaintCancelMockMvc;

    private ComplaintCancel complaintCancel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ComplaintCancelResource complaintCancelResource = new ComplaintCancelResource(complaintCancelService);
        this.restComplaintCancelMockMvc = MockMvcBuilders.standaloneSetup(complaintCancelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ComplaintCancel createEntity(EntityManager em) {
        ComplaintCancel complaintCancel = new ComplaintCancel()
            .complaintNumber(DEFAULT_COMPLAINT_NUMBER)
            .regDate(DEFAULT_REG_DATE)
            .text(DEFAULT_TEXT);
        return complaintCancel;
    }

    @Before
    public void initTest() {
        complaintCancelSearchRepository.deleteAll();
        complaintCancel = createEntity(em);
    }

    @Test
    @Transactional
    public void createComplaintCancel() throws Exception {
        int databaseSizeBeforeCreate = complaintCancelRepository.findAll().size();

        // Create the ComplaintCancel
        restComplaintCancelMockMvc.perform(post("/api/complaint-cancels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(complaintCancel)))
            .andExpect(status().isCreated());

        // Validate the ComplaintCancel in the database
        List<ComplaintCancel> complaintCancelList = complaintCancelRepository.findAll();
        assertThat(complaintCancelList).hasSize(databaseSizeBeforeCreate + 1);
        ComplaintCancel testComplaintCancel = complaintCancelList.get(complaintCancelList.size() - 1);
        assertThat(testComplaintCancel.getComplaintNumber()).isEqualTo(DEFAULT_COMPLAINT_NUMBER);
        assertThat(testComplaintCancel.getRegDate()).isEqualTo(DEFAULT_REG_DATE);
        assertThat(testComplaintCancel.getText()).isEqualTo(DEFAULT_TEXT);

        // Validate the ComplaintCancel in Elasticsearch
        ComplaintCancel complaintCancelEs = complaintCancelSearchRepository.findOne(testComplaintCancel.getId());
        assertThat(complaintCancelEs).isEqualToComparingFieldByField(testComplaintCancel);
    }

    @Test
    @Transactional
    public void createComplaintCancelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = complaintCancelRepository.findAll().size();

        // Create the ComplaintCancel with an existing ID
        complaintCancel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restComplaintCancelMockMvc.perform(post("/api/complaint-cancels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(complaintCancel)))
            .andExpect(status().isBadRequest());

        // Validate the ComplaintCancel in the database
        List<ComplaintCancel> complaintCancelList = complaintCancelRepository.findAll();
        assertThat(complaintCancelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkComplaintNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = complaintCancelRepository.findAll().size();
        // set the field null
        complaintCancel.setComplaintNumber(null);

        // Create the ComplaintCancel, which fails.

        restComplaintCancelMockMvc.perform(post("/api/complaint-cancels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(complaintCancel)))
            .andExpect(status().isBadRequest());

        List<ComplaintCancel> complaintCancelList = complaintCancelRepository.findAll();
        assertThat(complaintCancelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllComplaintCancels() throws Exception {
        // Initialize the database
        complaintCancelRepository.saveAndFlush(complaintCancel);

        // Get all the complaintCancelList
        restComplaintCancelMockMvc.perform(get("/api/complaint-cancels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(complaintCancel.getId().intValue())))
            .andExpect(jsonPath("$.[*].complaintNumber").value(hasItem(DEFAULT_COMPLAINT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].regDate").value(hasItem(DEFAULT_REG_DATE.toString())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())));
    }

    @Test
    @Transactional
    public void getComplaintCancel() throws Exception {
        // Initialize the database
        complaintCancelRepository.saveAndFlush(complaintCancel);

        // Get the complaintCancel
        restComplaintCancelMockMvc.perform(get("/api/complaint-cancels/{id}", complaintCancel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(complaintCancel.getId().intValue()))
            .andExpect(jsonPath("$.complaintNumber").value(DEFAULT_COMPLAINT_NUMBER.toString()))
            .andExpect(jsonPath("$.regDate").value(DEFAULT_REG_DATE.toString()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingComplaintCancel() throws Exception {
        // Get the complaintCancel
        restComplaintCancelMockMvc.perform(get("/api/complaint-cancels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComplaintCancel() throws Exception {
        // Initialize the database
        complaintCancelService.save(complaintCancel);

        int databaseSizeBeforeUpdate = complaintCancelRepository.findAll().size();

        // Update the complaintCancel
        ComplaintCancel updatedComplaintCancel = complaintCancelRepository.findOne(complaintCancel.getId());
        updatedComplaintCancel
            .complaintNumber(UPDATED_COMPLAINT_NUMBER)
            .regDate(UPDATED_REG_DATE)
            .text(UPDATED_TEXT);

        restComplaintCancelMockMvc.perform(put("/api/complaint-cancels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedComplaintCancel)))
            .andExpect(status().isOk());

        // Validate the ComplaintCancel in the database
        List<ComplaintCancel> complaintCancelList = complaintCancelRepository.findAll();
        assertThat(complaintCancelList).hasSize(databaseSizeBeforeUpdate);
        ComplaintCancel testComplaintCancel = complaintCancelList.get(complaintCancelList.size() - 1);
        assertThat(testComplaintCancel.getComplaintNumber()).isEqualTo(UPDATED_COMPLAINT_NUMBER);
        assertThat(testComplaintCancel.getRegDate()).isEqualTo(UPDATED_REG_DATE);
        assertThat(testComplaintCancel.getText()).isEqualTo(UPDATED_TEXT);

        // Validate the ComplaintCancel in Elasticsearch
        ComplaintCancel complaintCancelEs = complaintCancelSearchRepository.findOne(testComplaintCancel.getId());
        assertThat(complaintCancelEs).isEqualToComparingFieldByField(testComplaintCancel);
    }

    @Test
    @Transactional
    public void updateNonExistingComplaintCancel() throws Exception {
        int databaseSizeBeforeUpdate = complaintCancelRepository.findAll().size();

        // Create the ComplaintCancel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restComplaintCancelMockMvc.perform(put("/api/complaint-cancels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(complaintCancel)))
            .andExpect(status().isCreated());

        // Validate the ComplaintCancel in the database
        List<ComplaintCancel> complaintCancelList = complaintCancelRepository.findAll();
        assertThat(complaintCancelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteComplaintCancel() throws Exception {
        // Initialize the database
        complaintCancelService.save(complaintCancel);

        int databaseSizeBeforeDelete = complaintCancelRepository.findAll().size();

        // Get the complaintCancel
        restComplaintCancelMockMvc.perform(delete("/api/complaint-cancels/{id}", complaintCancel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean complaintCancelExistsInEs = complaintCancelSearchRepository.exists(complaintCancel.getId());
        assertThat(complaintCancelExistsInEs).isFalse();

        // Validate the database is empty
        List<ComplaintCancel> complaintCancelList = complaintCancelRepository.findAll();
        assertThat(complaintCancelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchComplaintCancel() throws Exception {
        // Initialize the database
        complaintCancelService.save(complaintCancel);

        // Search the complaintCancel
        restComplaintCancelMockMvc.perform(get("/api/_search/complaint-cancels?query=id:" + complaintCancel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(complaintCancel.getId().intValue())))
            .andExpect(jsonPath("$.[*].complaintNumber").value(hasItem(DEFAULT_COMPLAINT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].regDate").value(hasItem(DEFAULT_REG_DATE.toString())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ComplaintCancel.class);
        ComplaintCancel complaintCancel1 = new ComplaintCancel();
        complaintCancel1.setId(1L);
        ComplaintCancel complaintCancel2 = new ComplaintCancel();
        complaintCancel2.setId(complaintCancel1.getId());
        assertThat(complaintCancel1).isEqualTo(complaintCancel2);
        complaintCancel2.setId(2L);
        assertThat(complaintCancel1).isNotEqualTo(complaintCancel2);
        complaintCancel1.setId(null);
        assertThat(complaintCancel1).isNotEqualTo(complaintCancel2);
    }
}
