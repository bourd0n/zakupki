import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { PublishOrg } from './publish-org.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PublishOrgService {

    private resourceUrl = SERVER_API_URL + 'api/publish-orgs';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/publish-orgs';

    constructor(private http: Http) { }

    create(publishOrg: PublishOrg): Observable<PublishOrg> {
        const copy = this.convert(publishOrg);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(publishOrg: PublishOrg): Observable<PublishOrg> {
        const copy = this.convert(publishOrg);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PublishOrg> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PublishOrg.
     */
    private convertItemFromServer(json: any): PublishOrg {
        const entity: PublishOrg = Object.assign(new PublishOrg(), json);
        return entity;
    }

    /**
     * Convert a PublishOrg to a JSON which can be sent to the server.
     */
    private convert(publishOrg: PublishOrg): PublishOrg {
        const copy: PublishOrg = Object.assign({}, publishOrg);
        return copy;
    }
}
