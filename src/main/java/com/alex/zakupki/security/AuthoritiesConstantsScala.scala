package com.alex.zakupki.security

object AuthoritiesConstantsScala {
    final val ADMIN = "ROLE_ADMIN"
    final val USER = "ROLE_USER"
    final val ANONYMOUS = "ROLE_ANONYMOUS"
}
