package alex.zakupki.complaint.api.model

import javax.xml.bind.annotation.{XmlAccessType, XmlAccessorType, XmlRootElement}

@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class CheckResultNumber(@xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                             checkResultNumber: String) {
  private def this() = this(null)
}