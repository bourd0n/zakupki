package com.alex.zakupki.service.dto

import java.time.LocalDate

case class ComplaintResult(id: Long,
                           complaintNumber: String,
                           regDate: LocalDate,
                           result: String)
