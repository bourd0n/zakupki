import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams, BaseRequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

import {ComplaintsStats} from './complaints-stats.model';
import {JhiDateUtils} from 'ng-jhipster';

@Injectable()
export class ComplaintStatsService {

    private resourceUrl = 'api/complaints-stats';
    private resourceSearchUrl = 'api/_search/complaintsStats';

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
    }

    /*    create(client: Client): Observable<Client> {
     const copy: Client = Object.assign({}, client);
     return this.http.post(this.resourceUrl, copy).map((res: Response) => {
     return res.json();
     });
     }

     update(client: Client): Observable<Client> {
     const copy: Client = Object.assign({}, client);
     return this.http.put(this.resourceUrl, copy).map((res: Response) => {
     return res.json();
     });
     }

     find(id: number): Observable<Client> {
     return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
     return res.json();
     });
     }*/

    query(req?: any): Observable<Response> {
        const options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options);
    }

    /*
     delete(id: number): Observable<Response> {
     return this.http.delete(`${this.resourceUrl}/${id}`);
     }*/

    search(req?: any): Observable<Response> {
        const options = this.createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            ;
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        const options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            const params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            if (req.client) {
                params.set('clientId', req.client)
            }
            options.search = params;
        }
        return options;
    }
}
