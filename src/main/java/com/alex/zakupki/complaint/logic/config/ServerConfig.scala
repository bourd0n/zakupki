package com.alex.zakupki.complaint.logic.config

import com.typesafe.config.{ConfigFactory, Config}

object ServerConfig {
    lazy val config = ConfigFactory.load()
    //todo: think about better solution
    lazy val server_path = System.getProperty("user.dir")
}
