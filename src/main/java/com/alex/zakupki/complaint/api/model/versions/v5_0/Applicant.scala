package com.alex.zakupki.complaint.api.model.versions.v5_0

import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.GeneralizedEntity
import com.alex.zakupki.complaint.api.model.general
//@Entity
//@Table(name = "applicants")
@XmlAccessorType(XmlAccessType.FIELD)
case class Applicant(@xmlPath("oos:organizationName/text()")
                     organizationName: String,
                     @xmlPath("oos:applicantType/text()")
                     applicantType: String) extends GeneralizedEntity[general.Applicant] {
    private def this() = this(null, null)

    override def mapToGeneral(): general.Applicant = {
        //todo: parse applicant name
        general.Applicant(organizationName.trim, applicantType)
    }
}
