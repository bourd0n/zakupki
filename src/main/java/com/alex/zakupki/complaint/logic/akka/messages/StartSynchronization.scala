package com.alex.zakupki.complaint.logic.akka.messages

import com.alex.zakupki.domain.SyncInfo

case class StartSynchronization(syncInfo: SyncInfo)
