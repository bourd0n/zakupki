package com.alex.zakupki.service.dto

case class ComplaintsYearStats(year: String,
                               complaintsCount: Int,
                               noViolations: Int,
                               violations: Int,
                               partlyValid: Int,
                               notMatched: Int,
                               returned: Int)
