/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ZakupkiTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PublishOrgDetailComponent } from '../../../../../../main/webapp/app/entities/publish-org/publish-org-detail.component';
import { PublishOrgService } from '../../../../../../main/webapp/app/entities/publish-org/publish-org.service';
import { PublishOrg } from '../../../../../../main/webapp/app/entities/publish-org/publish-org.model';

describe('Component Tests', () => {

    describe('PublishOrg Management Detail Component', () => {
        let comp: PublishOrgDetailComponent;
        let fixture: ComponentFixture<PublishOrgDetailComponent>;
        let service: PublishOrgService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ZakupkiTestModule],
                declarations: [PublishOrgDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PublishOrgService,
                    JhiEventManager
                ]
            }).overrideTemplate(PublishOrgDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PublishOrgDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PublishOrgService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PublishOrg(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.publishOrg).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
