package com.alex.zakupki.complaint.api.model.versions.v5_0

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.GeneralizedEntity
import com.alex.zakupki.complaint.api.model.general
import com.alex.zakupki.complaint.api.model.versions.common.DateAdapter
import com.google.common.base.MoreObjects


//@Entity
//@Table(name = "planned_check_results")
@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class PlannedCheckResult(@xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                              checkResultNumber: String,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:checkNumber/text()")
                              checkNumber: String,
                              @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                              createDate: Date,
                              @xmlPath("checkResult/oos:commonInfo/oos:owner")
                              owner: Owner,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:act/oos:actDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actDate: Date,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:actPrescription/oos:prescriptionDate/text()")
                              @xmlJavaTypeAdapter(classOf[DateAdapter])
                              actPrescriptionDate: Date,
                              @xmlPath("checkResult/oos:base/oos:plannedCheck/oos:checkSubject/")
                              @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                              checkSubject: Client) extends GeneralizedEntity[general.PlannedCheckResult] {
    private def this() = this(null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }

    override def mapToGeneral(): general.PlannedCheckResult = general.PlannedCheckResult(
        checkResultNumber,
        checkNumber,
        createDate,
        owner.mapToGeneral(),
        actDate,
        actPrescriptionDate,
        Option(checkSubject).map(_.mapToGeneral()).orNull
    )
}
