import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('PublishOrg e2e test', () => {

    let navBarPage: NavBarPage;
    let publishOrgDialogPage: PublishOrgDialogPage;
    let publishOrgComponentsPage: PublishOrgComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PublishOrgs', () => {
        navBarPage.goToEntity('publish-org');
        publishOrgComponentsPage = new PublishOrgComponentsPage();
        expect(publishOrgComponentsPage.getTitle()).toMatch(/zakupkiApp.publishOrg.home.title/);

    });

    it('should load create PublishOrg dialog', () => {
        publishOrgComponentsPage.clickOnCreateButton();
        publishOrgDialogPage = new PublishOrgDialogPage();
        expect(publishOrgDialogPage.getModalTitle()).toMatch(/zakupkiApp.publishOrg.home.createOrEditLabel/);
        publishOrgDialogPage.close();
    });

    it('should create and save PublishOrgs', () => {
        publishOrgComponentsPage.clickOnCreateButton();
        publishOrgDialogPage.setRegNumInput('regNum');
        expect(publishOrgDialogPage.getRegNumInput()).toMatch('regNum');
        publishOrgDialogPage.setFullNameInput('fullName');
        expect(publishOrgDialogPage.getFullNameInput()).toMatch('fullName');
        publishOrgDialogPage.save();
        expect(publishOrgDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PublishOrgComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-publish-org div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PublishOrgDialogPage {
    modalTitle = element(by.css('h4#myPublishOrgLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    regNumInput = element(by.css('input#field_regNum'));
    fullNameInput = element(by.css('input#field_fullName'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setRegNumInput = function (regNum) {
        this.regNumInput.sendKeys(regNum);
    }

    getRegNumInput = function () {
        return this.regNumInput.getAttribute('value');
    }

    setFullNameInput = function (fullName) {
        this.fullNameInput.sendKeys(fullName);
    }

    getFullNameInput = function () {
        return this.fullNameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
