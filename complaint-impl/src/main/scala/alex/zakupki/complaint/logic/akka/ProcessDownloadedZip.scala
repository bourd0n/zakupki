package alex.zakupki.complaint.logic.akka

case class ProcessDownloadedZip(zipFileBytes: Array[Byte])
