package com.alex.zakupki.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.alex.zakupki.domain.UnfairSupplier;
import com.alex.zakupki.service.UnfairSupplierService;
import com.alex.zakupki.web.rest.util.HeaderUtil;
import com.alex.zakupki.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UnfairSupplier.
 */
@RestController
@RequestMapping("/api")
public class UnfairSupplierResource {

    private final Logger log = LoggerFactory.getLogger(UnfairSupplierResource.class);

    private static final String ENTITY_NAME = "unfairSupplier";

    private final UnfairSupplierService unfairSupplierService;

    public UnfairSupplierResource(UnfairSupplierService unfairSupplierService) {
        this.unfairSupplierService = unfairSupplierService;
    }

    /**
     * POST  /unfair-suppliers : Create a new unfairSupplier.
     *
     * @param unfairSupplier the unfairSupplier to create
     * @return the ResponseEntity with status 201 (Created) and with body the new unfairSupplier, or with status 400 (Bad Request) if the unfairSupplier has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/unfair-suppliers")
    @Timed
    public ResponseEntity<UnfairSupplier> createUnfairSupplier(@Valid @RequestBody UnfairSupplier unfairSupplier) throws URISyntaxException {
        log.debug("REST request to save UnfairSupplier : {}", unfairSupplier);
        if (unfairSupplier.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new unfairSupplier cannot already have an ID")).body(null);
        }
        UnfairSupplier result = unfairSupplierService.save(unfairSupplier);
        return ResponseEntity.created(new URI("/api/unfair-suppliers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /unfair-suppliers : Updates an existing unfairSupplier.
     *
     * @param unfairSupplier the unfairSupplier to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated unfairSupplier,
     * or with status 400 (Bad Request) if the unfairSupplier is not valid,
     * or with status 500 (Internal Server Error) if the unfairSupplier couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/unfair-suppliers")
    @Timed
    public ResponseEntity<UnfairSupplier> updateUnfairSupplier(@Valid @RequestBody UnfairSupplier unfairSupplier) throws URISyntaxException {
        log.debug("REST request to update UnfairSupplier : {}", unfairSupplier);
        if (unfairSupplier.getId() == null) {
            return createUnfairSupplier(unfairSupplier);
        }
        UnfairSupplier result = unfairSupplierService.save(unfairSupplier);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, unfairSupplier.getId().toString()))
            .body(result);
    }

    /**
     * GET  /unfair-suppliers : get all the unfairSuppliers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of unfairSuppliers in body
     */
    @GetMapping("/unfair-suppliers")
    @Timed
    public ResponseEntity<List<UnfairSupplier>> getAllUnfairSuppliers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UnfairSuppliers");
        Page<UnfairSupplier> page = unfairSupplierService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/unfair-suppliers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /unfair-suppliers/:id : get the "id" unfairSupplier.
     *
     * @param id the id of the unfairSupplier to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the unfairSupplier, or with status 404 (Not Found)
     */
    @GetMapping("/unfair-suppliers/{id}")
    @Timed
    public ResponseEntity<UnfairSupplier> getUnfairSupplier(@PathVariable Long id) {
        log.debug("REST request to get UnfairSupplier : {}", id);
        UnfairSupplier unfairSupplier = unfairSupplierService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(unfairSupplier));
    }

    /**
     * DELETE  /unfair-suppliers/:id : delete the "id" unfairSupplier.
     *
     * @param id the id of the unfairSupplier to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/unfair-suppliers/{id}")
    @Timed
    public ResponseEntity<Void> deleteUnfairSupplier(@PathVariable Long id) {
        log.debug("REST request to delete UnfairSupplier : {}", id);
        unfairSupplierService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/unfair-suppliers?query=:query : search for the unfairSupplier corresponding
     * to the query.
     *
     * @param query the query of the unfairSupplier search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/unfair-suppliers")
    @Timed
    public ResponseEntity<List<UnfairSupplier>> searchUnfairSuppliers(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of UnfairSuppliers for query {}", query);
        Page<UnfairSupplier> page = unfairSupplierService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/unfair-suppliers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
