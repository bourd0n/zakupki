package com.alex.zakupki.service;

import com.alex.zakupki.domain.UnfairSupplier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing UnfairSupplier.
 */
public interface UnfairSupplierService {

    /**
     * Save a unfairSupplier.
     *
     * @param unfairSupplier the entity to save
     * @return the persisted entity
     */
    UnfairSupplier save(UnfairSupplier unfairSupplier);

    /**
     *  Get all the unfairSuppliers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UnfairSupplier> findAll(Pageable pageable);

    /**
     *  Get the "id" unfairSupplier.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    UnfairSupplier findOne(Long id);

    /**
     *  Delete the "id" unfairSupplier.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the unfairSupplier corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<UnfairSupplier> search(String query, Pageable pageable);
}
