package com.alex.zakupki.complaint.web.config

import com.alex.zakupki.complaint.app.config.AppSecurityConfig
import org.springframework.context.annotation.{Import, ComponentScan, Bean, Configuration}
import org.springframework.web.servlet.ViewResolver
import org.springframework.web.servlet.config.annotation.{ResourceHandlerRegistry, WebMvcConfigurerAdapter, EnableWebMvc}
import org.springframework.web.servlet.view.velocity.{VelocityConfigurer, VelocityView, VelocityViewResolver}

//@Configuration
//@EnableWebMvc
//@ComponentScan(Array("alex.zakupki.complaint.web.config", "alex.zakupki.complaint.web.controller"))
//@Import(Array(classOf[AppSecurityConfig]))
class WebConfiguration /*extends WebMvcConfigurerAdapter*/ {
}
