import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PlannedCheckResult } from './planned-check-result.model';
import { PlannedCheckResultPopupService } from './planned-check-result-popup.service';
import { PlannedCheckResultService } from './planned-check-result.service';
import { Owner, OwnerService } from '../owner';
import { Client, ClientService } from '../client';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-planned-check-result-dialog',
    templateUrl: './planned-check-result-dialog.component.html'
})
export class PlannedCheckResultDialogComponent implements OnInit {

    plannedCheckResult: PlannedCheckResult;
    isSaving: boolean;

    owners: Owner[];

    clients: Client[];
    createdDateDp: any;
    actDateDp: any;
    actPrescriptionDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private plannedCheckResultService: PlannedCheckResultService,
        private ownerService: OwnerService,
        private clientService: ClientService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.ownerService.query()
            .subscribe((res: ResponseWrapper) => { this.owners = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.clientService.query()
            .subscribe((res: ResponseWrapper) => { this.clients = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.plannedCheckResult.id !== undefined) {
            this.subscribeToSaveResponse(
                this.plannedCheckResultService.update(this.plannedCheckResult));
        } else {
            this.subscribeToSaveResponse(
                this.plannedCheckResultService.create(this.plannedCheckResult));
        }
    }

    private subscribeToSaveResponse(result: Observable<PlannedCheckResult>) {
        result.subscribe((res: PlannedCheckResult) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PlannedCheckResult) {
        this.eventManager.broadcast({ name: 'plannedCheckResultListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackOwnerById(index: number, item: Owner) {
        return item.id;
    }

    trackClientById(index: number, item: Client) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-planned-check-result-popup',
    template: ''
})
export class PlannedCheckResultPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private plannedCheckResultPopupService: PlannedCheckResultPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.plannedCheckResultPopupService
                    .open(PlannedCheckResultDialogComponent as Component, params['id']);
            } else {
                this.plannedCheckResultPopupService
                    .open(PlannedCheckResultDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
