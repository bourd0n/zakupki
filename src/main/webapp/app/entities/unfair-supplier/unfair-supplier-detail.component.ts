import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { UnfairSupplier } from './unfair-supplier.model';
import { UnfairSupplierService } from './unfair-supplier.service';

@Component({
    selector: 'jhi-unfair-supplier-detail',
    templateUrl: './unfair-supplier-detail.component.html'
})
export class UnfairSupplierDetailComponent implements OnInit, OnDestroy {

    unfairSupplier: UnfairSupplier;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private unfairSupplierService: UnfairSupplierService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInUnfairSuppliers();
    }

    load(id) {
        this.unfairSupplierService.find(id).subscribe((unfairSupplier) => {
            this.unfairSupplier = unfairSupplier;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInUnfairSuppliers() {
        this.eventSubscriber = this.eventManager.subscribe(
            'unfairSupplierListModification',
            (response) => this.load(this.unfairSupplier.id)
        );
    }
}
