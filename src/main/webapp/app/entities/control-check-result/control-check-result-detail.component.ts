import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ControlCheckResult } from './control-check-result.model';
import { ControlCheckResultService } from './control-check-result.service';

@Component({
    selector: 'jhi-control-check-result-detail',
    templateUrl: './control-check-result-detail.component.html'
})
export class ControlCheckResultDetailComponent implements OnInit, OnDestroy {

    controlCheckResult: ControlCheckResult;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private controlCheckResultService: ControlCheckResultService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInControlCheckResults();
    }

    load(id) {
        this.controlCheckResultService.find(id).subscribe((controlCheckResult) => {
            this.controlCheckResult = controlCheckResult;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInControlCheckResults() {
        this.eventSubscriber = this.eventManager.subscribe(
            'controlCheckResultListModification',
            (response) => this.load(this.controlCheckResult.id)
        );
    }
}
