package com.alex.zakupki.complaint.api.model.versions.v6_2

import java.util.Date
import javax.xml.bind.annotation.XmlAccessType

import com.alex.zakupki.complaint.api.model.{GeneralizedEntity, general}
import com.google.common.base.MoreObjects
import org.apache.commons.lang3.ObjectUtils

@XmlRootElement(name = "export", namespace = "http://zakupki.gov.ru/oos/export/1")
@XmlAccessorType(XmlAccessType.FIELD)
case class CheckComplaintResult(@xmlPath("checkResult/oos:commonInfo/oos:regNumber/text()")
                                checkResultNumber: String,
                                @xmlPath("checkResult/oos:commonInfo/oos:checkResultNumber/text()")
                                checkResultNumber2: String,
                                @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:regNumber/text()")
                                complaintNumber: String,
                                @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:complaintNumber/text()")
                                complaintNumber2: String,
                                @xmlPath("checkResult/oos:complaint/oos:regNumber/text()")
                                complaintNumber3: String,
                                @xmlPath("checkResult/oos:commonInfo/oos:createDate/text()")
                                createDate: Date,
                                @xmlPath("checkResult/oos:commonInfo/oos:owner")
                                owner: Owner,
                                @xmlPath("checkResult/oos:commonInfo/oos:result/text()")
                                result: String,
                                @xmlPath("checkResult/oos:complaint/oos:complaintResult/text()")
                                result2: String,
                                @xmlPath("checkResult/oos:base/oos:unplannedCheckComplaint/oos:checkSubjects/oos:subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                checkSubject: Client,
                                @xmlPath("checkResult/oos:complaint/oos:checkSubjects/oos:subjectComplaint/")
                                @xmlJavaTypeAdapter(classOf[BodyClientAdapter])
                                checkSubject2: Client) extends GeneralizedEntity[general.CheckComplaintResult] {
    private def this() = this(null, null, null, null, null, null, null, null, null, null, null)

    override def toString: String = {
        MoreObjects.toStringHelper(this).add("result", result).add("checkResultNumber", checkResultNumber).add("createDate", createDate).toString
    }

    override def mapToGeneral(): general.CheckComplaintResult = general.CheckComplaintResult(
        ObjectUtils.firstNonNull(checkResultNumber, checkResultNumber2),
        createDate,
        owner.mapToGeneral(),
        ObjectUtils.firstNonNull(result, result2),
        Option(ObjectUtils.firstNonNull(checkSubject, checkSubject2)).map(_.mapToGeneral()).orNull,
        ObjectUtils.firstNonNull(complaintNumber, complaintNumber2, complaintNumber3)
    )
}
