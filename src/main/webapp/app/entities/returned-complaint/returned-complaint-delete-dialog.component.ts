import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ReturnedComplaint } from './returned-complaint.model';
import { ReturnedComplaintPopupService } from './returned-complaint-popup.service';
import { ReturnedComplaintService } from './returned-complaint.service';

@Component({
    selector: 'jhi-returned-complaint-delete-dialog',
    templateUrl: './returned-complaint-delete-dialog.component.html'
})
export class ReturnedComplaintDeleteDialogComponent {

    returnedComplaint: ReturnedComplaint;

    constructor(
        private returnedComplaintService: ReturnedComplaintService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.returnedComplaintService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'returnedComplaintListModification',
                content: 'Deleted an returnedComplaint'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-returned-complaint-delete-popup',
    template: ''
})
export class ReturnedComplaintDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private returnedComplaintPopupService: ReturnedComplaintPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.returnedComplaintPopupService
                .open(ReturnedComplaintDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
