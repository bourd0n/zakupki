"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
//todo nial uncomment
var JhiTrackerService = (function () {
    function JhiTrackerService(router, $document, $window, csrfService) {
        this.router = router;
        this.$document = $document;
        this.$window = $window;
        this.csrfService = csrfService;
        this.stompClient = null;
        this.subscriber = null;
        this.alreadyConnectedOnce = false;
        //todo
        // this.connection = this.createConnection();
        // this.listener = this.createListener();
    }
    JhiTrackerService.prototype.connect = function () {
        // if (this.connectedPromise === null) {
        //   this.connection = this.createConnection();
        // }
        // // building absolute path so that websocket doesnt fail when deploying with a context path
        // const loc = this.$window.location;
        // let url = '//' + loc.host + loc.pathname + 'websocket/tracker';
        // const socket = new SockJS(url);
        // this.stompClient = Stomp.over(socket);
        // let headers = {};
        // headers['X-XSRF-TOKEN'] = this.csrfService.getCSRF('XSRF-TOKEN');
        // this.stompClient.connect(headers, () => {
        //     this.connectedPromise('success');
        //     this.connectedPromise = null;
        //     this.sendActivity();
        //     if (!this.alreadyConnectedOnce) {
        //         this.subscription = this.router.events.subscribe((event) => {
        //           if (event instanceof NavigationEnd) {
        //             this.sendActivity();
        //           }
        //         });
        //         this.alreadyConnectedOnce = true;
        //     }
        // });
    };
    JhiTrackerService.prototype.disconnect = function () {
        // if (this.stompClient !== null) {
        //     this.stompClient.disconnect();
        //     this.stompClient = null;
        // }
        // if (this.subscription) {
        //     this.subscription.unsubscribe();
        //     this.subscription = null;
        // }
        // this.alreadyConnectedOnce = false;
    };
    JhiTrackerService.prototype.receive = function () {
        return this.listener;
    };
    JhiTrackerService.prototype.sendActivity = function () {
        // if (this.stompClient !== null && this.stompClient.connected) {
        //     this.stompClient.send(
        //         '/topic/activity', // destination
        //         JSON.stringify({'page': this.router.routerState.snapshot.url}), // body
        //         {} // header
        //     );
        // }
    };
    JhiTrackerService.prototype.subscribe = function () {
        // this.connection.then(() => {
        //     this.subscriber = this.stompClient.subscribe('/topic/tracker', data => {
        //         this.listenerObserver.next(JSON.parse(data.body));
        //     });
        // });
    };
    JhiTrackerService.prototype.unsubscribe = function () {
        // if (this.subscriber !== null) {
        //     this.subscriber.unsubscribe();
        // }
        // this.listener = this.createListener();
    };
    JhiTrackerService.prototype.createListener = function () {
        var _this = this;
        return new Rx_1.Observable(function (observer) {
            _this.listenerObserver = observer;
        });
    };
    JhiTrackerService.prototype.createConnection = function () {
        var _this = this;
        return new Promise(function (resolve, reject) { return _this.connectedPromise = resolve; });
    };
    return JhiTrackerService;
}());
JhiTrackerService = __decorate([
    core_1.Injectable()
], JhiTrackerService);
exports.JhiTrackerService = JhiTrackerService;
